var NewsMethods = function () {
    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true,
            language: $('html').attr('lang')
        });
    }

    var General_Settings_Validation = function () {

        // for more info visit the official plugin documentation: 

        // http://docs.jquery.com/Plugins/Validation


        var form1 = $('#General_Settings');

        if (form1.length) {

            var error1 = $('.alert-danger', form1);

            var success1 = $('.alert-success', form1);

            //IMPORTANT: update CKEDITOR textarea with actual content before submit
            form1.on('submit', function () {
                for (var instanceName in CKEDITOR.instances) {
                    CKEDITOR.instances[instanceName].updateElement();
                }
            })

            form1.validate({

                errorElement: 'span', //default input error message container

                errorClass: 'help-block help-block-error', // default input error message class

                focusInvalid: false, // do not focus the last invalid input

                ignore: "",  // validate all fields including form hidden input

                messages: {},

                rules: {


                    // shares_available: {
                    //
                    //
                    //     required: true,
                    //     number: true,
                    //
                    // },



                },


                invalidHandler: function (event, validator) { //display error alert on form submit

                    success1.hide();

                    error1.show();

                    // App.scrollTo(error1, -200);

                },


                errorPlacement: function (error, element) { // render error placement for each input type

                    var cont = $(element).parent('.input-group');

                    if (cont.size() > 0) {

                        cont.after(error);

                    } else {

                        element.after(error);

                    }

                },


                highlight: function (element) { // hightlight error inputs


                    $(element)

                        .closest('.form-group').addClass('has-error'); // set error class to the control group

                },


                unhighlight: function (element) { // revert the change done by hightlight

                    $(element)

                        .closest('.form-group').removeClass('has-error'); // set error class to the control group

                },


                success: function (label) {

                    label

                        .closest('.form-group').removeClass('has-error'); // set success class to the control group

                },


                submitHandler: function (form) {

                    success1.show();

                    error1.hide();


                    //upload files

                    var data = new FormData(form1[0]);

                    if ($('.file').length) {

                        jQuery.each(jQuery('.file')[0].files, function (i, file) {

                            data.append('file-' + i, file);

                        });

                    }

                    data.append('par1', form1.attr('par1'));
                    data.append('lang', $("html").attr('lang'));


                    $.ajax({

                        type: "POST",

                        url: APP_URL + "/Manage/Settings/Store",

                        data: data,

                        //async: false,

                        cache: false,

                        contentType: false,

                        processData: false,

                        dataType: "JSON",

                        beforeSend: function () {

                           // FormMod.showLoading(true);

                        },

                        complete: function () {

                           // FormMod.hideLoading(true);


                        },

                        error: function (data, xhr, ajaxOptions, thrownError) {

                            App.ErrorHandel(data, xhr, ajaxOptions, thrownError);

                        },


                        success: function (response) {

                            if (response !== null && response.hasOwnProperty("Errors")) {
                                var Resp = response.Errors;

                                $.alert({
                                    title: Resp.title,
                                    content: Resp.content,
                                    rtl: App.isRTL(),
                                    type: 'red',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function () {

                                            }
                                        },
                                    },
                                });


                            } else if (response !== null && response.hasOwnProperty("Success")) {

                                var Resp = response.Success;

                                $.alert({
                                    title: Resp.title,
                                    content: Resp.content,
                                    rtl: App.isRTL(),
                                    type: 'green',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function () {

                                            }
                                        },
                                    },
                                });

                            }

                        }

                    });

                }


            });
        }


    }

    var HomePage_Settings_Validation = function () {

        // for more info visit the official plugin documentation:

        // http://docs.jquery.com/Plugins/Validation


        var form1 = $('#HomePage_Settings');

        if (form1.length) {

            var error1 = $('.alert-danger', form1);

            var success1 = $('.alert-success', form1);

            //IMPORTANT: update CKEDITOR textarea with actual content before submit
            form1.on('submit', function () {
                for (var instanceName in CKEDITOR.instances) {
                    CKEDITOR.instances[instanceName].updateElement();
                }
            })

            form1.validate({

                errorElement: 'span', //default input error message container

                errorClass: 'help-block help-block-error', // default input error message class

                focusInvalid: false, // do not focus the last invalid input

                ignore: "",  // validate all fields including form hidden input

                messages: {},

                rules: {


                    // shares_available: {
                    //
                    //
                    //     required: true,
                    //     number: true,
                    //
                    // },



                },


                invalidHandler: function (event, validator) { //display error alert on form submit

                    success1.hide();

                    error1.show();

                    // App.scrollTo(error1, -200);

                },


                errorPlacement: function (error, element) { // render error placement for each input type

                    var cont = $(element).parent('.input-group');

                    if (cont.size() > 0) {

                        cont.after(error);

                    } else {

                        element.after(error);

                    }

                },


                highlight: function (element) { // hightlight error inputs


                    $(element)

                        .closest('.form-group').addClass('has-error'); // set error class to the control group

                },


                unhighlight: function (element) { // revert the change done by hightlight

                    $(element)

                        .closest('.form-group').removeClass('has-error'); // set error class to the control group

                },


                success: function (label) {

                    label

                        .closest('.form-group').removeClass('has-error'); // set success class to the control group

                },


                submitHandler: function (form) {

                    success1.show();

                    error1.hide();


                    //upload files

                    var data = new FormData(form1[0]);

                    if ($('.file').length) {

                        jQuery.each(jQuery('.file')[0].files, function (i, file) {

                            data.append('file-' + i, file);

                        });

                    }

                    data.append('par1', form1.attr('par1'));
                    data.append('lang', $("html").attr('lang'));


                    $.ajax({

                        type: "POST",

                        url: APP_URL + "/Manage/Settings/Store",

                        data: data,

                        //async: false,

                        cache: false,

                        contentType: false,

                        processData: false,

                        dataType: "JSON",

                        beforeSend: function () {

                           // FormMod.showLoading(true);

                        },

                        complete: function () {

                           // FormMod.hideLoading(true);


                        },

                        error: function (data, xhr, ajaxOptions, thrownError) {

                            App.ErrorHandel(data, xhr, ajaxOptions, thrownError);

                        },


                        success: function (response) {

                            if (response !== null && response.hasOwnProperty("Errors")) {
                                var Resp = response.Errors;

                                $.alert({
                                    title: Resp.title,
                                    content: Resp.content,
                                    rtl: App.isRTL(),
                                    type: 'red',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function () {

                                            }
                                        },
                                    },
                                });


                            } else if (response !== null && response.hasOwnProperty("Success")) {

                                var Resp = response.Success;

                                $.alert({
                                    title: Resp.title,
                                    content: Resp.content,
                                    rtl: App.isRTL(),
                                    type: 'green',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function () {

                                            }
                                        },
                                    },
                                });

                            }

                        }

                    });

                }


            });
        }


    }

    var AboutUs_Validation = function () {

        // for more info visit the official plugin documentation:

        // http://docs.jquery.com/Plugins/Validation


        var form1 = $('#AboutUs');

        if (form1.length) {

            var error1 = $('.alert-danger', form1);

            var success1 = $('.alert-success', form1);

            //IMPORTANT: update CKEDITOR textarea with actual content before submit
            form1.on('submit', function () {
                for (var instanceName in CKEDITOR.instances) {
                    CKEDITOR.instances[instanceName].updateElement();
                }
            })

            form1.validate({

                errorElement: 'span', //default input error message container

                errorClass: 'help-block help-block-error', // default input error message class

                focusInvalid: false, // do not focus the last invalid input

                ignore: "",  // validate all fields including form hidden input

                messages: {},

                rules: {


                    // shares_available: {
                    //
                    //
                    //     required: true,
                    //     number: true,
                    //
                    // },



                },


                invalidHandler: function (event, validator) { //display error alert on form submit

                    success1.hide();

                    error1.show();

                    // App.scrollTo(error1, -200);

                },


                errorPlacement: function (error, element) { // render error placement for each input type

                    var cont = $(element).parent('.input-group');

                    if (cont.size() > 0) {

                        cont.after(error);

                    } else {

                        element.after(error);

                    }

                },


                highlight: function (element) { // hightlight error inputs


                    $(element)

                        .closest('.form-group').addClass('has-error'); // set error class to the control group

                },


                unhighlight: function (element) { // revert the change done by hightlight

                    $(element)

                        .closest('.form-group').removeClass('has-error'); // set error class to the control group

                },


                success: function (label) {

                    label

                        .closest('.form-group').removeClass('has-error'); // set success class to the control group

                },


                submitHandler: function (form) {

                    success1.show();

                    error1.hide();


                    //upload files

                    var data = new FormData(form1[0]);

                    if ($('.file').length) {

                        jQuery.each(jQuery('.file')[0].files, function (i, file) {

                            data.append('file-' + i, file);

                        });

                    }

                    data.append('par1', form1.attr('par1'));
                    data.append('lang', $("html").attr('lang'));


                    $.ajax({

                        type: "POST",

                        url: APP_URL + "/Manage/Settings/Store",

                        data: data,

                        //async: false,

                        cache: false,

                        contentType: false,

                        processData: false,

                        dataType: "JSON",

                        beforeSend: function () {

                           // FormMod.showLoading(true);

                        },

                        complete: function () {

                           // FormMod.hideLoading(true);


                        },

                        error: function (data, xhr, ajaxOptions, thrownError) {

                            App.ErrorHandel(data, xhr, ajaxOptions, thrownError);

                        },


                        success: function (response) {

                            if (response !== null && response.hasOwnProperty("Errors")) {
                                var Resp = response.Errors;

                                $.alert({
                                    title: Resp.title,
                                    content: Resp.content,
                                    rtl: App.isRTL(),
                                    type: 'red',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function () {

                                            }
                                        },
                                    },
                                });


                            } else if (response !== null && response.hasOwnProperty("Success")) {

                                var Resp = response.Success;

                                $.alert({
                                    title: Resp.title,
                                    content: Resp.content,
                                    rtl: App.isRTL(),
                                    type: 'green',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function () {

                                            }
                                        },
                                    },
                                });

                            }

                        }

                    });

                }


            });
        }


    }



    return {


        //main function to initiate the module

        init: function () {

            initPickers();
            General_Settings_Validation();
            HomePage_Settings_Validation();
            AboutUs_Validation();

        }


    };


}();


jQuery(document).ready(function () {

    NewsMethods.init();

});