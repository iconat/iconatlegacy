var SliderMethods = function() {
    var initPickers = function() {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true,
            language: $('html').attr('lang')
        });
    }

    var ShowWithControl = function() {

        if ($("#datatable_ajax").length) {

            var grid = new Datatable();


            grid.init({

                src: $("#datatable_ajax"),

                onSuccess: function(grid, response) {

                    // grid:        grid object

                    // response:    json object of server side ajax response

                    // execute some code after table records loaded

                },

                onError: function(grid) {

                    // execute some code on network or other general error

                },

                onDataLoad: function(grid) {

                    // execute some code on ajax data load

                },


                dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options

                    "language": {

                        "url": APP_URL_Without_Lang + "/assets/global/plugins/datatables/languages/" + Lang.get("global.currentLang") + ".json"

                    },

                    // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout

                    // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).

                    // So when dropdowns used the scrollable div should be removed.

                    //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",


                    // save datatable state(pagination, sort, etc) in cookie.

                    "bStateSave": true,


                    // save custom filters to the state

                    "fnStateSaveParams": function(oSettings, sValue) {

                        $("#datatable_ajax tr.filter .form-control").each(function() {

                            sValue[$(this).attr('name')] = $(this).val();

                        });


                        return sValue;

                    },


                    // read the custom filters from saved state and populate the filter inputs

                    "fnStateLoadParams": function(oSettings, oData) {

                        //Load custom filters

                        $("#datatable_ajax tr.filter .form-control").each(function() {

                            var element = $(this);

                            if (oData[element.attr('name')]) {

                                element.val(oData[element.attr('name')]);

                            }

                        });


                        return true;

                    },


                    "lengthMenu": [

                        [10, 20, 50, 100, 150, -1],

                        [10, 20, 50, 100, 150, "All"] // change per page values here

                    ],

                    "columnDefs": [{

                        "targets": '_all',

                        "searchable": false,

                        className: "text-center"

                    }],

                    "pageLength": 20, // default record count per page

                    "ajax": {

                        "url": APP_URL + "/Manage/Slider/GetData", // ajax source

                    },

                    "ordering": false,

                    "order": [

                            [1, "asc"]

                        ] // set first column as a default sort by asc

                }

            });


            // handle group actionsubmit button click

            grid.getTableWrapper().on('click', '.table-group-action-submit', function(e) {

                e.preventDefault();

                var action = $(".table-group-action-input", grid.getTableWrapper());

                if (action.val() != "" && grid.getSelectedRowsCount() > 0) {

                    grid.setAjaxParam("customActionType", "group_action");

                    grid.setAjaxParam("customActionName", action.val());

                    grid.setAjaxParam("id", grid.getSelectedRows());

                    grid.getDataTable().ajax.reload();

                    grid.clearAjaxParams();

                } else if (action.val() == "") {

                    App.alert({

                        type: 'danger',

                        icon: 'warning',

                        message: 'Please select an action',

                        container: grid.getTableWrapper(),

                        place: 'prepend'

                    });

                } else if (grid.getSelectedRowsCount() === 0) {

                    App.alert({

                        type: 'danger',

                        icon: 'warning',

                        message: 'No record selected',

                        container: grid.getTableWrapper(),

                        place: 'prepend'

                    });

                }

            });


            //grid.setAjaxParam("customActionType", "group_action");

            //grid.getDataTable().ajax.reload();

            //grid.clearAjaxParams();
        }


    }

    var Validation = function(FormMod) {

        // for more info visit the official plugin documentation:

        // http://docs.jquery.com/Plugins/Validation


        var form1 = $('#DForm');

        if (form1.length) {
            var error1 = $('.alert-danger', form1);

            var success1 = $('.alert-success', form1);

            //IMPORTANT: update CKEDITOR textarea with actual content before submit
            form1.on('submit', function() {
                for (var instanceName in CKEDITOR.instances) {
                    CKEDITOR.instances[instanceName].updateElement();
                }
            })

            form1.validate({
                lang: Lang.get('global.currentLang'),
                errorElement: 'span', //default input error message container

                errorClass: 'help-block help-block-error', // default input error message class

                focusInvalid: false, // do not focus the last invalid input

                ignore: "", // validate all fields including form hidden input

                messages: {},

                rules: {


                    token: {


                        required: true,

                    },
                    //  name_en: {

                    //     minlength: 3,

                    //     required: true,

                    // },

                    //  role: {


                    //     required: true,

                    // },


                    //  email: {

                    //     email: true,

                    //     required: true,

                    // },


                },


                invalidHandler: function(event, validator) { //display error alert on form submit

                    success1.hide();

                    error1.show();

                    // App.scrollTo(error1, -200);

                },


                errorPlacement: function(error, element) { // render error placement for each input type

                    var cont = $(element).parent('.input-group');

                    if (cont.size() > 0) {

                        cont.after(error);

                    } else {

                        element.after(error);

                    }

                },


                highlight: function(element) { // hightlight error inputs


                    $(element)

                    .closest('.form-group').addClass('has-error'); // set error class to the control group

                },


                unhighlight: function(element) { // revert the change done by hightlight

                    $(element)

                    .closest('.form-group').removeClass('has-error'); // set error class to the control group

                },


                success: function(label) {

                    label

                        .closest('.form-group').removeClass('has-error'); // set success class to the control group

                },


                submitHandler: function(form) {

                    success1.show();

                    error1.hide();


                    //upload files

                    var data = new FormData(form1[0]);

                    if ($('.file').length) {

                        jQuery.each(jQuery('.file')[0].files, function(i, file) {

                            data.append('file-' + i, file);

                        });

                    }

                    data.append('par1', form1.attr("par1"));

                    data.append('par2', form1.attr("par2"));

                    $.ajax({

                        type: "POST",

                        url: APP_URL + "/Manage/Slider/Store",

                        data: data,

                        //async: false,

                        cache: false,

                        contentType: false,

                        processData: false,

                        dataType: "JSON",

                        beforeSend: function() {

                            FormMod.showLoading(true);

                        },

                        complete: function() {

                            FormMod.hideLoading(true);


                        },

                        error: function(data, xhr, ajaxOptions, thrownError) {

                            App.ErrorHandel(data, xhr, ajaxOptions, thrownError);

                        },


                        success: function(response) {

                            if (response !== null && response.hasOwnProperty("Errors")) {
                                var Resp = response.Errors;
                                //FormMod.close();
                                FormMod.buttons.formSubmit.hide();
                                FormMod.buttons.cancel.setText(Lang.get('global.ok'));
                                FormMod.setColumnClass('col-md-6');
                                FormMod.setTitle(Resp.title);
                                FormMod.setContent(Resp.content);
                                if ($('#datatable_ajax').length) {
                                    $('#datatable_ajax').DataTable().ajax.reload();
                                }

                            } else if (response !== null && response.hasOwnProperty("Success")) {

                                var Resp = response.Success;
                                FormMod.close();

                                $.alert({
                                    title: Resp.title,
                                    content: Resp.content,
                                    rtl: App.isRTL(),
                                    escapeKey: 'ok',
                                    closeIcon: true,
                                    type: 'green',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function() {

                                            }
                                        },
                                    },
                                });


                                FormMod.buttons.formSubmit.hide();
                                FormMod.buttons.cancel.setText(Lang.get('global.ok'));
                                FormMod.setColumnClass('col-md-6');
                                FormMod.setTitle(Resp.title);
                                FormMod.setContent(Resp.content);

                                if ($('#datatable_ajax').length) {
                                    $('#datatable_ajax').DataTable().ajax.reload();
                                }

                            }

                        }

                    });

                }


            });
        }


    }


    var open_modal = function() {

        $(document).on('click', '.AEObject', function() {
            var el = $(this);
            var FormMod = $.confirm({
                rtl: App.isRTL(),
                escapeKey: 'cancel',
                closeIcon: true,
                content: function() {
                    var self = this;
                    return $.ajax({
                        url: APP_URL + "/Manage/Slider/AE",
                        data: { par1: el.attr('par1') },
                        dataType: 'json',
                        method: 'get'
                    }).done(function(response) {
                        self.setContent(response.content);
                        self.setTitle(response.title);
                    }).fail(function() {
                        self.setTitle(Lang.get("global.error"));
                        self.setContent(Lang.get("global.something_went_error_rep_sent"));
                    }).always(function() {

                    });
                },
                onContentReady: function() {
                    App.initAjax();
                    Validation(FormMod);

                    ////////////////////////////change select

                    $('#selectpicker').on('change', function() {

                        var val = $(this).val();
                        if (val == 'pic') {
                            $("#pic").show();
                            $("#link_video").hide();
                            $("#upload_video").hide();
                            $("#link_video").select("val", "");
                            $("#upload_video").select("val", "");

                        }

                        if (val == 'link_video') {
                            $("#link_video").show();
                            $("#pic").hide();
                            $("#upload_video").hide();
                            $("#pic").select("val", "");
                            $("#upload_video").select("val", "");
                        }
                        if (val == 'upload_video') {
                            $("#upload_video").show();
                            $("#pic").hide();
                            $("#link_video").hide();
                            $("#pic").select("val", "");
                            $("#link_video").select("val", "");

                        }

                    })

                    /////////////////////////////




                },
                columnClass: 'col-md-12',
                backgroundDismiss: false,
                backgroundDismissAnimation: 'shake',
                buttons: {
                    formSubmit: {
                        text: Lang.get('global.submit'),
                        btnClass: 'btn-blue',
                        action: function() {
                            $('#DForm').submit();
                            return false;
                        }
                    },
                    cancel: {
                        text: Lang.get('global.cancel'),
                        action: function() {

                        }
                    },

                },

            });


        });


    }


    var delete_row = function() {


        $(document).on('click', '.Delete', function() {
            var el = $(this);

            App.DeleteItemAjax(el, "/Manage/Slider/Delete", '#datatable_ajax', '#datatable_ajax');

        });


    }

    return {


        //main function to initiate the module

        init: function() {

            initPickers();
            ShowWithControl();
            open_modal();
            delete_row();

        }


    };


}();


jQuery(document).ready(function() {

    SliderMethods.init();

});