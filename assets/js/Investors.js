var Investors = function () {
    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true,
            language: $('html').attr('lang'),
            format: 'yyyy-mm-dd',
        });
    }

    var ShowWithControl = function () {

        if ($("#datatable_ajax").length) {

            var grid = new Datatable();


            grid.init({

                src: $("#datatable_ajax"),

                onSuccess: function (grid, response) {

                    // grid:        grid object

                    // response:    json object of server side ajax response

                    // execute some code after table records loaded

                },

                onError: function (grid) {

                    // execute some code on network or other general error

                },

                onDataLoad: function (grid) {

                    // execute some code on ajax data load

                },


                dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options

                    "language": {

                        "url": APP_URL_Without_Lang + "/assets/global/plugins/datatables/languages/" + Lang.get("global.currentLang") + ".json"

                    },

                    // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout

                    // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).

                    // So when dropdowns used the scrollable div should be removed.

                    //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",


                    // save datatable state(pagination, sort, etc) in cookie.

                    "bStateSave": true,


                    // save custom filters to the state

                    "fnStateSaveParams": function (oSettings, sValue) {

                        $("#datatable_ajax tr.filter .form-control").each(function () {

                            sValue[$(this).attr('name')] = $(this).val();

                        });


                        return sValue;

                    },


                    // read the custom filters from saved state and populate the filter inputs

                    "fnStateLoadParams": function (oSettings, oData) {

                        //Load custom filters

                        $("#datatable_ajax tr.filter .form-control").each(function () {

                            var element = $(this);

                            if (oData[element.attr('name')]) {

                                element.val(oData[element.attr('name')]);

                            }

                        });


                        return true;

                    },


                    "lengthMenu": [

                        [10, 20, 50, 100, 150, -1],

                        [10, 20, 50, 100, 150, "All"] // change per page values here

                    ],

                    "columnDefs": [{

                        "targets": '_all',

                        "searchable": false,

                        className: "text-center"

                    }],

                    "pageLength": 20, // default record count per page

                    "ajax": {

                        "url": APP_URL + "/Manage/Investors/GetData", // ajax source

                    },

                    "ordering": false,

                    "order": [

                        [1, "asc"]

                    ]// set first column as a default sort by asc

                }

            });


            // handle group actionsubmit button click

            grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {

                e.preventDefault();

                var action = $(".table-group-action-input", grid.getTableWrapper());

                if (action.val() != "" && grid.getSelectedRowsCount() > 0) {

                    grid.setAjaxParam("customActionType", "group_action");

                    grid.setAjaxParam("customActionName", action.val());

                    grid.setAjaxParam("id", grid.getSelectedRows());

                    grid.getDataTable().ajax.reload();

                    grid.clearAjaxParams();

                } else if (action.val() == "") {

                    App.alert({

                        type: 'danger',

                        icon: 'warning',

                        message: 'Please select an action',

                        container: grid.getTableWrapper(),

                        place: 'prepend'

                    });

                } else if (grid.getSelectedRowsCount() === 0) {

                    App.alert({

                        type: 'danger',

                        icon: 'warning',

                        message: 'No record selected',

                        container: grid.getTableWrapper(),

                        place: 'prepend'

                    });

                }

            });


            //grid.setAjaxParam("customActionType", "group_action");

            //grid.getDataTable().ajax.reload();

            //grid.clearAjaxParams();
        }


    }

    var Validation = function (FormMod) {

        // for more info visit the official plugin documentation: 

        // http://docs.jquery.com/Plugins/Validation


        var form1 = $('#DForm');

        if (form1.length) {

            //IMPORTANT: update CKEDITOR textarea with actual content before submit
            // form1.on('submit', function () {
            //     for (var instanceName in CKEDITOR.instances) {
            //         CKEDITOR.instances[instanceName].updateElement();
            //     }
            // })

            form1.on('submit', function () {
                Identity_Details();
                share_amount_details();

            })

            form1.validate({

                errorElement: 'span', //default input error message container

                errorClass: 'help-block help-block-error', // default input error message class

                focusInvalid: false, // do not focus the last invalid input

                ignore: "",  // validate all fields including form hidden input

                messages: {},

                rules: {



                    'firstname': {

                        minlength: 1,
                        maxlength: 255,
                        required: true,

                    },
                    'secondname': {

                        minlength: 1,
                        maxlength: 255,
                        required: true,

                    },
                    'thirdname': {

                        minlength: 1,
                        maxlength: 255,
                        required: true,

                    },
                    'fourthname': {

                        minlength: 1,
                        maxlength: 255,
                        required: true,

                    },
                    'profileimg': {

                        required: true,
                    },
                    'country': {

                        required: true,
                    },

                    'phone': {

                        required: true,
                        number: true

                    },
                    'birthday': {

                        required: true,
                    },
                    'email': {

                        required: true,
                        email: true,

                    },
                    'password': {

                        required: true,

                    },
                    'password_confirmation': {
                        equalTo: "#password1"

                    },

                    'address': {
                        required: true,
                    },

                    'city': {
                        required: true,

                    },
                    'state': {
                        required: true,

                    },
                    'id_type': {
                        required: true,

                    },
                    'id_number': {
                        required: true,

                    },
                    'id_image_path': {
                        required: true,

                    },
                    'share_amount': {
                        required: true,

                    },
                    'share_number': {
                        required: true,

                    },
                    'share_date': {
                        required: true,

                    },
                    'share_image_path': {
                        required: true,

                    },
                    'no_payment_image': {
                        required: true,
                    },
                    'no_contract_image': {
                        required: true,
                    },
                    'identity_details[1][id_type]': {
                        required: true,
                    },
                    'share_amount_details[1][share_amount]': {
                        required: true,
                    },


                },


                invalidHandler: function (event, validator) { //display error alert on form submit

                    $.alert({
                        title: Lang.get("global.error"),
                        content: Lang.get("global.please_recheck_fields"),
                        rtl: App.isRTL(),
                        escapeKey: 'cancel',
                        closeIcon: true,
                        type: 'orange',
                        buttons: {
                            ok: {
                                text: Lang.get('global.ok'),
                                btnClass: 'btn-blue',
                                action: function () {

                                }
                            },
                        },
                    });

                },


                errorPlacement: function (error, element) { // render error placement for each input type

                    if (element.parents('.mt-radio-list').size() > 0 || element.parents('.mt-checkbox-list').size() > 0) {
                        if (element.parents('.mt-radio-list').size() > 0) {
                            error.appendTo(element.parents('.mt-radio-list')[0]);
                        }
                        if (element.parents('.mt-checkbox-list').size() > 0) {
                            error.appendTo(element.parents('.mt-checkbox-list')[0]);
                        }
                    } else if (element.parents('.mt-radio-inline').size() > 0 || element.parents('.mt-checkbox-inline').size() > 0) {
                        if (element.parents('.mt-radio-inline').size() > 0) {
                            error.appendTo(element.parents('.mt-radio-inline')[0]);
                        }
                        if (element.parents('.mt-checkbox-inline').size() > 0) {
                            error.appendTo(element.parents('.mt-checkbox-inline')[0]);
                        }
                    } else if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) {
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }

                },


                highlight: function (element) { // hightlight error inputs


                    $(element)

                        .closest('.form-group').addClass('has-error'); // set error class to the control group

                },


                unhighlight: function (element) { // revert the change done by hightlight

                    $(element)

                        .closest('.form-group').removeClass('has-error'); // set error class to the control group

                },


                success: function (label) {

                    label

                        .closest('.form-group').removeClass('has-error'); // set success class to the control group

                },


                submitHandler: function (form) {




                    //upload files

                    var data = new FormData(form1[0]);

                    if ($('.file').length) {

                        jQuery.each(jQuery('.file')[0].files, function (i, file) {

                            data.append('file-' + i, file);

                        });

                    }

                    data.append('par1', form1.attr("par1"));

                    data.append('par2', form1.attr("par2"));

                    $.ajax({

                        type: "POST",

                        url: APP_URL + "/Manage/Investors/Store",

                        data: data,

                        //async: false,

                        cache: false,

                        contentType: false,

                        processData: false,

                        dataType: "JSON",

                        beforeSend: function () {

                            FormMod.showLoading(true);

                        },

                        complete: function () {

                            FormMod.hideLoading(true);


                        },

                        error: function (data, xhr, ajaxOptions, thrownError) {

                            App.ErrorHandel(data, xhr, ajaxOptions, thrownError);

                        },


                        success: function (response) {

                            if (response !== null && response.hasOwnProperty("Errors")) {
                                var Resp = response.Errors;
                                //FormMod.close();
                                // FormMod.buttons.formSubmit.hide();
                                // FormMod.buttons.cancel.setText(Lang.get('global.ok'));
                                // FormMod.setColumnClass('col-md-6');
                                // FormMod.setTitle(Resp.title);
                                // FormMod.setContent(Resp.content);

                                $.alert({
                                    title: Resp.title,
                                    content: Resp.content,
                                    rtl: App.isRTL(),
                                    escapeKey: 'cancel',
                                    closeIcon: true,
                                    type: 'red',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function () {

                                            }
                                        },
                                    },
                                });


                                if ($('#datatable_ajax').length) {
                                    $('#datatable_ajax').DataTable().ajax.reload();
                                }

                            } else if (response !== null && response.hasOwnProperty("Success")) {

                                var Resp = response.Success;
                                FormMod.close();

                                $.alert({
                                    title: Resp.title,
                                    content: Resp.content,
                                    rtl: App.isRTL(),
                                    escapeKey: 'cancel',
                                    closeIcon: true,
                                    type: 'green',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function () {

                                            }
                                        },
                                    },
                                });


                                FormMod.buttons.formSubmit.hide();
                                FormMod.buttons.cancel.setText(Lang.get('global.ok'));
                                FormMod.setColumnClass('col-md-6');
                                FormMod.setTitle(Resp.title);
                                FormMod.setContent(Resp.content);

                                if ($('#datatable_ajax').length) {
                                    $('#datatable_ajax').DataTable().ajax.reload();
                                }

                            }

                        }

                    });

                }


            });
            function Identity_Details() {

                $('.identity_details').each(function (index) {
                    var itt = $(this);
                    var do_rule = false;
                    $('input,select', $(this)).each(function () {
                        if ($(this).val().length != 0) {
                            do_rule = true;
                        }
                    });
                    if (do_rule === true) {
                        $("select[name*='id_type']", itt).rules('add', {
                            "required": true,
                        });
                        $("input[name*='id_number']", itt).rules('add', {
                            "required": true,
                        });
                        var objid = $("#DForm").attr("par1");
                        if(!(Math.floor(objid) == objid && $.isNumeric(objid))){
                            $("input[name*='id_image_path']", itt).rules('add', {
                                "required": true,
                            });
                        }

                    }
                });

            }
            function share_amount_details() {

                $('.share_amount_details').each(function (index) {
                    var itt = $(this);
                    var do_rule = false;
                    $('input,select', $(this)).each(function () {
                        if ($(this).val().length != 0) {
                            do_rule = true;
                        }
                    });
                    if (do_rule === true) {
                        $("input[name*='share_amount']", itt).rules('add', {
                            "required": true,
                            "digits": true,
                        });
                        $("input[name*='share_number']", itt).rules('add', {
                            "required": true,
                        });
                        $("input[name*='share_date']", itt).rules('add', {
                            "required": true,
                            "date": true
                        });
                    }
                });

            }
        }


    }


    var open_modal = function () {

        $(document).on('click', '.AEObject', function () {
            var el = $(this);
            var FormMod = $.confirm({
                rtl: App.isRTL(),
                escapeKey: 'cancel',
                closeIcon: true,
                content: function () {
                    var self = this;
                    return $.ajax({
                        url: APP_URL + "/Manage/Investors/AE",
                        data: {par1: el.attr('par1')},
                        dataType: 'json',
                        method: 'get'
                    }).done(function (response) {
                        self.setContent(response.content);
                        self.setTitle(response.title);
                    }).fail(function () {
                        self.setTitle(Lang.get("global.error"));
                        self.setContent(Lang.get("global.something_went_error_rep_sent"));
                    }).always(function () {

                    });
                },
                onContentReady: function () {
                    App.initAjax();
                    Validation(FormMod);
                    initPickers();
                    repeater();


                },
                columnClass: 'col-md-12',
                backgroundDismiss: false,
                backgroundDismissAnimation: 'shake',
                buttons: {
                    formSubmit: {
                        text: Lang.get('global.submit'),
                        btnClass: 'btn-blue',
                        action: function () {
                            $('#DForm').submit();
                            return false;
                        }
                    },
                    cancel: {
                        text: Lang.get('global.cancel'),
                        action: function () {

                        }
                    },

                },

            });


        });

        $(document).on('click', '.ShowInvsProfile', function () {
            var el = $(this);
            var FormMod = $.confirm({
                rtl: App.isRTL(),
                escapeKey: 'cancel',
                closeIcon: true,
                content: function () {
                    var self = this;
                    return $.ajax({
                        url: APP_URL + "/Manage/Investors/Profile",
                        data: {par1: el.attr('par1')},
                        dataType: 'json',
                        method: 'get'
                    }).done(function (response) {
                        self.setContent(response.content);
                        self.setTitle(response.title);
                    }).fail(function () {
                        self.setTitle(Lang.get("global.error"));
                        self.setContent(Lang.get("global.something_went_error_rep_sent"));
                    }).always(function () {

                    });
                },
                onContentReady: function () {
                    App.initAjax();
                    Validation(FormMod);
                    initPickers();


                    $('.parent-container').magnificPopup({
                        delegate: 'a', // child items selector, by clicking on it popup will open
                        type: 'image'
                        // other options
                    });

                    $('.popup-link').magnificPopup({
                        type: 'image'
                        // other options
                    });


                },
                columnClass: 'col-md-12',
                backgroundDismiss: false,
                backgroundDismissAnimation: 'shake',
                buttons: {
                    formSubmit: {
                        text: Lang.get('global.submit'),
                        btnClass: 'btn-blue',
                        action: function () {
                            $('#DForm').submit();
                            return false;
                        }
                    },
                    cancel: {
                        text: Lang.get('global.cancel'),
                        action: function () {

                        }
                    },

                },

            });


        });


    }


    var delete_row = function () {


        $(document).on('click', '.Delete', function () {
            var el = $(this);

            App.DeleteItemAjax(el, "/Manage/Investors/Delete", '#datatable_ajax', '#datatable_ajax');

        });


    }
    var ActiveM = function () {


        $(document).on('click', '.ActiveM', function () {
            var el = $(this);

            $.confirm({
                title: el.attr('Dtitle'),
                content: el.attr('Dcontent'),
                type: 'green',
                buttons: {
                    confirm: {
                        text: Lang.get('global.yes'),
                        btnClass: 'btn-success',
                        keys: ['enter'],
                        action: function () {
                            $.ajax({
                                type: "POST",
                                url: APP_URL + "/Manage/Investors/Active",
                                data: {id: el.attr('par1')},
                                //async: false,
                                dataType: "JSON",
                                beforeSend: function () {
                                    App.blockUI({
                                        target: "#datatable_ajax",
                                        overlayColor: 'none',
                                        cenrerY: true,
                                        animate: true
                                    });

                                },
                                complete: function () {
                                    App.unblockUI("#datatable_ajax");
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $.alert({
                                        title: Lang.get('global.error'),
                                        content: xhr.status + " - " + thrownError,
                                        type: 'red',
                                        rtl: App.isRTL(),
                                        closeIcon: true,
                                        buttons: {
                                            cancel: {
                                                text: Lang.get('global.ok'),
                                                action: function () {
                                                }
                                            }
                                        }
                                    });
                                },
                                success: function (response) {
                                    if (response !== null && response.hasOwnProperty("Success")) {

                                        //refresh table
                                        if ($("#datatable_ajax").length) {
                                            var table = $("#datatable_ajax").DataTable();
                                            table.ajax.reload();
                                        }


                                        var Res = response['Success'];
                                        $.alert({
                                            title: Res.title,
                                            content: Res.content,
                                            autoClose: 'cancel|10000',
                                            type: 'green',
                                            rtl: App.isRTL(),
                                            closeIcon: true,
                                            buttons: {
                                                cancel: {
                                                    text: Lang.get('global.ok'),
                                                    action: function () {
                                                    }
                                                }
                                            }
                                        });

                                    } else if (response !== null && response.hasOwnProperty("Errors")) {

                                        //refresh table
                                        if ($("#datatable_ajax").length) {
                                            var table = $("#datatable_ajax").DataTable();
                                            table.ajax.reload();
                                        }


                                        var Error = response['Errors'];
                                        $.alert({
                                            title: Error.title,
                                            content: Error.content,
                                            type: 'red',
                                            rtl: App.isRTL(),
                                            closeIcon: true,
                                            buttons: {
                                                cancel: {
                                                    text: Lang.get('global.ok'),
                                                    action: function () {
                                                    }
                                                }
                                            }
                                        });

                                    }
                                }
                            });
                        }
                    },
                    cancel: {
                        text: Lang.get('global.no'),
                        keys: ['esc'],
                        action: function () {
                        }
                    },
                }
            });

        });


    }

    var repeater = function () {

        $('.mt-repeater').each(function () {
            $(this).repeater({
                show: function () {
                    $(this).slideDown();

                    $('.date-picker').datepicker({
                        rtl: App.isRTL(),
                        autoclose: true,
                        language: $('html').attr('lang'),
                        format: 'yyyy-mm-dd',
                    });

                    // $(this).find('.BarCode_Select').each(function (i, obj) {
                    //     BarCode_Select($(obj));
                    // });
                    // $(this).find('.ItemNumber_Select').each(function (i, obj) {
                    //     ItemNumber_Select($(obj));
                    // });
                    // $(this).find('.Item_Select').each(function (i, obj) {
                    //     Item_Select($(obj));
                    // });

                },
                hide: function (deleteElement) {
                    var tt = $(this);
                    var el = $(this).find(".mt-repeater-del-right");
                    if (el.attr('par1')) {
                        $.confirm({
                            title: el.attr('Dtitle'),
                            content: el.attr('Dcontent'),
                            type: 'red',
                            buttons: {
                                confirm: {
                                    text: el.attr('confirmButton'),
                                    btnClass: 'btn-danger',
                                    keys: ['enter'],
                                    action: function () {
                                        $.ajax({
                                            type: "POST",
                                            url: APP_URL + "/Manage/Investors/DeleteChild",
                                            data: {id: el.attr('par1'), par2: el.attr('par2'),par3: el.attr('par3')},
                                            //async: false,
                                            dataType: "JSON",
                                            beforeSend: function () {
                                                App.blockUI({
                                                    target: '#DForm',
                                                    overlayColor: 'none',
                                                    cenrerY: true,
                                                    animate: true
                                                });
                                            },
                                            complete: function () {
                                                App.unblockUI('#DForm');
                                            },
                                            error: function (xhr, ajaxOptions, thrownError) {
                                                $.alert({
                                                    title: Lang.Error,
                                                    content: xhr.status + " - " + thrownError,
                                                    type: 'red',
                                                    rtl: App.isRTL(),
                                                    closeIcon: true,
                                                    buttons: {
                                                        cancel: {
                                                            text: Lang.get('global.ok'),
                                                            action: function () {
                                                            }
                                                        }
                                                    }
                                                });
                                            },
                                            success: function (response) {
                                                if (response !== null && response.hasOwnProperty("Success")) {
                                                    tt.slideUp(deleteElement);

                                                    var Res = response['Success'];
                                                    $.alert({
                                                        title: Res.title,
                                                        content: Res.content,
                                                        autoClose: 'cancel|10000',
                                                        type: 'green',
                                                        rtl: App.isRTL(),
                                                        closeIcon: true,
                                                        buttons: {
                                                            cancel: {
                                                                text: Lang.get('global.ok'),
                                                                action: function () {
                                                                }
                                                            }
                                                        }
                                                    });

                                                } else if (response !== null && response.hasOwnProperty("Errors")) {

                                                    var Error = response['Errors'];
                                                    $.alert({
                                                        title: Error.title,
                                                        content: Error.content,
                                                        type: 'red',
                                                        rtl: App.isRTL(),
                                                        closeIcon: true,
                                                        buttons: {
                                                            cancel: {
                                                                text: Lang.get('global.ok'),
                                                                action: function () {
                                                                }
                                                            }
                                                        }
                                                    });

                                                }
                                            }
                                        });
                                    }
                                },
                                cancel: {
                                    text: el.attr('cancelButton'),
                                    keys: ['esc'],
                                    action: function () {
                                    }
                                },
                            }
                        });
                    } else {
                        $.confirm({
                            title: el.attr('Dtitle'),
                            content: el.attr('Dcontent'),
                            type: 'red',
                            buttons: {
                                confirm: {
                                    text: el.attr('confirmButton'),
                                    btnClass: 'btn-danger',
                                    keys: ['enter'],
                                    action: function () {
                                        tt.slideUp(deleteElement);
                                    }
                                },
                                cancel: {
                                    text: el.attr('cancelButton'),
                                    keys: ['esc'],
                                    action: function () {
                                    }
                                },
                            }
                        });
                    }


                },
                ready: function (setIndexes) {
                    setIndexes;

                }

            });
        });

    };

    return {


        //main function to initiate the module

        init: function () {

            initPickers();
            ShowWithControl();

            open_modal();

            delete_row();
            ActiveM();

        }


    };


}();


jQuery(document).ready(function () {

    Investors.init();

});