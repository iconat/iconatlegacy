var SubscribeMethods = function () {
    
   

    var SubscribeValidation = function () {
        var form1 = $('#newsletterform');

        if (form1.length) {
            form1.validate({
                lang: Lang.get('global.currentLang'),
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                messages: {},
                rules: {
                  
                    
                    email: {
                        email: true,
                        required: true,
                    },

                   
                },


                invalidHandler: function (event, validator) { //display error alert on form submit

                    $.alert({
                        title: Lang.get('global.sorry'),
                        content: Lang.get('global.please_recheck_fields'),
                        rtl: $("html").attr("dir") == 'rtl',
                        escapeKey: 'cancel',
                        closeIcon: true,
                        type: 'orange',
                        buttons: {
                            ok: {
                                text: Lang.get('global.ok'),
                                btnClass: 'btn-blue',
                                action: function () {

                                }
                            },
                        },
                    });

                },


                errorPlacement: function (error, element) { // render error placement for each input type

                    var cont = $(element).parent('.input-group');

                    if (cont.size() > 0) {

                        cont.after(error);

                    } else {

                        element.after(error);

                    }

                },


                highlight: function (element) { // hightlight error inputs
                  
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },


                unhighlight: function (element) { // revert the change done by hightlight

                    $(element)

                        .closest('.form-group').removeClass('has-error'); // set error class to the control group

                },


                success: function (label) {

                    label

                        .closest('.form-group').removeClass('has-error'); // set success class to the control group

                },


                submitHandler: function (form) {

                    var data = new FormData(form1[0]);
                    if ($('.file').length) {
                        jQuery.each(jQuery('.file')[0].files, function (i, file) {
                            data.append('file-' + i, file);
                        });
                    }

                    data.append('par1', form1.attr("par1"));
                    data.append('lang', $("html").attr("lang"));

                    $.ajax({
                        type: "POST",
                        url: APP_URL + "/SendSubscribeToDB",
                        data: data,
                        //async: false,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "JSON",
                        beforeSend: function () {
                           // FormMod.showLoading(true);
                        },

                        complete: function () {
                           //FormMod.hideLoading(true);
                        },

                        error: function (data, xhr, ajaxOptions, thrownError) {




                            //  console.log(data);
                            var response = data.responseJSON;

                            if (response != undefined && response !== null && response.hasOwnProperty("errors")) {
                                var errors = response.errors;
                                var eee = '';
                                $.each(errors, function (key, value) {
                                    //$('#' + key).parent().addClass('error');
                                    eee += ' * ' + value + '</br>';
                                });
                                $.alert({
                                    title: Lang.get('global.error'),
                                    content: eee,
                                    rtl: $("html").attr("dir") == 'rtl',
                                    type: 'red',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function () {

                                            }
                                        },
                                    },
                                });

                            } else {
                                $.alert({
                                    title: Lang.get('global.error'),
                                    content: Lang.get('global.something_went_error_rep_sent'),
                                    rtl: $("html").attr("dir") == 'rtl',
                                    type: 'red',
                                });
                            }
                        },


                        success: function (response) {

                            if (response !== null && response.hasOwnProperty("Errors")) {
                                var Resp = response.Errors;
                                //FormMod.close();
                               // FormMod.buttons.formSubmit.hide();
                               //FormMod.buttons.cancel.setText(Lang.get('global.ok'));
                               // //FormMod.setColumnClass('col-md-6');
                               // FormMod.setTitle(Resp.title);
                               // FormMod.setContent(Resp.content);


                            } else if (response !== null && response.hasOwnProperty("Success")) {

                                var Resp = response.Success;
                                //FormMod.close();

                                $.alert({
                                    title: Resp.title,
                                    content: Resp.content,
                                    rtl: $("html").attr("dir") == 'rtl',
                                    escapeKey: 'ok',
                                    closeIcon: true,
                                    type: 'green',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function () {
                                                if (Resp.redirect == true) {
                                                    location.reload();
                                                }
                                            }
                                        },
                                    },
                                });


                                //FormMod.buttons.formSubmit.hide();
                                //FormMod.buttons.cancel.setText(Lang.get('global.ok'));
                                //FormMod.setColumnClass('col-md-6');
                                //FormMod.setTitle(Resp.title);
                                //FormMod.setContent(Resp.content);


                            }

                        }

                    });

                }


            });
        }


    }


    

   

   

   

    


 

    return {


        //main function to initiate the module

        init: function () {

         
          
         

            SubscribeValidation();


        }


    };


}();


jQuery(document).ready(function () {

    SubscribeMethods.init();

});