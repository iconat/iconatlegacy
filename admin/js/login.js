var FormValidation2 = function () {

   

    // validation using icons
    var handleValidation3 = function() {
    
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation
            var form2 = $('#form_login');
            var error2 = $('.alert-danger', form2);
            var success2 = $('.alert-success', form2);

            form2.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {

                    'password': {
                        required: true,
                    },

                    'email': {
                        required: true,
                        
                    },
                  
                   
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success2.hide();
                    error2.show();
                    // App.scrollTo(error2, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                  if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }                },
                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .removeClass("has-success").addClass('has-error'); // set error class to the control group   
                },

                unhighlight: function (element) { // revert the change done by hightlight
                                        $(element)
                        .removeClass("has-success").removeClass('has-error'); // set error class to the control group   

                },

                success: function (label, element) {
                    var icon = $(element).parent('.input-icon').children('i');
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    icon.removeClass("fa-warning").addClass("fa-check");
                },

                submitHandler: function (form) {
                    success2.show();
                    error2.hide();

                   // form[0].submit(); // submit the form
                    //upload files
                var data = new FormData(form2[0]);
                // jQuery.each(jQuery('.file')[0].files, function (i, file) {
                //     data.append('file-' + i, file);
                // });
                data.append('par1', form2.attr("par1"));//add part1
                data.append('par2', form2.attr("par2"));
                $.ajax({
                    type: "POST",
                    url:APP_URL+"/login",
                    data: data,
                    //async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: "JSON",
                    beforeSend: function () {

                        // App.blockUI({
                        //     target: '#form_login',
                        //     overlayColor: 'none',
                        //     cenrerY: true,
                        //     animate: true
                        // });
                    },
                    complete: function () {
                        // App.unblockUI('#form_login');

                    },
                     error: function (data ,xhr, ajaxOptions, thrownError) {


                    // console.log(data);
        //               var res = data.responseJSON;
        //               var errors = res.errors;
        //               var eee = '';
        //               $.each(errors, function (key, value) {
        //     //$('#' + key).parent().addClass('error');
        //     eee += ' * ' + value + '</br>';
        // });

        //               toastr['error']( 'الخطأ' + '</br>' + eee)
        //               toastr.options = {
        //                   "closeButton": true,
        //                   "debug": false,
        //                   "positionClass": "toast-top-right",
        //                   "onclick": null,
        //                   "showDuration": "1000",
        //                   "hideDuration": "1000",
        //                   "timeOut": "5000",
        //                   "extendedTimeOut": "1000",
        //                   "showEasing": "swing",
        //                   "hideEasing": "linear",
        //                   "showMethod": "fadeIn",
        //                   "hideMethod": "fadeOut"
        //               }


                  },

                    success: function (response){

                
                         if (response !== null && response.hasOwnProperty("success")) {
                        
                             window.location.replace(APP_URL+response.redirectto);
                            
                         toastr['success']("", "تم الاضافة بنجاح");



                        // window.location.replace(response.redirectto);
                        toastr.options = {
                          "closeButton": true,
                          "debug": false,
                          "positionClass": "toast-top-right",
                          "onclick": null,
                          "showDuration": "1000",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                        }                            
                        }
                        else if (response !== null && response.hasOwnProperty("invs_msg")) {

                      $("#msg").html("طلبك تحت المعالجة طلبك");
                         // $('#msg').html(response.Result);



                        }
                        else if (response !== null && response.hasOwnProperty("errors")) {
                         //   alert(response.errors);
                             $("#msg").html("البيانات المدخله غير صحيحه");
                        }
                    }
                });
                }
            });


    }

 

    return {
        //main function to initiate the module
        init: function () {

            
            handleValidation3();

        }

    };

}();

jQuery(document).ready(function() {
    FormValidation2.init();
});