







var CheckValidation = function () {



    // basic validation

    var handleValidation1 = function() {

        // for more info visit the official plugin documentation: 

            // http://docs.jquery.com/Plugins/Validation



            var form1 = $('#check_form');

            var error1 = $('.alert-danger', form1);

            var success1 = $('.alert-success', form1);



            form1.validate({

                // lang:lang,

                errorElement: 'span', //default input error message container

                errorClass: 'help-block help-block-error', // default input error message class

                focusInvalid: false, // do not focus the last invalid input

                ignore: "",  // validate all fields including form hidden input

                messages: {

                    

                },

                rules: {

                  

                    'email': {

                    
                    required: true,

                         },
                          'password': {

                    
                    required: true,

                         },



                   

                },



                invalidHandler: function (event, validator) { //display error alert on form submit              

                    success1.hide();

                    error1.show();

                    // App.scrollTo(error1, -200);

                },



                errorPlacement: function (error, element) { // render error placement for each input type

                    var cont = $(element).parent('.input-group');

                    if (cont.size() > 0) {

                        cont.after(error);

                    } else {

                        element.after(error);

                    }

                },



                highlight: function (element) { // hightlight error inputs



                    $(element)

                        .addClass('has-error'); // set error class to the control group

                },



                unhighlight: function (element) { // revert the change done by hightlight

                    $(element)

                        .removeClass('has-error'); // set error class to the control group

                },



                success: function (label) {

                    label

                        .closest('.form-group').removeClass('has-error'); // set success class to the control group

                },



             

           submitHandler: function (form) {

                success1.show();

                error1.hide();

                //upload files

                var data = new FormData(form1[0]);


                $.ajax({

                    type: "POST",

                    url: APP_URL+"/GetCheckData",

                    data: data,

                    //async: false,

                    cache: false,

                    contentType: false,

                    processData: false,

                    dataType: "JSON",

                    beforeSend: function () {



                        // App.blockUI({

                        //     target: 'check_form',

                        //     overlayColor: 'none',

                        //     cenrerY: true,

                        //     animate: true

                        // });

                    },

                    complete: function () {



                        // App.unblockUI('#check_form');

                    },

                       error: function (data ,xhr, ajaxOptions, thrownError) {


                      //  console.log(data);
                      var res = data.responseJSON;
                      var errors = res.errors;
                      var eee = '';
                      $.each(errors, function (key, value) {
            //$('#' + key).parent().addClass('error');
            eee += ' * ' + value + '</br>';
        });

                      toastr['error']( res.massege + '</br>' + eee)
                      toastr.options = {
                          "closeButton": true,
                          "debug": false,
                          "positionClass": "toast-top-right",
                          "onclick": null,
                          "showDuration": "1000",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                      }


                  },

          

                    success: function (response)

                    {

                        if (response !== null && response.hasOwnProperty("UnderReview")) {
                            var Res = response.UnderReview;

                            
                            $.alert({
                                title: "",
                                content: Res.Msg,
                                type: 'green',
                               // autoClose: 'cancel|10000',
                                rtl: true,
                                closeIcon: true,
                                useBootstrap: false,
                                buttons: {
                                    cancel: {
                                        text: 'حسناً',
                                        action: function () {
                                            window.location.replace(APP_URL + Res.Redirect);
                                        }
                                    }
                                }
                            });
                            

                        } else if (response !== null && response.hasOwnProperty("ErrorData")) {
                            var Res = response.ErrorData;


                            $.alert({
                                title: "خطأ",
                                content: Res.Msg,
                                type: 'red',
                                rtl:true,
                                closeIcon: true,
                                useBootstrap: false,
                                buttons: {
                                    cancel: {
                                        text: "حسناً",
                                        action: function () {
                                        }
                                    }
                                }
                            });
                        }
                  

                    }

                });

            }



            });





    }



    return {

        //main function to initiate the module

        init: function () {



            handleValidation1();

           



        }



    };



}();



jQuery(document).ready(function() {

    CheckValidation.init();

});