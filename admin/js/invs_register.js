var RegisterFunction = function () {
    var select_date = function () {

        $(".select_date").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: '1945:' + (new Date).getFullYear()

        });
    }
    var image_preview = function () {
        $('.img_preview').uploadPreview({
            width: '200px',
            height: '200px',
            backgroundSize: 'cover',
            fontSize: '16px',
            borderRadius: '200px',
            border: '3px solid #dedede',
            lang: 'ar', //language
            cursor: 'pointer',

        });
    }
    var country_d = function () {
        $('#country').on('change', function () {
            $("#country_selected").val($(this).val());
        })

    }

// $('#password, #confirm_password').on('keyup', function () {
//   if ($('#password').val() == $('#confirm_password').val()) {
//   } else 
//     $('#message').html('الرقم السري غير متطابق').css('color', 'red');
// });


    // validation using icons
    var RegisterValidation = function () {
        // for more info visit the official plugin documentation:
        // http://docs.jquery.com/Plugins/Validation
        var form1 = $('#registration_form');
        var error2 = $('.alert-danger', form1);
        var success2 = $('.alert-success', form1);

        form1.on('submit', function () {
            CheckPayment_Details();
            CheckContract_Details();
            Identity_Details();
            share_amount_details();

        })


        var x = form1.validate({
            lang: 'ar',
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {

                'firstname': {

                    minlength: 1,
                    maxlength: 255,
                    required: true,

                },
                'secondname': {

                    minlength: 1,
                    maxlength: 255,
                    required: true,

                },
                'thirdname': {

                    minlength: 1,
                    maxlength: 255,
                    required: true,

                },
                'fourthname': {

                    minlength: 1,
                    maxlength: 255,
                    required: true,

                },
                'profileimg': {

                    required: true,
                },
                'country': {

                    required: true,
                },

                'phone': {

                    required: true,
                    number: true

                },
                'birthday': {

                    required: true,
                },
                'email': {

                    required: true,
                    email: true,

                },
                'password': {

                    required: true,

                },
                'password_confirmation': {
                    equalTo: "#password1"

                },

                'address': {
                    required: true,
                },

                'city': {
                    required: true,

                },
                'state': {
                    required: true,

                },
                'id_type': {
                    required: true,

                },
                'id_number': {
                    required: true,

                },
                'id_image_path': {
                    required: true,

                },
                'share_amount': {
                    required: true,

                },
                'share_number': {
                    required: true,

                },
                'share_date': {
                    required: true,

                },
                'share_image_path': {
                    required: true,

                },
                'no_payment_image': {
                    required: true,
                },
                'no_contract_image': {
                    required: true,
                },
                'identity_details[1][id_type]': {
                    required: true,
                },
                'share_amount_details[1][share_amount]': {
                    required: true,
                },


            },

            invalidHandler: function (event, validator) { //display error alert on form submit

                success2.hide();
                error2.show();
                var body = $("html, body");
                body.stop().animate({scrollTop: 0}, 500, 'swing', function () {

                });


            },

            errorPlacement: function (error, element) { // render error placement for each input type
                element.closest('.collapse').addClass('in');
                element.closest('.collapse').prev().addClass('collapsed');
                element.closest('.collapse').prev().attr("data-toggle", "");


                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .removeClass("has-success").addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .removeClass("has-error").addClass('has-success'); // set error class to the control group
            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            },

            submitHandler: function (form) {
                success2.show();
                error2.hide();

                // form[0].submit(); // submit the form
                //upload files
                var data = new FormData(form1[0]);
                // jQuery.each(jQuery('.file')[0].files, function (i, file) {
                //     data.append('file-' + i, file);
                // });
                data.append('par1', form1.attr("par1"));//add part1
                data.append('par2', form1.attr("par2"));
                $.ajax({
                    type: "POST",
                    url: APP_URL + "/SendDataToDB",
                    data: data,
                    //async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: "JSON",
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        var started_at = new Date();
                        var current_class = "p0";
                        xhr.upload.addEventListener('progress', function (e) {

                            if (e.lengthComputable) {
                                // Append progress percentage.
                                var loaded = e.loaded;

                                var total = e.total;
                                var progressValue = Math.round((loaded / total) * 100);

                                // Bytes received.
                                jQuery('.recievedValue').html('');
                                jQuery('.recievedValue').append(loaded / 2000000);
                                //
                                // Total bytes.
                                // jQuery('.totalValue').html('');
                                //jQuery('.totalValue').append(total/2000000);

                                // Time Remaining
                                var seconds_elapsed = (new Date().getTime() - started_at.getTime()) / 1000;
                                var bytes_per_second = seconds_elapsed ? loaded / seconds_elapsed : 0;
                                var Kbytes_per_second = bytes_per_second / 1000;
                                var remaining_bytes = total - loaded;
                                var seconds_remaining = seconds_elapsed ? remaining_bytes / bytes_per_second : 'calculating';
                                //jQuery('.timeRemaining').html('');
                                //jQuery('.timeRemaining').append(seconds_remaining);

                                // Percentage.
                                //$("#loader").css("width", progressValue + '%');
                                $("#loader").removeClass(current_class);

                                $("#loader").addClass("p" + progressValue);
                                current_class = "p" + progressValue;
                                $("#loader span").html(progressValue + "%");
                                // progressElement.find('input').val(progressValue).change();
                            }
                        }, false);
                        return xhr;
                    },
                    beforeSend: function () {
                        $.blockUI({
                            css: {backgroundColor: 'none', border: '0', top: '30%'},
                            message: '<div id="loader" class="c100 p0 big"><span>0%</span><div class="slice"><div class="bar"></div><div class="fill"></div></div></div>'
                        });

                    },
                    complete: function () {
                        $.unblockUI();

                    },
                    error: function (data, xhr, ajaxOptions, thrownError) {
                        $.unblockUI();

                        //  console.log(data);
                        var response = data.responseJSON;

                        if (response !== null && response.hasOwnProperty("errors")) {
                            var errors = response.errors;
                            var eee = '';
                            $.each(errors, function (key, value) {
                                //$('#' + key).parent().addClass('error');
                                eee += ' * ' + value + '</br>';
                            });
                            $.alert({
                                title: Lang.get('global.error'),
                                content: eee,
                                rtl: true,
                                type: 'red',
                                useBootstrap: false,
                                buttons: {
                                    ok: {
                                        text: Lang.get('global.ok'),
                                        btnClass: 'btn-blue',
                                        action: function () {

                                        }
                                    },
                                },
                            });

                        } else {
                            $.alert({
                                title: Lang.get('global.error'),
                                content: Lang.get('global.something_went_error_rep_sent'),
                                rtl: true,
                                type: 'red',
                                useBootstrap: false,
                            });
                        }


                    },
                    success: function (response) {


                        if (response !== null && response.hasOwnProperty("Success")) {


                            window.location.href = APP_URL + response.Success.redirectto;

                        } else if (response !== null && response.hasOwnProperty("errors")) {

//                             toastr['success']("", "تم الاضافة بنجاح");
//
//
//
// // window.location.replace(response.redirectto);
//
//                             toastr.options = {
//                                 "closeButton": true,
//                                 "debug": false,
//                                 "positionClass": "toast-top-right",
//                                 "onclick": null,
//                                 "showDuration": "1000",
//                                 "hideDuration": "1000",
//                                 "timeOut": "5000",
//                                 "extendedTimeOut": "1000",
//                                 "showEasing": "swing",
//                                 "hideEasing": "linear",
//                                 "showMethod": "fadeIn",
//                                 "hideMethod": "fadeOut"
//                             }

                            alert(response.errors);
                        }
                    }
                });
            }
        });

        function CheckPayment_Details() {

            $('.payment_details').each(function (index) {
                var itt = $(this);
                var do_rule = false;
                $('input,select', $(this)).each(function () {
                    if ($(this).val().length != 0) {
                        do_rule = true;
                    }
                });
                if (do_rule === true) {
                    $('input[name="no_payment_image"]').rules("remove", "required");
                }
            });

        }

        function CheckContract_Details() {

            $('.contracts_details').each(function (index) {
                var itt = $(this);
                var do_rule = false;
                $('input,select', $(this)).each(function () {
                    if ($(this).val().length != 0) {
                        do_rule = true;
                    }
                });
                if (do_rule === true) {
                    $('input[name="no_contract_image"]').rules("remove", "required");
                }
            });

        }

        function Identity_Details() {

            $('.identity_details').each(function (index) {
                var itt = $(this);
                var do_rule = false;
                $('input,select', $(this)).each(function () {
                    if ($(this).val().length != 0) {
                        do_rule = true;
                    }
                });
                if (do_rule === true) {
                    $("select[name*='id_type']", itt).rules('add', {
                        "required": true,
                    });
                    $("input[name*='id_number']", itt).rules('add', {
                        "required": true,
                    });
                    $("input[name*='id_image_path']", itt).rules('add', {
                        "required": true,
                    });
                }
            });

        }

        function share_amount_details() {

            $('.share_amount_details').each(function (index) {
                var itt = $(this);
                var do_rule = false;
                $('input,select', $(this)).each(function () {
                    if ($(this).val().length != 0) {
                        do_rule = true;
                    }
                });
                if (do_rule === true) {
                    $("input[name*='share_amount']", itt).rules('add', {
                        "required": true,
                        "digits": true,
                    });
                    $("input[name*='share_number']", itt).rules('add', {
                        "required": true,
                    });
                    $("input[name*='share_date']", itt).rules('add', {
                        "required": true,
                        "date": true
                    });
                }
            });

        }


    }


    var repeat_x = function () {
        $('.repeater').repeater({

            repeaters: [{
                // (Required)
                // Specify the jQuery selector for this nested repeater
                selector: '.group-a'
            }],

            // (Optional)
            // start with an empty list of repeaters. Set your first (and only)
            // "data-repeater-item" with style="display:none;" and pass the
            // following configuration flag
            initEmpty: true,
            // (Optional)
            // "defaultValues" sets the values of added items.  The keys of
            // defaultValues refer to the value of the input's name attribute.
            // If a default value is not specified for an input, then it will
            // have its value cleared.
            defaultValues: {
                'text-input': 'foo'
            },
            // (Optional)
            // "show" is called just after an item is added.  The item is hidden
            // at this point.  If a show callback is not given the item will
            // have $(this).show() called on it.
            show: function () {
                $(".select_date").datepicker({
                    dateFormat: 'yy-mm-dd',
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '1945:' + (new Date).getFullYear()
                });

                $(this).slideDown();
            },
            // (Optional)
            // "hide" is called when a user clicks on a data-repeater-delete
            // element.  The item is still visible.  "hide" is passed a function
            // as its first argument which will properly remove the item.
            // "hide" allows for a confirmation step, to send a delete request
            // to the server, etc.  If a hide callback is not given the item
            // will be deleted.
            hide: function (deleteElement) {
                if (confirm('هل انت متأكد من رغبتك حذف هذا العنصر؟')) {
                    $(this).slideUp(deleteElement);
                }
            },
            // (Optional)
            // You can use this if you need to manually re-index the list
            // for example if you are using a drag and drop library to reorder
            // list items.
            ready: function (setIndexes) {
                // $dragAndDrop.on('drop', setIndexes);
            },
            // (Optional)
            // Removes the delete button from the first list item,
            // defaults to false.
            isFirstItemUndeletable: true
        })

    }

    return {
        //main function to initiate the module
        init: function () {
            repeat_x();
            country_d();
            RegisterValidation();
            image_preview();
            select_date();

        }

    };

}();

jQuery(document).ready(function () {
    RegisterFunction.init();
});