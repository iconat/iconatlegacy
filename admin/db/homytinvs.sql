-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 26, 2017 at 03:58 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `homytinvs`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `state` varchar(200) NOT NULL,
  `country` int(11) NOT NULL,
  `zipcode` varchar(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_by` bigint(20) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `state` varchar(200) NOT NULL,
  `country` int(11) NOT NULL,
  `zipcode` varchar(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_by` bigint(20) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `investors`
--

CREATE TABLE IF NOT EXISTS `investors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) NOT NULL,
  `secondname` varchar(50) NOT NULL,
  `thirdname` varchar(50) NOT NULL,
  `fourthname` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `company` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `dob` date NOT NULL,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `country` varchar(200) DEFAULT NULL,
  `phone_country` varchar(100) NOT NULL,
  `zipcode` varchar(20) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `profileimg` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `investors`
--

INSERT INTO `investors` (`id`, `firstname`, `secondname`, `thirdname`, `fourthname`, `username`, `password`, `email`, `company`, `address`, `dob`, `city`, `state`, `country`, `phone_country`, `zipcode`, `status`, `profileimg`, `phone`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES
(1, 'First Name', 'Second Name', 'Third Name', 'Fourth Name', 'rajesh1may@gmail.com', 'rajesh', 'rajesh1may@gmail.com', NULL, 'Test Address', '2011-02-23', 'Test City', 'Test State', NULL, '', '', 0, '1509025882_20171026192122_1915445190.jpg', '324234234', '2017-10-26 19:21:22', '2017-10-26 19:21:22', 0, 0),
(2, 'adsfasd', 'asdfasdf', 'adsfasdf', 'asdfadsfasd', 'testname1@gmail.com', '12345', 'testname1@gmail.com', NULL, '3r2342', '0000-00-00', '34234234', 'sdfasdfa', NULL, '', '', 0, '1509026177_20171026192617_54081609.png', '234234234', '2017-10-26 19:26:17', '2017-10-26 19:26:17', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `investors_contracts`
--

CREATE TABLE IF NOT EXISTS `investors_contracts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `investor_id` bigint(20) NOT NULL,
  `contract_image` varchar(255) NOT NULL,
  `contract_image_name` varchar(255) NOT NULL,
  `uploaded_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `investors_contracts`
--

INSERT INTO `investors_contracts` (`id`, `investor_id`, `contract_image`, `contract_image_name`, `uploaded_on`) VALUES
(1, 2, '1509026177_20171026192617_INVS_CONTRACT_2033233500.png', 'Screenshot from 2017-09-19 17-37-30.png', '2017-10-26 19:26:17');

-- --------------------------------------------------------

--
-- Table structure for table `investors_ids`
--

CREATE TABLE IF NOT EXISTS `investors_ids` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `investor_id` bigint(20) NOT NULL,
  `id_type` varchar(200) NOT NULL,
  `id_number` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `investors_ids`
--

INSERT INTO `investors_ids` (`id`, `investor_id`, `id_type`, `id_number`, `status`, `created_on`) VALUES
(1, 2, 'Passport', 'adfasd', 1, '2017-10-26 19:26:17'),
(2, 2, 'Personal Id', 'w3234234', 1, '2017-10-26 19:26:17');

-- --------------------------------------------------------

--
-- Table structure for table `investors_ids_image`
--

CREATE TABLE IF NOT EXISTS `investors_ids_image` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `investors_ids_id` bigint(20) NOT NULL,
  `image_path` varchar(250) NOT NULL,
  `image_name` varchar(250) NOT NULL,
  `uploaded_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `investors_investment`
--

CREATE TABLE IF NOT EXISTS `investors_investment` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `investor_id` bigint(20) unsigned NOT NULL,
  `type_of_investment` varchar(50) DEFAULT NULL,
  `cheque_no` varchar(200) DEFAULT NULL,
  `bank_name` varchar(200) DEFAULT NULL,
  `cheque_date` date NOT NULL,
  `bank_location` varchar(200) DEFAULT NULL,
  `amount` float(30,2) NOT NULL,
  `invest_on` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `investors_old`
--

CREATE TABLE IF NOT EXISTS `investors_old` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `company` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `country` int(11) NOT NULL DEFAULT '0',
  `zipcode` varchar(20) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `profileimg` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `investors_shares`
--

CREATE TABLE IF NOT EXISTS `investors_shares` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `investor_id` bigint(20) NOT NULL,
  `share_amount` int(11) NOT NULL,
  `share_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `investors_shares`
--

INSERT INTO `investors_shares` (`id`, `investor_id`, `share_amount`, `share_date`) VALUES
(1, 2, 234234, '2011-02-25'),
(2, 2, 233333, '2011-02-25');

-- --------------------------------------------------------

--
-- Table structure for table `investors_shares_image`
--

CREATE TABLE IF NOT EXISTS `investors_shares_image` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `investors_shares_id` bigint(20) NOT NULL,
  `image_path` varchar(250) NOT NULL,
  `image_name` varchar(250) NOT NULL,
  `uploaded_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `short_title` varchar(200) DEFAULT NULL,
  `description` longtext NOT NULL,
  `image` varchar(250) NOT NULL,
  `video` varchar(250) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `source` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `tags` varchar(250) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `news_images`
--

CREATE TABLE IF NOT EXISTS `news_images` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `news_id` bigint(20) NOT NULL,
  `image_path` varchar(200) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE IF NOT EXISTS `offers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `image` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `shares`
--

CREATE TABLE IF NOT EXISTS `shares` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `date` date NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `specialoffer`
--

CREATE TABLE IF NOT EXISTS `specialoffer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `image` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `specialoffer_images`
--

CREATE TABLE IF NOT EXISTS `specialoffer_images` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `special_offer_id` bigint(20) NOT NULL,
  `image_path` varchar(200) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `profileimg` varchar(100) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `phone`, `status`, `profileimg`, `created_on`, `updated_on`, `created_by`) VALUES
(1, 'Rajesh Vishwakarma', 'rajesh1may@gmail.com', 'rajesh1may@gmail.com', 'e360bc13bcba071f4746adbb334cd78e', '234234324234', 1, '/profileimg.png', '2017-10-12 06:30:32', '0000-00-00 00:00:00', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
