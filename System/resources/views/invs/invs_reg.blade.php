@extends('invs.app')
@section('content')


<section class="flexbox-container" style="align-items: start;">

    <div class="col-lg-4 col-md-3 col-sm-2 col-xs-1 login-back-line">
        <div class="row">
            <p class="line_login_register_forgot">&nbsp;</p>
        </div>
    </div>

    <div class="col-lg-4 col-md-6 col-sm-8 col-xs-10 box-shadow-2 p-0">
        <div class="card border-grey m-0">
            <div class="card-header1 no-border">
                <div class="card-title text-xs-center">
                    <div class=""><img src="{{url('admin')}}/app-assets/images/logo/robust-logo-light-small.png"
                     alt="Homyt"></div>
                 </div>
                 <h6 class="card-subtitle line-on-side text-xs-center pt-2">تقديم طلب استخدام
                 النظام</h6>
             </div>

             <form id="registration_form" enctype="multipart/form-data">
                <div id="accordion" class="user-details collapsed" role="tablist" aria-multiselectable="true">
                    <div class="card">
                        <div class="card-header collapsed" role="tab" id="headingOne" data-toggle="collapse"
                        data-parent="#accordion" href="#collapseOne" aria-expanded="true"
                        aria-controls="collapseOne">
                        <a id="fullname_text"> الإسم الرباعي </a>
                    </div>

                    <div id="collapseOne" class="collapse " role="tabpanel" aria-labelledby="headingOne">



                        <div class="form-group" >
                            <small id="fileHelp" class="form-text text-muted">أدخل إسمك الأول باللغة العربية،
                                كما هو مسجل في بطاقة الهوية الشخصية
                            </small>
                            <input type="text" name="firstname" class="form-control" style="text-align: center;"
                            placeholder="إسمك الشخصي"/>

                            <small id="fileHelp" class="form-text text-muted">أدخل إسم الأب باللغة العربية، كما
                                هو مسجل في بطاقة الهوية الشخصية
                            </small>
                            <input type="text" name="secondname" class="form-control"
                            style="text-align: center;" placeholder="إسم الأب"/>

                            <small id="fileHelp" class="form-text text-muted">أدخل إسم الجد باللغة العربية، كما
                                هو مسجل في بطاقة الهوية الشخصية
                            </small>
                            <input type="text" name="thirdname" class="form-control" style="text-align: center;"
                            placeholder="إسم الجد"/>

                            <small id="fileHelp" class="form-text text-muted">أدخل إسم العائلة باللغة العربية،
                                كما هو مسجل في بطاقة الهوية الشخصية
                            </small>
                            <input type="text" name="fourthname" class="form-control"
                            style="text-align: center;" placeholder="إسم العائلة"/>


                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header  collapsed" role="tab" id="headingseven" data-toggle="collapse"
                    data-parent="#accordion" href="#collapseseven" aria-expanded="false"
                    aria-controls="collapseseven">
                    <a> الصورة الشخصية </a>
                </div>
                <div id="collapseseven" class="collapse" role="tabpanel" aria-labelledby="headingseven">


                    <div id="FileTypePersonalPicture">
                        <div id="TextBoxDiv3" class="form-group">
                            <small id="fileHelp" class="form-text text-muted">يرجى إدخال صورتك الشخصية,
                                ويشترط ان تكون ذات وضوح
                            </small>
                            
                            <div class="img_preview">
                                <input name="profileimg" type="file" accept="image/gif, image/jpeg, image/png">
                            </div>


                        </div>
                    </div>

                </div>
            </div>

            <div class="card">
                <div class="card-header  collapsed" role="tab" id="headingTwo" data-toggle="collapse"
                data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
                aria-controls="collapseTwo">
                <a> بيانات التواصل </a>
            </div>
            <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">


                <div class="form-group">
                    <small id="fileHelp" class="form-text text-muted" style="text-align: center; ">ما هي
                        الدولة التي تحمل وثيقتها الوطنية (بطاقة الهوية)
                    </small>
                    <select class="form-control selectpicker" id="country" name="country">
                        <option value="">-إختر دولتك-</option>

                         @foreach(App\Models\Countries::where('arabic_country','=', 1)->orderBy('order', 'desc')->get() as  $countray)
                    <option value="{{ $countray->id}}" data-phonenum="{{ $countray->countryCode}}">{{ $countray->name_ar}}</option>
                   
                    @endforeach
                   
                </select>
            </div>
            <div class="form-group">
                <small id="fileHelp" class="form-text text-muted">
                يرجى إدخال رقم هاتفك المحمول لنتمكن من التواصل معكم عند الحاجة, ادخل رقمك المحلي
            المكون من عشرة خانات, وابتدئ من الرقم 0, مثلاً 0123456789</small>
         <input class="form-control" style="text-align: center;" name="phone" title="" type="text" value="" placeholder="رقم هاتفك "/>
        </div>
        <div class="form-group"> <!-- Date input -->
            <small id="fileHelp" class="form-text text-muted">يرجى إدخال تاريخ ميلادك كما في
                وثيقتك الوطنية
            </small>
            <input class="form-control select_date" name="birthday" style="text-align: center;"
            placeholder="YYYY-MM-DD" value="" type="text"/>
        </div>
        <div class="form-group">
            <small id="fileHelp" class="form-text text-muted">يجب أن يكون بريدك الاكتروني
                فعالاً, بريدك الإلكتروني يعتبر مهماً في حال فقدت بياناتك, ونقوم بالرد على طلبك
                من خلاله, يفضل أن يكون بريد تابع لـ Gmail
            </small>
            <input type="email" class="form-control" id="exampleInputEmail1"
            aria-describedby="emailHelp" style="text-align: center;"
            placeholder="بريدك الإلكتروني (الإيميل)" name="email"/>
        </div>


    </div>
</div>

<div class="card">
    <div class="card-header  collapsed" role="tab" id="headingThree" data-toggle="collapse"
    data-parent="#accordion" href="#collapseThree" aria-expanded="false"
    aria-controls="collapseThree">
    <a> بيانات الحماية </a>
</div>
<div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
    <div class="form-group">
        <small id="fileHelp" class="form-text text-muted">رمز المرور السري الخاص بك هو ما
            يجعلنا نتأكد منك عند محاولتك الدخول لنظام الاستثمار الشخصي الخاص بك, يرجى ادخال
            رمز مرور يسهل عليك تذكره ويصعب توقعه من الآخرين, لانك تتحمل مسؤولية ما يحصل داخل
            نظامك
        </small>
        <input type="password" name="password" class="form-control" id="password1" style="text-align: center;" placeholder="يرجى إدخال رمز المرور السري">
    </div>
    <div class="form-group">
        <small id="fileHelp" class="form-text text-muted">تكرارك لكتابة رمز مرورك السري ليس
            لإضاعة وقتك, إنما لكي تتأكد أنك قمت بإدخاله بطريقة صحيحة كما يجب
        </small>
        <input type="password" name="password_confirmation" class="form-control"
        id="confirm_password" style="text-align: center;"
        placeholder="يرجى إعادة إدخال رمز المرور السري">
    </div>

</div>
</div>

<div class="card">
    <div class="card-header  collapsed" role="tab" id="headingfour" data-toggle="collapse"
    data-parent="#accordion" href="#collapsefour1" aria-expanded="false"
    aria-controls="collapsefour1">
    <a> بيانات الدفع  </a>
</div>

<div id="collapsefour1" class="collapse repeater" role="tabpanel"
aria-labelledby="headingfour">
<small id="fileHelp" class="form-text text-muted">  قم برفع صور بيانات الدفع الخاصة بك (سند قبض، وصل/فيشة دفع/تحويل بنكي، صورة لورقة عملية التحويل لخدمات التحويل المالي أو الالكتروني، ويسترن يونيون، موني جرام..الخ . وذلك لنتأكد من من صحة عملية التحويل وارفاقها في ملفك </small>
<div data-repeater-list="payment_details" >
    <div data-repeater-item class="payment_details">
        <button data-repeater-delete type="button" class="btn btn-danger">الغاء</button>

        <!-- innner repeater -->
        <div class="inner-repeater">
            <div data-repeater-list="inner-list">
                <div data-repeater-item>
                    <div class="form-group row">
                        <div class="col-md-12">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                 <img src="{{url('')}}/image/pament.jpg" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 150px;"> </div>
                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new btn btn-info"> صورة بيانات الدفع </span>
                                    <span class="fileinput-exists btn btn-info"> تغيير الصورة </span>
                                    <input type="file" name="payment_image" accept="image/*"> </span>
                                <a href="javascript:;" class="fileinput-exists btn btn-danger" style="color:#FFFFFF;" data-dismiss="fileinput" > حذف </a>
                            </div>
                        </div>
                        </div>
                       
                    </div>
                </div>
                <hr>
            </div>
        </div>

    </div>
</div>

<div class="form-group">
    <input data-repeater-create type="button" value="أضف صوراً أخرى"
    class="btn btn-primary" id="addButton"/>
</div>

<div class="form-check " style="margin-right: 35px;">
    <small id="fileHelp" class="form-text text-muted">إذا لم تكن تمتلك نسخة عن بيانات الدفع الخاصة باستثمارك, يرجى اختيار المربع الصغير أدناه
    </small>
</br>
<input type="checkbox" class="form-check-input" value="1" name="no_payment_image"/>
لا أمتلك بيانات الدفع الخاصة باستثماري
</div>

</div>
</div>
<div class="card">
    <div class="card-header  collapsed" role="tab" id="headingfour" data-toggle="collapse"
    data-parent="#accordion" href="#collapsefour" aria-expanded="false"
    aria-controls="collapsefour">
    <a> بيانات عقود الاستثمار </a>
</div>

<div id="collapsefour" class="collapse repeater" role="tabpanel"
aria-labelledby="headingfour">
<small id="fileHelp" class="form-text text-muted">قم برفع صور عقودك الخاصة, وذلك
            لنتأكد من هويتك ومن صحة البيانات المقدمة
        </small>

<div id="FileTypeGroup">
    <div id="TextBoxDiv1" class="form-group">
     
        <div data-repeater-list="contract_details">
            <div data-repeater-item>
                <button data-repeater-delete type="button" class="btn btn-danger">الغاء</button>

                <!-- innner repeater -->
                <div class="inner-repeater">
                    <div data-repeater-list="inner-list">
                        <div data-repeater-item class="contracts_details">

                         <div class="form-group row">
                        <div class="col-md-12">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="{{url('')}}/image/contract.jpg" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 150px;"> </div>
                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new btn btn-info"> صورة عقد الاستثمار </span>
                                    <span class="fileinput-exists btn btn-info"> تغيير الصورة </span>
                                    <input type="file" name="contract_image" accept="image/*"> </span>
                                <a href="javascript:;" class="fileinput-exists btn btn-danger" style="color:#FFFFFF;" data-dismiss="fileinput" > حذف </a>
                            </div>
                        </div>
                        </div>
                       
                    </div>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>
</div>
<div class="form-group">

    <input data-repeater-create type="button" value="أضف صوراً أخرى"
    class="btn btn-primary" id="addButton1">
</div>

<div class="form-check" style="margin-right: 35px;">
    <small id="fileHelp" class="form-text text-muted">إذا لم تكن تمتلك عقد إستثمار خاص
        بك حتى الآن, يرجى اختيار المربع الصغير أدناه حتى نقوم بإجراءات إيصال عقدك الخاص
    </small>
</br>
<input type="checkbox" class="form-check-input" value="1" name="no_contract_image"/>
لا أمتلك عقد لاستثماري حتى الآن
</div>

</div>
</div>

<div class="card">
    <div class="card-header  collapsed" role="tab" id="headingfive" data-toggle="collapse"
    data-parent="#accordion" href="#collapsefive" aria-expanded="false"
    aria-controls="collapsefive">
    <a> بيانات الإقامة </a>
</div>
<div id="collapsefive" class="collapse" role="tabpanel" aria-labelledby="headingfive">
    <div class="form-group">
        <small id="fileHelp" class="form-text text-muted">في بعض الحالات, نحتاج عنوانك
            الشخصي لإيصال مستندات عبر البريد أو بعض العقود أو الهدايا أو غيره بحسب الحاجة,
            فيرجى توضيح عنوانك بدقة
        </small>
        <input type="text" name="address" class="form-control"
        placeholder="الشارع, عنوان المنزل تحديداً"/>

        <small id="fileHelp" class="form-text text-muted">المدينة التي تسكن فيها</small>
        <input type="text" id="city" name="city" class="form-control"
        placeholder="المدينة"/>

        <small id="fileHelp" class="form-text text-muted">على سبيل المثال, في دولة فلسطين قم
            بتحديد الضفة الغربية أو غزة, لكن إن لم تكن دولتك تدعم نظام الولايات او التقسيمات
            الداخلية يمكنك وضع اسم المدينة مرة أخرى
        </small>
        <input type="text" id="state" name="state" class="form-control"
        placeholder="الولاية"/>

        <small id="fileHelp" class="form-text text-muted"> لقد قمت بتحديد دولتك مسبقاً,
            وبناءاً عليه تم اعتمادها هنا
        </small>
        <!-- <input type="text" class="form-control"
        placeholder="الدولة - لا عليك, لقد قمت بالفعل بتحديدها مسبقاً"
        id="country_selected"/>
 -->
          <select class="form-control selectpicker" id="country_selected" name="country_live">
                        <option value="">-إختر دولتك-</option>


                  @foreach(App\Models\Countries::orderBy('order', 'desc')->get() as  $countray)
                    <option value="{{ $countray->id}}" data-phonenum="{{ $countray->countryCode}}">{{ $countray->name_ar}}</option>
                   
                    @endforeach


                </select>



    </div>
</div>
</div>

<div class="card">
    <div class="card-header  collapsed" role="tab" id="headingsix" data-toggle="collapse"
    data-parent="#accordion" href="#collapsesix" aria-expanded="false"
    aria-controls="collapsesix">
    <a> الوثائق القانونية </a>
</div>
<div id="collapsesix" class="collapse repeater" role="tabpanel"
aria-labelledby="headingsix">
<div data-repeater-list="identity_details">
    <div data-repeater-item>
        <button data-repeater-delete type="button" class="btn btn-danger">الغاء</button>

        <!-- innner repeater -->
        <div class="inner-repeater">
            <div data-repeater-list="inner-list">
                <div data-repeater-item  class="identity_details">
                    <div class="form-group">
                        <small id="fileHelp" class="form-text text-muted">يرجى إختيار
                            أدناه نوع الوثيقة التي ترغب بتأكيدها لهوميت, وثيقك الشخصية
                            هي ما تؤهلك لاستلام أرباحك أو ما تم إستخدامها عند إجراءات
                            استثمارك الشخصي, يمكنك الاختيار بين بطاقة هوية شخصية أو جواز
                            سفر, الوثيقة المدخلة ستكون معتدة بشكل دائم في هوميت
                        </small>
                        <select class="form-control" name="id_type">
                            <option value="">نوع الوثيقة</option>
                            <option value="Personal Id">بطاقة هوية شخصية</option>
                            <option value="Passport">جواز سفر</option>
                        </select>

                        <small id="fileHelp" class="form-text text-muted">كافة الوثائق
                            القانونية الخشصية تحمل رقماً خاصاً, يرجى إدخال رقم وثيقتك.
                            يرجى العلم أن رقم الوثيقة الوطنية هو معرف دخولك لنظام هوميت
                        </small>
                        <input type="text" name="id_number" class="form-control"
                        style="text-align: center; "
                        placeholder="رقم الوثقية الشخصية الوطنية"/>


                    </div>
                    <div>
                        <div id="TextBoxDiv2" class="form-group row">
                            <small id="fileHelp" class="form-text text-muted">قم بإرفاق
                                صور/ة واضحة لوثيقتك الوطنية الشخصية
                            </small>

                              <div class="col-md-12">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="{{url('')}}/image/idntety.jpg" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 150px;"> </div>
                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new btn btn-info">صورة الوثيقة </span>
                                    <span class="fileinput-exists btn btn-info"> تغيير الصورة </span>
                                    <input type="file" name="id_image_path"  accept="image/*"> </span>
                                <a href="javascript:;" class="fileinput-exists btn btn-danger" style="color:#FFFFFF;" data-dismiss="fileinput" > حذف </a>
                            </div>
                        </div>
                        </div>

                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>

</div>

<div class="form-group">

    <input data-repeater-create type="button" value="إضافة المزيد (ليس ضروري - اختياري)"
    class="btn btn-primary" id="addButton2"/>
</div>

</div>
</div>


<div class="card">
    <div class="card-header collapsed" role="tab" id="headingeight" data-toggle="collapse"
    data-parent="#accordion" href="#collapseeight" aria-expanded="false"
    aria-controls="collapseeight">
    <a> بيانات الحصص الاستثمارية</a>
</div>
<div id="collapseeight" class="collapse repeater" role="tabpanel"
aria-labelledby="headingeight">
<div data-repeater-list="share_amount_details">
    <div data-repeater-item>
        <button data-repeater-delete type="button" class="btn btn-danger">الغاء</button>

        <!-- innner repeater -->
        <div class="inner-repeater">
            <div data-repeater-list="inner-list">
                <div data-repeater-item class="share_amount_details">
                    <div class="form-group">
                        <small id="fileHelp" class="form-text text-muted">في كل مرة قمت
                            بالاستثمار فيها, يرجى ادخال بياناتك أدناه, يرجى العلم أنك قد
                            تكون قمت باستثمار معين, واستثمار آخر في وقت لاحق, في هذه
                            الحالة يرجى ادخال كل استثمار على حدى عبر النقر على زر اضافة
                            المزيد
                        </small>
                        <input type="text" name="share_amount" class="form-control"
                        style="text-align: center;"
                        placeholder="المبلغ الاجمالي بالدولار الاستثماري للحصة في المرة الواحدة"/>
                        <small id="fileHelp" class="form-text text-muted">تذكر أن تقوم
                            بإدخال بياناتك للمرة الواحدة وليست اجمالاً
                        </small>
                        <input type="text" name="share_number" class="form-control"
                        style="text-align: center;"
                        placeholder="عدد الحصص الاستثماري في المرة الواحدة"/>
                        <small id="fileHelp" class="form-text text-muted">ما هو التاريخ
                            الذي أتممت فيه إستثمارك للمرة الواحدة؟
                        </small>
                        <input type="text" name="share_date"
                        class="form-control select_date" style="text-align: center;"
                        placeholder="تاريخ استثمارك"/>
                    </div>
                
                </div>
            </div>
            <hr>
        </div>
    </div>
</div>
</br>
<div class="form-group">
    <input data-repeater-create type="button"
    value="إضافة المزيد من الحصص (لذوي الاستثمار المتعدد)"
    class="btn btn-primary" id="addButton4"/>
</div>
</div>
</div>

<br>

<div class="form-group" style="text-align: center; margin-top: 30px;">

    <input type="submit" value="تقديم طلب بيانات استخدام النظام" class="btn btn-info">

</div>


</div>


</form>

<div class="card-footer">

    <div class="row">

      <div class="col-md-4">
        <a href="{{route('login')}}" class="btn btn-green btn-block">تسجيل الدخول</a>
    </div>

    <div class="col-md-4">
        <a href="{{route('check')}}" class="btn btn-warning btn-block">تفقد حالة طلبك</a>
    </div>

    <div class="col-md-4">
        <a href="{{route('forget')}}" class="btn btn-danger btn-block">نسيت بياناتك؟</a>
    </div>

</div>


</div>

</div>
</div>

<div class="col-lg-4 col-md-3 col-sm-2 col-xs-1 login-back-line">
    <div class="row">
        <p class="line_login_register_forgot">&nbsp;</p>
    </div>
</div>

</section>

   <div style="width: 100%; bottom: 0px;">

                    <footer class="footer footer-static footer-light navbar-border">
    <p class="clearfix text-muted text-sm-center mb-0 px-2" style="color: #FFFFFF !important;">
        <span class="float-md- d-xs-block d-md-inline-block">جميع الحقوق محفوظة لـِ  <a href="https://www.homyt.com" target="_blank" class="text-bold-800 grey darken-2" style="color: #FFFFFF !important;">شركة هوميت لتكنولوجيا المعلومات م.خ.م</a> &copy; 2014-2018</span>
        
    </p>
</footer>
                   
                </div>

@endsection

@section('script')

<script src="{{url('')}}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>

<script src="{{url('')}}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script type="text/javascript" src="{{url('')}}/assets/global/plugins/uploadPreview/jquery.upload_preview.js"></script>
<script src="{{url('')}}/assets/global/plugins/jquery-repeater/jquery.repeater.min.js"></script>
<script src="{{url('admin')}}/js/invs_register.js" type="text/javascript"></script>


@endsection
