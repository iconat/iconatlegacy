@extends('invs.app')

@section('content')
    <style type="text/css">
        html { overflow-y: hidden; }

    </style>
    <section class="flexbox-container" style="align-items: start; margin: 52px 0 !important;">

        <div class="col-md-4 col-xs-1 login-back-line">
            <div class="row">
                <p class="line_login_register_forgot">&nbsp;</p>
            </div>
        </div>

        <div class="col-md-4 col-xs-10   box-shadow-2 p-0"><!--  offset-md-4 offset-xs-1 --><!-- <div class="row"> -->
            <div class="card border-grey m-0" style="padding-bottom: 15px;padding-right: 5px;padding-left: 5px;"><!-- border-lighten-3 -->

                @php echo setting('terms_and_conditions' , ""); @endphp




            </div>

            <div class="card-footer" style="text-align: center;">
                <div class="row">
                    <div class="col-md-7">
                        <a href="{{route('My')}}" class="btn btn-green btn-block">موافق، المتابعة للنظام</a>
                    </div>

                    <div class="col-md-5">
                        <a href="{{ route('logout') }}" class="btn btn-danger btn-block">تسجيل الخروج</a>
                    </div>

                </div>

            </div>

            <!-- </div> -->
        </div>

        <div class="col-md-4 col-xs-1 login-back-line">
            <div class="row">
                <p class="line_login_register_forgot">&nbsp;</p>
            </div>
        </div>

    </section>
@endsection