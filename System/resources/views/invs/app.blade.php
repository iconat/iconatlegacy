<!DOCTYPE html>
<html lang="ar" dir="rtl" data-textdirection="rtl" class="loading">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title>هوميت</title>
        <link rel="apple-touch-icon" sizes="60x60" href="{{url('admin')}}/app-assets/images/ico/apple-icon-60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{{url('admin')}}/app-assets/images/ico/apple-icon-76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{{url('admin')}}/app-assets/images/ico/apple-icon-120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{{url('admin')}}/app-assets/images/ico/apple-icon-152.png">
    <link rel="shortcut icon" type="image/x-icon" href="{{url('admin')}}/app-assets/images/ico/favicon.ico">
    <link rel="shortcut icon" type="image/png" href="{{url('admin')}}/app-assets/images/ico/favicon-32.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{url('admin')}}/app-assets/css-rtl/bootstrap.css">
  
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="{{url('admin')}}/app-assets/fonts/icomoon.css">
    <link rel="stylesheet" type="text/css" href="{{url('admin')}}/app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('admin')}}/app-assets/vendors/css/extensions/pace.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN ROBUST CSS-->
    <link rel="stylesheet" type="text/css" href="{{url('admin')}}/app-assets/css-rtl/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="{{url('admin')}}/app-assets/css-rtl/app.css">
    <link rel="stylesheet" type="text/css" href="{{url('admin')}}/app-assets/css-rtl/colors.css">
    <link rel="stylesheet" type="text/css" href="{{url('admin')}}/app-assets/css-rtl/custom-rtl.css">
    <!-- END ROBUST CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{url('admin')}}/app-assets/css-rtl/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="{{url('admin')}}/app-assets/css-rtl/core/menu/menu-types/vertical-overlay-menu.css">
    <link rel="stylesheet" type="text/css" href="{{url('admin')}}/app-assets/css-rtl/pages/login-register.css">
    <!-- END Page Level CSS-->


<link href="{{url('')}}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />

<link href="{{url('')}}/assets/global/plugins/loading/circle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{url('')}}/assets/global/plugins/uploadPreview/upload_preview.css">


<link href="{{url('')}}/assets/global/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />

<link href="{{url('')}}/assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />

     



    
    <style>
    @media (max-width:992px)  {
        html { overflow-y: hidden; }
}
  

        body  {
 

            background: url("{{url('admin')}}/app-assets/images/blur/2.jpg") no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
        }
        .user-details { margin: 50px 10px; }
        .user-details .card { text-align: center; padding-top: 0px; margin-bottom: 0.7em; }
        .user-details .card a {text-align: center; color: #3e3e3e; }
        .user-details .card a:hover { color: #262626; }
        .user-details .form-group { margin: 1em; }
        .user-details .card-header { border-bottom: 0; padding: .5rem 1.5rem; cursor: pointer;}
        .user-details .form-control { margin: .5em 0 1em; cursor: pointer; }
        .user-details .card a.collapsed{
            padding-top: 10px;
            padding-bottom: 10px;
            padding-right: 100px;
            padding-left: 100px;
        }
        .user-details .form-control-file { margin-bottom    : 15px; }
        .user-details #removeButton { border: 0; background: none; }
        .user-details #FileTypeGroup span, .user-details #FileTypeIdData span, .user-details #FileTypePersonalPicture span, .user-details #FileTypeSharedData span { text-align: right;}
        
    </style>

         <link href="{{url('')}}/assets/global/plugins/jquery-confirm/jquery-confirm.min.css" rel="stylesheet" type="text/css" />

</head>
<body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column  blank-page blank-page">
    
    <div class="app-content content container-fluid">
        <div class="content-wrapper">
           
            <div class="content-body">









            @yield('content')














@if(url('/invs_reg') == URL::to('/').'/'.Request::segment(1)) 




@else



 
                <div style="position: fixed; width: 100%; bottom: 0px;">

                    <footer class="footer footer-static footer-light navbar-border">
    <p class="clearfix text-muted text-sm-center mb-0 px-2" style="color: #FFFFFF !important;">
        <span class="float-md- d-xs-block d-md-inline-block">جميع الحقوق محفوظة لـِ  <a href="https://www.homyt.com" target="_blank" class="text-bold-800 grey darken-2" style="color: #FFFFFF !important;">شركة هوميت لتكنولوجيا المعلومات م.خ.م</a> &copy; 2014-2018</span>
        
    </p>
</footer>
                   
                </div>


@endif
            </div>
        </div>
    </div>


    <!-- BEGIN VENDOR JS-->
    

    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN ROBUST JS-->
        <script src="{{url('admin')}}/app-assets/js/core/libraries/jquery.min.js" type="text/javascript"></script>
    <script src="{{url('')}}/assets/js/lang.dist.js" type="text/javascript"></script>


    <script>
        $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>

    <script type="text/javascript">
var APP_URL = {!! json_encode(url('/')) !!}
</script>
   
    <script src="{{url('')}}/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="{{url('')}}/assets/global/plugins/jquery-validation/js/localization/messages_ar.min.js" type="text/javascript"></script>
    <link href="{{url('')}}/assets/global/plugins/bootstrap-toastr/toastr.min.js" rel="stylesheet" type="text/css" />
  
    <script src="{{url('')}}/assets/global/plugins/jquery-confirm/jquery-confirm.min.js" type="text/javascript"></script>



    <script src="{{url('')}}/assets/global/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="{{url('')}}/assets/global/plugins/jquery-ui/i18n/datepicker-ar.js"></script>

    <script src="{{url('admin')}}/app-assets/vendors/js/ui/tether.min.js" type="text/javascript"></script>
    <script src="{{url('admin')}}/app-assets/js/core/libraries/bootstrap.min.js" type="text/javascript"></script>


    @yield('script')


    <!-- END ROBUST JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
</body>
</html>

