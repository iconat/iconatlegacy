<!DOCTYPE html>
<html lang="{{App::getLocale()}}" dir="{{ $currentLanguage->dir }}">
   <head>
      <meta charset="utf-8">
      <title>@isset($title) {{ $title }} | @endisset {{setting('website_title_'.App::getLocale() , "")}}</title>
      <meta name="keywords" content="Iconat">
      <meta name="description" content="Iconat">
      <meta name="author" content="Iconat">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <!-- Favicon -->
      <link rel="shortcut icon" href="{{url('')}}/favicon.ico">
      <!-- Font Icon -->
      @if(\App::getLocale() == "ar")
      <link href="{{url('')}}/assets/css/droid-arabic-kufi/font.css" rel="stylesheet" type="text/css">
      @endif
      <link rel="stylesheet" href="{{url('')}}/assets/site/css/theme.css">
      <!-- CSS Global -->
      @if($currentLanguage->dir == "rtl")
      <link rel="stylesheet" href="{{url('')}}/assets/site/css/rtl.css">
      @endif
      <link rel="stylesheet" href="{{url('')}}/assets/global/plugins/jquery-confirm/jquery-confirm.min.css">
      <link rel="stylesheet" href="{{url('')}}/assets/site/a/settings.css">
      <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <style>
         #main-banner{
         direction: ltr;
         }
      </style>
      @section('css')
      @show
   </head>
   <body>
      <div id="loader-wrapper">
         <div id="loader">
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
         </div>
      </div>
      <header>
         <div class="tt-color-scheme-01">
            <div class="container">
               <div class="tt-header-row tt-top-row">
                  <div class="tt-col-left">
                     <div class="tt-box-info">
                        <ul>
                           @if(!empty(Auth::user()))
                           <li><i class="icon-f-94"></i> &nbsp;&nbsp; {{trans('general.logged_in_by')}} :- {{Auth::user()->name}}</li>
                           @endif
                        </ul>
                     </div>
                  </div>
                  <div class="tt-col-right ml-auto">
                     <ul class="tt-social-icon">
                        <li>
                           <a class="icon-g-64" target="_blank" href="{{setting('facebook' , "")}}"></a>
                        </li>
                        <li>
                           <a class="icon-h-58" target="_blank" href="{{setting('twitter' , "")}}"></a>
                        </li>
                        <li>
                           <a class="icon-g-66" target="_blank" href="{{setting('google-plus' , "")}}"></a>
                        </li>
                        <li>
                           <a class="icon-g-67" target="_blank" href="{{setting('instagram' , "")}}"></a>
                        </li>
                        <li>
                           <a class="icon-g-77" target="_blank" href="{{setting('snapchat' , "")}}"></a>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <!-- tt-mobile menu -->
         <nav class="panel-menu mobile-main-menu">
            <ul>
               <li  class="@if(url('Home') == URL::to('/').'/'.Request::segment(2) ) active @endif"><a href="{{url('')}}">{{trans('general.home')}}</a></li>
               <li class="@if(url('About') == URL::to('/').'/'.Request::segment(2) ) active @endif"><a href="{{url('/About')}}">{{trans('iconat.about_us')}}</a></li>
               <li class="@if(url('Cites') == URL::to('/').'/'.Request::segment(2) ) active @endif"><a href="{{url('/Cites')}}">{{trans('cites.main_cites')}}</a></li>
               <li class="@if(url('AllCategoreis') == URL::to('/').'/'.Request::segment(2) ) active @endif"><a href="{{url('/AllCategoreis')}}">{{trans('categories.categories')}}</a></li>
               @if(!empty(Auth::user()) && Auth::user()->hasRole(['dealer']))
               <li class="@if(url('Account') == URL::to('/').'/'.Request::segment(2) ) active @endif"><a href="{{url("/Account")}}">{{trans('general.account')}}</a></li>
               <li class="@if(url('ShopEdit') == URL::to('/').'/'.Request::segment(2) ) active @endif"><a href="{{url("/ShopEdit")}}">{{trans('general.shop')}}</a></li>
               @endif
               <li class="@if(url('Contact') == URL::to('/').'/'.Request::segment(2) ) active @endif"><a href="{{url("/Contact")}}">{{trans('iconat.contact')}}</a></li>
            </ul>
            <div class="mm-navbtn-names">
               <div class="mm-closebtn">Close</div>
               <div class="mm-backbtn">Back</div>
            </div>
         </nav>
         <!-- tt-mobile-header -->
         <div class="tt-mobile-header">
            <div class="container-fluid">
               <div class="tt-header-row">
                  <div class="tt-mobile-parent-menu">
                     <div class="tt-menu-toggle">
                        <i class="icon-03"></i>
                     </div>
                  </div>
                  <!-- search -->
                  <div class="tt-mobile-parent-search tt-parent-box"></div>
                  <!-- /search -->
                  <!-- cart -->
                  <div class="tt-mobile-parent-cart tt-parent-box"></div>
                  <!-- /cart -->
                  <!-- account -->
                  <div class="tt-mobile-parent-account tt-parent-box"></div>
                  <!-- /account -->
                  <!-- currency -->
                  <div class="tt-mobile-parent-multi tt-parent-box"></div>
                  <!-- /currency -->
               </div>
            </div>
            <div class="container-fluid tt-top-line">
               <div class="row">
                  <div class="tt-logo-container">
                     <!-- mobile logo -->
                     @php
                     $Dark_Logo = \App\Models\SettingsMediable::where('key','website_logo_'.$currentLanguage->locale)->first();
                     $Img_Dark_Logo = NULL;
                     $S = $Dark_Logo->lastMedia('website_logo_'.$currentLanguage->locale."_thumb");
                     if($Dark_Logo && $S){
                     $Img_Dark_Logo = $S->getUrl();
                     }
                     @endphp
                     <a class="tt-logo tt-logo-alignment" href="{{url('')}}"><img src="{{$Img_Dark_Logo ?? url(setting('default_image_logo' , ""))}}" alt="{{setting('website_title_'.$currentLanguage->locale , "")}}"></a>
                     <!-- /mobile logo -->
                  </div>
               </div>
            </div>
         </div>
         <!-- tt-desktop-header -->
         <div class="tt-desktop-header  headerunderline">
            <div class="container">
               <div class="tt-header-holder">
                  <div class="tt-col-obj tt-obj-logo">
                     <!-- logo -->
                     <a class="tt-logo tt-logo-alignment" href="{{url('')}}"><img src="{{$Img_Dark_Logo ?? url(setting('default_image_logo' , ""))}}" alt="{{setting('website_title_'.$currentLanguage->locale , "")}}"></a>
                     <!-- /logo -->
                  </div>
                  <div class="tt-col-obj tt-obj-menu">
                     <!-- tt-menu -->
                     <div class="tt-desctop-parent-menu tt-parent-box">
                        <div class="tt-desctop-menu">
                           <nav>
                              <ul>
                                 <li class="dropdown @if(url('Home') == URL::to('/').'/'.Request::segment(2) ) active @endif"><a href="{{url('')}}">{{trans('general.home')}}</a></li>
                                 <li class="dropdown @if(url('About') == URL::to('/').'/'.Request::segment(2) ) active @endif "><a href="{{url("/About")}}">{{trans('iconat.about_us')}}</a></li>
                                 <li class="dropdown @if(url('Cites') == URL::to('/').'/'.Request::segment(2) ) active @endif "><a href="{{url('/Cites')}}">{{trans('cites.main_cites')}}</a></li>
                                 <li class="dropdown @if(url('AllCategoreis') == URL::to('/').'/'.Request::segment(2) ) active @endif "><a href="{{url('/AllCategoreis')}}">{{trans('categories.categories')}}</a></li>
                                 @if(!empty(Auth::user()) && Auth::user()->hasRole(['dealer']))
                                 <li class="dropdown @if(url('Account') == URL::to('/').'/'.Request::segment(2) ) active @endif "><a href="{{url("/Account")}}">{{trans('general.account')}}</a></li>
                                 <li class="dropdown @if(url('ShopEdit') == URL::to('/').'/'.Request::segment(2) ) active @endif "><a href="{{url("/ShopEdit")}}">{{trans('general.shop')}}</a></li>
                                 @endif
                                 <li class="dropdown @if(url('Contact') == URL::to('/').'/'.Request::segment(2) ) active @endif "><a href="{{url("/Contact")}}">{{trans('iconat.contact')}}</a></li>
                              </ul>
                           </nav>
                        </div>
                     </div>
                     <!-- /tt-menu -->
                  </div>
                  <div class="tt-col-obj tt-obj-options obj-move-right">
                     <!-- tt-search -->
                     <div class="tt-desctop-parent-search tt-parent-box">
                        <div class="tt-search tt-dropdown-obj">
                           <button class="tt-dropdown-toggle" data-tooltip="Search" data-tposition="bottom">
                           <i class="icon-f-85"></i>
                           </button>
                           <div class="tt-dropdown-menu">
                              <div class="container">
                                 <form>
                                    <div class="tt-col">
                                       <input type="text" class="tt-search-input" id="search" name="search" placeholder="{{trans('general.search_here')}}">
                                       {{-- <button class="tt-btn-search" type="submit"></button> --}}
                                    </div>
                                    <div class="tt-col">
                                       <button class="tt-btn-close icon-g-80"></button>
                                    </div>
                                    <div class="tt-info-text">
                                       {{trans('general.search_here2')}}
                                    </div>
                                    <div class="search-results">
                                       <ul id="me">
                                          {{--
                                          <li>
                                             <a href="#">
                                                <div class="thumbnail"><img src="{{url('')}}/assets/site/images/loader.svg" data-src="{{url('')}}/assets/site/images/product/product-03.jpg" alt=""></div>
                                                <div class="tt-description">
                                                   <div class="tt-title">Title Here</div>
                                                </div>
                                             </a>
                                          </li>
                                          --}}
                                       </ul>
                                       {{-- <button type="button" class="tt-view-all">View all</button> --}}
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- /tt-search -->
                     <!-- tt-account -->
                     <div class="tt-desctop-parent-account tt-parent-box">
                        <div class="tt-account tt-dropdown-obj">
                           <button class="tt-dropdown-toggle" data-tooltip="{{trans('general.login')}} / {{trans('general.register')}}" data-tposition="bottom"><i class="icon-f-94"></i></button>
                           <div class="tt-dropdown-menu">
                              <div class="tt-mobile-add">
                                 <button class="tt-close">Close</button>
                              </div>
                              <div class="tt-dropdown-inner">
                                 <ul>
                                    @guest
                                    <li><a href="{{url("/login")}}"><i class="icon-f-76"></i>{{trans('general.login')}}</a></li>
                                    <li><a href="{{url("/register")}}" ><i class="icon-f-94"></i>{{trans('general.register')}}</a></li>
                                    @endguest
                                    @auth
                                    @if(!empty(Auth::user()) && Auth::user()->hasRole(['admin']))
                                    <li><a href="{{url("/My")}}"><i class="icon-f-61"></i>{{trans('general.admin_panel')}}</a></li>
                                    @endif
                                    <li><a href="{{url("/logout")}}"><i class="icon-f-77"></i>{{trans('general.logout')}}</a></li>
                                    @endauth
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- /tt-account -->
                     <!-- tt-langue and tt-currency -->
                     <div class="tt-desctop-parent-multi tt-parent-box">
                        <div class="tt-multi-obj tt-dropdown-obj">
                           <button class="tt-dropdown-toggle" data-tooltip="{{ $currentLanguage->name }}" data-tposition="bottom"><i class="icon-f-79"></i></button>
                           <div class="tt-dropdown-menu">
                              <div class="tt-mobile-add">
                                 <button class="tt-close">Close</button>
                              </div>
                              <div class="tt-dropdown-inner">
                                 <ul>
                                    @foreach ($altLocalizedUrls as $alt)
                                    <li class="@if($currentLanguage->name ==  $alt['name'] ) active @endif "><a href="{{ $alt['url'] }}" hreflang="{{ $alt['locale'] }}">{{ $alt['name'] }}</a></li>
                                    @endforeach
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- /tt-langue and tt-currency -->
                  </div>
               </div>
            </div>
         </div>
         <!-- stuck nav -->
         <div class="tt-stuck-nav">
            <div class="container">
               <div class="tt-header-row ">
                  <div class="tt-stuck-parent-menu"></div>
                  <div class="tt-stuck-parent-search tt-parent-box"></div>
                  <div class="tt-stuck-parent-cart tt-parent-box"></div>
                  <div class="tt-stuck-parent-account tt-parent-box"></div>
                  <div class="tt-stuck-parent-multi tt-parent-box"></div>
               </div>
            </div>
         </div>
      </header>
      @section('content')
      @show
      <footer class="">
         <div class="tt-footer-default tt-color-scheme-02">
            <div class="container">
               <div class="row">
                  <div class="col-12 col-md-9">
                     <div class="tt-newsletter-layout-01">
                        <div class="tt-newsletter">
                           <div class="tt-mobile-collapse">
                              <h4 class="tt-collapse-title">
                                 {{trans('general.be_in_touch_with_us')}}
                              </h4>
                              <div class="tt-collapse-content">
                                 <form id="newsletterform" class="form-inline form-default" >
                                    <div class="form-group">
                                       <input type="text" name="email" class="form-control" placeholder="{{trans('general.enter_your_e-mail')}}">
                                       <button type="submit" class="btn">{{trans('general.enter_your_e-mail')}}</button>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-auto">
                     <ul class="tt-social-icon">
                        <li>
                           <a class="icon-g-64" target="_blank" href="{{setting('facebook' , "")}}"></a>
                        </li>
                        <li>
                           <a class="icon-h-58" target="_blank" href="{{setting('twitter' , "")}}"></a>
                        </li>
                        <li>
                           <a class="icon-g-66" target="_blank" href="{{setting('google-plus' , "")}}"></a>
                        </li>
                        <li>
                           <a class="icon-g-67" target="_blank" href="{{setting('instagram' , "")}}"></a>
                        </li>
                        <li>
                           <a class="icon-g-77" target="_blank" href="{{setting('snapchat' , "")}}"></a>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="tt-footer-col tt-color-scheme-01">
            <div class="container">
               <div class="row">
                  <div class="col-md-6 col-lg-2 col-xl-3">
                     <div class="tt-mobile-collapse">
                        <h4 class="tt-collapse-title">
                           {{setting('website_title_'.App::getLocale() , "")}}
                        </h4>
                        <div class="tt-collapse-content">
                           <ul class="tt-list">
                              <li><a href="{{url("/About")}}">{{trans('iconat.about_us')}}</a></li>
                              <li><a href="{{url('/Cites')}}">{{trans('cites.main_cites')}}</a></li>
                              <li><a href="{{url('/Categoreis')}}">{{trans('categories.categories')}}</a></li>
                              <li><a href="{{url("/Contact")}}">{{trans('iconat.contact')}}</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6 col-lg-2 col-xl-3">
                     <div class="tt-mobile-collapse">
                        <h4 class="tt-collapse-title">
                           {{trans("iconat.users")}}
                        </h4>
                        <div class="tt-collapse-content">
                           <ul class="tt-list">
                              @guest
                              <li><a href="{{url("/login")}}">{{trans('general.login')}}</a></li>
                              <li><a href="{{url("/register")}}">{{trans('general.register')}}</a></li>
                              @endguest
                              @auth
                              <li><a href="{{url("/logout")}}"><i class="icon-f-77"></i>{{trans('general.logout')}}</a></li>
                              @endauth
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6 col-lg-4 col-xl-3">
                     <div class="tt-mobile-collapse">
                        <h4 class="tt-collapse-title">
                           {{trans("iconat.about_iconat")}}
                        </h4>
                        <div class="tt-collapse-content">
                           <p>
                              {{setting('about_iconat_'.App::getLocale() , "")}}
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6 col-lg-4 col-xl-3">
                     <div class="tt-newsletter">
                        <div class="tt-mobile-collapse">
                           <h4 class="tt-collapse-title">
                              {{trans('iconat.contact')}}
                           </h4>
                           <div class="tt-collapse-content">
                              <address>
                                 <p><span>{{trans('general.address')}}:</span> {{setting('address_'.App::getLocale() , "")}}</p>
                                 <p><span>{{trans('general.phone')}}:</span> {{setting('general_phone' , "")}}</p>
                                 <p><span>{{trans('general.email')}}:</span> <a href="mailto:{{setting('general_email' , "")}}">{{setting('general_email' , "")}}</a></p>
                              </address>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="tt-footer-custom">
            <div class="container">
               <div class="tt-row">
                  <div class="tt-col-left">
                     <div class="tt-col-item tt-logo-col">
                        <!-- logo -->
                        <a class="tt-logo tt-logo-alignment" href="{{url('')}}">
                        <img src="{{$Img_Dark_Logo ?? url(setting('default_image_logo' , ""))}}" alt="">
                        </a>
                        <!-- /logo -->
                     </div>
                     <div class="tt-col-item">
                        <!-- copyright -->
                        <div class="tt-box-copyright">
                           &copy; {{date("Y")}} {{trans("general.:name,_all_rights_reserved_",["name" => setting('website_title_'.App::getLocale() , "")])}}
                        </div>
                        <!-- /copyright -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </footer>
      <!-- modalVideoProduct -->
      <div class="modal fade"  id="modalVideoProduct" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
         <div class="modal-dialog modal-video">
            <div class="modal-content ">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
               </div>
               <div class="modal-body">
                  <div class="modal-video-content">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <a href="#" class="tt-back-to-top">{{trans("general.back_to_top")}}</a>
      <script src="{{url('')}}/assets/site/external/jquery/jquery.min.js"></script>
      <script src="{{url('')}}/assets/site/external/bootstrap/js/bootstrap.min.js"></script>
      <script src="{{url('')}}/assets/site/external/elevatezoom/jquery.elevatezoom.js"></script>
      <script src="{{url('')}}/assets/site/external/slick/slick.min.js"></script>
      <script src="{{url('')}}/assets/site/external/perfect-scrollbar/perfect-scrollbar.min.js"></script>
      <script src="{{url('')}}/assets/site/external/panelmenu/panelmenu.js"></script>
      <script src="{{url('')}}/assets/site/external/instafeed/instafeed.min.js"></script>
      <script src="{{url('')}}/assets/site/external/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
      <script src="{{url('')}}/assets/site/external/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
      <script src="{{url('')}}/assets/site/external/countdown/jquery.plugin.min.js"></script>
      <script src="{{url('')}}/assets/site/external/countdown/jquery.countdown.min.js"></script>
      <script src="{{url('')}}/assets/site/external/magnific-popup/jquery.magnific-popup.min.js"></script>
      <script src="{{url('')}}/assets/site/external/lazyLoad/lazyload.min.js"></script>















      <script src="{{url('')}}/assets/site/js/main.js"></script>
















      <!-- form validation and sending to mail -->
      <script src="{{url('')}}/assets/site/external/form/jquery.form.js"></script>
      <script src="{{url('')}}/assets/site/external/form/jquery.validate.min.js"></script>
      <script src="{{url('')}}/assets/site/external/form/jquery.form-init.js"></script>
      <script src="{{url('')}}/assets/js/lang.dist.js" type="text/javascript"></script>
      <script type="text/javascript">
         $.ajaxSetup({
             data: {'lang':$("html").attr("lang")},
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             }
         });
      </script>
      <script type="text/javascript">
         var APP_URL = "{{url('/')}}/{{$currentLanguage->locale }}";
         var APP_URL_Without_Lang = "{{url('/')}}";
      </script>
      <script src="{{url('/')}}/assets/global/plugins/jquery-confirm/jquery-confirm.min.js"></script>
      <script src="{{url('/')}}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
      <script src="{{url('/')}}/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
      <script src="{{url('/')}}/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
      <script src="{{url('')}}/assets/global/plugins/jquery-confirm/jquery-confirm.min.js"></script>
      <script src="{{url('')}}/assets/site/js/subscribe.js"></script>
      <!--Revolution SLider-->
      <script src="{{url('')}}/assets/site/a/revolution/jquery.themepunch.tools.min.js"></script>
      <script src="{{url('')}}/assets/site/a/revolution/jquery.themepunch.revolution.min.js"></script>
      <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
      <script src="{{url('')}}/assets/site/a/revolution/extensions/revolution.extension.actions.min.js"></script>
      <script src="{{url('')}}/assets/site/a/revolution/extensions/revolution.extension.carousel.min.js"></script>
      <script src="{{url('')}}/assets/site/a/revolution/extensions/revolution.extension.kenburn.min.js"></script>
      <script src="{{url('')}}/assets/site/a/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
      <script src="{{url('')}}/assets/site/a/revolution/extensions/revolution.extension.migration.min.js"></script>
      <script src="{{url('')}}/assets/site/a/revolution/extensions/revolution.extension.navigation.min.js"></script>
      <script src="{{url('')}}/assets/site/a/revolution/extensions/revolution.extension.parallax.min.js"></script>
      <script src="{{url('')}}/assets/site/a/revolution/extensions/revolution.extension.slideanims.min.js"></script>
      <script src="{{url('')}}/assets/site/a/revolution/extensions/revolution.extension.video.min.js"></script>
      <script type="text/javascript">
         $('#search').on('keyup',function(){
         $value=$(this).val();
         $.ajax({
         type : 'get',
         url: APP_URL + "/search/",
         data:{'search':$value},
         success:function(data){
         $('#me').html(data);
         }
         });

         })
      </script>
      <script type="text/javascript">



         $("#main-banner").show().revolution({
            sliderType: "standard",
            sliderLayout: "auto",
            scrollbarDrag: "true",
            dottedOverlay: "none",
            delay:{{setting('duration' , "10")}}000,
            minHeight: '450',
            navigation: {
               keyboardNavigation: "off",
               keyboard_direction: "horizontal",
               mouseScrollNavigation: "off",
               bullets: {
                  style: "",
                  enable: false,
                  rtl: false,
                  hide_onmobile: false,
                  hide_onleave: false,
                  hide_under: 767,
                  hide_over: 9999,
                  tmp: '',
                  direction: "vertical",
                  space: 10,
                  h_align: "right",
                  v_align: "center",
                  h_offset: 40,
                  v_offset: 0
               },
                arrows: {

              enable: true,
              style: 'hesperiden',
              tmp: '',
              rtl: false,
              hide_onleave: false,
              hide_onmobile: true,
              hide_under: 0,
              hide_over: 9999,
              hide_delay: 200,
              hide_delay_mobile: 1200,

              left: {
                  container: 'slider',
                  h_align: 'left',
                  v_align: 'center',
                  h_offset: 20,
                  v_offset: 0
              },

              right: {
                  container: 'slider',
                  h_align: 'right',
                  v_align: 'center',
                  h_offset: 20,
                  v_offset: 0
              }

          }
         ,
               touch: {
                  touchenabled: "on",
                  swipe_threshold: 75,
                  swipe_min_touches: 1,
                  swipe_direction: "horizontal",
                  drag_block_vertical: false,
               }
            },
            viewPort: {
               enable: true,
               outof: "pause",
               visible_area: "90%"
            },
            responsiveLevels: [4096, 1024, 778, 480],
            gridwidth: [1140, 1024, 768, 480],
            gridheight: [600, 500, 500, 350],
            lazyType: "none",
            parallax: {
               type: "mouse",
               origo: "slidercenter",
               speed: 9000,
               levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50],
            },
            shadow: 0,
            spinner: "off",
            stopLoop: "off",
            stopAfterLoops: -1,
            stopAtSlide: -1,
            shuffle: "off",
            autoHeight: "off",
            hideThumbsOnMobile: "off",
            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            debugMode: false,
            fallbacks: {
               simplifyAll: "off",
               nextSlideOnWindowFocus: "off",
               disableFocusListener: false,
            }
         });




      </script>
      <script type="text/javascript">
         $("#main-banner2").show().revolution({
            sliderType: "standard",
            sliderLayout: "auto",
            scrollbarDrag: "true",
            dottedOverlay: "none",
            delay:{{setting('duration' , "10")}}000,
            minHeight: '450',
            navigation: {
               keyboardNavigation: "off",
               keyboard_direction: "horizontal",
               mouseScrollNavigation: "off",
               bullets: {
                  style: "",
                  enable: false,
                  rtl: false,
                  hide_onmobile: false,
                  hide_onleave: false,
                  hide_under: 767,
                  hide_over: 9999,
                  tmp: '',
                  direction: "vertical",
                  space: 10,
                  h_align: "right",
                  v_align: "center",
                  h_offset: 40,
                  v_offset: 0
               },
                arrows: {

              enable: true,
              style: 'hesperiden',
              tmp: '',
              rtl: false,
              hide_onleave: false,
              hide_onmobile: true,
              hide_under: 0,
              hide_over: 9999,
              hide_delay: 200,
              hide_delay_mobile: 1200,

              left: {
                  container: 'slider',
                  h_align: 'left',
                  v_align: 'center',
                  h_offset: 20,
                  v_offset: 0
              },

              right: {
                  container: 'slider',
                  h_align: 'right',
                  v_align: 'center',
                  h_offset: 20,
                  v_offset: 0
              }

          }
         ,
               touch: {
                  touchenabled: "on",
                  swipe_threshold: 75,
                  swipe_min_touches: 1,
                  swipe_direction: "horizontal",
                  drag_block_vertical: false,
               }
            },
            viewPort: {
               enable: true,
               outof: "pause",
               visible_area: "90%"
            },
            responsiveLevels: [4096, 1024, 778, 480],
            gridwidth: [1140, 1024, 768, 480],
            gridheight: [600, 500, 500, 350],
            lazyType: "none",
            parallax: {
               type: "mouse",
               origo: "slidercenter",
               speed: 9000,
               levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50],
            },
            shadow: 0,
            spinner: "off",
            stopLoop: "off",
            stopAfterLoops: -1,
            stopAtSlide: -1,
            shuffle: "off",
            autoHeight: "off",
            hideThumbsOnMobile: "off",
            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            debugMode: false,
            fallbacks: {
               simplifyAll: "off",
               nextSlideOnWindowFocus: "off",
               disableFocusListener: false,
            }
         });

      </script>



      <!-- Custom JS -->
      @section('script')
      @show
   </body>
</html>
