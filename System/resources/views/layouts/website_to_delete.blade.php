<!DOCTYPE html>
<html  lang="{{App::getLocale()}}" dir="rtl">

<head>

    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <meta name="author" content=""/>

    <!-- Page Title -->
    <title>@isset($title) {{ $title }} | @endisset {{setting('website_title_'.App::getLocale() , "")}}</title>

    <!-- Favicon and Touch Icons -->
    <link href="{{url('')}}/assets/site/imgs/fav.ico" rel="shortcut icon" >
    <link href="{{url('')}}/assets/site/images/apple-touch-icon.png" rel="apple-touch-icon">
    <link href="{{url('')}}/assets/site/images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
    <link href="{{url('')}}/assets/site/images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
    <link href="{{url('')}}/assets/site/images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

    <!-- Stylesheet -->
    <link href="{{url('')}}/assets/site/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="{{url('')}}/assets/site/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <link href="{{url('')}}/assets/site/css/animate.css" rel="stylesheet" type="text/css">
    <link href="{{url('')}}/assets/site/css/css-plugin-collections.css" rel="stylesheet"/>
    <!-- CSS | menuzord megamenu skins -->
    <link id="menuzord-menu-skins" href="{{url('')}}/assets/site/css/menuzord-skins/menuzord-rounded-boxed.css" rel="stylesheet"/>
    <!-- CSS | Main style file -->
    <link href="{{url('')}}/assets/site/css/style-main.css" rel="stylesheet" type="text/css">
    <!-- CSS | Preloader Styles -->
    <link href="{{url('')}}/assets/site/css/preloader.css" rel="stylesheet" type="text/css">
    <!-- CSS | Custom Margin Padding Collection -->
    <link href="{{url('')}}/assets/site/css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
    <!-- CSS | Responsive media queries -->
    <link href="{{url('')}}/assets/site/css/responsive.css" rel="stylesheet" type="text/css">
    <!-- CSS | RTL Layout -->
    <link href="{{url('')}}/assets/site/css/style-main-rtl.css" rel="stylesheet" type="text/css">
    <link href="{{url('')}}/assets/site/css/style-main-rtl-extra.css" rel="stylesheet" type="text/css">
    <link href="{{url('')}}/assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css" />

    <!-- Revolution Slider 5.x CSS settings -->
    <link href="{{url('')}}/assets/site/js/revolution-slider/css/settings.css" rel="stylesheet" type="text/css"/>
    <link href="{{url('')}}/assets/site/js/revolution-slider/css/layers.css" rel="stylesheet" type="text/css"/>
    <link href="{{url('')}}/assets/site/js/revolution-slider/css/navigation.css" rel="stylesheet" type="text/css"/>

    <!-- CSS | Theme Color -->
    <link href="{{url('')}}/assets/site/css/colors/theme-skin-colorset.css" rel="stylesheet" type="text/css">

    <!-- external javascripts -->
    <script src="{{url('')}}/assets/site/js/jquery-2.2.4.min.js"></script>
    <script src="{{url('')}}/assets/site/js/jquery-ui.min.js"></script>
    <script src="{{url('')}}/assets/site/js/bootstrap.min.js"></script>
    <!-- JS | jquery plugin collection for this theme -->
    <script src="{{url('')}}/assets/site/js/jquery-plugin-collection.js"></script>

    <!-- Revolution Slider 5.x SCRIPTS -->
    <script src="{{url('')}}/assets/site/js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
    <script src="{{url('')}}/assets/site/js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="rtl">
<div id="wrapper" class="clearfix">
    <!-- preloader -->
    <div id="preloader">
        <div id="spinner">
            <img class="floating" src="{{url('/assets/site/imgs/preloader.png')}}" alt="">
            <h5 class="line-height-50 font-18 ml-15">{{trans("general.loading")}}...</h5>
        </div>
        <div id="disable-preloader" class="btn btn-default btn-sm">{{trans("general.disable_preloader")}}</div>
    </div>

    <!-- Header -->
    <header id="header" class="header">
        <div class="header-top border-bottom sm-text-center p-0 bg-theme-color-blue">
            <div class="container pt-5 pb-5">
                <div class="row">

                    <div class="col-md-6  sm-text-center">
                        <div class="widget no-border m-0">
                            <p class="mb-0 text-white">{{setting('website_title_'.App::getLocale() , "")}}</p>
                        </div>
                    </div>
                    <div class="col-md-6 sm-text-center text-right">
                        <div class="widget no-border m-0">
                            <ul class="list-inline xs-text-center m-0">
                                <li class="m-0 pl-10 pr-10">
                                    <a href="tel:{{setting('general_phone' , "")}}" class="text-white"><i class="fa fa-phone"></i> {{setting('general_phone' , "")}}</a>
                                </li>
                                <li class="m-0 pl-10 pr-10">
                                    <a href="mailto:{{setting('general_email' , "")}}" class="text-white"><i class="fa fa-envelope-o mr-5"></i>
                                        {{setting('general_email' , "")}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-nav">
            <div class="header-nav-wrapper navbar-scrolltofixed bg-white">
                <div class="container">
                    <nav id="menuzord-right" class="menuzord default">
                        <a class="menuzord-brand pull-left flip xs-pull-center mt-20 pt-5 mt-sm-10 pt-sm-0" href="{{url("/Home")}}">
                            <img src="{{url('')}}/assets/site/images/logo-wide.png" alt="">
                        </a>
                        <ul class="menuzord-menu">
                            <li>
                                <a href="{{url("/Home")}}">{{trans("general.home")}}</a>
                            </li>
                            <li><a href="JavaScript:Void(0);">{{trans("general.books")}}</a>
                                <ul class="dropdown">
                                    @foreach(\App\Models\Books::OrderBY("class_id")->get() as $Book)
                                        <li><a href="{{url("/Book/".$Book->id)}}">{{$Book->ClassOf->name .' ('.$Book->name.')'}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <li><a href="JavaScript:Void(0);">{{trans("classes.users_guide")}}</a>
                                <ul class="dropdown">
                                    @foreach(\App\Models\Classes::all() as $Classe)
                                        <li><a target="_blank" href="{{url("/Users_Guide/BPDF/".$Classe->id)}}">{{$Classe->name}}</a></li>
                                    @endforeach
                                </ul>
                            </li>

                            <li>
                                <a href="{{url("/AboutUs")}}">{{setting('about_us_title' , "")}}</a>
                            </li>
                            <li><a href="JavaScript:Void(0);">{{setting("recommended_sites_title" , "")}}</a>
                                <ul class="dropdown">
                                        <li><a target="_blank" href="http://sakhnin.yahlla.com/">אופק</a></li>
                                        <li><a target="_blank" href="http://yahlla.com/">אופק 2</a></li>
                                </ul>
                            </li>

                            <li>
                                <a href="javascript:void(0);">{{trans("classes.teacher_login")}}</a>
                            </li>

                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>

    <!-- Start main-content -->
    @section('content')
    @show
    <!-- end main-content -->



<!-- Footer -->
    <footer id="footer" class="footer dot-style-down" data-bg-color="#212733">
        <div class="container pt-70 pb-40">

            <div class="row mt-10">
                <div class="col-md-3">
                    <div class="widget dark">
                        <h5 class="widget-title mb-10">קרא לנו עכשיו</h5>
                        <div class="text-gray">
                            +972 3 1234 5678 <br>
                            +972 3 1234 5678
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="widget dark">
                        <h5 class="widget-title mb-10">התחבר אלינו</h5>
                        <ul class="styled-icons icon-sm icon-bordered icon-circled clearfix">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="widget dark">
                        <h5 class="widget-title mb-10">הצטרף כמנוי</h5>
                        <!-- Mailchimp Subscription Form Starts Here -->
                        <form id="mailchimp-subscription-form" class="newsletter-form">
                            <div class="input-group">
                                <input type="email" value="" name="EMAIL" placeholder="Your Email"
                                       class="form-control input-lg font-16" data-height="45px" id="mce-EMAIL-footer"
                                       style="height: 45px;">
                                <span class="input-group-btn">
									<button data-height="45px"
                                            class="btn btn-colored btn-theme-color-red btn-xs m-0 font-14"
                                            type="submit">הירשם</button>
								</span>
                            </div>
                        </form>
                        <!-- Mailchimp Subscription Form Validation-->
                        <script type="text/javascript">
                            $('#mailchimp-subscription-form').ajaxChimp({
                                callback: mailChimpCallBack,
                                url: '//thememascot.us9.list-manage.com/subscribe/post?u=a01f440178e35febc8cf4e51f&amp;id=49d6d30e1e'
                            });

                            function mailChimpCallBack(resp) {
                                // Hide any previous response text
                                var $mailchimpform = $('#mailchimp-subscription-form'),
                                    $response = '';
                                $mailchimpform.children(".alert").remove();
                                console.log(resp);
                                if (resp.result === 'success') {
                                    $response = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                                } else if (resp.result === 'error') {
                                    $response = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                                }
                                $mailchimpform.prepend($response);
                            }
                        </script>
                        <!-- Mailchimp Subscription Form Ends Here -->
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom" data-bg-color="#2A333D">
            <div class="container pt-15 pb-10">
                <div class="row">
                    <div class="col-md-6">
                        <p class="font-12 text-black-777 m-0 sm-text-center">Copyright &copy;2017 ThemeMascot. All
                            Rights Reserved</p>
                    </div>
                    <div class="col-md-6 text-right">
                        <div class="widget no-border m-0">
                            <ul class="list-inline sm-text-center mt-5 font-12">
                                <li>
                                    <a href="#">שאלות נפוצות</a>
                                </li>
                                <li>|</li>
                                <li>
                                    <a href="#">דלפק עזרה</a>
                                </li>
                                <li>|</li>
                                <li>
                                    <a href="#">תמיכה</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="{{url('')}}/assets/site/js/custom.js"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS
			(Load Extensions only on Local File Systems !
			 The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="{{url('')}}/assets/site/js/revolution-slider/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="{{url('')}}/assets/site/js/revolution-slider/js/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="{{url('')}}/assets/site/js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="{{url('')}}/assets/site/js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="{{url('')}}/assets/site/js/revolution-slider/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="{{url('')}}/assets/site/js/revolution-slider/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="{{url('')}}/assets/site/js/revolution-slider/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="{{url('')}}/assets/site/js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="{{url('')}}/assets/site/js/revolution-slider/js/extensions/revolution.extension.video.min.js"></script>
@section('script')
@show
</body>

<!-- Mirrored from thememascot.net/demo/personal/j/childhaven/v2.0/demo/index-rtl-mp-layout1.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 21 Aug 2018 02:58:35 GMT -->
</html>