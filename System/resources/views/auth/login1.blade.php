@extends('layouts.website', ['title' => trans('general.login'),'main_classes' => 'home-wrap'])

@section('content')
<div class="tt-breadcrumb">
   <div class="container">
      <ul>
          <li><a href="{{url('')}}">{{trans('general.home')}}</a></li>
         <li>{{trans('general.login')}}</li>
      </ul>
   </div>
</div>
<div id="tt-pageContent">
   <div class="container-indent">
      <div class="container">
         <h1 class="tt-title-subpages noborder">{{trans("general.login_to_your_account")}}</h1>
         <div class="tt-login-form">
            <div class="row">
               <div class="col-xs-12 col-md-6">
                  <div class="tt-item">
                     <h2 class="tt-title">{{trans("general.new_customer")}}</h2>
                     <p>
                       {{trans("general.by_creating_an_account_on_the_site")}}
                     </p>
                     <div class="form-group">
                        <a href="{{ route('register') }}" class="btn btn-top btn-border">{{trans('general.create_new_account')}}</a>
                     </div>
                  </div>
               </div>
               <div class="col-xs-12 col-md-6">
                  <div class="tt-item">
                     <h2 class="tt-title">{{trans("general.login_to_your_account")}}</h2>
                     {{ trans("general.enter_any_username_and_password") }}
                     <div class="form-default form-top">
                        <form  class="login-form"  >
                           <div class="form-group">
                              <label for="loginInputName">{{ trans("general.username") }} *</label>
                              <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="{{trans("general.username")}}" name="username" />
                           </div>
                           <div class="form-group">
                              <label for="loginInputEmail">{{ trans("general.password") }} *</label>
                              <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="{{ trans("general.password") }}" name="password" />
                           </div>
                           <div class="row">
                              <div class="col-auto mr-auto">
                                 <div class="form-group">
                                    <button class="btn btn-border" type="submit"> {{ trans("general.login") }}</button>
                                 </div>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
{{-- <!-- BEGIN LOGO -->
<div class="logo">
   <a href="javascript:void(0);">
   <img src="{{ asset('assets') }}/pages/img/logo-big.png" alt="" />
   </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
   <!-- BEGIN LOGIN FORM -->
   <form class="login-form" >
      <h3 class="form-title">{{trans("general.login_to_your_account")}}</h3>
      <div class="alert alert-danger display-hide">
         <button class="close" data-close="alert"></button>
         <span> {{trans("general.enter_any_username_and_password")}}. </span>
      </div>
      <div class="form-group">
         <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
         <label class="control-label visible-ie8 visible-ie9">{{ trans("general.username") }}</label>
         <div class="input-icon">
            <i class="fa fa-user"></i>
            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="{{trans("general.username")}}" name="username" />
         </div>
      </div>
      <div class="form-group">
         <label class="control-label visible-ie8 visible-ie9">{{trans("general.password")}}</label>
         <div class="input-icon">
            <i class="fa fa-lock"></i>
            <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="{{trans("general.password")}}" name="password" />
         </div>
      </div>
      <div class="form-actions">
         <label class="rememberme mt-checkbox mt-checkbox-outline">
         <input type="checkbox" name="remember" value="1" /> {{trans("general.remember_me")}}
         <span></span>
         </label>
         <button type="submit" class="btn green pull-right"> {{ trans("general.login") }} </button>
      </div>
   </form>
   <!-- END LOGIN FORM -->
</div>
--}}
@section('script')
<script src="{{ asset('assets') }}/global/scripts/app.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/js/login.js" type="text/javascript"></script>
@endsection
