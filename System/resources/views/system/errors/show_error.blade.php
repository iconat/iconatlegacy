@extends('admin.app',['title' => $error_no .' - '.$error_title])
@section('css')

    <link href="{{url('')}}/assets/layouts/layout5/css/error.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')


    <!-- BEGIN BREADCRUMBS -->

    <div class="breadcrumbs">
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{url('/My')}}">{{trans('general.home')}}</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>{{trans('general.system')}}</span>
                </li>
            </ul>

        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> {{$error_no}}
            <small>{{$error_title}}</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12 page-404">
                <div class="number font-green"> {{$error_no}} </div>
                <div class="details">
                    <h3>{{$error_title}}</h3>
                    <p> {{$error_description}}
                        <br/>
                        <a href="{{url('My')}}"> {{trans('general.return_home')}} </a> </p>
                    <form action="#">
                        <div class="input-group input-medium">
                            <input type="text" class="form-control" placeholder="{{trans('general.search')}}...">
                            <span class="input-group-btn">
                                <button type="submit" class="btn green">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        <!-- /input-group -->
                    </form>
                </div>
            </div>
        </div>



    </div>

    <!-- END BREADCRUMBS -->



@endsection



