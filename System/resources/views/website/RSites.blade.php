@extends('layouts.website', ['title' => setting('recommended_sites_title' , "")])

@section('content')

    <div class="main-content">
        <!-- Section: inner-header -->
        <section class="inner-header divider layer-overlay overlay-dark-7" data-bg-img="{{url('')}}/assets/site/images/bg/inner-header.jpg">
            <div class="container pt-120 pb-60">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-6">
                            <h2 class="text-theme-color-yellow font-36">{{setting('recommended_sites_title' , "")}}</h2>
                            <ol class="breadcrumb text-right mt-10 white">
                                <li><a href="{{url("/Home")}}">{{trans("general.home")}}</a></li>
                                <li class="active">{{setting('recommended_sites_title' , "")}}</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Section: Course Details -->
        <div class="row">

            <div class="col-md-12" style="padding: 20px;">
                @php echo setting('recommended_sites_full_Text' , ""); @endphp
            </div>

        </div>
    </div>

@endsection



@section('script')


@endsection