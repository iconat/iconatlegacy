@extends('layouts.website', ['title' => trans('products.shop')])

@section('content')

    <!--Breadcrumb Section Start-->
    <section class="breadcrumb-bg bg-2">
        <div class="theme-container container ">
            <div class="site-breadcumb white-clr">
                <h2 class="section-title"> <strong class="clr-txt">{{trans("products.shop")}}</strong>   </h2>
                <ol class="breadcrumb breadcrumb-menubar">
                    <li> <a href="{{url("/")}}"> {{trans("general.home")}} </a> {{trans("products.shop")}} </li>
                </ol>
            </div>
        </div>
    </section>
    <!--Breadcrumb Section End-->


    <!-- Shop Starts-->
    <section class="shop-wrap sec-space-bottom">
        <div class="pattern">
            <img alt="" src="{{url('')}}/assets/site/img/icons/white-pattern.png">
        </div>

        <div class="container rel-div">
            @if(false)
            <div class="row sort-bar">
                <div class="icon"> <img alt="" src="{{url('')}}/assets/site/img/logo/logo-2.png" /> </div>
                <div class="col-lg-6">
                    <div class="sort-dropdown left">
                        <span>القسم</span>
                        <div class="search-selectpicker selectpicker-wrapper">
                            <select class="selectpicker input-price"  data-width="100%"
                                    data-toggle="tooltip">
                                <option>كل المنتجات</option>
                                <option>أ - ي</option>
                                <option>ي - أ</option>
                                <option> قسم 1 </option>
                                <option> قسم 2 </option>
                            </select>
                        </div>
                    </div>
                    <div class="sort-dropdown">
                        <span>الترتيب حسب</span>
                        <div class="search-selectpicker selectpicker-wrapper">
                            <select class="selectpicker input-price"  data-width="100%"
                                    data-toggle="tooltip">
                                <option>المنتجات المشهورة</option>
                                <option>أ - ي</option>
                                <option>ي - أ</option>
                                <option> منخفض - مرتفع </option>
                                <option> مرتفع - منخفض </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 right">
                    <div class="sort-dropdown">
                        <span>حسب السعر</span>
                        <div class="search-selectpicker selectpicker-wrapper">
                            <select class="selectpicker input-price"  data-width="100%"
                                    data-toggle="tooltip">
                                <option> منخفض - مرتفع </option>
                                <option> مرتفع - منخفض </option>
                            </select>
                        </div>
                    </div>
                    <div class="sort-range">
                        <span>السعر</span> <div id="price-range"></div>
                    </div>
                </div>
            </div>

            <div class="divider-full-1"></div>

            @endif


            <div class="tab-content shop-content">
                <div id="grid-view" class="tab-pane fade active in" role="tabpanel">
                    <div class="row">
                        @foreach($Products as $Product)
                        <div class="col-lg-3 col-md-4 col-sm-6">
                                @include("website/products/product_list")
                        </div>
                        @endforeach
                    </div>

                    <div class="pagination-wrap">
                        {{ $Products->appends(['sort' => 'id'])->links('vendor.pagination.theam1') }}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- / Shop Ends -->


@endsection



@section('script')


@endsection