<div class="product-box">
    <div class="product-media">
        @php $Img_Product = $Product->MainImage(); @endphp
        @if($Img_Product) <img class="prod-img" alt="" src="{{$Img_Product}}"/> @endif
        <img class="shape" alt="" src="{{url('/assets/site/img/icons/shap-small.png')}}"/>
        <div class="prod-icons">
            <a href="#" class="fa fa-heart"></a>
            <a href="javascript:void(0);" class="fa fa-shopping-basket ViewProduct" p="{{$Product->id}}"></a>
            <a href="javascript:void(0);" class="fa fa-expand ViewProduct" p="{{$Product->id}}"></a>
        </div>
    </div>
    <div class="product-caption">
        <h3 class="product-title">
            <a> <span class="light-font"> {{$Product->Get_Trans(App::getLocale(),'name')}} </span></a>
        </h3>
        <div class="price">
            <strong class="clr-txt"> {{$Product->Get_Price_Depend_Currency($Product->Get_Price())}} <i class="fa fa-{{config("Default_currency_Icon")}}"></i> </strong>
        </div>
    </div>
</div>