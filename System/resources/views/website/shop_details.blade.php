@extends('layouts.website', ['title' => trans('shops.the_shop'),'main_classes' => 'home-wrap'])

@section('content')


<div class="tt-breadcrumb">
    <div class="container">
        <ul>
           <li><a href="{{url('')}}">{{trans('general.home')}}</a></li>
            <li>{{trans('shops.the_shop')}}</li>
        </ul>
    </div>
</div>


<div id="tt-pageContent">
    <div class="container-indent">
        <!-- mobile product slider  -->
        <div class="tt-mobile-product-layout visible-xs">
            <div class="tt-mobile-product-slider arrow-location-center slick-animated-show-js">
               <div><img src="{{$Shops_Details->MainImage()}}" alt=""></div>
              @foreach($Shops_Details->AllImage(['shop_images'])->where('deleted_at', NULL ) as $Image)
                <div><img src="{{$Image->lastMedia(config('col'))->getUrl()}}" alt=""></div>


                 @endforeach
              <!--   <div>
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="http://www.youtube.com/embed/GhyKqj_P2E4" allowfullscreen></iframe>
                    </div>
                </div -->>
               <!--  <div>
                    <div class="tt-video-block">
                        <a href="#" class="link-video"></a>
                        <video class="movie" src="{{url('')}}/assets/site/video/video.mp4" poster="video/video_img.jpg"></video>
                    </div>
                </div> -->
            </div>
        </div>
        <!-- /mobile product slider  -->
        <div class="container container-fluid-mobile">
            <div class="row">
                <div class="col-12 col-lg-9">
                    <div class="row custom-single-page">
                        <div class="col-6 hidden-xs">
                            <div class="tt-product-vertical-layout">
                                <div class="tt-product-single-img">
                                    <div>
                                        <button class="tt-btn-zomm tt-top-right"><i class="icon-f-86"></i></button>
                                        <img class="zoom-product" src='{{$Shops_Details->MainImage()}}' data-zoom-image="{{$Shops_Details->MainImage()}}" alt="">
                                    </div>
                                </div>
                                <div class="tt-product-single-carousel-vertical">
                                    <ul id="smallGallery" class="tt-slick-button-vertical  slick-animated-show-js">

                                       <li><a class="zoomGalleryActive" href="#" data-image="{{ $Shops_Details->MainImage() }}" data-zoom-image="{{$Shops_Details->MainImage2()}}"><img src="{{$Shops_Details->MainImage()}}" alt=""></a></li>

                                          @if ($Shops_Details->video)


                                         <li>
                                            <div class="video-link-product" data-toggle="modal" data-type="youtube" data-target="#modalVideoProduct" data-value="https://www.youtube.com/embed/{{ $Shops_Details->video }}">
                                                <img src="{{url('')}}/assets/site/images/product/product-small-empty.png" alt="">
                                                <div>
                                                    <i class="icon-f-32"></i>
                                                </div>
                                            </div>
                                        </li>

                                        @endif


                                       @foreach($Shops_Details->AllImage(['shop_images'])->where('deleted_at', NULL ) as $Image)
                                       <li><a class="zoomGalleryActive" href="#" data-image="{{$Image->lastMedia(config('col'))->getUrl()}}" data-zoom-image="{{$Image->getUrl()}}"><img src="{{$Image->lastMedia(config('col'))->getUrl()}}" alt=""></a></li>

                                       @endforeach











                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="tt-product-single-info">
                               <!--  <div class="tt-wrapper">
                                    <div class="tt-label">
                                        <div class="tt-label-new">New</div>
                                        <div class="tt-label-out-stock">Out Of Stock</div>
                                        <div class="tt-label tt-label-sale">Sale 40%</div>
                                        <div class="tt-label tt-label-our-fatured">Fatured</div>
                                    </div>
                                </div> -->
                                <div class="tt-add-info">
                                    <ul>
                                        <li><span>{{trans('categories.categories')}}:</span> {{$Shops_Details->Shop_Categories->Get_Trans(App::getLocale(),'name')}}</li>
                                    </ul>
                                </div>
                                <h1 class="tt-title">{{$Shops_Details->Get_Trans(App::getLocale(),'name')}}</h1>
                               <!--  <div class="tt-price">
                                    <span class="new-price">$29</span>
                                </div>  -->
                                <div class="tt-review">
                                    <div class="tt-rating">
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star-empty"></i>
                                    </div>
                                    <a class="product-page-gotocomments-js" href="#"></a>
                                </div>
                                <div class="tt-wrapper">
                                    {{$Shops_Details->Get_Trans(App::getLocale(),'text')}}
                                </div>

                                <div class="tt-wrapper">
                                    <div class="tt-add-info">
                                        <ul>
                                            <li>

                                               <div class="tt-product-single-aside">
                                                <div class="tt-services-aside">


                                                   <a href="#" class="tt-services-block">
                                                    <div class="tt-col-icon">
                                                        <i class="icon-e-09"></i>
                                                    </div>
                                                    <div class="tt-col-description">
                                                     <h4 class="tt-title">{{trans('general.address')}}</h4>
                                                     <p>{{$Shops_Details->Get_Trans(App::getLocale(),'address')}} </p>
                                                 </div>
                                             </a>


                                              <a href="#" class="tt-services-block">
                                                        <div class="tt-col-icon">
                                                            <i class="icon-f-93"></i>
                                                        </div>
                                                        <div class="tt-col-description">
                                                           <h4 class="tt-title">{{trans('general.phone')}}</h4>
                                                           <p> {{$Shops_Details->mobile}} </p>
                                                       </div>
                                                   </a>
                                         </div>


                                     </div>

                                 </li>
                             </ul>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <div class="col-12 col-lg-3">
        <div class="tt-product-single-aside">
            <div class="tt-services-aside">

              {{-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13547.27326265766!2d35.200596669775386!3d31.911533449999997!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x151d2ab5f7e48241%3A0x1b67dc9fec085be2!2sZamn+cafe!5e0!3m2!1sen!2s!4v1552743320145" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> --}}

              <iframe
              width="100%"
              height="450"
              frameborder="0" style="border:0"
              src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBw9YOQ6A0RrWGodIJ6WTZCat1rSwUpfb8&q={{$Shops_Details->latitude}},{{$Shops_Details->longitude}}&zoom=14&maptype=roadmap" allowfullscreen>
          </iframe>



          <br />
          <small>
            <a href="https://www.google.com/maps/search/?api=1&query={{$Shops_Details->latitude}},{{$Shops_Details->longitude}}"
             style="color:#fff; "
             target="_blank" class="btn btn-3d btn-primary">
             {{trans('general.get_direction')}}
            </a>
     </small>


 </div>

</div>
</div>
</div>
</div>
</div>
<div class="container-indent wrapper-social-icon">
    <div class="container">
        <ul class="tt-social-icon justify-content-center">
            <li><a class="icon-g-64" href="{{$Shops_Details->facebook}}"></a></li>
            <li><a class="icon-h-58" href="{{$Shops_Details->twitter}}"></a></li>
            <li><a class="icon-g-66" href="{{$Shops_Details->gmail}}"></a></li>
            <li><a class="icon-g-67" href="{{$Shops_Details->instagram}}"></a></li>
            <li><a class="icon-g-68" href="{{$Shops_Details->linkedin}}"></a></li>
            <li><a class="icon-g-77" href="{{$Shops_Details->snapchat}}"></a></li>
        </ul>
    </div>
</div>




<div class="container-indent">
    <div class="container">
        <div class="tt-collapse-block">
            <div class="tt-item">
                <div class="tt-collapse-title">{{trans('general.description')}}</div>
                <div class="tt-collapse-content">
                 {{$Shops_Details->Get_Trans(App::getLocale(),'text')}}
             </div>
         </div>
                <!-- <div class="tt-item">
                    <div class="tt-collapse-title">ADDITIONAL INFORMATION - Maps</div>
                    <div class="tt-collapse-content">
                        <table class="tt-table-03">
                            <tbody>
                                <tr>
                                    <td>Color:</td>
                                    <td>Blue, Purple, White</td>
                                </tr>
                                <tr>
                                    <td>Size:</td>
                                    <td>20, 24</td>
                                </tr>
                                <tr>
                                    <td>Material:</td>
                                    <td>100% Polyester</td>
                                </tr>
                            </tbody>
                        </table>







                    </div>
                </div> -->


            </div>
        </div>
    </div>


</div>






































@endsection



@section('script')


@endsection
