@extends('layouts.website', ['title' => trans('cites.main_cites'),'main_classes' => 'home-wrap'])

@section('content')


<div class="tt-breadcrumb">
    <div class="container">
        <ul>
            <li><a href="{{url('')}}">{{trans('general.home')}}</a></li>
            <li>{{trans('cites.main_cites')}}</li>
        </ul>
    </div>
</div>
<div id="tt-pageContent">



  
  <div class="container-indent">
            <div class="container container-fluid-custom-mobile-padding">
                <div class="tt-block-title">
                    <h2 class="tt-title">{{trans('cites.main_cites')}}</h2>
                    
                </div>
                <div class="row tt-img-box-listing">




                    @foreach(\App\Models\Cites::where('active',1)->orderBY("order",'desc')->get() as $city)
                    <div class="col-md-2">
                        <a href="{{url('/Categoreis')}}/{{$city->id}}" class="tt-img-box">
                            <img src="images/loader.svg" data-src="{{ $city->MainImage()}}" alt="">
                        </a>
                        <div class="aa text-center">
                            <h4 class="tt-title">{{$city->Get_Trans(App::getLocale(),'name')}}</h4>
                        </div>
                    </div>

                     @endforeach   

                  
                 
        


                </div>
            </div>
        </div>





</div>



@endsection



@section('script')


@endsection