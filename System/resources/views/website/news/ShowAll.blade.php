@extends('layouts.website', ['title' => trans('news.news')])

@section('content')

    <!--Breadcrumb Section Start-->
    <section class="breadcrumb-bg bg-2">
        <div class="theme-container container ">
            <div class="site-breadcumb white-clr">
                <h2 class="section-title"> <strong class="clr-txt">@php echo trans("news.news_of_:name",["name" => '<span class="light-font">' . setting('website_title_'.App::getLocale() , "").'</span>']) @endphp </strong>   </h2>
                <ol class="breadcrumb breadcrumb-menubar">
                    <li> <a href="{{url("/")}}"> {{trans("general.home")}} </a> {{trans("news.news")}} </li>
                </ol>
            </div>
        </div>
    </section>
    <!--Breadcrumb Section End-->


    <!-- Blog Starts-->
    <section class="blog-wrap sec-space-bottom">
        <div class="container pt-50">
            <div class="row">
                <div class="col-md-12 pt-15">
                    <div class="blog-wrap">
                        <div class="blog-heading">
                            <h2 class="title-2"> @php echo trans("news.news_of_:name",["name" => '<span class="light-font">' . setting('website_title_'.App::getLocale() , "").'</span>']) @endphp </h2>
                        </div>

                        <div class="divider-full-1"></div>

                        <div class="blog-content tab-content">
                            <div id="blog-tab-1" class="tab-pane fade active in">
                                @foreach($News as $New)
                                <div class="row ptb-15">
                                    <div class="col-sm-3">
                                        @if(!empty($New->MainImage()))
                                        <a href="{{$New->Get_URL()}}" class=""> <img alt="" class="img-responsive" src="{{$New->MainImage()}}" /> </a>
                                        @endif
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="blog-caption">
                                            <h4 class="sub-title-1"> {{$New->created_at}} </h4>
                                            <h2 class="title-2"> <a href="{{$New->Get_URL()}}"> {{$New->Get_Trans(\App::getLocale(),"name")}} </a> </h2>
                                            <span class="divider-1 crl-bg"></span>
                                            <div class="ptb-10">
                                                <p>{{$New->Get_Trans(\App::getLocale(),"description")}}</p>
                                            </div>
                                            <a href="{{$New->Get_URL()}}" class="clr-txt"> <strong> {{trans("general.read_more")}} <i class="fa fa-long-arrow-{{($currentLanguage->dir == 'rtl')?'left':'right'}}"></i> </strong> </a>
                                        </div>
                                    </div>
                                </div>
                                @endforeach

                                <div class="pagination-wrap">
                                    <div class="divider-full-1"></div>
                                    {{ $News->appends(['sort' => 'id'])->links('vendor.pagination.theam1') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>
    <!-- / Blog Ends -->


@endsection



@section('script')


@endsection