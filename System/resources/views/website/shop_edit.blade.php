@extends('layouts.website', ['title' => trans('shops.the_shop'),'main_classes' => 'home-wrap'])


@section('css')
<link href="{{url('')}}/assets/global/plugins/dropzone/min/dropzone.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('')}}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{url('')}}/assets/global/plugins/jquery-confirm/jquery-confirm.min.css">
@endsection



@section('content')
<div id="tt-pageContent">
   <div class="container-indent">
      <div class="container container-fluid-custom-mobile-padding">
         <form id="DForm" par1="{{$CurrentObj->id  ?? NULL}}" class="contact-form form-default form-horizontal">
            <div class="form-body">
               <div class="form-row">
                  @foreach ($AllLangs as $alt)
                  <div class="form-group col-md-6">
                     <label class="col-md-12 control-label">{{trans("general.name_of_:name",["name"=>trans('shops.the_shop')])}} - {{trans('general.'.$alt['locale'])}}</label>
                     <div class="col-md-12">
                        <input placeholder="{{trans("general.name_of_:name",["name"=>trans('shops.the_shop')])}} - {{trans('general.'.$alt['locale'])}}" type="text" dir="{{$alt['dir']}}" name="{{$alt['locale']}}[name]" class="form-control" value="@if($CurrentObj){{$CurrentObj->Get_Trans($alt['locale'],'name') ?? NULL}}@endif">
                     </div>
                  </div>
                  @endforeach
               </div>
               <div class="form-row">
                  @foreach ($AllLangs as $alt)
                  <div class="form-group col-md-6">
                     <label class="col-md-12 control-label">{{trans("general.text_of_:name",["name"=>trans('shops.the_shop')])}} - {{trans('general.'.$alt['locale'])}}</label>
                     <div class="col-md-12">
                        <textarea dir="{{$alt['dir']}}" name="{{$alt['locale']}}[text]" class="form-control" placeholder="{{trans("general.text_of_:name",["name"=>trans('shops.the_shop')])}} - {{trans('general.'.$alt['locale'])}}">@if($CurrentObj){{$CurrentObj->Get_Trans($alt['locale'],'text') ?? NULL}}@endif</textarea>
                     </div>
                  </div>
                  @endforeach
               </div>
               <div class="form-row">
                  @foreach ($AllLangs as $alt)
                  <div class="form-group  col-md-6">
                     <label class="col-md-12 control-label">{{ trans('shops.the_shop_address') }} - {{trans('general.'.$alt['locale'])}}</label>
                     <div class="col-md-12">
                        <input placeholder="{{ trans('shops.the_shop_address') }} - {{trans('general.'.$alt['locale'])}}" type="text" dir="{{$alt['dir']}}" name="{{$alt['locale']}}[address]" class="form-control" value="@if($CurrentObj){{$CurrentObj->Get_Trans($alt['locale'],'address') ?? NULL}}@endif">
                     </div>
                  </div>
                  @endforeach
               </div>
               <div class="form-row">
                  <div class="form-group  col-md-6">
                     <label class="col-md-12 control-label">{{ trans('shops.the_shop_cat') }}</label>
                     <div class="col-md-12">
                        <select class="select2me form-control" name="shop_categories">
                           <option class=""></option>
                           @foreach(\App\Models\Shop_Categories::where(function ($query) use ($CurrentObj) {
                           //To Select if item marked as not active after select this...
                           $query->where('active',1);
                           if($CurrentObj && (!empty($CurrentObj->shop_categories))){
                           $query->orWhere('id',$CurrentObj->shop_categories);
                           }
                           })->get() as $value)
                           <option @if(count($value->children) > 0) disabled=""  style="color: red;" @endif  value="{{$value->id}}" @if($CurrentObj && ($CurrentObj->shop_categories == $value->id)) selected @endif >{{$value->Get_Trans(App::getLocale(),'name')}}</option>
                           @endforeach
                        </select>
                     </div>
                  </div>
                  <div class="form-group  col-md-6">
                     <label class="col-md-12 control-label">{{ trans('shops.the_shop_city') }}</label>
                     <div class="col-md-12">
                        <select class="select2me form-control" name="city">
                           <option class=""></option>
                           @foreach(\App\Models\Cites::where(function ($query) use ($CurrentObj) {
                           //To Select if item marked as not active after select this...
                           $query->where('active',1);
                           if($CurrentObj && (!empty($CurrentObj->city))){
                           $query->orWhere('id',$CurrentObj->city);
                           }
                           })->get() as $value)
                           <option value="{{$value->id}}" @if($CurrentObj && ($CurrentObj->city == $value->id)) selected @endif >{{$value->Get_Trans(App::getLocale(),'name')}}</option>
                           @endforeach
                        </select>
                     </div>
                  </div>
               </div>
               <div class="form-row">
                  <div class="form-group  col-md-6">
                     <label class="col-md-12 control-label">{{trans("shops.shop_code")}}</label>
                     <div class="col-md-12">
                        <input type="text" disabled value="{{$CurrentObj->shop_code ?? trans("general.automatically_generated")}}" class="form-control">
                        <span>{{trans("general.serial_number_generates_automatic")}}</span>
                     </div>
                  </div>
                  <div class="form-group  col-md-6">
                     <label class="col-md-12 control-label">{{trans("general.mobile")}}</label>
                     <div class="col-md-12">
                        <input type="text" name="mobile" value="{{$CurrentObj->mobile ?? NULL}}" class="form-control">
                     </div>
                  </div>
               </div>
               <div class="form-row">
                  <div class="form-group  col-md-6">
                     <label class="col-md-12 control-label">{{trans("general.facebook")}}</label>
                     <div class="col-md-12">
                        <input type="text" name="facebook" value="{{$CurrentObj->facebook ?? NULL}}" class="form-control">
                     </div>
                  </div>
                  <div class="form-group  col-md-6">
                     <label class="col-md-12 control-label">{{trans("general.instagram")}}</label>
                     <div class="col-md-12">
                        <input type="text" name="instagram" value="{{$CurrentObj->instagram ?? NULL}}" class="form-control">
                     </div>
                  </div>
               </div>
               <div class="form-row">
                  <div class="form-group  col-md-6">
                     <label class="col-md-12 control-label">{{trans("general.snapchat")}}</label>
                     <div class="col-md-12">
                        <input type="text" name="snapchat" value="{{$CurrentObj->snapchat ?? NULL}}" class="form-control">
                     </div>
                  </div>
                  <div class="form-group  col-md-6">
                     <label class="col-md-12 control-label">{{trans("general.twitter")}}</label>
                     <div class="col-md-12">
                        <input type="text" name="twitter" value="{{$CurrentObj->twitter ?? NULL}}" class="form-control">
                     </div>
                  </div>
               </div>
               <div class="form-row">
                  <div class="form-group  col-md-6">
                     <label class="col-md-12 control-label">{{trans("general.linkedin")}}</label>
                     <div class="col-md-12">
                        <input type="text" name="linkedin" value="{{$CurrentObj->linkedin ?? NULL}}" class="form-control">
                     </div>
                  </div>
                  <div class="form-group  col-md-6">
                     <label class="col-md-12 control-label">{{trans("general.gmail")}}</label>
                     <div class="col-md-12">
                        <input type="text" name="gmail" value="{{$CurrentObj->gmail ?? NULL}}" class="form-control">
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-md-12 control-label">{{trans("general.video")}}</label>
                  <div class="col-md-12">
                     <input type="text" name="video" value="{{$CurrentObj->video ?? NULL}}" class="form-control">
                  </div>
               </div>



               <div class="form-row">


                  <div class="form-group col-md-3">
                     <label class="control-label col-md-12">{{ trans('shops.logo') }}</label>
                     <div class="col-md-12">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                           <div class="fileinput-new thumbnail" style="width: 100%; height: 100%;">
                              <img src="{{$main_logo_img_thumb ?? url(setting('default_product_image' , ""))}}" class="img-thumbnail img-fluid" width="100%" alt="" style="border: 1px solid #ccc; padding: 3px; border-radius: 5px;" />
                           </div>
                           <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"> </div>
                           <div>
                              <span class="btn default btn-file">
                              <span class="fileinput-new"> {{trans('general.select_image')}} </span>
                              <span class="fileinput-exists"> {{trans('general.change')}} </span>
                              <input type="file" name="main_logo_shop"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{trans('general.remove')}} </a>
                           </div>
                        </div>
                        <span class="help-block"></span>
                     </div>
                  </div>
               </div>
                  <div class="form-row">
                  <div class="form-group col-md-7">
                     <label class="control-label col-md-12">{{trans("general.image_of_name",["name"=>trans('shops.the_shop')])}}</label>
                     <div class="col-md-12">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                           <div class="fileinput-new thumbnail" style="width: 100%; height: 100%;">
                              <img src="{{$main_img_thumb ?? url(setting('default_product_image' , ""))}}" class="img-thumbnail img-fluid" width="100%" alt="" style="border: 1px solid #ccc; padding: 3px; border-radius: 5px;" />
                           </div>
                           <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100%; max-height: 100%;"> </div>
                           <div>
                              <span class="btn default btn-file">
                              <span class="fileinput-new"> {{trans('general.select_image')}} </span>
                              <span class="fileinput-exists"> {{trans('general.change')}} </span>
                              <input type="file" name="main_shop"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{trans('general.remove')}} </a>
                           </div>
                        </div>
                        <span class="help-block"></span>
                     </div>
                  </div>



               </div>






                <div class="form-group">
                  <label class="control-label col-md-3">{{ trans('shops.images') }}</label>
                  <div class="col-md-9">
                  <div id="Dzonea" class="dropzone" style="overflow-y: hidden;"></div>
                  </div>
                  </div>
















                  <style type="text/css">
		.removeImage {     position: absolute;
    /* right: 15px; */
    top: 0px;
    border-radius: 5px;
    padding: 5px 10px;
    cursor: pointer;
    background-color: red;
    color: white; }
		.upldPhoto{ text-align: center; margin-bottom: 20px; }

</style>
    <style>
    /* .dz-max-files-reached {background-color: red}; */

</style>


                <div class="form-row">
                @if (!empty($CurrentObj->id))

                 @php
                        $x=0;
                        $y=0;
                    @endphp

                <label class="control-label col-md-12">{{ trans('shops.images') }}</label>
                    <div class="row">
                 @foreach($CurrentObj->AllImage(['shop_images'])->where('deleted_at', NULL ) as $Image)



                    <div class="col-md-2 imgList">
                        <div class="upldPhoto">
                        <span title="Delete image" id="deleteRecord" data-id="{{ $Image->id }}" class="alert-danger pull-left removeImage">X</span>
                            <img src="{{$Image->lastMedia(config('col'))->getUrl()}}" alt="" width="100%">
                        </div>
                    </div>
 @php
                        $x++;
                    @endphp


                 @endforeach

                  @php
                        $y=10-$x;
                    @endphp
                    </div>


                @endif

                </div>











                    <div class="form-group">
               <label class="col-md-3 control-label ">{{trans("cites.location_on_the_map")}}</label>
               <div class="col-md-9">
                  <span>{{trans("general.to_illustrate_more_about_the_site_you_may_need_to_specify_the_location")}}</span>
                  <button class="btn btn-3d btn-primary initializeMarker" type="button">{{trans("general.get_my_current_location")}}</button>
               </div>
            </div>


                    <div class="form-group">
               <label class="col-md-3 control-label"></label>
               <div class="col-md-9">
                  <input id="latitude" name="latitude" value="{{$CurrentObj ? $CurrentObj->latitude :  '31.9013'}}" hidden="">
                  <input id="longitude" name="longitude" value="{{$CurrentObj ? $CurrentObj->longitude : '35.2092'}}" hidden="">
                  <style>
                     #map-canvas{
                     width: 100%;
                     height: 300px;
                     }
                  </style>
                  <div id="map-canvas"></div>
               </div>
            </div>








<hr>

               <div class="row">
                  <div class="col-auto col-md-12 center-block">
                     <div class="form-group">
                        <button class="btn btn-border col-md-12" id="btnSubmit" type="submit">{{ trans('general.submit') }}</button>
                     </div>
                  </div>
               </div>





            </div>
         </form>
      </div>
   </div>
</div>
@endsection
@section('script')
<script src="{{url('')}}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="{{url('assets')}}/global/plugins/dropzone/min/dropzone.min.js" type="text/javascript"></script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBw9YOQ6A0RrWGodIJ6WTZCat1rSwUpfb8&libraries=geometry"></script>
<script src="{{url('')}}/assets/global/scripts/app.js" type="text/javascript"></script>
<script src="{{url('')}}/assets/global/plugins/jquery-confirm/jquery-confirm.min.js"></script>

<script src="{{url('assets')}}/site/js/shop_edit.js" type="text/javascript"></script>

<script>
Dropzone.options.Dzonea = {
  maxFiles: {{ $y ?? 10 }},
  accept: function(file, done) {
    console.log("uploaded");
    done();
  },
  init: function() {
    this.on("maxfilesexceeded", function(file){
        alert("No more files please!");
    });
  }
};
</script>


@endsection
