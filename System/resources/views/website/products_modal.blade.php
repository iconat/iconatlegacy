<div class="product-single pb-50 clearfix">
    <!-- Single Products Slider Starts -->
    <div class="col-lg-4 col-sm-6 col-sm-offset-2 col-lg-offset-0 pt-50">
        <div class="prod-slider sync1">
            @isset($main_img_thumb)
                <div class="item">
                    <img src="{{$main_img_thumb->lastMedia(config('col'))->getUrl()}}" alt="">
                    <a href="{{$main_img_thumb->lastMedia(config('col'))->getUrl()}}" data-gal="prettyPhoto[prettyPhoto]"
                       title="Product" class="caption-link"><i class="arrow_expand"></i>
                    </a>
                </div>
            @endisset
            @foreach($All_Images as $Image)
                <div class="item">
                    <img src="{{$Image->lastMedia(config('col'))->getUrl()}}" alt="">
                    <a href="{{$Image->lastMedia(config('col'))->getUrl()}}" data-gal="prettyPhoto[prettyPhoto]"
                       title="Product" class="caption-link"><i class="arrow_expand"></i>
                    </a>
                </div>
            @endforeach

        </div>
        @if(count($All_Images) > 0)
        <div class="sync2">
            @isset($main_img_thumb)
            <div class="item">
                <a href="#"> <img src="{{$main_img_thumb->lastMedia("xs")->getUrl()}}" alt=""> </a>
            </div>
            @endisset
            @foreach($All_Images as $Image)
            <div class="item">
                <a href="#"> <img src="{{$Image->lastMedia("xs")->getUrl()}}" alt=""> </a>
            </div>
            @endforeach
        </div>
            @endif
    </div>
    <!-- Single Products Slider Ends -->

    <div class="col-lg-6 pt-50">
        <div class="product-content block-inline">
            @if(false)
            <div class="tag-rate">
                <span class="prod-tag tag-1">جديد</span> <span class="prod-tag tag-2">تنزيلات</span>
                <div class="rating">
                    <span class="star active"></span>
                    <span class="star active"></span>
                    <span class="star active"></span>
                    <span class="star active"></span>
                    <span class="star active"></span>
                    <span class="fsz-12"> بناءا على 25 رأي</span>
                </div>
            </div>
            @endif

            <div class="single-caption">
                <h3 class="section-title">
                     <span class="light-font"> {{$CurrentObj->Get_Trans(\App::getLocale(),'name')}} </span>
                </h3>
                <span class="divider-2"></span>
                <p class="price">
                    <strong class="clr-txt fsz-20">{{$CurrentObj->Get_Price_Depend_Currency($CurrentObj->Get_Price())}} <i class="fa fa-{{config("Default_currency_Icon")}}"></i> </strong>
                    <del class="light-font">{{$CurrentObj->Get_Price_Depend_Currency($CurrentObj->real_price)}} <i class="fa fa-{{config("Default_currency_Icon")}}"></i></del>
                </p>

                <div class="fsz-16">
                    <p>@php echo nl2br($CurrentObj->Get_Trans(\App::getLocale(),'text')); @endphp</p>
                </div>

                @if(false)
                <ul class="meta">
                    <li><strong> {{trans("products.product_code")}} </strong> <span>: {{$CurrentObj->product_code}}</span></li>
                    <li><strong> {{trans('categories.the_categorie')}} </strong> <span>:  {{$CurrentObj->Categorie->Get_Trans(\App::getLocale(),'name')}}</span></li>
                    <li class="tags-widget"><strong> التأشيراات </strong> <span>:  <a href="#">معسل</a> <a
                                    href="#">فواكه</a> <a href="#">اراجيل</a></span></li>
                </ul>
                @endif
                <div class="divider-full-1"></div>
                <div class="prod-btns" style="border: none;">
                    <div class="quantity">
                        <button class="btn minus QA" r="m"><i class="fa fa-minus-circle"></i></button>
                        <input id="qty" title="Qty"  placeholder="01" value="1" class="form-control qty" type="text">
                        <button class="btn plus QA" r="p"><i class="fa fa-plus-circle"></i></button>
                    </div>
                    <div class="sort-dropdown">
                        <button id="AddToCart" class="theme-btn btn" p="{{$CurrentObj->id}}"> <strong> {{trans("products.add_to_shopping_cart")}} </strong> </button>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>