@extends('layouts.website', ['title' => setting('about_us_title' , "")])
@section('content')
<div class="tt-breadcrumb">
   <div class="container">
      <ul>
         <li><a href="{{url('')}}">{{trans("general.home")}}</a></li>
         <li>{{trans('general.account')}}</li>
      </ul>
   </div>
</div>
<div id="tt-pageContent">
   <div class="container-indent">
      <div class="container container-fluid-custom-mobile-padding">
         <h1 class="tt-title-subpages noborder">{{trans('general.account')}}</h1>
         <div class="tt-shopping-layout">
            <h2 class="tt-title-border">{{trans('general.edit_my_account_data')}}</h2>



            <form id="UserEditForm" par1="{{$UserInfo->id  ?? NULL}}" lang="{{App::getLocale()}}" class="contact-form form-default form-horizontal">

                  <div class="form-group">
                     <label class="col-md-3 control-label">{{trans("general.username")}}</label>
                     <div class="col-md-9">
                        <input class="form-control" name="username" value="{{$UserInfo->username ?? NULL}}" placeholder="{{trans("general.username")}}" type="text">
                     </div>
                  </div>

                  <div class="form-group">
                     <label class="col-md-3 control-label">{{trans("general.name")}}</label>
                     <div class="col-md-9">
                        <input class="form-control" name="name" value="{{$UserInfo->name ?? NULL}}" placeholder="{{trans("general.name")}}" type="text">
                     </div>
                  </div>

                  <div class="form-group">
                     <label class="col-md-3 control-label">{{trans("general.email")}}</label>
                     <div class="col-md-9">
                        <input class="form-control" name="email" value="{{$UserInfo->email ?? NULL}}" placeholder="{{trans("general.email")}}" type="text">
                     </div>
                  </div>

                  <div class="form-group">
                     <label class="col-md-3 control-label">{{trans("general.password")}}</label>
                     <div class="col-md-9">
                        <input class="form-control" name="password" value="" placeholder="{{trans("general.password")}}" type="text">
                     </div>
                  </div>

                  <div class="form-group">
                     <label class="col-md-3 control-label">{{trans("general.shopname")}}</label>
                     <div class="col-md-9">
                        <input class="form-control" name="shopCompanyName" value="{{$UserInfo->InfoUser->shopCompanyName ?? NULL }}" placeholder="{{trans("general.shopCompanyName")}}" type="text">
                     </div>
                  </div>

                  <div class="form-group">
                     <label for="shopCompanyName" class="col-md-3 control-label">{{trans("general.city")}}</label>
                     <div class="col-md-9">
                        <select class="form-control" name="city">
                           <option value=""></option>
                           @foreach(App\Models\Cites::orderBy('id', 'desc')->get() as  $city)
                           <option value="{{$city->id}}" @if(!empty($UserInfo->InfoUser->city) && ($UserInfo->InfoUser->city == $city->id)) selected @endif >{{ $city->Get_Trans(App::getLocale(),'name') }}</option>
                           @endforeach
                        </select>
                     </div>
                  </div>

                  <div class="form-group">
                     <label for="shopAddress" class="col-md-3 control-label">{{trans("general.phone")}}</label>
                     <div class="col-md-9">
                        <input class="form-control" name="shopAddress" value="{{$UserInfo->InfoUser->shopAddress ?? NULL}}" placeholder="{{trans("general.shopAddress")}}" type="text">
                     </div>
                  </div>

                  <div class="form-group">
                     <label for="shopTown" class="col-md-3 control-label">TOWN / CITY *</label>
                     <div class="col-md-9">
                        <input type="text" class="form-control" name="shopTown" id="shopTown" value="{{$UserInfo->InfoUser->shopTown ?? NULL }}">
                     </div>
                  </div>

                  <div class="form-group">
                     <label for="shopState" class="col-md-3 control-label">STATE / COUNTY *</label>
                     <div class="col-md-9">
                        <input type="text" class="form-control" name="shopState" id="shopState" value="{{$UserInfo->InfoUser->shopState ?? NULL}}">
                     </div>
                  </div>

                  <div class="form-group">
                     <label for="shopPostCode" class="col-md-3 control-label">POSTCODE / ZIP *</label>
                     <div class="col-md-9">
                        <input type="text" class="form-control" name="shopPostCode" id="shopPostCode" value="{{$UserInfo->InfoUser->shopPostCode ?? NULL}}">
                     </div>
                  </div>

                  <div class="form-group">
                     <label for="shopPhone" class="col-md-3 control-label">{{trans("general.phone")}}</label>
                     <div class="col-md-9">
                        <input type="text" class="form-control" name="shopPhone" id="shopPhone" value="{{$UserInfo->InfoUser->shopPhone ?? NULL}}">
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-auto">
                        <div class="form-group">
                           <button class="btn btn-border" type="submit">{{trans("general.edit_info")}}</button>
                        </div>
                     </div>
                  </div>


\            </form>





         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
<script src="{{url('')}}/assets/site/js/customedit.js"></script>
@endsection
