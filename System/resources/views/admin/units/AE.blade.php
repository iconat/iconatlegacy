<div class="portlet light portlet-fit portlet-form bordered">
    <div class="portlet-body">
        <form id="DForm" par1="{{$CurrentObj->id  ?? NULL}}" class="form-horizontal">
            <div class="form-body">

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.state")}}</label>
                    <div class="col-md-9">
                        <div class="mt-checkbox-inline">
                            <label class="mt-checkbox mt-checkbox-outline">
                                <input type="checkbox" name="active" @if($CurrentObj && ($CurrentObj->active == 1)) checked @endif> {{trans("general.active")}}
                                <span></span>
                            </label>

                        </div>
                    </div>
                </div>


                @foreach ($AllLangs as $alt)
                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.name_of_:name",["name"=>trans('units.the_unit')])}} - {{trans('general.'.$alt['locale'])}}</label>
                    <div class="col-md-9">
                        <input type="text" dir="{{$alt['dir']}}" name="{{$alt['locale']}}[name]" class="form-control" value="@if($CurrentObj){{$CurrentObj->Get_Trans($alt['locale'],'name') ?? NULL}}@endif">
                    </div>
                </div>
                @endforeach

                @foreach ($AllLangs as $alt)
                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("units.fraction")}} - {{trans('general.'.$alt['locale'])}}</label>
                    <div class="col-md-9">
                        <input type="text" dir="{{$alt['dir']}}" name="{{$alt['locale']}}[fraction]" class="form-control" value="@if($CurrentObj){{$CurrentObj->Get_Trans($alt['locale'],'fraction') ?? NULL}}@endif">
                    </div>
                </div>
                @endforeach


                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("units.fractions_in_original")}}</label>
                    <div class="col-md-2">
                        <input type="text" name="fractions_in_original" value="{{$CurrentObj->fractions_in_original ?? NULL}}" class="form-control">
                    </div>
                </div>

            </div>
        </form>
    </div>
</div>