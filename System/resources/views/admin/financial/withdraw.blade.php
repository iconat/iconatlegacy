@extends('admin.app', ['title' => trans('financial.financial_reports')])

@section('content')

   <!-- BEGIN PAGE BAR -->
   <div class="page-bar">
      <ul class="page-breadcrumb">
         <li>
            <a href="{{url("My")}}">{{trans("general.home")}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
            <span>{{ trans('financial.financial_reports') }}</span>
         </li>
      </ul>
   </div>
   <!-- END PAGE BAR -->
   <!-- BEGIN PAGE BASE CONTENT -->
   <div class="row margin-top-15">
      <div class="col-md-12">
         @php echo setting('financial_reporting_letter' , ""); @endphp
      </div>
      <div class="col-md-12 text-center">
         @if(empty($Withdraw))
         <button id="Withdraw" Dtitle="{{trans("financial.withdraw_personal_profit")}}" Dcontent="{{trans("financial.are_you_sure_you_want_to_withdraw_your_earnings")}}" class="btn blue">
            <i class="fa fa fa-hand-grab-o"></i> {{trans("financial.withdraw_personal_profit")}}
         </button>
         @else
            <div class="alert alert-info margin-top-10">
               <strong>{{trans("financial.you_have_requested_a_withdrawal_of_earnings,_your_order_is_currently_under_review_by_the_administration")}}</strong>
            </div>
         @endif
      </div>
   </div>
   <!-- END PAGE BASE CONTENT -->



@endsection



@section('script')

   <script src="{{url('assets/js/Financial_Reports.js')}}" type="text/javascript"></script>

@endsection