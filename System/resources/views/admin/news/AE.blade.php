<div class="portlet light portlet-fit portlet-form bordered">
    <div class="portlet-body">
        <form id="DForm" par1="{{$CurrentObj->id  ?? NULL}}" class="form-horizontal">
            <div class="form-body">
                @foreach ($AllLangs as $alt)
                    <div class="form-group">
                        <label class="col-md-3 control-label">{{trans("general.title_of_name",["name"=>trans('news.the_new')])}} - {{trans('general.'.$alt['locale'])}}</label>
                        <div class="col-md-9">
                            <input placeholder="{{trans("general.title_of_name",["name"=>trans('news.the_new')])}} - {{trans('general.'.$alt['locale'])}}" type="text" dir="{{$alt['dir']}}" name="{{$alt['locale']}}[name]" class="form-control" value="@if($CurrentObj){{$CurrentObj->Get_Trans($alt['locale'],'name') ?? NULL}}@endif">
                        </div>
                    </div>
                @endforeach

                @foreach ($AllLangs as $alt)
                    <div class="form-group">
                        <label class="col-md-3 control-label">{{trans("general.brief_description")}} - {{trans('general.'.$alt['locale'])}}</label>
                        <div class="col-md-9">
                            <textarea dir="{{$alt['dir']}}" name="{{$alt['locale']}}[description]" class="form-control" placeholder="{{trans("general.brief_description")}} - {{trans('general.'.$alt['locale'])}}">@if($CurrentObj){{$CurrentObj->Get_Trans($alt['locale'],'description') ?? NULL}}@endif</textarea>
                        </div>
                    </div>
                @endforeach

                @foreach ($AllLangs as $alt)
                    <div class="form-group">
                        <label class="col-md-3 control-label">{{trans("general.text_of_:name",["name"=>trans('news.the_new')])}} - {{trans('general.'.$alt['locale'])}}</label>
                        <div class="col-md-9">
                            <textarea dir="{{$alt['dir']}}" name="{{$alt['locale']}}[text]" class="form-control ckeditor" placeholder="{{trans("general.text_of_:name",["name"=>trans('products.the_product')])}} - {{trans('general.'.$alt['locale'])}}">@if($CurrentObj){{$CurrentObj->Get_Trans($alt['locale'],'text') ?? NULL}}@endif</textarea>
                        </div>
                    </div>
                @endforeach



                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.image_of_name",["name"=>trans('news.the_new')])}}</label>
                    <div class="col-md-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 200px;">
                                <img src="{{$main_img_thumb ?? url(setting('default_product_image' , ""))}}" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"> </div>
                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new"> {{trans('general.select_image')}} </span>
                                    <span class="fileinput-exists"> {{trans('general.change')}} </span>
                                    <input type="file" name="main_img"> </span>
                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{trans('general.remove')}} </a>
                            </div>
                        </div>
                        <span class="help-block">{{trans("general.the_image_will_be_crop_with_:width_pixels_width_and_:height_pixels_height",['width' => 650,'height' => 650])}}</span>
                    </div>
                </div>





            </div>
        </form>
    </div>
</div>