@extends('admin.app', ['title' => $Obj->{'title_'.\App::getLocale()}.' | '. trans("news.news_of_company")])
@section('css')
   <link href="{{url('')}}/assets/pages/css/blog-rtl.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')

   <!-- BEGIN PAGE BAR -->
   <div class="page-bar">
      <ul class="page-breadcrumb">
         <li>
            <a href="{{url("My")}}">{{trans("general.home")}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
            <a href="{{url("Show/News")}}"><span>{{trans("news.news_of_company")}}</span></a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
            <span>{{ $Obj->{'title_'.\App::getLocale()} }}</span>
         </li>
      </ul>
   </div>
   <!-- END PAGE BAR -->
   <!-- BEGIN PAGE BASE CONTENT -->
   <div class="blog-page blog-content-2  margin-top-15">
      <div class="row">
         <div class="col-lg-12">
            <div class="blog-single-content bordered blog-container">
               <div class="blog-single-head">
                  <h1 class="blog-single-head-title">{{ $Obj->{'title_'.\App::getLocale()} }}</h1>
                  <div class="blog-single-head-date">
                     <i class="icon-calendar font-blue"></i>
                     <a href="javascript:;">{{ date('Y/m/d',strtotime($Obj->created_at)) }}</a>
                  </div>
               </div>

               <div class="blog-single-desc">
                  @php echo $text; @endphp
               </div>

            </div>
         </div>
      </div>
   </div>
   <!-- END PAGE BASE CONTENT -->




@endsection



