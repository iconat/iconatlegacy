<div class="portlet light portlet-fit portlet-form bordered">
    <div class="portlet-body">
        <form id="DForm" par1="{{$CurrentObj->id  ?? NULL}}" class="form-horizontal">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.state")}}</label>
                    <div class="col-md-9">
                        <select class="form-control form-filter select2me" name="state">
                            <option value=""></option>
                            <option value="effective" @if($CurrentObj && ($CurrentObj->state == 1)) selected @endif>{{trans('shares.effective_share')}}</option>
                            <option value="ineffective" @if($CurrentObj && ($CurrentObj->state == NULL)) selected @endif>{{trans('shares.share_ineffective')}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("business_fields.field")}}</label>
                    <div class="col-md-9">
                        <select class="form-control form-filter select2me" name="field_id">
                            <option value=""></option>
                            @foreach(App\Models\Classes::orderBy('id', 'desc')->get() as  $Field)
                                <option value="{{$Field->id}}" @if(!empty($CurrentObj->field_id) && ($CurrentObj->field_id == $Field->id)) selected @endif >{{ $Field->name_ar }}</option>
                            @endforeach
                        </select>
                        @if(!empty($CurrentObj->field_id_need_review))
                            <span class="help-block">{{trans("general.you_have_requested_a_change_of_value_name_to_value",['name'=>trans("shares.share_amount"),'value'=>$CurrentObj->FieldNeeded->name_ar])}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("business_fields.investor")}}</label>
                    <div class="col-md-9">
                        @if(empty($CurrentObj->invs_id))
                        <select class="form-control form-filter select2me" name="invs_id">
                            <option value=""></option>
                                @foreach(App\Models\Invs::whereNotNull('user_id')->orderBy('id', 'desc')->get() as  $Invs)
                                    <option value="{{$Invs->id}}" @if(!empty($CurrentObj->invs_id) && ($CurrentObj->invs_id == $Invs->id)) selected @endif >{{ $Invs->get_Full_name() }}</option>
                                @endforeach
                        </select>
                        @else
                        <p class="form-control-static font-purple-seance">{{$CurrentObj->Investor->get_Full_name()}}</p>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("shares.share_amount")}}</label>
                    <div class="col-md-9">
                        <input type="text" name="share_amount" value="{{$CurrentObj->share_amount ?? NULL}}" class="form-control">
                        @if(!empty($CurrentObj->share_amount_need_review))
                        <span class="help-block">{{trans("general.you_have_requested_a_change_of_value_name_to_value",['name'=>trans("shares.share_amount"),'value'=>$CurrentObj->share_amount_need_review])}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("shares.share_number")}}</label>
                    <div class="col-md-9">
                        <input type="text" name="share_number" value="{{$CurrentObj->share_number ?? NULL}}" class="form-control">
                        @if(!empty($CurrentObj->share_number_need_review))
                            <span class="help-block">{{trans("general.you_have_requested_a_change_of_value_name_to_value",['name'=>trans("shares.share_number"),'value'=>$CurrentObj->share_number_need_review])}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("shares.share_date")}}</label>
                    <div class="col-md-9">
                        <input type="text" name="share_date" value="{{$CurrentObj->share_date ?? NULL}}" class="form-control date-picker">
                        @if(!empty($CurrentObj->share_date_need_review))
                            <span class="help-block">{{trans("general.you_have_requested_a_change_of_value_name_to_value",['name'=>trans("shares.share_date"),'value'=>$CurrentObj->share_date_need_review])}}</span>
                        @endif
                    </div>
                </div>

            </div>
        </form>
    </div>
</div>