@extends('admin.app', ['title' => trans('general.control_of_name_contents',['name' => $title])])

@section('content')

   <!-- BEGIN PAGE BAR -->
   <div class="page-bar">
      <ul class="page-breadcrumb">
         <li>
            <a href="{{url("My")}}">{{trans("general.home")}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
            <a href="{{url("/Manage/Shop_Categories")}}">
            {{trans("categories.categories")}}
            </a>
            <i class="fa fa-circle"></i>
         </li>
         @if($shop_category && count($shop_category->parents))
            @foreach($shop_category->getParentsAttribute() as $p)
            <li>
               <a href="{{url("/Manage/Shop_Categories?M=".$p->id)}}">
                  {{ $p->Get_Trans(\App::getLocale(),"name") }}
               </a>
               <i class="fa fa-circle"></i>
            </li>
            @endforeach
         @elseif(!$shop_category)
            <li>
               <span>
                  {{ trans("general.view_all_:name",["name" => trans("categories.main_categories")]) }}
               </span>
            </li>
         @endif
         @if($shop_category)
         <li>
               <span>
                  {{ $shop_category->Get_Trans(\App::getLocale(),"name") }}
               </span>
         </li>
         @endif
      </ul>
   </div>
   <!-- END PAGE BAR -->

   <div class="breadcrumbs">

      <h1>{{$title}}</h1>

   </div>

   <div class="row">

      <div class="col-md-12">
         <div class="portlet light portlet-fit portlet-datatable bordered">

            <div class="portlet-title">

               <div class="caption">

                  <i class="icon-eye font-dark"></i>

                  <span class="caption-subject font-dark sbold uppercase">{{trans("general.browse_and_search_in_name",array("name" => $title))}}</span>

               </div>

               <div class="actions">

                  <div class="btn-group">
                     @if($Add)
                     <button  par1="Add" par2="{{$shop_category->id ?? NULL}}" class="btn btn-sm green AEObject">
                        <i class="fa fa-plus"></i> {{trans("general.add_name_element",array("name" => trans("shop_categories.categorie")))}}</button>
                     @endif

                  </div>

               </div>

            </div>

            <div class="portlet-body">

               <div class="table-container">

                  <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax" par1="{{$shop_category->id ?? NULL}}">

                     <thead>

                     <tr role="row" class="heading">

                        <th class="text-center" width="30%">{{trans("general.title_of_name",["name"=>trans('shop_categories.the_categorie')])}}</th>
                        <th class="text-center" width="15%">{{trans("general.state")}}</th>
                        <th class="text-center" width="20%">{{trans("general.created_at")}}</th>
                        <th class="text-center" width="30%">{{trans("general.control")}}</th>

                     </tr>

                     <tr role="row" class="filter">

                        <td class="text-center" style="padding: 8px;">

                           <input type="text" class="form-control form-filter input-sm" name="name">

                        </td>

                        <td class="text-center">
                           <select class="form-control form-filter select2me" name="active">
                              <option value=""></option>
                              <option value="active">{{trans("general.active")}}</option>
                              <option value="not_active">{{trans("general.not_active")}}</option>
                           </select>
                        </td>


                        <td class="text-center">
                           <div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                              <input type="text" class="form-control form-filter input-sm" readonly name="date_from" placeholder="{{trans('general.from')}}">
                              <span class="input-group-btn">
                                  <button class="btn btn-sm default"
                                          type="button">
                                      <i class="fa fa-calendar"></i>
                                  </button>
                              </span>
                           </div>
                           <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                              <input type="text" class="form-control form-filter input-sm" readonly name="date_to" placeholder="{{trans('general.to')}}">
                              <span class="input-group-btn">
                                  <button class="btn btn-sm default"
                                          type="button">
                                      <i class="fa fa-calendar"></i>
                                  </button>
                              </span>
                           </div>
                        </td>




                        <td class="text-center">

                           <div class="margin-bottom-5">

                              <button class="btn btn-sm green btn-outline filter-submit margin-bottom">

                                 <i class="fa fa-search"></i> {{trans("general.search")}}</button>

                           </div>

                           <button class="btn btn-sm red btn-outline filter-cancel">

                              <i class="fa fa-times"></i> {{trans("general.cancel_filters")}}</button>

                        </td>

                     </tr>

                     </thead>

                     <tbody> </tbody>

                  </table>

               </div>

            </div>

         </div>
      </div>

   </div>



@endsection



@section('script')
<script src="{{url('assets')}}/global/plugins/ckeditor4/ckeditor.js" type="text/javascript"></script>
<script src="{{url('assets')}}/js/Shop_Categories.js" type="text/javascript"></script>

@endsection
