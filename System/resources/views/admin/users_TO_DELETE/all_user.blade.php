@extends('admin.app')

@section('content')


<!-- BEGIN BREADCRUMBS -->

<div class="breadcrumbs">

   <h1>مستخدمي النظام</h1>




</div>

<!-- END BREADCRUMBS -->
<div class="portlet light portlet-fit portlet-datatable bordered">

   <div class="portlet-title">

      <div class="caption">

         <i class="icon-settings font-dark"></i>

         <span class="caption-subject font-dark sbold uppercase">التحكم بمستخدمي النظام</span>

      </div>

      <div class="actions">

         <div class="btn-group">

               <button data-url="{{route('AE_USERS')}}" class="btn btn-sm green AEObject " data-toggle="modal">

            <i class="fa fa-check"></i>اضافه مستخدم</button>

         </div>

      </div>

   </div>

   <div class="portlet-body">

      <div class="table-container">

        



        

         <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">

            <thead>

               <tr role="row" class="heading">

                  <th class="text-center" width="27%">الاسم</th>
                 



                    <th class="text-center" width="15%">الايميل</th>


                  <th class="text-center" width="15%">  نشاط</th>

               </tr>

               <tr role="row" class="filter">

                  <td class="text-center" colspan="1">

                     <input type="text" class="form-control form-filter input-sm" name="name"> 

                  </td>
               
                
                  <td class="text-center">

                     <input type="text" class="form-control form-filter input-sm" name="email"> 

                  </td>
              
                  
                  

                  <td class="text-center">

                     <div class="margin-bottom-5">

                        <button class="btn btn-sm green btn-outline filter-submit margin-bottom">

                        <i class="fa fa-search"></i>  بحث</button>

                     </div>

                     <button class="btn btn-sm red btn-outline filter-cancel">

                     <i class="fa fa-times"></i>  اعادة توجيه</button>

                  </td>

               </tr>

            </thead>

            <tbody> </tbody>

         </table>

      </div>

   </div>

</div>


@endsection



@section('script')

<script src="{{url('')}}/assets/global/scripts/datatable.js" type="text/javascript"></script>

<script src="{{url('')}}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>

<script src="{{url('')}}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>

<script src="{{url('')}}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>

<script src="{{url('admin')}}/js/viewalluser.js" type="text/javascript"></script>

@endsection