<div class="portlet light portlet-fit portlet-form bordered">
    <div class="portlet-body">
        <form id="DForm" par1="{{$CurrentObj->id  ?? NULL}}" par2="{{$par2}}" class="form-horizontal">
            <div class="form-body">

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.order")}} / {{trans("general.paragraph_no")}} </label>
                    <div class="col-md-2">
                        <input type="text" name="order" value="{{$CurrentObj->order ?? NULL}}" class="form-control">
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.title")}}</label>
                    <div class="col-md-9">
                        <input type="text" name="title" value="{{$CurrentObj->title ?? NULL}}" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.image_of_name",["name"=>trans('classes.the_paragraph')])}}</label>
                    <div class="col-md-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 300px; height: 420px;">
                                <img src="{{$image ?? url(setting('default_image' , ""))}}" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 300px; max-height: 420px;"> </div>
                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new"> {{trans('general.select_image')}} </span>
                                    <span class="fileinput-exists"> {{trans('general.change')}} </span>
                                    <input type="file" name="img"> </span>
                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{trans('general.remove')}} </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("classes.audio_file")}}</label>
                    <div class="col-md-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="input-group input-large">
                                <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                    <span class="fileinput-filename"> </span>
                                </div>
                                <span class="input-group-addon btn default btn-file">
                                    <span class="fileinput-new"> {{trans('general.select_file')}} </span>
                                    <span class="fileinput-exists"> {{trans('general.change')}} </span>
                                    <input type="file" name="audio_file" accept=".mp3,audio/*">
                                </span>
                                <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> {{trans('general.remove')}} </a>
                            </div>
                        </div>
                        @php
                            if(!empty($CurrentObj)){
                                $media = $CurrentObj->lastMedia('paragraph_as_audio');
                            }
                        @endphp
                        @isset($media)
                            <div id="List_File" class="help-block">
                                <div style="display: inline-block;">
                                    {{trans("classes.to_listen_to_the_current_file")}}:
                                </div>
                                <div style="display: inline-block;">
                                    <audio controls>
                                        <source src="{{$media->getUrl()}}" type="audio/mpeg">
                                        {{trans("general.your_browser_does_not_support_the_audio_element")}}
                                    </audio>
                                </div>
                                <div style="display: inline-block;">
                                    <button type="button" par1="{{$media->id}}" Dtitle="{{trans("general.are_you_sure")}}" Dcontent="{{trans("general.are_you_sure_you_want_to_delete_name", array('name' => trans("classes.audio_file")))}}"  class="Delete_File btn btn-sm btn-outline red"><i class="fa fa-remove"></i>  {{trans("general.delete")}} </button>
                                </div>
                            </div>

                        @endisset
                    </div>
                </div>

                <div class="portlet box green-meadow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-pencil"></i>{{trans("classes.worksheets")}} </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="mt-repeater" style="margin: 10px 0;">
                            <div data-repeater-list="Worksheets">
                                @php
                                $Item_View = view('admin.classes.worksheets_repeater');
                                $Item_View->with('Items_Of_worksheets', $Items_Of_worksheets);
                                !empty($CurrentObj)?$Item_View->with('CurrentObj', $CurrentObj):NULL;
                               echo $Item_View;


                                    foreach ($Items_Of_worksheets as $Item) {
                                        $Item_View = view('admin.classes.worksheets_repeater');
                                        $Item_View->with('Items_Of_worksheets', $Items_Of_worksheets);
                                        $Item_View->with('Item', $Item);
                                        echo $Item_View;
                                    }
                                @endphp
                            </div>
                            <button type="button" data-repeater-create class="btn blue-steel mt-repeater-add col-md-3 col-md-offset-9">
                                <i class="fa fa-plus"></i> {{trans("general.add_name_element",["name" => trans("classes.worksheet")])}}
                            </button>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.text_of_:name",["name"=>trans('classes.the_paragraph')])}}</label>
                    <div class="col-md-9">
                        <textarea name="text" class="form-control ckeditor">{{$text ?? NULL}}</textarea>
                    </div>
                </div>

            </div>
        </form>
    </div>
</div>