<div class="portlet light portlet-fit portlet-form bordered">
    <div class="portlet-body">
        <form id="DForm" par1="{{$CurrentObj->id  ?? NULL}}" class="form-horizontal">
            <div class="form-body">

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.name")}}</label>
                    <div class="col-md-9">
                        <input type="text" name="name" value="{{$CurrentObj->name ?? NULL}}" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("classes.users_guide")}}</label>
                    <div class="col-md-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="input-group input-large">
                                <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                    <span class="fileinput-filename"> </span>
                                </div>
                                <span class="input-group-addon btn default btn-file">
                                    <span class="fileinput-new"> {{trans('general.select_file')}} </span>
                                    <span class="fileinput-exists"> {{trans('general.change')}} </span>
                                    <input type="file" name="users_guide" accept="application/pdf">
                                </span>
                                <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> {{trans('general.remove')}} </a>
                            </div>
                        </div>
                        @php
                            if(!empty($CurrentObj)){
                                $media = $CurrentObj->lastMedia('users_guide');
                            }
                        @endphp
                        @isset($media)
                            <span class="help-block">{{trans("general.to_view_the_current_file")}} <a class="btn btn-circle btn-xs grey-mint" target="_blank" href="{{url('Users_Guide/BPDF/' . $CurrentObj->id)}}">{{trans("general.click_here")}}</a></span>
                        @endisset
                    </div>
                </div>


            </div>
        </form>
    </div>
</div>