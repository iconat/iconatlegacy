<div class="portlet light portlet-fit portlet-form bordered">
    <div class="portlet-body">
        <form id="DForm" par1="{{$CurrentObj->id  ?? NULL}}" par2="{{$CurrentObj->user_id  ?? NULL}}" class="form-horizontal">
            <div class="form-body">

                 <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.state")}}</label>
                    <div class="col-md-9">
                        <div class="mt-checkbox-inline">
                            <label class="mt-checkbox mt-checkbox-outline">
                                <input type="checkbox" name="active" @if((empty($CurrentObj)) ||($CurrentObj && ($CurrentObj->active == 1))) checked @endif> {{trans("general.active")}}
                                <span></span>
                            </label>

                        </div>
                    </div>
                </div>





                 <div class="form-group">
                        <label class="col-md-3 control-label">{{ trans('shops.the_shop_cat') }}</label>
                        <div class="col-md-9">
                            <select class="select2me" name="shop_categories">
                                <option class=""></option>
                                @foreach(\App\Models\Shop_Categories::where('active',1)->get() as $value)


                                    <option @if(count($value->children) > 0) disabled=""   style="color: red;" @endif value="{{$value->id}}" @if($CurrentObj && ($CurrentObj->shop_categories == $value->id)) selected @endif >{{$value->Get_Trans(App::getLocale(),'name')}}</option>


                                @endforeach
                            </select>
                        </div>
                    </div>







                    <div class="form-group">
                        <label class="col-md-3 control-label">{{ trans('shops.the_shop_city') }}</label>
                        <div class="col-md-9">
                            <select class="select2me" name="city">
                                <option class=""></option>
                                @foreach(\App\Models\Cites::where(function ($query) use ($CurrentObj) {
                                    //To Select if item marked as not active after select this...
                                    $query->where('active',1);
                                    if($CurrentObj && (!empty($CurrentObj->city))){
                                        $query->orWhere('id',$CurrentObj->city);
                                    }
                                    })->get() as $value)

                                    <option value="{{$value->id}}" @if($CurrentObj && ($CurrentObj->city == $value->id)) selected @endif >{{$value->Get_Trans(App::getLocale(),'name')}}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>

                         <div class="form-group">
                        <label class="col-md-3 control-label">{{ trans('general.dealer_name') }}</label>
                        <div class="col-md-9">
                            <select class="select2me" name="user_id">

                                @if(!empty($CurrentObj->id) && $CurrentObj->owner_shop)
                                <option value="{{ $CurrentObj->user_id }}" class="">{{ $CurrentObj->owner_shop->name }}
                                    </option>
                                     <option value="no" class="">لا يوجد يوزر
                                    </option>
                                    @else
                                    <option value="no" class="">لا يوجد يوزر
                                    </option>
                                 @endif

@php
$x=array();
foreach(\App\Models\Shops::get() as $DealerName){
array_push($x, $DealerName->user_id);
}
 @endphp


                                @foreach(\App\User::where('active' , 1)->get() as $value)

                                @if(!in_array($value->id, $x)){
                                    <option value="{{$value->id}}" @if($CurrentObj && ($CurrentObj->user_id == $value->id)) selected @endif >{{ $value->name }}</option>
                                    @endif



                                @endforeach

                            </select>
                        </div>
                    </div>



                    <hr>





              <div class="form-group">

                    <label class="col-md-3 control-label">{{trans("general.sale")}}</label>
                    <div class="col-md-9">
                        <input type="text" name="sale" value="{{$CurrentObj->sale ?? NULL}}" class="form-control">


                        <span class="help-block" style="color: red;">نسبة الخصم ان وجدت   مثال  (40 , 50 )  ليست اجباري</span>
                    </div>
                 </div>
                 <hr>


    <div class="form-group">
                <label class="control-label col-md-3">{{trans("general.order")}}</label>
                <div class="col-md-9">
                    <select class="bs-select form-control" name="order">
                        <option value=""></option>
                        @for ($x = 1; $x <= 100; $x++)
                        <option value="{{$x}}" @if(!empty($CurrentObj->order) && ($CurrentObj->order == $x)) selected @endif>{{$x}}</option>
                        @endfor

                    </select>
                </div>
            </div>



                @foreach ($AllLangs as $alt)
                    <div class="form-group">
                        <label class="col-md-3 control-label">{{trans("general.name_of_:name",["name"=>trans('shops.the_shop')])}} - {{trans('general.'.$alt['locale'])}}</label>
                        <div class="col-md-9">
                            <input placeholder="{{trans("general.name_of_:name",["name"=>trans('shops.the_shop')])}} - {{trans('general.'.$alt['locale'])}}" type="text" dir="{{$alt['dir']}}" name="{{$alt['locale']}}[name]" class="form-control" value="@if($CurrentObj){{$CurrentObj->Get_Trans($alt['locale'],'name') ?? NULL}}@endif">
                        </div>
                    </div>
                @endforeach

                @foreach ($AllLangs as $alt)
                    <div class="form-group">
                        <label class="col-md-3 control-label">{{trans("general.text_of_:name",["name"=>trans('shops.the_shop')])}} - {{trans('general.'.$alt['locale'])}}</label>
                        <div class="col-md-9">
                            <textarea dir="{{$alt['dir']}}" name="{{$alt['locale']}}[text]" class="form-control" placeholder="{{trans("general.text_of_:name",["name"=>trans('shops.the_shop')])}} - {{trans('general.'.$alt['locale'])}}">@if($CurrentObj){{$CurrentObj->Get_Trans($alt['locale'],'text') ?? NULL}}@endif</textarea>
                        </div>
                    </div>
                @endforeach


                @foreach ($AllLangs as $alt)
                    <div class="form-group">
                        <label class="col-md-3 control-label">{{ trans('shops.the_shop_address') }} - {{trans('general.'.$alt['locale'])}}</label>
                        <div class="col-md-9">
                            <input placeholder="{{ trans('shops.the_shop_address') }} - {{trans('general.'.$alt['locale'])}}" type="text" dir="{{$alt['dir']}}" name="{{$alt['locale']}}[address]" class="form-control" value="@if($CurrentObj){{$CurrentObj->Get_Trans($alt['locale'],'address') ?? NULL}}@endif">
                        </div>
                    </div>
                @endforeach











                <div class="form-group">


                    <label class="col-md-3 control-label">{{trans("shops.shop_code")}}</label>
                    <div class="col-md-3">
                        <input type="text" disabled value="{{$CurrentObj->shop_code ?? trans("general.automatically_generated")}}" class="form-control">
                    </div>

                       <label class="col-md-3 control-label">{{trans("general.mobile")}}</label>
                    <div class="col-md-3">
                        <input type="text" name="mobile" value="{{$CurrentObj->mobile ?? NULL}}" class="form-control">
                    </div>






                </div>




                 <div class="form-group">



                     <label class="col-md-3 control-label">{{trans("general.facebook")}}</label>
                    <div class="col-md-3">
                        <input type="text" name="facebook" value="{{$CurrentObj->facebook ?? NULL}}" class="form-control">
                    </div>

                     <label class="col-md-3 control-label">{{trans("general.instagram")}}</label>
                    <div class="col-md-3">
                        <input type="text" name="instagram" value="{{$CurrentObj->instagram ?? NULL}}" class="form-control">
                    </div>

                 </div>

                 <div class="form-group">

                     <label class="col-md-3 control-label">{{trans("general.snapchat")}}</label>
                    <div class="col-md-3">
                        <input type="text" name="snapchat" value="{{$CurrentObj->snapchat ?? NULL}}" class="form-control">
                    </div>


                     <label class="col-md-3 control-label">{{trans("general.twitter")}}</label>
                    <div class="col-md-3">
                        <input type="text" name="twitter" value="{{$CurrentObj->twitter ?? NULL}}" class="form-control">
                    </div>


                 </div>

                 <div class="form-group">

                    <label class="col-md-3 control-label">{{trans("general.linkedin")}}</label>
                    <div class="col-md-3">
                        <input type="text" name="linkedin" value="{{$CurrentObj->linkedin ?? NULL}}" class="form-control">
                    </div>

                    <label class="col-md-3 control-label">{{trans("general.gmail")}}</label>
                    <div class="col-md-3">
                        <input type="text" name="gmail" value="{{$CurrentObj->gmail ?? NULL}}" class="form-control">
                    </div>

                 </div>





                  <div class="form-group">

                    <label class="col-md-3 control-label">{{trans("general.video")}}</label>
                    <div class="col-md-9">
                        <input type="text" name="video" value="{{$CurrentObj->video ?? NULL}}" class="form-control">
                    </div>

                 </div>



















                <div class="form-group">
                    <label class="control-label col-md-3">{{ trans('shops.logo') }}</label>
                    <div class="col-md-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: {{$main_logo_thumb_width_xs}}px; height: 100%;">
                            <img src="{{$main_logo_img_thumb ?? url(setting('default_product_image' , ""))}}" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: {{$main_logo_thumb_width_xs}}px; max-height: {{$main_logo_thumb_height_xs}}px;"> </div>
                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new"> {{trans('general.select_image')}} </span>
                                    <span class="fileinput-exists"> {{trans('general.change')}} </span>
                                    <input type="file" name="main_logo_shop"> </span>
                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{trans('general.remove')}} </a>
                            </div>
                        </div>
                        <span class="help-block">{{trans("general.the_image_will_be_crop_with_:width_pixels_width_and_:height_pixels_height",['width' => $main_logo_thumb_width_lg,'height' => $main_logo_thumb_height_lg])}}</span>
                    </div>
                </div>







                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.image_of_name",["name"=>trans('shops.the_shop')])}}</label>
                    <div class="col-md-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: {{$main_thumb_width_xs}}px; height: 100%;">
                                <img src="{{$main_img_thumb ?? url(setting('default_product_image' , ""))}}" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: {{$main_thumb_width_xs}}px; max-height: {{$main_thumb_height_xs}}px;"> </div>
                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new"> {{trans('general.select_image')}} </span>
                                    <span class="fileinput-exists"> {{trans('general.change')}} </span>
                                    <input type="file" name="main_shop"> </span>
                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{trans('general.remove')}} </a>
                            </div>
                        </div>
                        <span class="help-block">{{trans("general.the_image_will_be_crop_with_:width_pixels_width_and_:height_pixels_height",['width' => $main_thumb_width_lg,'height' => $main_thumb_height_lg])}}</span>
                    </div>
                </div>

                <div class="form-group">
                      <label class="control-label col-md-3">{{ trans('shops.images') }}</label>
                    <div class="col-md-9">
                    <div id="Dzone" class="dropzone"></div>
                    </div>
                </div>

<style type="text/css">
		.removeImage {  position: absolute; right: 15px; top: 0px; border-radius: 50%; padding: 5px; cursor: pointer; }
		.upldPhoto{ text-align: center; margin-bottom: 20px; }
	</style>

                <div class="form-group">
                @if (!empty($CurrentObj->id))

                <label class="control-label col-md-3">{{ trans('shops.images') }}</label>
                    <div class="col-md-9">

                 @foreach($CurrentObj->AllImage(['shop_images'])->where('deleted_at', NULL ) as $Image)



                    <div class="col-sm-2 imgList">
                        <div class="upldPhoto">
                        <span title="Delete image" id="deleteRecord" data-id="{{ $Image->id }}" class="alert-danger pull-left removeImage">X</span>
                            <img src="{{$Image->lastMedia(config('col'))->getUrl()}}" alt="" >
                        </div>
                    </div>

                 @endforeach

                    </div>


                @endif

                </div>













                <div class="form-group">
               <label class="col-md-3 control-label ">{{trans("cites.location_on_the_map")}}</label>
               <div class="col-md-9">
                  <span>{{trans("general.to_illustrate_more_about_the_site_you_may_need_to_specify_the_location")}}</span>
                  <button class="btn btn-3d btn-primary initializeMarker" type="button">{{trans("general.get_my_current_location")}}</button>
               </div>
            </div>

            <div class="form-group">
               <label class="col-md-3 control-label"></label>
               <div class="col-md-9">
                  <input id="latitude" name="latitude" value="{{$CurrentObj ? $CurrentObj->latitude :  '31.9013'}}" hidden="">
                  <input id="longitude" name="longitude" value="{{$CurrentObj ? $CurrentObj->longitude : '35.2092'}}" hidden="">
                  <style>
                     #map-canvas{
                     width: 100%;
                     height: 300px;
                     }
                  </style>
                  <div id="map-canvas"></div>
               </div>
            </div>













            </div>
        </form>
    </div>
</div>
