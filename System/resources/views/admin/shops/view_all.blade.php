@extends('admin.app', ['title' => trans('general.control_of_name_contents',['name' => trans("shops.shops")])])

@section('css')
    <link href="{{url('')}}/assets/global/plugins/dropzone/min/dropzone.min.css" rel="stylesheet" type="text/css" />
     <!--   <link href="{{url('')}}/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
        <link href="{{url('')}}/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.css" rel="stylesheet" type="text/css" /> -->
@endsection

@section('content')

   <!-- BEGIN PAGE BAR -->
   <div class="page-bar">
      <ul class="page-breadcrumb">
         <li>
            <a href="{{url("My")}}">{{trans("general.home")}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
            <span>{{trans("shops.shops")}}</span>
         </li>
      </ul>
   </div>
   <!-- END PAGE BAR -->

   <div class="breadcrumbs">

      <h1>{{trans("shops.shops")}}</h1>




   </div>

   <div class="row">

      <div class="col-md-12">
         <div class="portlet light portlet-fit portlet-datatable bordered">

            <div class="portlet-title">

               <div class="caption">

                  <i class="icon-eye font-dark"></i>

                  <span class="caption-subject font-dark sbold uppercase">{{trans("general.browse_and_search_in_name",array("name" => trans("shops.shops")))}}</span>

               </div>

               <div class="actions">

                  <div class="btn-group">
                     @if($Add)
                     <button  par1="Add" class="btn btn-sm green AEObject">
                        <i class="fa fa-plus"></i> {{trans("general.add_name_element",array("name" => trans("shops.shop")))}}</button>
                     @endif

                  </div>

               </div>

            </div>

            <div class="portlet-body">

               <div class="table-container">

                  <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">

                     <thead>

                     <tr role="row" class="heading">

                        <th class="text-center" width="25%">{{trans("stores.store_name")}}</th>
                        <th class="text-center" width="15%">{{trans("general.city")}}</th>
                        <th class="text-center" width="15%">{{trans("categories.categories")}}</th>
                        <th class="text-center" width="10%">{{trans("general.state")}}</th>
                        <th class="text-center" width="15%">{{trans("general.created_at")}}</th>
                        <th class="text-center" width="15%">{{trans("general.control")}}</th>

                     </tr>

                     <tr role="row" class="filter">

                        <td class="text-center" style="padding: 8px;">

                           <input type="text" class="form-control form-filter input-sm" name="name">

                        </td>


                        <td class="text-center" style="padding: 8px;">

                          <select class="form-control form-filter select2me" name="city">
                              <option value=""></option>
                       @foreach(App\Models\Cites::where('active',1)->orderBY("order",'desc')->get() as  $city)
                          <option value="{{$city->id}}">{{ $city->Get_Trans(App::getLocale(),'name')}}</option>
                          @endforeach
                           </select>
                        </td>

                         <td class="text-center" style="padding: 8px;">

                            <select class="form-control form-filter select2me" name="shop_categories">
                              <option value=""></option>
                  @foreach(App\Models\Shop_Categories::where('active',1)->where("deleted_at",NULL)->get() as  $cat)
                          <option value="{{$cat->id}}">{{ $cat->Get_Trans(App::getLocale(),'name')}}</option>
                          @endforeach
                           </select>

                        </td>

                         <td class="text-center" style="padding: 8px;">

                         <select class="form-control form-filter " name="active">

                              <option value=""></option>

                              <option value="active">{{trans("general.active")}}</option>
                              <option value="inactive">{{trans("general.inactive")}}</option>
                           </select>

                        </td>


                        <td class="text-center">
                           <div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                              <input type="text" class="form-control form-filter input-sm" readonly name="date_from" placeholder="{{trans('general.from')}}">
                              <span class="input-group-btn">
                                  <button class="btn btn-sm default"
                                          type="button">
                                      <i class="fa fa-calendar"></i>
                                  </button>
                              </span>
                           </div>
                           <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                              <input type="text" class="form-control form-filter input-sm" readonly name="date_to" placeholder="{{trans('general.to')}}">
                              <span class="input-group-btn">
                                  <button class="btn btn-sm default"
                                          type="button">
                                      <i class="fa fa-calendar"></i>
                                  </button>
                              </span>
                           </div>
                        </td>




                        <td class="text-center">

                           <div class="margin-bottom-5">

                              <button class="btn btn-sm green btn-outline filter-submit margin-bottom">

                                 <i class="fa fa-search"></i> {{trans("general.search")}}</button>

                           </div>

                           <button class="btn btn-sm red btn-outline filter-cancel">

                              <i class="fa fa-times"></i> {{trans("general.cancel_filters")}}</button>

                        </td>

                     </tr>

                     </thead>

                     <tbody> </tbody>

                  </table>

               </div>

            </div>

         </div>
      </div>

   </div>



@endsection



@section('script')


<!-- start repeater -->
<!-- <script type="text/javascript" src="{{url('')}}/assets/global/plugins/uploadPreview/jquery.upload_preview.js"></script>
<script src="{{url('')}}/assets/global/plugins/jquery-repeater/jquery.repeater.min.js"></script> -->
<!-- end repeater -->

<script src="{{url('assets')}}/global/plugins/ckeditor4/ckeditor.js" type="text/javascript"></script>
<script src="{{url('assets')}}/global/plugins/dropzone/min/dropzone.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBw9YOQ6A0RrWGodIJ6WTZCat1rSwUpfb8&libraries=geometry"></script>





<script src="{{url('assets')}}/js/Shops.js" type="text/javascript"></script>



<script>

</script>

@endsection
