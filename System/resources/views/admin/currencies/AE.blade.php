<div class="portlet light portlet-fit portlet-form bordered">
    <div class="portlet-body">
        <form id="DForm" par1="{{$CurrentObj->id  ?? NULL}}" class="form-horizontal">
            <div class="form-body">

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.state")}}</label>
                    <div class="col-md-9">
                        <div class="mt-checkbox-inline">
                            <label class="mt-checkbox mt-checkbox-outline">
                                <input type="checkbox" name="active" @if($CurrentObj && ($CurrentObj->active == 1)) checked @endif> {{trans("general.active")}}
                                <span></span>
                            </label>

                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("currencies.iso_code")}}</label>
                    <div class="col-md-2">
                        <input name="iso_code"  value="{{$CurrentObj->iso_code ?? NULL}}" class="form-control" readonly />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("currencies.symbols")}}</label>
                    <div class="col-md-2">
                        <input name="symbols"  value="{{$CurrentObj->symbols ?? NULL}}" class="form-control"  />
                    </div>
                </div>

                @foreach ($AllLangs as $alt)
                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.name_of_:name",["name"=>trans('currencies.the_currency')])}} - {{trans('general.'.$alt['locale'])}}</label>
                    <div class="col-md-9">
                        <input type="text" dir="{{$alt['dir']}}" name="{{$alt['locale']}}[name]" class="form-control" value="@if($CurrentObj){{$CurrentObj->Get_Trans($alt['locale'],'name') ?? NULL}}@endif">
                    </div>
                </div>
                @endforeach

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.icon")}}</label>
                    <div class="col-md-3">
                        <select class="bs-select form-control" name="icon" data-show-subtext="true" data-placeholder="{{trans("general.select")}}">
                                <option value="" ></option>
                        @foreach($Fa_Currencies_Icons as $value)
                                <option value="{{$value}}" data-icon="fa-{{$value}}" @if($CurrentObj && $value == $CurrentObj->icon) selected @endif >{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>


            </div>
        </form>
    </div>
</div>