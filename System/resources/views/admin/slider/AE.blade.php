<div class="portlet light portlet-fit portlet-form bordered">
   <div class="portlet-body">
      <form id="DForm" par1="{{$CurrentObj->id  ?? NULL}}" class="form-horizontal">
         <div class="form-body">



            <div class="form-group">
               <label class="col-md-3 control-label">{{trans("general.state")}}</label>
               <div class="col-md-9">
                  <div class="mt-checkbox-inline">
                     <label class="mt-checkbox mt-checkbox-outline">
                     <input type="checkbox" name="active" @if(!empty($CurrentObj->id) &&($CurrentObj->active == 1)) checked @endif> {{trans("general.active")}}
                     <span></span>
                     </label>
                  </div>
               </div>
            </div>


            <div class="form-group">
               <label class="col-md-3 control-label">{{trans("shops.advertisement_to")}}</label>
               <div class="col-md-9">
                  <select class="form-control" name="shop_link">
                     <option value="">الاعلان غير مرتبط بمتجر	</option>
                     @foreach(App\Models\Shops::where('active', 1)->orderBy('id', 'desc')->get() as  $shop)
                     <option value="{{$shop->id}}" @if(!empty($CurrentObj->shop_link) && ($CurrentObj->shop_link == $shop->id)) selected @endif >{{ $shop->Get_Trans(App::getLocale(),'name') }}</option>
                     @endforeach
                  </select>
               </div>
            </div>


            <!-- <div class="form-group">
               <label class="col-md-3 control-label">{{trans("general.title_of_name",["name"=>trans('slider.slider')])}}</label>
               <div class="col-md-9">
                  <input type="text" name="name" value="{{$CurrentObj->name ?? NULL}}" class="form-control">
                  <span class="help-block">{{trans('general.for_admin_use_only')}}</span>
               </div>
               </div> -->


            <div class="form-group">
               <label class="control-label col-md-3">{{trans("general.order")}}</label>
               <div class="col-md-9">
                  <select class="bs-select form-control" name="order">
                     @for ($x = 1; $x <= 15; $x++)
                     <option value="{{$x}}" @if(!empty($CurrentObj->order) && ($CurrentObj->order == $x)) selected @endif>{{$x}}</option>
                     @endfor
                  </select>
               </div>
            </div>





            @if(empty($CurrentObj->id))
            <div class="form-group" >
               <label class="col-md-3 control-label">{{trans("general.download")}}</label>
               <div class="col-md-9">
                  <select class="form-control form-filter select" name="type_upload" id="selectpicker">
                     <option value=""></option>
                     <option value="pic" @if(!empty($CurrentObj->id) && !empty($main_slider_img) )  selected @endif>{{trans("slider.picture")}}</option>
                     <option value="link_video" @if(!empty($CurrentObj->link_video) )  selected @endif>{{trans("general.video")}}</option>
                     <option value="upload_video" @if(!empty($CurrentObj->id) && !empty($CurrentObj->GetvideoURL('video'))  )  selected @endif>{{trans("general.upload_video")}}</option>
                  </select>
               </div>
            </div>
            @else
               <input type="hidden" name="type_upload" value="upload_video" />
            @endif




            <div class="form-group" id="pic" @if(!empty($CurrentObj->id) && !empty($main_slider_img) )  @else  style="display: none;" @endif>
               <label class="control-label col-md-3">{{trans("general.image_of_name",["name"=>trans('slider.slider')])}}</label>
               <div class="col-md-9">
                  <div class="fileinput fileinput-new" data-provides="fileinput">
                     <div class="fileinput-new thumbnail" style="width: 100%; height: {{$main_thumb_height_xs}}px;">
                        <img src="{{$main_img_thumb ?? "https://via.placeholder.com/".$main_thumb_width_md."x".$main_thumb_height_md.".png"}}" alt="" />
                     </div>
                     <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: {{$main_thumb_width_xs}}px; max-height: {{$main_thumb_height_xs}}px;"> </div>
                     <div>
                        <span class="btn default btn-file">
                        <span class="fileinput-new"> {{trans('general.select_image')}} </span>
                        <span class="fileinput-exists"> {{trans('general.change')}} </span>
                        <input type="file" name="main_slider"> </span>
                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{trans('general.remove')}} </a>
                     </div>
                  </div>
                  <span class="help-block">{{trans("general.the_image_will_be_crop_with_:width_pixels_width_and_:height_pixels_height",['width' => $main_thumb_width_lg,'height' => $main_thumb_height_lg])}}</span>
               </div>
            </div>



         <div class="form-group" id="link_video"  @if(!empty($CurrentObj->link_video) )  @else style="display: none;" @endif >
               <label class="col-md-3 control-label">{{trans("general.video")}}</label>
               <div class="col-md-9">
                  <input type="text" name="link_video" value="{{$CurrentObj->link_video ?? NULL}}" class="form-control">
                  <span class="help-block">{{trans("general.video")}}</span>
               </div>





         </div>



         <div class="form-group" id="upload_video" @if(!empty($CurrentObj->id) && !empty($CurrentObj->GetvideoURL('video'))  )  @else style="display: none;" @endif >




            <div class="form-group">
            <label class="control-label col-md-3">{{trans('general.upload_video')}}</label>
            <div class="col-md-9">
            <div class="fileinput fileinput-new" data-provides="fileinput">
            <div class="input-group input-large">
            <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
            <i class="fa fa-file fileinput-exists"></i>&nbsp;
            <span class="fileinput-filename"> </span>
            </div>
            <span class="input-group-addon btn default btn-file">
            <span class="fileinput-new"> Select file </span>
            <span class="fileinput-exists"> Change </span>
            <input type="file" name="video"> </span>
            <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
            </div>
            </div>
            <span class="help-block">حجم الفيديو</span>
            </div>
            </div>







         </div>







         @foreach ($AllLangs as $alt)
         <div class="form-group">
         <label class="col-md-3 control-label">{{trans("general.text_of_:name",["name"=>trans('slider.slider')])}} - {{trans('general.'.$alt['locale'])}}</label>
         <div class="col-md-9">
         <textarea dir="{{$alt['dir']}}" name="{{$alt['locale']}}[text]" class="form-control">@if($CurrentObj){{$CurrentObj->GetSliderText($alt['locale']) ?? NULL}}@endif</textarea>
         </div>
         </div>
         @endforeach




</div>
</form>
</div>
</div>
