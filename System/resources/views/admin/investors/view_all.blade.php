@extends('admin.app', ['title' => trans('general.manage_:name',['name' => trans("business_fields.investors")])])
@section('css')
   <link rel="stylesheet" href="{{url('/assets/global/plugins/magnific-popup/magnific-popup.css')}}">
@endsection
@section('content')

   <!-- BEGIN PAGE BAR -->
   <div class="page-bar">
      <ul class="page-breadcrumb">
         <li>
            <a href="{{url("My")}}">{{trans("general.home")}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
            <span>{{trans("business_fields.investors")}}</span>
         </li>
      </ul>
   </div>
   <!-- END PAGE BAR -->

   <div class="breadcrumbs">

      <h1>{{trans("business_fields.investors")}}</h1>




   </div>

   <div class="row">

      <div class="col-md-12">
         <div class="portlet light portlet-fit portlet-datatable bordered">

            <div class="portlet-title">

               <div class="caption">

                  <i class="icon-eye font-dark"></i>

                  <span class="caption-subject font-dark sbold uppercase">{{trans("general.browse_and_search_in_name",array("name" => trans("business_fields.investors")))}}</span>

               </div>

               <div class="actions">

                  <div class="btn-group">
                     @if($Add)
                     <button  par1="Add" class="btn btn-sm green AEObject">
                        <i class="fa fa-plus"></i> {{trans("general.add_name_element",array("name" => trans("general.investor")))}}</button>
                     @endif

                  </div>

               </div>

            </div>

            <div class="portlet-body">

               <div class="table-container">

                  <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">

                     <thead>

                     <tr role="row" class="heading">

                        <th class="text-center" width="15%">{{trans("investors.investor_status")}}</th>
                        <th class="text-center" width="20%">{{trans("general.name")}}</th>
                        <th class="text-center" width="15%">{{trans("general.email")}}</th>
                        <th class="text-center" width="30%">{{trans("general.control")}}</th>

                     </tr>

                     <tr role="row" class="filter">

                        <td class="text-center" style="padding: 8px;">
                           <select class="form-control form-filter select2me" name="state">
                              <option value=""></option>
                              <option value="effective">{{trans('investors.effective_investor')}}</option>
                              <option value="ineffective">{{trans('investors.investor_ineffective')}}</option>
                           </select>
                        </td>

                        <td class="text-center" >

                           <input type="text" class="form-control form-filter input-sm" name="name">

                        </td>


                        <td class="text-center">

                           <input type="text" class="form-control form-filter input-sm" name="email">

                        </td>





                        <td class="text-center">

                           <div class="margin-bottom-5">

                              <button class="btn btn-sm green btn-outline filter-submit margin-bottom">

                                 <i class="fa fa-search"></i> {{trans("general.search")}}</button>

                           </div>

                           <button class="btn btn-sm red btn-outline filter-cancel">

                              <i class="fa fa-times"></i> {{trans("general.cancel_filters")}}</button>

                        </td>

                     </tr>

                     </thead>

                     <tbody> </tbody>

                  </table>

               </div>

            </div>

         </div>
      </div>

   </div>



@endsection



@section('script')
   <script src="{{url('assets')}}/global/plugins/jquery-repeater/jquery.repeater.min.js" type="text/javascript"></script>
   <script src="{{url('assets/global/plugins/magnific-popup/jquery.magnific-popup.js')}}"></script>
<script src="{{url('assets/js/Investors.js')}}" type="text/javascript"></script>

@endsection