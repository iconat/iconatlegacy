<div class="portlet light portlet-fit portlet-form bordered">
    <div class="portlet-body">
        <form id="DForm" par1="{{$CurrentObj->id  ?? NULL}}" class="form-horizontal">
            <div class="form-body">

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.state")}}</label>
                    <div class="col-md-9">
                        <div class="mt-checkbox-inline">
                            <label class="mt-checkbox mt-checkbox-outline">
                                <input type="checkbox" name="active" @if(($CurrentObj && ($CurrentObj->active == 1)) || empty($CurrentObj)) checked @endif> {{trans("general.active")}}
                                <span></span>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.order")}}</label>
                    <div class="col-md-2">
                        <select class="form-control form-filter select2me" name="order">
                            <option value=""></option>
                            @for($x = 1;$x <= 50;$x++)
                                <option value="{{$x}}"  {{ ($CurrentObj && ($CurrentObj->order == $x))?"selected":NULL }}>{{$x}}</option>
                            @endfor
                        </select>
                    </div>
                </div>



                @foreach ($AllLangs as $alt)
                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.name_of_:name",["name"=>trans('general.state')])}} - {{trans('general.'.$alt['locale'])}}</label>
                    <div class="col-md-9">
                        <input type="text" dir="{{$alt['dir']}}" name="{{$alt['locale']}}[name]" class="form-control" value="@if($CurrentObj){{$CurrentObj->Get_Trans($alt['locale'],'name') ?? NULL}}@endif">
                    </div>
                </div>
                @endforeach




            </div>
        </form>
    </div>
</div>