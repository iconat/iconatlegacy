<div class="portlet light portlet-fit portlet-form bordered">
    <div class="portlet-body">
        <form id="DForm" par1="{{$CurrentObj->id  ?? NULL}}" lang="{{App::getLocale()}}" class="form-horizontal">
            <div class="form-body">





                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.state")}}</label>
                    <div class="col-md-9">
                        <div class="mt-checkbox-inline">
                            <label class="mt-checkbox mt-checkbox-outline">
                                <input type="checkbox" name="active" @if($CurrentObj && ($CurrentObj->active == 1)) checked @endif> {{trans("general.active")}}
                                <span></span>
                            </label>

                        </div>
                    </div>
                </div>




                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.username")}}</label>
                    <div class="col-md-9">
                        <input class="form-control" name="username" value="{{$CurrentObj->username ?? NULL}}" placeholder="{{trans("general.username")}}" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.name")}}</label>
                    <div class="col-md-9">
                        <input class="form-control" name="name" value="{{$CurrentObj->name ?? NULL}}" placeholder="{{trans("general.name")}}" type="text">
                    </div>
                </div>

                
                        <div class="form-group">
                            <label class="col-md-3 control-label">{{trans("general.email")}}</label>
                            <div class="col-md-9">
                                <input class="form-control" name="email" value="{{$CurrentObj->email ?? NULL}}" placeholder="{{trans("general.email")}}" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">{{trans("general.password")}}</label>
                            <div class="col-md-9">
                                <input class="form-control" name="password" value="" placeholder="{{trans("general.password")}}" type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">{{trans("general.shopname")}}</label>
                            <div class="col-md-9">
                                <input class="form-control" name="shopCompanyName" value="{{$CurrentObj->InfoUser->shopCompanyName ?? NULL }}" placeholder="{{trans("general.shopCompanyName")}}" type="text">
                            </div>
                        </div>




                        <div class="form-group">
                            <label for="shopCompanyName" class="col-md-3 control-label">{{trans("general.city")}}</label>
                            <div class="col-md-9">
                            <select class="form-control" name="city">

                                <option value=""></option>
                           
                            @foreach(App\Models\Cites::orderBy('id', 'desc')->get() as  $city)
                                <option value="{{$city->id}}" @if(!empty($CurrentObj->InfoUser->city) && ($CurrentObj->InfoUser->city == $city->id)) selected @endif >{{ $city->Get_Trans(App::getLocale(),'name') }}</option>
                            @endforeach

                          

                            </select>
                        </div>
                        </div>
                        <div class="form-group">
                            <label for="shopAddress" class="col-md-3 control-label"> {{trans("general.address")}}</label>
                            <div class="col-md-9">
                                <input class="form-control" name="shopAddress" value="{{$CurrentObj->InfoUser->shopAddress ?? NULL}}" placeholder="{{trans("general.shopAddress")}}" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shopTown" class="col-md-3 control-label">TOWN / CITY *</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="shopTown" id="shopTown" value="{{$CurrentObj->InfoUser->shopTown ?? NULL }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shopState" class="col-md-3 control-label">STATE / COUNTY *</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="shopState" id="shopState" value="{{$CurrentObj->InfoUser->shopState ?? NULL}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shopPostCode" class="col-md-3 control-label">POSTCODE / ZIP *</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="shopPostCode" id="shopPostCode" value="{{$CurrentObj->InfoUser->shopPostCode ?? NULL}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shopPhone" class="col-md-3 control-label">{{trans("general.phone")}}</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="shopPhone" id="shopPhone" value="{{$CurrentObj->InfoUser->shopPhone ?? NULL}}">
                            </div>
                        </div>




                        <div class="form-group">
                            <label class="col-md-3 control-label">{{trans("general.user_groups")}}</label>
                            <div class="col-md-9">
                                <div class="mt-checkbox-list" data-error-container="#form_2_services_error">
                                    @foreach(App\Models\Role::get() as  $Role)
                                    <label class="mt-checkbox mt-checkbox-outline">

                                        <input type="checkbox" value="{{$Role->id}}" name="roles[]" @if(!empty($CurrentObj) && ($CurrentObj->hasRole($Role->name) == $Role->id)) checked @endif> {{trans("general.".$Role->name)}}
                                        <span></span>
                                    </label>
                                    @endforeach
                                </div>
                                <div id="form_2_services_error"> </div>
                            </div>
                        </div>



                    </div>
                </form>
            </div>
        </div>