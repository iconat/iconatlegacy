@extends('admin.app', ['title' => trans('general.control_of_name_contents',['name' => trans("cites.cites") ])])

@section('content')

   <!-- BEGIN PAGE BAR -->
   <div class="page-bar">
      <ul class="page-breadcrumb">
         <li>
            <a href="{{url("My")}}">{{trans("general.home")}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
            <a href="{{url("/Manage/Cites")}}">
            {{ trans("cites.cites") }}
            </a>
            <i class="fa fa-circle"></i>
         </li>
         @if($City && count($City->parents))
            @foreach($City->getParentsAttribute() as $p)
            <li>
               <a href="{{url("/Manage/Cites?M=".$p->id)}}">
                  {{ $p->Get_Trans(\App::getLocale(),"name") }}
               </a>
               <i class="fa fa-circle"></i>
            </li>
            @endforeach
         @elseif(!$City)
            <li>
               <span>
                  {{ trans("general.view_all_:name",["name" => trans("cites.main_cites")]) }}
               </span>
            </li>
         @endif
         @if($City)
         <li>
               <span>
                  {{ $City->Get_Trans(\App::getLocale(),"name") }}
               </span>
         </li>
         @endif
      </ul>
   </div>
   <!-- END PAGE BAR -->

   <div class="breadcrumbs">

      <h1> {{ trans("cites.cites") }}</h1>

   </div>

   <div class="row">

      <div class="col-md-12">
         <div class="portlet light portlet-fit portlet-datatable bordered">

            <div class="portlet-title">

               <div class="caption">

                  <i class="icon-eye font-dark"></i>

                  <span class="caption-subject font-dark sbold uppercase">{{trans("general.browse_and_search_in_name",array("name" => trans("cites.cites") ))}}</span>

               </div>

               <div class="actions">

                  <div class="btn-group">
                     @if($Add)
                     <button  par1="Add" par2="{{$City->id ?? NULL}}" class="btn btn-sm green AEObject">
                        <i class="fa fa-plus"></i> {{trans("general.add_name_element",array("name" => trans("cites.city")))}}</button>
                     @endif

                  </div>

               </div>

            </div>

            <div class="portlet-body">

               <div class="table-container">

                  <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax" par1="{{$City->id ?? NULL}}">

                     <thead>

                     <tr role="row" class="heading">

                        <th class="text-center" width="27%">{{trans("general.title_of_name",["name"=>trans('cites.the_city')])}}</th>
                        <th class="text-center" width="15%">{{trans("general.state")}}</th>
                        <th class="text-center" width="15%">{{trans("general.created_at")}}</th>
                        <th class="text-center" width="15%">{{trans("general.control")}}</th>

                     </tr>

                     <tr role="row" class="filter">

                        <td class="text-center" style="padding: 8px;">

                           <input type="text" class="form-control form-filter input-sm" name="name">

                        </td>

                        <td class="text-center">
                           <select class="form-control form-filter select2me" name="active">
                              <option value=""></option>
                              <option value="active">{{trans("general.active")}}</option>
                              <option value="not_active">{{trans("general.not_active")}}</option>
                           </select>
                        </td>


                        <td class="text-center">
                           <div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                              <input type="text" class="form-control form-filter input-sm" readonly name="date_from" placeholder="{{trans('general.from')}}">
                              <span class="input-group-btn">
                                  <button class="btn btn-sm default"
                                          type="button">
                                      <i class="fa fa-calendar"></i>
                                  </button>
                              </span>
                           </div>
                           <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                              <input type="text" class="form-control form-filter input-sm" readonly name="date_to" placeholder="{{trans('general.to')}}">
                              <span class="input-group-btn">
                                  <button class="btn btn-sm default"
                                          type="button">
                                      <i class="fa fa-calendar"></i>
                                  </button>
                              </span>
                           </div>
                        </td>




                        <td class="text-center">

                           <div class="margin-bottom-5">

                              <button class="btn btn-sm green btn-outline filter-submit margin-bottom">

                                 <i class="fa fa-search"></i> {{trans("general.search")}}</button>

                           </div>

                           <button class="btn btn-sm red btn-outline filter-cancel">

                              <i class="fa fa-times"></i> {{trans("general.cancel_filters")}}</button>

                        </td>

                     </tr>

                     </thead>

                     <tbody> </tbody>

                  </table>

               </div>

            </div>

         </div>
      </div>

   </div>



@endsection



@section('script')
<script src="{{url('assets')}}/global/plugins/ckeditor4/ckeditor.js" type="text/javascript"></script>
<script src="{{url('assets')}}/js/Cites.js" type="text/javascript"></script>

@endsection
