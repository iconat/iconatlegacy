<div class="portlet light portlet-fit portlet-form bordered">
   <div class="portlet-body">
      <form id="DForm" par1="{{$CurrentObj->id  ?? NULL}}" par2="{{$Parent->id ?? NULL}}" class="form-horizontal">
         <div class="form-body">
            <div class="form-group">
               <label class="col-md-3 control-label">{{trans("general.state")}}</label>
               <div class="col-md-9">
                  <div class="mt-checkbox-inline">
                     <label class="mt-checkbox mt-checkbox-outline">
                     <input type="checkbox" name="active" @if((empty($CurrentObj)) ||($CurrentObj && ($CurrentObj->active == 1))) checked @endif> {{trans("general.active")}}
                     <span></span>
                     </label>
                  </div>
               </div>
            </div>
            <div class="form-group">
               <label class="col-md-3 control-label">{{trans("cites.city_order")}}</label>
               <div class="col-md-9">
                  <select class="form-control form-filter select2me" name="order">
                     <option value=""></option>
                     @foreach($order_array as $key=>$value)
                     <option value="{{$key}}" @if($CurrentObj && ($CurrentObj->order == $key)) selected @endif>{{trans($value)}}</option>
                     @endforeach
                  </select>
                  <span class="help-block">{{trans("cites.the_order_of_cites")}}</span>
               </div>
            </div>
            @foreach ($AllLangs as $alt)
            <div class="form-group">
               <label class="col-md-3 control-label">{{trans("general.name_of_:name",["name"=>trans('cites.the_city')])}} - {{trans('general.'.$alt['locale'])}}</label>
               <div class="col-md-9">
                  <input type="text" dir="{{$alt['dir']}}" name="{{$alt['locale']}}[name]" class="form-control" value="@if($CurrentObj){{$CurrentObj->Get_Trans($alt['locale'],'name') ?? NULL}}@endif">
               </div>
            </div>
            @endforeach
            <div class="form-group">
               <label class="control-label col-md-3">{{trans("general.image_of_name",["name"=>trans('cites.city')])}}</label>
               <div class="col-md-9">
                  <div class="fileinput fileinput-new" data-provides="fileinput">
                     <div class="fileinput-new thumbnail" style="width: {{$main_thumb_width_xs}}px; height: {{$main_thumb_height_xs}}px;">
                        <img src="{{$Main_img_city ?? url(setting('default_product_image' , ""))}}" alt="" />
                     </div>
                     <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: {{$main_thumb_width_xs}}px; max-height: {{$main_thumb_height_xs}}px;"> </div>
                     <div>
                        <span class="btn default btn-file">
                        <span class="fileinput-new"> {{trans('general.select_image')}} </span>
                        <span class="fileinput-exists"> {{trans('general.change')}} </span>
                        <input type="file" name="main_city"> </span>
                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{trans('general.remove')}} </a>
                     </div>
                  </div>
                  <span class="help-block">{{trans("general.the_image_will_be_crop_with_:width_pixels_width_and_:height_pixels_height",['width' => $main_thumb_width_lg,'height' => $main_thumb_height_lg])}}</span>
               </div>
            </div>






         </div>
      </form>
   </div>
</div>
