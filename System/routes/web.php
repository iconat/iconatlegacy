<?php
use Waavi\Translation\Facades\UriLocalizer;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/** ... Non localized urls here **/
Route::group(['prefix' => UriLocalizer::localeFromRequest(1), 'middleware' => ['localize:1']], function () {


    Route::get('search','Search@search')->name('search');
        Route::delete('Manage/deleteImage/{id}','Shops@deleteImage')->name('deleteImage');

            Route::post('/UploadForCkeditor', 'GeneralController@UploadForCkeditor')->name('UploadForCkeditor');
    Route::post('/Dropzone/Upload/{Section?}', 'GeneralController@DropzoneUpload');

    /*
     * General Routes
     */
    Route::get('/', function () {
        //return redirect(url("/login"));
        return redirect(url("/Home"));

    });



    Route::get('/anas', 'HomeController@anas')->name('anas');


// adminpanel
//    Route::get('TOS', function () {
//        return view('invs.TOS');
//    });



    Route::get('logout', 'Auth\LoginController@logout')->name('logout');
    Route::post('login', 'LoginController@authenticate')->name('login');
    Route::get('/login', function () {
        if (Auth::user()) {
            return redirect(url("/My"));
        } else {
            return view('auth.login1');
        }
    })->name("login");
    Route::post('register', 'RegisterController@authenticate')->name('register');
    Route::get('/register', function () {
        if (Auth::user()) {
            return redirect(url("/My"));
        } else {
            return view('auth.register');
        }
    })->name("register");


    /*
     * Routes For Gustes
     */

    Route::get('Home',  'HomeController@index');
    Route::get('About', 'HomeController@AboutUs');
    Route::get('Cites', 'HomeController@Cites');
    Route::get('AllCategoreis/', 'HomeController@AllCategoreis');

    Route::get('Categoreis/{city?}', 'HomeController@Categoreis')->name('Categoreis');

    Route::get('Cat/{id?}', 'HomeController@Cat')->name('Cat');
    Route::get('Contact', 'HomeController@Contact');

    Route::get('Shops/{id?}', 'HomeController@Shops')->name('Shops');
    Route::get('ShopsDetails/{id?}', 'HomeController@ShopsDetails')->name('ShopsDetails');

     Route::get('ShopEdit', 'HomeController@ShopEdit');
     Route::post('Shop/ShopEdit', 'Website\Users@ShopEdit');


    Route::get('Account', 'HomeController@Account');
    Route::post('Users/Edit', 'Website\Users@Edit');


    Route::post('SendContactToDB', 'Website\ContactController@store')->name('SendContactToDB');
    Route::post('SendSubscribeToDB', 'Website\ContactController@subscribe')->name('SendSubscribeToDB');


    Route::get('Product/ViewModal', 'Website\Products@ViewProductModal');

    Route::post('Product/AddToCart', 'Website\Products@AddToCart');
    Route::post('Product/DeleteItem', 'Website\Products@DeleteItem');
    Route::post('Product/ChangeQTY', 'Website\Products@ChangeQTY');

    Route::get('Shop', 'Website\Products@ShowList');


    Route::post('Cart/SetPaymentMethod', 'Website\Products@SetPaymentMethod');
    Route::post('Cart/SetDeliveryMethod', 'Website\Products@SetDeliveryMethod');
    Route::post('Cart/Set_Shipping_Information', 'Website\Products@Set_Shipping_Information');
    Route::post('Cart/CheckOutCart', 'Website\Products@CheckOutCart');




    Route::get('Cart/Checkout', 'Website\Products@Checkout');
    Route::post('CheckOut/GetPaymentForm', 'Website\Products@GetPaymentForm');


    Route::get('Users/RegisterModal', 'Website\Users@RegisterModal');
    Route::get('Users/LoginModal', 'Website\Users@LoginModal');
    Route::post('Users/CreateUser', 'Website\Users@CreateUser');
    Route::post('Users/LoginFromModal', 'Website\Users@LoginFromModal');
    Route::get('User/Login', 'Website\Users@LoginForm');
    Route::post('User/ProcessLogin', 'Website\Users@authenticate');


    Route::get('News', 'Website\News@ShowList');
    Route::get('News/V/{post}/{title?}', 'Website\News@Show');


});

Route::group(['prefix' => UriLocalizer::localeFromRequest(1), 'middleware' => ['role:admin', 'localize:1']], function () {
    /*
  * Tests Routes
  */
    // Route::get('send_test_email', function () {
    //     Mail::raw('Hi .. we need to change the world', function ($message) {
    //         $message->from('invs@homyt.com', 'Homyt');
    //         $message->to('st-3-far1bqim74@glockapps.com');
    //         $message->subject('Helllo man');

    //     });
    // });

    /*
 * Routes of Accounts
 */
    // Route::get('Manage/MyAccount', 'MyAccount@index')->name("Manage/MyAccount");
    Route::get('My', 'MyAccount@Account')->name('My');


      /*
     * Routes of AllImage
     */
    Route::get('Manage/AllImage', 'AllImage@index')->name("Manage/AllImage");


    /*
     * Routes of Settings
     */
    Route::get('Manage/Settings', 'Settings@index')->name("Manage/Settings");
    Route::post('Manage/Settings/Store', 'Settings@Store')->name("Manage/Settings/Store");


    /*
    * Routes of Users
    */
    Route::get('Manage/Users', 'Users@index')->name("Manage/Users");
    Route::post('Manage/Users/GetData', 'Users@getdata')->name("Manage/Users/GetData");
    Route::get('Manage/Users/AE', 'Users@AE')->name("Manage/Users/AE");
    Route::post('Manage/Users/Store', 'Users@Store')->name("Manage/Users/Store");
    Route::post('Manage/Users/Delete', 'Users@Delete')->name("Manage/Users/Delete");

      /*
 * Routes of Contact
 */
    Route::get('Manage/Contact', 'Contact@index');
    Route::post('Manage/Contact/GetData', 'Contact@getdata');
    Route::get('Manage/Contact/ShowMessage', 'Contact@Show');

    Route::get('Manage/Subscribe', 'Contact@Subscribe');
    Route::post('Manage/Subscribe/GetData', 'Contact@GetDataSub');

    /*
 * Routes of Products
 */
    Route::get('Manage/Products', 'Products@index');
    Route::post('Manage/Products/GetData', 'Products@getdata');
    Route::get('Manage/Products/AE', 'Products@AE');
    Route::post('Manage/Products/Store', 'Products@Store');
    Route::post('Manage/Products/Delete', 'Products@Delete');


    /*
     * Routes of WhyUS
     */
    Route::get('Manage/Whyus', 'Whyus@index');
    Route::post('Manage/Whyus/GetData', 'Whyus@getdata');
    Route::get('Manage/Whyus/AE', 'Whyus@AE');
    Route::post('Manage/Whyus/Store', 'Whyus@Store');
    Route::post('Manage/Whyus/Delete', 'Whyus@Delete');
    /*
     * Routes of News
     */
    Route::get('Manage/News', 'News@index');
    Route::post('Manage/News/GetData', 'News@getdata');
    Route::get('Manage/News/AE', 'News@AE');
    Route::post('Manage/News/Store', 'News@Store');
    Route::post('Manage/News/Delete', 'News@Delete');

    /*
 * Routes of Dictionary
 */
    Route::get('Dictionary', 'General\Dictionary@index')->name("Dictionary");
    Route::post('DictionaryData', 'General\Dictionary@getdata')->name("DictionaryData");
    Route::get('AE_Dictionary', 'General\Dictionary@AE')->name("AE_Dictionary");
    Route::post('Store_Dictionary', 'General\Dictionary@Store')->name("Store_Dictionary");
    Route::post('Delete_Dictionary', 'General\Dictionary@Delete')->name("Delete_Dictionary");

    /*
     * Routes of Slider
     */
    Route::get('Manage/Slider', 'Slider@index');
    Route::post('Manage/Slider/GetData', 'Slider@getdata');
    Route::get('Manage/Slider/AE', 'Slider@AE');
    Route::post('Manage/Slider/Store', 'Slider@Store');
    Route::post('Manage/Slider/Delete', 'Slider@Delete');


        /*
     * Routes of Slider
     */
    Route::get('Manage/Slider_Category', 'Slider_Category@index');
    Route::post('Manage/Slider_Category/GetData', 'Slider_Category@getdata');
    Route::get('Manage/Slider_Category/AE', 'Slider_Category@AE');
    Route::post('Manage/Slider_Category/Store', 'Slider_Category@Store');
    Route::post('Manage/Slider_Category/Delete', 'Slider_Category@Delete');


    /*
     * Routes of Categories
     */
    Route::get('Manage/Categories', 'Categories@index');
    Route::post('Manage/Categories/GetData', 'Categories@getdata');
    Route::get('Manage/Categories/AE', 'Categories@AE');
    Route::post('Manage/Categories/Store', 'Categories@Store');
    Route::post('Manage/Categories/Delete', 'Categories@Delete');


    /*
     * Routes of Currencies
     */
    Route::get('Manage/Currencies', 'Currencies@index');
    Route::post('Manage/Currencies/GetData', 'Currencies@getdata');
    Route::get('Manage/Currencies/AE', 'Currencies@AE');
    Route::post('Manage/Currencies/Store', 'Currencies@Store');
    Route::post('Manage/Currencies/Delete', 'Currencies@Delete');
    //Route::get('Manage/Currencies/rrr', 'Currencies@ss');


    /*
     * Routes of Stores
     */
    Route::get('Manage/Stores', 'Stores@index');
    Route::post('Manage/Stores/GetData', 'Stores@getdata');
    Route::get('Manage/Stores/AE', 'Stores@AE');
    Route::post('Manage/Stores/Store', 'Stores@Store');
    Route::post('Manage/Stores/Delete', 'Stores@Delete');

    /*
     * Routes of OrdersStatus
     */
    Route::get('Manage/OrdersStatus', 'OrdersStatus@index');
    Route::post('Manage/OrdersStatus/GetData', 'OrdersStatus@getdata');
    Route::get('Manage/OrdersStatus/AE', 'OrdersStatus@AE');
    Route::post('Manage/OrdersStatus/Store', 'OrdersStatus@Store');
    Route::post('Manage/OrdersStatus/Delete', 'OrdersStatus@Delete');

    /*
     * Routes of Units
     */
    Route::get('Manage/Units', 'Units@index');
    Route::post('Manage/Units/GetData', 'Units@getdata');
    Route::get('Manage/Units/AE', 'Units@AE');
    Route::post('Manage/Units/Store', 'Units@Store');
    Route::post('Manage/Units/Delete', 'Units@Delete');

    /*
     * Routes of DeliveryMethods
     */
    Route::get('Manage/DeliveryMethods', 'DeliveryMethods@index');
    Route::post('Manage/DeliveryMethods/GetData', 'DeliveryMethods@getdata');
    Route::get('Manage/DeliveryMethods/AE', 'DeliveryMethods@AE');
    Route::post('Manage/DeliveryMethods/Store', 'DeliveryMethods@Store');
    Route::post('Manage/DeliveryMethods/Delete', 'DeliveryMethods@Delete');

    /*
     * Routes of PayMethods
     */
    Route::get('Manage/PayMethods', 'PayMethods@index');
    Route::post('Manage/PayMethods/GetData', 'PayMethods@getdata');
    Route::get('Manage/PayMethods/AE', 'PayMethods@AE');
    Route::post('Manage/PayMethods/Store', 'PayMethods@Store');
    Route::post('Manage/PayMethods/Delete', 'PayMethods@Delete');

    /*
     * Routes of PayMethods
     */
    Route::get('Manage/Orders', 'Orders@index');
    Route::post('Manage/Orders/GetData', 'Orders@getdata');
    Route::get('Manage/Orders/AE', 'Orders@AE');
    Route::post('Manage/Orders/Store', 'Orders@Store');
    Route::post('Manage/Orders/Delete', 'Orders@Delete');




        /*
     * Routes of Cites
     */
    Route::get('Manage/Cites', 'Cites@index');
    Route::post('Manage/Cites/GetData', 'Cites@getdata');
    Route::get('Manage/Cites/AE', 'Cites@AE');
    Route::post('Manage/Cites/Store', 'Cites@Store');
    Route::post('Manage/Cites/Delete', 'Cites@Delete');







            /*
     * Routes of Shop_Categories
     */
    Route::get('Manage/Shop_Categories', 'Shop_Categories@index');
    Route::post('Manage/Shop_Categories/GetData', 'Shop_Categories@getdata');
    Route::get('Manage/Shop_Categories/AE', 'Shop_Categories@AE');
    Route::post('Manage/Shop_Categories/Store', 'Shop_Categories@Store');
    Route::post('Manage/Shop_Categories/Delete', 'Shop_Categories@Delete');



        /*
 * Routes of Shops
 */
    Route::get('Manage/Shops', 'Shops@index');
    Route::post('Manage/Shops/GetData', 'Shops@getdata');
    Route::get('Manage/Shops/AE', 'Shops@AE');
    Route::post('Manage/Shops/Store', 'Shops@Store');
    Route::post('Manage/Shops/Delete', 'Shops@Delete');






        /*
     * Routes of Ads
     */
    Route::get('Manage/Ads', 'Ads@index');
    Route::post('Manage/Ads/GetData', 'Ads@getdata');
    Route::get('Manage/Ads/AE', 'Ads@AE');
    Route::post('Manage/Ads/Store', 'Ads@Store');
    Route::post('Manage/Ads/Delete', 'Ads@Delete');



        /*
     * Routes of Gallery
     */
    Route::get('Manage/Gallery', 'Gallery@index');
    Route::post('Manage/Gallery/GetData', 'Gallery@getdata');
    Route::get('Manage/Gallery/AE', 'Gallery@AE');
    Route::post('Manage/Gallery/Store', 'Gallery@Store');
    Route::post('Manage/Gallery/Delete', 'Gallery@Delete');

});


