<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use View;
use Validator;
use App;
use Carbon\Carbon;
use ImageOptimizer;

class Orders extends Controller
{
    protected $Browse = FALSE;
    protected $Add = FALSE;
    protected $Edit = FALSE;
    protected $Delete = FALSE;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->Browse = Auth::user()->ability(array('admin'), array('B_Carts'));
            //$this->Add = Auth::user()->ability(array('admin'), array('A_Orders'));
            $this->Edit = Auth::user()->ability(array('admin'), array('E_Carts'));
            $this->Delete = Auth::user()->ability(array('admin'), array('D_Carts'));

            View::share('Browse', $this->Browse);
            View::share('Add', $this->Add);
            View::share('Edit', $this->Edit);
            View::share('Delete', $this->Delete);


            return $next($request);
        });
    }


    public function index()
    {
        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            return view('admin.orders.view_all');
        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('general.You do not have permission to access this content'));
        }
    }


    public function AE(Request $request)
    {
        $ReqData = $request->all();
        $par1 = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $CurrentObj = NULL;
        if ($par1 == 'add') {
            $title = trans("orders.add_a_new_order");
        } else {
            $CurrentObj = \App\Models\Orders::find($par1);
            $title = trans('orders.edit_and_follow_up_the_order_no_',['No' => $CurrentObj->order_no]);
        }

        if (($this->Add && $par1 == 'add') || ($this->Edit && !empty($CurrentObj))) {
            return json_encode(array(
                'title' => $title,
                'content' => view('admin.orders.AE')->with('par1', $par1)->with('CurrentObj', $CurrentObj)->render()
            ));
        } else {
            return json_encode(array(
                'title' => trans("general.error"),
                'content' => view('system.errors.show_error_modal')->with('error_description', trans("general.You do not have permission to access this content"))->render()
            ));
        }
    }

    public function store(Request $request)
    {

        $ReqData = \Purifier::clean($request->all());
        //$ReqData = $request->all();

        $id = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $results = array();

        if (($this->Edit && !empty(\App\Models\Orders::where('id', $id)->count())) || ($this->Add && empty($id))) {

            if (!$request->has('active')) {
                $ReqData['active'] = NULL;
            } else {
                $ReqData['active'] = 1;
            }

            Validator::make($ReqData, [
//                'name' => [
//                    'required',
//                ],
            ])->validate();


            $Obj = \App\Models\Orders::updateOrCreate(
                ['id' => $id],
                $ReqData
            );


            if ($Obj->wasRecentlyCreated) {
                $content = trans("general.successfully_added_new_:name", ['name' => trans("general.element")]);
                $Obj->created_by = Auth::user()->id;
                $Obj->update();

                activity("paymethods.payment_methods")
                    ->performedOn($Obj)
                    ->causedBy(Auth::user()->id)
                    ->log('paymethods.added_a_new_payment_method');

            } else {
                $content = trans("general.modified_successfully");
                $Obj->updated_by = Auth::user()->id;
                $Obj->update();

                activity("paymethods.payment_methods")
                    ->performedOn($Obj)
                    ->causedBy(Auth::user()->id)
                    ->log('paymethods.edit_payment_method');

            }

            if (!empty($Obj->id)) {


                $All_Langs = \Waavi\Translation\Models\Language::orderBY('id')->get();
                foreach ($All_Langs as $Lang) {
                    if (isset($ReqData[$Lang->locale])) {
                        foreach ($ReqData[$Lang->locale] as $k => $v) {
                            $ObjTrans = \App\Models\Translations\OrdersTranslations::updateOrCreate(
                                ['ref_id' => $Obj->id, 'locale' => $Lang->locale],
                                [$k => $v]
                            );
                            if ($ObjTrans->wasRecentlyCreated) {
                                $ObjTrans->created_by = Auth::user()->id;
                                $ObjTrans->update();

                            } else {
                                $ObjTrans->updated_by = Auth::user()->id;
                                $ObjTrans->update();

                            }
                        }
                    }
                }

                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => $content,
                );
            }

        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }

        echo json_encode($results);


    }


    public function getdata(Request $request)
    {
        App::setLocale($request->lang);
        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            $Objs = \App\Models\Orders::orderBy('id', 'desc');


            //Check if request is serach
            if (isset($request["action"]) && $request['action'] == "filter") {
                if (!empty($request["order_no"])) {
                    $Objs = $Objs->where("order_no", $request["order_no"]);
                }

                if (!empty($request["creator"])) {
                    $Objs = $Objs->whereHas('Creator', function ($query) use ($request) {
                        $query->where("name", 'LIKE', '%' . $request["creator"] . '%');
                    });
                }
                if (!empty($request["state"])) {
                    $Objs = $Objs->where("state", $request["state"]);
                }

                if (!empty($request["date_from"]) || !empty($request["date_to"])) {
                    $date_from = isset($request["date_from"]) ? $request["date_from"] : NULL;
                    $date_to = isset($request["date_to"]) ? $request["date_to"] : NULL;
                    $newDateTo = date('Y-m-d', strtotime($date_to.' +1 day'));
                    if($date_from && $date_to){
                        $Objs = $Objs->whereBetween('created_at', [$date_from, $newDateTo]);
                    }
                    elseif ($date_from && (!$date_to)){
                        $Objs = $Objs->where('created_at',">=", $date_from);
                    }
                    elseif ((!$date_from) && $date_to){
                        $Objs = $Objs->where('created_at',"<=", $newDateTo);
                    }
                }

                if (!empty($request["price_from"]) || !empty($request["price_to"])) {
                    $price_from = isset($request["price_from"]) ? $request["price_from"] : NULL;
                    $price_to = isset($request["price_to"]) ? $request["price_to"] : NULL;
                    if($price_from && $price_to){
                        $Objs = $Objs->whereBetween('total_cost', [$price_from, $price_to]);
                    }
                    elseif ($price_from && (!$price_to)){
                        $Objs = $Objs->where('total_cost',">=", $price_from);
                    }
                    elseif ((!$price_from) && $price_to){
                        $Objs = $Objs->where('total_cost',"<=", $price_to);
                    }
                }
            }


            $iTotalRecords = $Objs->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $Objs = $Objs->offset($iDisplayStart);
            $Objs = $Objs->limit($iDisplayLength);


            $records = array();
            $records["data"] = array();


            $Objs = $Objs->get();

            foreach ($Objs as $value) {


                $buttons = '';
                if ($this->Edit) {
                    $buttons .= '<button par1="' . $value->id . '" class="AEObject btn btn-sm btn-outline green"><i class="fa fa-eye"></i> ' . trans("general.view") . '</button>';
                }
                if ($this->Delete) {
                    $OTitle = trans($value->id);
                    //$buttons .= '<button par1="' . $value->id . '" par2="' . $OTitle . '" Dtitle="' . trans("general.are_you_sure") . '" Dcontent="' . trans("general.are_you_sure_you_want_to_delete_name", array('name' => $OTitle)) . '"  class="Delete btn btn-sm btn-outline red"><i class="fa fa-remove"></i>  ' . trans("general.delete") . '</button>';
                }
                $created_at = new Carbon($value->created_at);

                $records["data"][] = array(
                    $value->order_no,
                    (!empty($value->Creator->name)) ? $value->Creator->name : trans("general.guest"),
                    (!empty($value->Status)) ? $value->Status->Get_Trans(\App::getlocale(), "name") : NULL,
                    (!empty($value->total_cost) ? $value->total_cost : 0) . ' <i class="fa fa-' . config("Default_currency_Icon") . '"></i>',
                    $created_at->toDayDateTimeString(),
                    $buttons,
                );
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);


        }
    }


    public function Delete(Request $request)
    {
        $results = array();
        if ($this->Delete) {
            $ReqData = $request->all();
            $id = isset($ReqData['id']) ? mb_strtolower($ReqData['id']) : NULL;
            $par2 = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;

            $Obj = $q = \App\Models\Orders::where('id', '=', $id)->first();

            $q = \App\Models\Orders::where('id', '=', $id)
                ->where('deleted_at', NULL)
                ->update([
                    'deleted_at' => date("Y-m-d H:i:s"),
                    'deleted_by' => Auth::user()->id,
                ]);

            if (!empty($q)) {

                activity("paymethods.payment_methods")
                    ->performedOn($Obj)
                    ->causedBy(Auth::user()->id)
                    ->log('paymethods.delete_a_payment_method');

                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => trans('general.name_successfully_deleted', array('name' => $par2)),
                );
            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans('general.the_operation_was_not_done_correctly'),
                );
            }
        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);


    }

}



