<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use View;
use Validator;
use App;
use Sunra\PhpSimple\HtmlDomParser;

class Classes extends Controller
{
    protected $Browse = FALSE;
    protected $Add = FALSE;
    protected $Edit = FALSE;
    protected $Delete = FALSE;

    protected $thumb_width = 300;
    protected $thumb_height = 300;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->Browse = Auth::user()->ability(array('admin'), array('B_Classes'));
            $this->Add = Auth::user()->ability(array('admin'), array('A_Classes'));
            $this->Edit = Auth::user()->ability(array('admin'), array('E_Classes'));
            $this->Delete = Auth::user()->ability(array('admin'), array('D_Classes'));


            View::share('Browse', $this->Browse);
            View::share('Add', $this->Add);
            View::share('Edit', $this->Edit);
            View::share('Delete', $this->Delete);

            View::share('thumb_width', $this->thumb_width);
            View::share('thumb_height', $this->thumb_height);

            return $next($request);
        });


    }

    //show form register for invs
    function utf8_strrev($str)
    {
        preg_match_all('/./us', $str, $ar);
        return join('', array_reverse($ar[0]));
    }

//    public function ImagesToDb()
//    {
//        for ($x = 1; $x <= 120; $x++) {
//            $Img_Link = 'data/third/2/images/ilovepdf_com-' . $x . '.jpg';
//            $PathToSave = 'paragraphs/2018/08/21';
//
//            $folder_ = storage_path("app/public/") . $PathToSave;
//            if (!file_exists($folder_)) {
//                mkdir($folder_, 666, true);
//            }
//            $exp = explode("/", $Img_Link);
//            $file_name = '';
//            foreach ($exp as $x1) {
//                $file_name = $x1;
//            }
//
//            //echo public_path($Img_Link);
//
//            $image = \Image::make(public_path("/" . $Img_Link))
//                ->save($folder_ . '/' . strtolower($file_name));
//            $Filee = explode('.', $file_name);
//            $Img = new \Plank\Mediable\Media;
//            $Img->disk = "public";
//            $Img->directory = $PathToSave;
//            $Img->filename = $Filee[0];
//            $Img->extension = $Filee[1];
//            $Img->mime_type = mime_content_type($folder_ . '/' . $file_name);
//            $Img->aggregate_type = "image";
//            $Img->size = filesize($folder_ . '/' . $file_name);
//            $Img->rename(str_random(10));
//            $Img->save();
//
//
//            $Obj = \App\Models\Paragraphs::updateOrCreate(
//                ['id' => 0],
//                [
//                    'order' => $x,
//                    'book_id' => 4,
//                    'title' => 'פסקה '.$x,
//                ]
//            );
//            $Obj->attachMedia($Img, ['paragraph_as_jpg']);
//
//
//            //break;
//        }
//
//    }

    public function MakeThumbs()
    {
        $Paragraphs = \App\Models\Paragraphs::all();
        foreach ($Paragraphs as $Paragraph) {
            //  echo  $Paragraph->GetImageURL("paragraph_as_jpg");
            $Or = $Paragraph->lastMedia("paragraph_as_jpg");
            $folder_ = storage_path("app/public/") . $Or->directory;
            if (!file_exists($folder_)) {
                mkdir($folder_, 666, true);
            }
            $image = \Image::make($Paragraph->GetImageURL("paragraph_as_jpg"))
                ->resize(264, 374)
                ->save($folder_ . '/' . $Or->filename . '_thumb.' . $Or->extension);
            $thumb = new \Plank\Mediable\Media;
            $thumb->disk = "public";
            $thumb->directory = $Or->directory;
            $thumb->filename = $Or->filename . '_thumb';
            $thumb->extension = $Or->extension;
            $thumb->mime_type = $Or->mime_type;
            $thumb->aggregate_type = "image";
            $thumb->size = filesize($folder_ . '/' . $Or->filename . '_thumb.' . $Or->extension);
            $thumb->save();
            $Paragraph->attachMedia($thumb, ['paragraph_as_jpg_thumb']);

        }
    }

    public function PToDb()
    {
        // return view('website.home');
        $css_File = file_get_contents(public_path("a.html")); // Can use single quot as well...
        $text = str_replace("*BASE*", url(""), $css_File);

        $CssArray = $this->str_replace();

        $html = HtmlDomParser::str_get_html($text);
        $ret = $html->find('.PARENT');
        //echo dd($ret);
        foreach ($ret as $value) {

            $html1 = HtmlDomParser::str_get_html($value->outertext);
            $ret1 = $html->find('span');
            foreach ($ret1 as $aa) {
                $aa->innertext = $this->utf8_strrev($aa->innertext); //To Reverse Text

                $Style = isset($CssArray['span.' . $aa->class]) ? $CssArray['span.' . $aa->class] : NULL;
                if (!empty($Style)) {
                    $Style = str_replace('"', "'", $Style);
                    $aa->class = "";
                    $aa->style = $Style;
                }

            }


            echo $value->outertext;

        }

        //   echo $text;


//        $Links = $this->getContents($css_File, 'url(', ')') ;
//        //print_r($Links);
//        $i = 1;
//        foreach ($Links as $source){
//
//            $sourcewww = explode("?",$source);
//            $source = $sourcewww[0];
//
//            $ch = curl_init();
//            curl_setopt($ch, CURLOPT_URL, $source);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//            $data = curl_exec ($ch);
//            curl_close ($ch);
//
//
//            $file_name = basename($source, '?' . $_SERVER['QUERY_STRING']);
//
//            $destination = "asubfolder/".$file_name;
//            $file = fopen($destination, "w+");
//            fputs($file, $data);
//            fclose($file);
//            echo $destination.'</br>';
//
//            $i++;
//        }

    }

    function getContents($str, $startDelimiter, $endDelimiter)
    {
        $contents = array();
        $startDelimiterLength = strlen($startDelimiter);
        $endDelimiterLength = strlen($endDelimiter);
        $startFrom = $contentStart = $contentEnd = 0;
        while (false !== ($contentStart = strpos($str, $startDelimiter, $startFrom))) {
            $contentStart += $startDelimiterLength;
            $contentEnd = strpos($str, $endDelimiter, $contentStart);
            if (false === $contentEnd) {
                break;
            }
            $contents[] = substr($str, $contentStart, $contentEnd - $contentStart);
            $startFrom = $contentEnd + $endDelimiterLength;
        }

        return $contents;
    }

    public function index()
    {
        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            return view('admin.classes.view_all');
        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('general.You do not have permission to access this content'));
        }
    }

    public function getdata(Request $request)
    {

        App::setLocale($request->lang);

        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            $Objs = \App\Models\Classes::orderBy('id', 'desc');

            //Check if request is serach
            if (isset($request["action"]) && $request['action'] == "filter") {

                if (!empty($request["name"])) {
                    $Objs = $Objs->where(DB::raw('lower(concat(`group`,".",`item`))'), 'LIKE', '%' . mb_strtolower($request["name"]) . '%');

                }
            }


            $iTotalRecords = $Objs->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $Objs = $Objs->offset($iDisplayStart);
            $Objs = $Objs->limit($iDisplayLength);


            $records = array();
            $records["data"] = array();


            foreach ($Objs->get() as $value) {

                $buttons = '';

                $buttons .= '<a href="' . url('/Manage/Books/V/' . $value->id) . '" class=" btn btn-sm btn-outline grey-salsa"><i class="fa fa-book"></i> ' . trans("general.books") . '</a>';

                if ($this->Edit) {
                    $buttons .= '<button par1="' . $value->id . '" class="AEObject btn btn-sm btn-outline green"><i class="fa fa-edit"></i> ' . trans("general.edit") . '</button>';
                }
                if ($this->Delete) {
                    $buttons .= '<button par1="' . $value->id . '" Dtitle="' . trans("general.are_you_sure") . '" Dcontent="' . trans("general.are_you_sure_you_want_to_delete_name", array('name' => $value->{'name_' . App::getLocale()})) . '"  class="Delete btn btn-sm btn-outline red"><i class="fa fa-remove"></i>  ' . trans("general.delete") . '</button>';
                }
                $records["data"][] = array(
                    $value->name,

                    $buttons,
                );
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);


        }
    }

    public function AE(Request $request)
    {
        $ReqData = $request->all();
        $par1 = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $CurrentObj = NULL;
        if ($par1 == 'add') {
            $title = trans("general.add_name_element", array("name" => trans("classes.class")));
        } else {
            $CurrentObj = \App\Models\Classes::find($par1);
            $title = trans('general.edit_element');
        }

        if (($this->Add && $par1 == 'add') || (!empty($CurrentObj) && ($this->Edit))) {
            return json_encode(array(
                'title' => $title,
                'content' => view('admin.classes.AE')->with('par1', $par1)->with('CurrentObj', $CurrentObj)->render()
            ));
        } else {
            return json_encode(array(
                'title' => trans("general.error"),
                'content' => view('system.errors.show_error_modal')->with('error_description', trans("general.You do not have permission to access this content"))->render()
            ));
        }
    }

    public function store(Request $request)
    {
        App::setLocale($request->lang);
        //$ReqData = \Purifier::clean($request->all());
        $ReqData = $request->all();
        $id = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $CurrentObj = \App\Models\Classes::where('id', $id)->first();


        $results = array();
        if (($this->Add && empty($id)) || (!empty($CurrentObj) && ($this->Edit))) {


            $idd = array(
                'name' => 'required',
            );
            Validator::make($ReqData, $idd)->validate();

            try {
                if ($request->hasFile('users_guide')) {
                    $users_guide = $request->file('users_guide');

                    $users_guide_pdf = \MediaUploader::fromSource($users_guide);
                    $users_guide_pdf->setAllowedMimeTypes(['application/pdf']);
                    $users_guide_pdf->setAllowedExtensions(['pdf']);
                    $users_guide_pdf->toDirectory('classes/' . date("Y") . '/' . date("m") . '/' . date("d"));
                    $users_guide_pdf->useFilename(str_random(5));
                    $users_guide_pdf_media = $users_guide_pdf->upload();


                }
            } catch (\Exception $e) {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => $e->getMessage(),
                );
            }

            $Obj = \App\Models\Classes::updateOrCreate(
                ['id' => $id],
                $ReqData
            );

            if (isset($users_guide_pdf_media)) {
                $Obj->attachMedia($users_guide_pdf_media, ['users_guide']);
            }

            if ($Obj->wasRecentlyCreated) {
                $content = trans("general.successfully_added_new_:name", ['name' => trans("classes.class")]);
                $Obj->created_by = Auth::user()->id;
                $Obj->update();
            } else {
                $content = trans("general.modified_successfully");
                $Obj->updated_by = Auth::user()->id;
                $Obj->update();
            }



            if (!empty($Obj->id)) {
                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => $content,
                );
            }

        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }

        echo json_encode($results);


    }

    public function Delete(Request $request)
    {
        App::setLocale($request->lang);
        $results = array();
        if ($this->Delete) {
            $ReqData = $request->all();
            $id = isset($ReqData['id']) ? mb_strtolower($ReqData['id']) : NULL;

            $q = \App\Models\Classes::where('id', '=', $id)
                ->where('deleted_at', NULL)
                ->update([
                    'deleted_at' => date("Y-m-d H:i:s"),
                    'deleted_by' => Auth::user()->id,
                ]);

            if (!empty($q)) {
                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => trans('general.name_successfully_deleted', array('name' => trans('classes.class'))),
                );
            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans('general.the_operation_was_not_done_correctly'),
                );
            }
        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);


    }

    public function Books(\App\Models\Classes $Class)
    {
        if (!empty($Class) && ($this->Browse || $this->Add || $this->Edit || $this->Delete)) {
            return view('admin.classes.view_all_Books')->with('Class', $Class);
        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('general.You do not have permission to access this content'));
        }
    }

    public function getBooks(Request $request)
    {
        App::setLocale($request->lang);
        $Class = !empty($request["par1"]) ? $request["par1"] : NULL;

        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            $Objs = \App\Models\Books::orderBy('id', 'desc');
            $Objs = $Objs->where("class_id", '=', $Class);


            //Check if request is serach
            if (isset($request["action"]) && $request['action'] == "filter") {

                if (!empty($request["name"])) {

                    $Objs = $Objs->where(\DB::raw("lower(name)"), 'LIKE', '%' . strtolower($request["name"]) . '%');
                }
            }


            $iTotalRecords = $Objs->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $Objs = $Objs->offset($iDisplayStart);
            $Objs = $Objs->limit($iDisplayLength);


            $records = array();
            $records["data"] = array();


            foreach ($Objs->get() as $value) {

                $buttons = '';

                $buttons .= '<a href="' . url('/Manage/Paragraphs/V/' . $value->id) . '" class=" btn btn-sm btn-outline grey-salsa"><i class="fa fa-book"></i> ' . trans("general.paragraphs") . '</a>';


                if ($this->Edit) {
                    $buttons .= '<button par1="' . $value->id . '" class="AEObject btn btn-sm btn-outline green"><i class="fa fa-edit"></i> ' . trans("general.edit") . '</button>';
                }
                if ($this->Delete) {
                    $buttons .= '<button par1="' . $value->id . '" Dtitle="' . trans("general.are_you_sure") . '" Dcontent="' . trans("general.are_you_sure_you_want_to_delete_name", array('name' => $value->name)) . '"  class="Delete btn btn-sm btn-outline red"><i class="fa fa-remove"></i>  ' . trans("general.delete") . '</button>';
                }
                $records["data"][] = array(
                    $value->name,
                    $buttons,
                );
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);


        }
    }

    public function AEBooks(Request $request)
    {
        $ReqData = $request->all();
        $par1 = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;
        $par2 = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;

        $CurrentObj = NULL;
        if ($par1 == 'add') {
            $title = trans("general.add_name_element", array("name" => trans("classes.book")));

        } else {
            $CurrentObj = \App\Models\Books::find($par1);
            $title = trans('general.edit_element');
        }

        if (($this->Add && $par1 == 'add') || (!empty($CurrentObj) && ($this->Edit))) {
            $image = !empty($CurrentObj) ? ((($CurrentObj->lastMedia("main_img_thumb")) ? $CurrentObj->lastMedia("main_img_thumb")->getUrl() : NULL)) : NULL;
            $text = !empty($CurrentObj) ? str_replace("App_Path_Re", url(""), $CurrentObj->text) : NULL;

            return json_encode(array(
                'title' => $title,
                'content' => view('admin.classes.AEBooks')->with('image', $image)->with('text', $text)->with('par1', $par1)->with('par2', $par2)->with('CurrentObj', $CurrentObj)->render()
            ));
        } else {
            return json_encode(array(
                'title' => trans("general.error"),
                'content' => view('system.errors.show_error_modal')->with('error_description', trans("general.You do not have permission to access this content"))->render()
            ));
        }
    }

    public function StoreBooks(Request $request)
    {
        App::setLocale($request->lang);

        $ReqData_NotCleaned = $request->all();
        $ReqData = \Purifier::clean($ReqData_NotCleaned);

        $id = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;
        $class_id = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;

        $CurrentObj = \App\Models\Books::where('id', $id)->first();

        $results = array();

        if (($this->Add && empty($id)) || (!empty($CurrentObj) && ($this->Edit))) {

            $validateR = array(
                'name' => 'required',
                //  'text' => 'required',
            );
            if (empty($id)) {
                $validateR['class_id']['text'] = 'required';
                $ReqData['class_id'] = $class_id;
            }

            $ReqData['text'] = $ReqData_NotCleaned['text'];

            Validator::make($ReqData, $validateR)->validate();


            try {
                if ($request->hasFile('img')) {
                    $file = $request->file('img');

                    $uploader = \MediaUploader::fromSource($file);
                    $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                    $uploader->toDirectory('files/books/' . date("Y") . '/' . date("m") . '/' . date("d"));
                    $uploader->useFilename(str_random(5));
                    $orginal_media = $uploader->upload();

                    $folder_ = storage_path("app/public") . '/' . $orginal_media->directory;
                    if (!file_exists($folder_)) {
                        mkdir($folder_, 666, true);
                    }

                    $image = \Image::make($orginal_media->getAbsolutePath())
                        ->resize(500, 700)
                        ->save($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);

                    $thumb = new \Plank\Mediable\Media;
                    $thumb->disk = $orginal_media->disk;
                    $thumb->directory = $orginal_media->directory;
                    $thumb->filename = $orginal_media->filename . '_thumb';
                    $thumb->extension = $orginal_media->extension;
                    $thumb->mime_type = $orginal_media->mime_type;
                    $thumb->aggregate_type = $orginal_media->aggregate_type;
                    $thumb->size = filesize($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);
                    $thumb->save();


                }
                if ($request->hasFile('users_guide')) {
                    $users_guide = $request->file('users_guide');

                    $users_guide_pdf = \MediaUploader::fromSource($users_guide);
                    $users_guide_pdf->setAllowedMimeTypes(['application/pdf']);
                    $users_guide_pdf->setAllowedExtensions(['pdf']);
                    $users_guide_pdf->toDirectory('paragraphs/' . date("Y") . '/' . date("m") . '/' . date("d"));
                    $users_guide_pdf->useFilename(str_random(5));
                    $users_guide_pdf_media = $users_guide_pdf->upload();


                }
            } catch (\Exception $e) {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => $e->getMessage(),
                );
            }
            $Obj = \App\Models\Books::updateOrCreate(
                ['id' => $id],
                $ReqData
            );


            if (isset($orginal_media)) {
                $Obj->attachMedia($orginal_media, ['main_img']);
            }
            if (isset($thumb)) {
                $Obj->attachMedia($thumb, ['main_img_thumb']);
            }



            if ($Obj->wasRecentlyCreated) {
                $content = trans("general.successfully_added_new_:name", ['name' => trans("classes.book")]);
                $Obj->created_by = Auth::user()->id;
                $Obj->update();

            } else {

                $content = trans("general.modified_successfully");
                $Obj->updated_by = Auth::user()->id;
                $Obj->update();

            }

            if (!empty($Obj->id)) {
                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => $content,
                );
            }

        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }

        echo json_encode($results);


    }

    public function DeleteBooks(Request $request)
    {
        $results = array();
        if ($this->Delete) {
            $ReqData = $request->all();
            $id = isset($ReqData['id']) ? mb_strtolower($ReqData['id']) : NULL;

            $q = \App\Models\Books::where('id', '=', $id)
                ->where('deleted_at', NULL)
                ->update([
                    'deleted_at' => date("Y-m-d H:i:s"),
                    'deleted_by' => Auth::user()->id,
                ]);

            if (!empty($q)) {
                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => trans('general.name_successfully_deleted', array('name' => trans('classes.the_book'))),
                );
            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans('general.the_operation_was_not_done_correctly'),
                );
            }
        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);


    }

    public function Paragraphs(\App\Models\Books $Book)
    {
        if (!empty($Book) && ($this->Browse || $this->Add || $this->Edit || $this->Delete)) {
            return view('admin.classes.view_all_Paragraphs')->with('Book', $Book);
        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('general.You do not have permission to access this content'));
        }
    }

    public function getParagraphs(Request $request)
    {
        App::setLocale($request->lang);

        $Class = !empty($request["par1"]) ? $request["par1"] : NULL;

        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            $Objs = \App\Models\Paragraphs::orderBy('id', 'desc');
            $Objs = $Objs->where("book_id", '=', $Class);


            //Check if request is serach
            if (isset($request["action"]) && $request['action'] == "filter") {

                if (!empty($request["name"])) {

                    $Objs = $Objs->where(\DB::raw("lower(name)"), 'LIKE', '%' . strtolower($request["name"]) . '%');
                }
            }


            $iTotalRecords = $Objs->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $Objs = $Objs->offset($iDisplayStart);
            $Objs = $Objs->limit($iDisplayLength);


            $records = array();
            $records["data"] = array();


            foreach ($Objs->get() as $value) {

                $buttons = '';

                if ($this->Edit) {
                    $buttons .= '<button par1="' . $value->id . '" class="AEObject btn btn-sm btn-outline green"><i class="fa fa-edit"></i> ' . trans("general.edit") . '</button>';
                }
                if ($this->Delete) {
                    $buttons .= '<button par1="' . $value->id . '" Dtitle="' . trans("general.are_you_sure") . '" Dcontent="' . trans("general.are_you_sure_you_want_to_delete_name", array('name' => $value->name)) . '"  class="Delete btn btn-sm btn-outline red"><i class="fa fa-remove"></i>  ' . trans("general.delete") . '</button>';
                }
                $records["data"][] = array(
                    $value->title,
                    $buttons,
                );
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);


        }
    }

    public function AEParagraphs(Request $request)
    {
        $ReqData = $request->all();
        $par1 = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;
        $par2 = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;

        $CurrentObj = NULL;
        if ($par1 == 'add') {
            $title = trans("general.add_name_element", array("name" => trans("classes.paragraph")));

        } else {
            $CurrentObj = \App\Models\Paragraphs::find($par1);
            $title = trans('general.edit_element');
        }

        if (($this->Add && $par1 == 'add') || (!empty($CurrentObj) && ($this->Edit))) {
            $image = !empty($CurrentObj) ? ((($CurrentObj->lastMedia("paragraph_as_jpg")) ? $CurrentObj->lastMedia("paragraph_as_jpg")->getUrl() : NULL)) : NULL;
            $text = !empty($CurrentObj) ? str_replace("*BASE*", url(""), $CurrentObj->text) : NULL;
            $Items_Of_worksheets = array();
            if (!empty($CurrentObj)) {
                $Items_Of_worksheets = $CurrentObj->Worksheets()->get();
            }
            return json_encode(array(
                'title' => $title,
                'content' => view('admin.classes.AEParagraphs')->with("Items_Of_worksheets", $Items_Of_worksheets)->with('image', $image)->with('text', $text)->with('par1', $par1)->with('par2', $par2)->with('CurrentObj', $CurrentObj)->render()
            ));
        } else {
            return json_encode(array(
                'title' => trans("general.error"),
                'content' => view('system.errors.show_error_modal')->with('error_description', trans("general.You do not have permission to access this content"))->render()
            ));
        }
    }

    public function StoreParagraphs(Request $request)
    {
        App::setLocale($request->lang);

        $ReqData_NotCleaned = $request->all();
        $ReqData = \Purifier::clean($ReqData_NotCleaned);

        $id = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;
        $class_id = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;

        $CurrentObj = \App\Models\Paragraphs::where('id', $id)->first();

        $results = array();

        if (($this->Add && empty($id)) || (!empty($CurrentObj) && ($this->Edit))) {

            $validateR = array(
                'title' => 'required',
                //  'text' => 'required',
            );
            try {
                if ($request->hasFile('img')) {
                    $file = $request->file('img');

                    $uploader = \MediaUploader::fromSource($file);
                    $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                    $uploader->toDirectory('paragraphs/' . date("Y") . '/' . date("m") . '/' . date("d"));
                    $uploader->useFilename(str_random(5));
                    $orginal_media = $uploader->upload();

                    $folder_ = storage_path("app/public") . '/' . $orginal_media->directory;
                    if (!file_exists($folder_)) {
                        mkdir($folder_, 666, true);
                    }

                    $image = \Image::make($orginal_media->getAbsolutePath())
                        ->resize(264, 374)
                        ->save($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);

                    $thumb = new \Plank\Mediable\Media;
                    $thumb->disk = $orginal_media->disk;
                    $thumb->directory = $orginal_media->directory;
                    $thumb->filename = $orginal_media->filename . '_thumb';
                    $thumb->extension = $orginal_media->extension;
                    $thumb->mime_type = $orginal_media->mime_type;
                    $thumb->aggregate_type = $orginal_media->aggregate_type;
                    $thumb->size = filesize($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);
                    $thumb->save();

                }

                if ($request->hasFile('audio_file')) {
                    $audio_file = $request->file('audio_file');

                    $uploader_mp3 = \MediaUploader::fromSource($audio_file);
                    $uploader_mp3->setAllowedMimeTypes(['audio/mpeg']);
                    $uploader_mp3->setAllowedExtensions(['mp3']);
                    $uploader_mp3->toDirectory('paragraphs/' . date("Y") . '/' . date("m") . '/' . date("d"));
                    $uploader_mp3->useFilename(str_random(5));
                    $mp3_media = $uploader_mp3->upload();


                }
            } catch (\Exception $e) {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => $e->getMessage(),
                );
            }
            if (empty($id)) {
                $validateR['book_id']['text'] = 'required';
                $ReqData['book_id'] = $class_id;
            }

            $ReqData['text'] = $ReqData_NotCleaned['text'];

            Validator::make($ReqData, $validateR)->validate();

            $Obj = \App\Models\Paragraphs::updateOrCreate(
                ['id' => $id],
                $ReqData
            );
            if (isset($orginal_media)) {
                $Obj->attachMedia($orginal_media, ['paragraph_as_jpg']);
            }
            if (isset($thumb)) {
                $Obj->attachMedia($thumb, ['main_img_thumb']);
            }
            if (isset($mp3_media)) {
                $Obj->attachMedia($mp3_media, ['paragraph_as_audio']);
            }

            //بدء التعامل مع اوراق العمل
            if (isset($request->Worksheets)) {
                foreach ($request->Worksheets as $key => $Worksheet) {

                    $id_p = !empty($Worksheet['id']) ? $Worksheet['id'] : NULL;
                    $Worksheet['paragraph_id'] = $Obj->id;

                    $file1 = isset($Worksheet['worksheet_file']) ? $Worksheet['worksheet_file'] : NULL;

                    if (!empty($file1)) {
                        try {


                            $uploader3 = \MediaUploader::fromSource($file1);
                            $uploader3->setAllowedMimeTypes(['application/pdf']);
                            $uploader3->setAllowedExtensions(['pdf']);
                            $uploader3->toDirectory('paragraphs/' . date("Y") . '/' . date("m") . '/' . date("d"));
                            $uploader3->useFilename(str_random(5));
                            $pdf_file = $uploader3->upload();


                        } catch (\Exception $e) {
                            $results['Errors'] = array(
                                'title' => trans('general.error'),
                                'content' => $e->getMessage(),
                            );
                        }


                        $Worksheet_O = \App\Models\Worksheets::updateOrCreate(
                            ['id' => $id_p, 'paragraph_id' => $Obj->id],
                            $Worksheet
                        );

                        if ($Worksheet_O) {
                            if (isset($pdf_file)) {
                                $Worksheet_O->attachMedia($pdf_file, ['Worksheet']);
                            }

                        }
                    }


                }
            }

            if ($Obj->wasRecentlyCreated) {
                $content = trans("general.successfully_added_new_:name", ['name' => trans("classes.paragraph")]);
                $Obj->created_by = Auth::user()->id;
                $Obj->update();

            } else {

                $content = trans("general.modified_successfully");
                $Obj->updated_by = Auth::user()->id;
                $Obj->update();

            }

            if (!empty($Obj->id)) {
                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => $content,
                );
            }

        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }

        echo json_encode($results);


    }

    public function DeleteParagraphs(Request $request)
    {
        $results = array();
        if ($this->Delete) {
            $ReqData = $request->all();
            $id = isset($ReqData['id']) ? mb_strtolower($ReqData['id']) : NULL;

            $q = \App\Models\Paragraphs::where('id', '=', $id)
                ->where('deleted_at', NULL)
                ->update([
                    'deleted_at' => date("Y-m-d H:i:s"),
                    'deleted_by' => Auth::user()->id,
                ]);

            if (!empty($q)) {
                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => trans('general.name_successfully_deleted', array('name' => trans('classes.the_book'))),
                );
            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans('general.the_operation_was_not_done_correctly'),
                );
            }
        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);


    }

    public function DeleteWorkSheet(Request $request)
    {
        App::setLocale($request->lang);

        $results = array();
        if ($this->Delete) {
            $ReqData = $request->all();
            $id = isset($ReqData['id']) ? mb_strtolower($ReqData['id']) : NULL;
            $par2 = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;
            $par3 = isset($ReqData['par3']) ? mb_strtolower($ReqData['par3']) : NULL;
            if (in_array($par3, array("worksheet"))) {
                if ($par3 == "worksheet") {
                    $q = \App\Models\Worksheets::where('id', '=', $id)
                        ->where('deleted_at', NULL)
                        ->update([
                            'deleted_at' => date("Y-m-d H:i:s"),
                            'deleted_by' => Auth::user()->id,
                        ]);
                }


                if (!empty($q)) {
                    $results['Success'] = array(
                        'title' => trans('general.done'),
                        'content' => trans('general.name_successfully_deleted', array('name' => $par2)),
                    );
                } else {
                    $results['Errors'] = array(
                        'title' => trans('general.error'),
                        'content' => trans('general.the_operation_was_not_done_correctly'),
                    );
                }
            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans('general.the_operation_was_not_done_correctly'),
                );
            }


        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);


    }
    public function DeletePSound(Request $request)
    {
        App::setLocale($request->lang);

        $results = array();
        if ($this->Delete) {
            $ReqData = $request->all();
            $id = isset($ReqData['id']) ? mb_strtolower($ReqData['id']) : NULL;
            $par1 = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

            $q = \Plank\Mediable\Media::where('id',$id)->delete();




                if (!empty($q)) {
                    $results['Success'] = array(
                        'title' => trans('general.done'),
                        'content' => trans('general.name_successfully_deleted', array('name' => trans("classes.audio_file"))),
                    );
                } else {
                    $results['Errors'] = array(
                        'title' => trans('general.error'),
                        'content' => trans('general.the_operation_was_not_done_correctly'),
                    );
                }



        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);


    }

    function str_replace()
    {
        $str = '
        span.cls_003{font-family:Times,serif;font-size:36.3px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:Times,serif;font-size:36.3px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Times,serif;font-size:36.3px;color:rgb(40,80,132);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_004{font-family:Times,serif;font-size:36.3px;color:rgb(40,80,132);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_005{font-family:Times,serif;font-size:22.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Times,serif;font-size:22.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:Times,serif;font-size:23.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Times,serif;font-size:23.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Times,serif;font-size:23.1px;color:rgb(40,80,132);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:Times,serif;font-size:23.1px;color:rgb(40,80,132);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Times,serif;font-size:21.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Times,serif;font-size:21.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Times,serif;font-size:19.1px;color:rgb(92,91,91);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Times,serif;font-size:19.1px;color:rgb(92,91,91);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_002{font-family:Times,serif;font-size:21.3px;color:rgb(255,213,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:Times,serif;font-size:21.3px;color:rgb(255,213,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:"David Bold",serif;font-size:33.0px;color:rgb(255,213,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_011{font-family:"David Bold",serif;font-size:33.0px;color:rgb(255,213,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_010{font-family:"David Bold",serif;font-size:33.0px;color:rgb(217,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_010{font-family:"David Bold",serif;font-size:33.0px;color:rgb(217,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_012{font-family:"David Bold",serif;font-size:26.1px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_012{font-family:"David Bold",serif;font-size:26.1px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_013{font-family:"David Bold",serif;font-size:20.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_013{font-family:"David Bold",serif;font-size:20.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_014{font-family:"David",serif;font-size:20.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:"David",serif;font-size:20.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:"David Bold",serif;font-size:40.0px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_015{font-family:"David Bold",serif;font-size:40.0px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_016{font-family:"David Bold",serif;font-size:23.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_016{font-family:"David Bold",serif;font-size:23.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_017{font-family:"David",serif;font-size:23.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:"David",serif;font-size:23.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:"David Bold",serif;font-size:54.0px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_018{font-family:"David Bold",serif;font-size:54.0px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_019{font-family:"David Bold",serif;font-size:50.0px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_019{font-family:"David Bold",serif;font-size:50.0px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_020{font-family:"David Bold",serif;font-size:30.7px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_020{font-family:"David Bold",serif;font-size:30.7px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_022{font-family:"David Bold",serif;font-size:30.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_022{font-family:"David Bold",serif;font-size:30.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_023{font-family:"David Bold",serif;font-size:20.1px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_023{font-family:"David Bold",serif;font-size:20.1px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_021{font-family:Times,serif;font-size:12.7px;color:rgb(254,255,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_021{font-family:Times,serif;font-size:12.7px;color:rgb(254,255,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_024{font-family:"David Bold",serif;font-size:19.3px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_024{font-family:"David Bold",serif;font-size:19.3px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_025{font-family:"David Bold",serif;font-size:20.1px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_025{font-family:"David Bold",serif;font-size:20.1px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_026{font-family:"David",serif;font-size:20.1px;color:rgb(254,255,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_026{font-family:"David",serif;font-size:20.1px;color:rgb(254,255,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_027{font-family:Times,serif;font-size:12.1px;color:rgb(254,255,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_027{font-family:Times,serif;font-size:12.1px;color:rgb(254,255,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_028{font-family:"David Bold",serif;font-size:22.6px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_028{font-family:"David Bold",serif;font-size:22.6px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_029{font-family:"David",serif;font-size:21.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_029{font-family:"David",serif;font-size:21.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_030{font-family:"David Bold",serif;font-size:51.0px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_030{font-family:"David Bold",serif;font-size:51.0px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_031{font-family:"David Bold",serif;font-size:46.0px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_031{font-family:"David Bold",serif;font-size:46.0px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_032{font-family:"David Bold",serif;font-size:43.0px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_032{font-family:"David Bold",serif;font-size:43.0px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_033{font-family:"David Bold",serif;font-size:20.1px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_033{font-family:"David Bold",serif;font-size:20.1px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_034{font-family:"David Bold",serif;font-size:42.0px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_034{font-family:"David Bold",serif;font-size:42.0px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_037{font-family:"David Bold",serif;font-size:60.0px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_037{font-family:"David Bold",serif;font-size:60.0px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_035{font-family:"David",serif;font-size:28.3px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_035{font-family:"David",serif;font-size:28.3px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_036{font-family:"David",serif;font-size:19.4px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_036{font-family:"David",serif;font-size:19.4px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_038{font-family:"Brush Script MT Italic",serif;font-size:20.1px;color:rgb(218,32,36);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_038{font-family:"Brush Script MT Italic",serif;font-size:20.1px;color:rgb(218,32,36);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_040{font-family:"David Bold",serif;font-size:57.0px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_040{font-family:"David Bold",serif;font-size:57.0px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_039{font-family:"David Bold",serif;font-size:49.0px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_039{font-family:"David Bold",serif;font-size:49.0px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_042{font-family:"David Bold",serif;font-size:20.1px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_042{font-family:"David Bold",serif;font-size:20.1px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_043{font-family:"David",serif;font-size:22.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_043{font-family:"David",serif;font-size:22.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_044{font-family:"David Bold",serif;font-size:36.6px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_044{font-family:"David Bold",serif;font-size:36.6px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_045{font-family:"David Bold",serif;font-size:52.3px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_045{font-family:"David Bold",serif;font-size:52.3px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_046{font-family:"David Bold",serif;font-size:16.8px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_046{font-family:"David Bold",serif;font-size:16.8px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_047{font-family:"David Bold",serif;font-size:25.1px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_047{font-family:"David Bold",serif;font-size:25.1px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_048{font-family:"David Bold",serif;font-size:17.9px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_048{font-family:"David Bold",serif;font-size:17.9px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_049{font-family:"David",serif;font-size:20.1px;color:rgb(218,32,36);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_049{font-family:"David",serif;font-size:20.1px;color:rgb(218,32,36);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_051{font-family:"David Bold",serif;font-size:57.0px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_051{font-family:"David Bold",serif;font-size:57.0px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_050{font-family:"David Bold",serif;font-size:50.0px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_050{font-family:"David Bold",serif;font-size:50.0px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_052{font-family:"David Bold",serif;font-size:20.1px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_052{font-family:"David Bold",serif;font-size:20.1px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_053{font-family:"David",serif;font-size:19.8px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_053{font-family:"David",serif;font-size:19.8px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_054{font-family:"David Bold",serif;font-size:60.0px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_054{font-family:"David Bold",serif;font-size:60.0px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_055{font-family:"David",serif;font-size:24.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_055{font-family:"David",serif;font-size:24.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_056{font-family:Times,serif;font-size:14.1px;color:rgb(254,255,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_056{font-family:Times,serif;font-size:14.1px;color:rgb(254,255,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_057{font-family:"David",serif;font-size:18.5px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_057{font-family:"David",serif;font-size:18.5px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_058{font-family:"David Bold",serif;font-size:57.0px;color:rgb(0,52,69);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_058{font-family:"David Bold",serif;font-size:57.0px;color:rgb(0,52,69);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_059{font-family:"David Bold",serif;font-size:50.0px;color:rgb(0,52,69);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_059{font-family:"David Bold",serif;font-size:50.0px;color:rgb(0,52,69);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_060{font-family:"David Bold",serif;font-size:29.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_060{font-family:"David Bold",serif;font-size:29.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_061{font-family:"David Bold",serif;font-size:20.1px;color:rgb(0,52,69);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_061{font-family:"David Bold",serif;font-size:20.1px;color:rgb(0,52,69);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_063{font-family:"David Bold",serif;font-size:40.0px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_063{font-family:"David Bold",serif;font-size:40.0px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_062{font-family:"David Bold",serif;font-size:54.0px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_062{font-family:"David Bold",serif;font-size:54.0px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_064{font-family:"David",serif;font-size:10.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_064{font-family:"David",serif;font-size:10.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_065{font-family:"David",serif;font-size:15.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_065{font-family:"David",serif;font-size:15.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_066{font-family:"David Bold",serif;font-size:25.8px;color:rgb(0,52,69);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_066{font-family:"David Bold",serif;font-size:25.8px;color:rgb(0,52,69);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_067{font-family:"David Bold",serif;font-size:25.8px;color:rgb(25,81,148);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_067{font-family:"David Bold",serif;font-size:25.8px;color:rgb(25,81,148);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_068{font-family:"David Bold",serif;font-size:25.8px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_068{font-family:"David Bold",serif;font-size:25.8px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_069{font-family:"David Bold",serif;font-size:25.8px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_069{font-family:"David Bold",serif;font-size:25.8px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_070{font-family:"David Bold",serif;font-size:25.8px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_070{font-family:"David Bold",serif;font-size:25.8px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_071{font-family:"David Bold",serif;font-size:25.8px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_071{font-family:"David Bold",serif;font-size:25.8px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_072{font-family:"David",serif;font-size:19.7px;color:rgb(80,131,180);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_072{font-family:"David",serif;font-size:19.7px;color:rgb(80,131,180);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_073{font-family:"David Bold",serif;font-size:19.7px;color:rgb(80,131,180);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_073{font-family:"David Bold",serif;font-size:19.7px;color:rgb(80,131,180);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_074{font-family:"David Bold",serif;font-size:26.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_074{font-family:"David Bold",serif;font-size:26.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_075{font-family:"David Bold",serif;font-size:25.3px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_075{font-family:"David Bold",serif;font-size:25.3px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_076{font-family:"David Bold",serif;font-size:19.8px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_076{font-family:"David Bold",serif;font-size:19.8px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_077{font-family:"David Bold",serif;font-size:32.0px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_077{font-family:"David Bold",serif;font-size:32.0px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_078{font-family:"David Bold",serif;font-size:32.8px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_078{font-family:"David Bold",serif;font-size:32.8px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_079{font-family:"David Bold",serif;font-size:32.8px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_079{font-family:"David Bold",serif;font-size:32.8px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_080{font-family:"David",serif;font-size:20.1px;color:rgb(80,131,180);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_080{font-family:"David",serif;font-size:20.1px;color:rgb(80,131,180);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_081{font-family:"David Bold",serif;font-size:20.2px;color:rgb(80,131,180);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_081{font-family:"David Bold",serif;font-size:20.2px;color:rgb(80,131,180);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_082{font-family:"David Bold",serif;font-size:20.2px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_082{font-family:"David Bold",serif;font-size:20.2px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_083{font-family:"David",serif;font-size:17.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_083{font-family:"David",serif;font-size:17.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_084{font-family:"David Bold",serif;font-size:60.0px;color:rgb(111,182,54);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_084{font-family:"David Bold",serif;font-size:60.0px;color:rgb(111,182,54);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_085{font-family:"David Bold",serif;font-size:19.8px;color:rgb(227,31,37);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_085{font-family:"David Bold",serif;font-size:19.8px;color:rgb(227,31,37);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_086{font-family:"David Bold",serif;font-size:22.1px;color:rgb(67,67,66);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_086{font-family:"David Bold",serif;font-size:22.1px;color:rgb(67,67,66);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_088{font-family:"David Bold",serif;font-size:42.0px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_088{font-family:"David Bold",serif;font-size:42.0px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_089{font-family:"David Bold",serif;font-size:42.0px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_089{font-family:"David Bold",serif;font-size:42.0px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_090{font-family:"David Bold",serif;font-size:42.0px;color:rgb(236,98,90);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_090{font-family:"David Bold",serif;font-size:42.0px;color:rgb(236,98,90);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_091{font-family:"David Bold",serif;font-size:42.0px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_091{font-family:"David Bold",serif;font-size:42.0px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_092{font-family:"David Bold",serif;font-size:42.0px;color:rgb(72,72,71);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_092{font-family:"David Bold",serif;font-size:42.0px;color:rgb(72,72,71);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_094{font-family:"David Bold",serif;font-size:57.0px;color:rgb(78,128,46);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_094{font-family:"David Bold",serif;font-size:57.0px;color:rgb(78,128,46);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_093{font-family:"David Bold",serif;font-size:51.0px;color:rgb(164,187,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_093{font-family:"David Bold",serif;font-size:51.0px;color:rgb(164,187,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_095{font-family:"David Bold",serif;font-size:27.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_095{font-family:"David Bold",serif;font-size:27.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_096{font-family:"David Bold",serif;font-size:65.0px;color:rgb(78,128,46);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_096{font-family:"David Bold",serif;font-size:65.0px;color:rgb(78,128,46);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_099{font-family:"David Bold",serif;font-size:57.0px;color:rgb(227,31,37);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_099{font-family:"David Bold",serif;font-size:57.0px;color:rgb(227,31,37);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_098{font-family:"David Bold",serif;font-size:50.0px;color:rgb(204,38,39);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_098{font-family:"David Bold",serif;font-size:50.0px;color:rgb(204,38,39);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_097{font-family:"David Bold",serif;font-size:66.0px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_097{font-family:"David Bold",serif;font-size:66.0px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_100{font-family:"David Bold",serif;font-size:64.6px;color:rgb(231,43,125);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_100{font-family:"David Bold",serif;font-size:64.6px;color:rgb(231,43,125);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_101{font-family:"David Bold",serif;font-size:20.1px;color:rgb(231,43,125);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_101{font-family:"David Bold",serif;font-size:20.1px;color:rgb(231,43,125);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_102{font-family:"David Bold",serif;font-size:24.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_102{font-family:"David Bold",serif;font-size:24.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_103{font-family:"David Bold",serif;font-size:22.1px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_103{font-family:"David Bold",serif;font-size:22.1px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_104{font-family:"Arabic Typesetting",serif;font-size:22.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_104{font-family:"Arabic Typesetting",serif;font-size:22.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_105{font-family:"David Bold",serif;font-size:24.1px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_105{font-family:"David Bold",serif;font-size:24.1px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_107{font-family:"David Bold",serif;font-size:28.5px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_107{font-family:"David Bold",serif;font-size:28.5px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_106{font-family:"David Bold",serif;font-size:28.5px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_106{font-family:"David Bold",serif;font-size:28.5px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_108{font-family:"David Bold",serif;font-size:28.5px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_108{font-family:"David Bold",serif;font-size:28.5px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_109{font-family:"David Bold",serif;font-size:28.5px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_109{font-family:"David Bold",serif;font-size:28.5px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_110{font-family:"David",serif;font-size:19.6px;color:rgb(226,81,34);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_110{font-family:"David",serif;font-size:19.6px;color:rgb(226,81,34);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_111{font-family:"David",serif;font-size:19.6px;color:rgb(201,209,83);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_111{font-family:"David",serif;font-size:19.6px;color:rgb(201,209,83);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_112{font-family:"David Bold",serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_112{font-family:"David Bold",serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_113{font-family:"David Bold",serif;font-size:30.1px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_113{font-family:"David Bold",serif;font-size:30.1px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_114{font-family:Times,serif;font-size:10.5px;color:rgb(254,255,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_114{font-family:Times,serif;font-size:10.5px;color:rgb(254,255,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_115{font-family:"David",serif;font-size:33.0px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_115{font-family:"David",serif;font-size:33.0px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_116{font-family:"David Bold",serif;font-size:47.0px;color:rgb(198,53,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_116{font-family:"David Bold",serif;font-size:47.0px;color:rgb(198,53,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_117{font-family:"David Bold",serif;font-size:69.0px;color:rgb(223,67,71);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_117{font-family:"David Bold",serif;font-size:69.0px;color:rgb(223,67,71);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_118{font-family:"David Bold",serif;font-size:73.1px;color:rgb(78,128,46);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_118{font-family:"David Bold",serif;font-size:73.1px;color:rgb(78,128,46);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_119{font-family:"David Bold",serif;font-size:20.1px;color:rgb(78,128,46);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_119{font-family:"David Bold",serif;font-size:20.1px;color:rgb(78,128,46);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_122{font-family:"David Bold",serif;font-size:50.0px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_122{font-family:"David Bold",serif;font-size:50.0px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_123{font-family:"David Bold",serif;font-size:69.0px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_123{font-family:"David Bold",serif;font-size:69.0px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_124{font-family:"David Bold",serif;font-size:26.2px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_124{font-family:"David Bold",serif;font-size:26.2px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_120{font-family:"David Bold",serif;font-size:37.0px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_120{font-family:"David Bold",serif;font-size:37.0px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_121{font-family:"David Bold",serif;font-size:48.6px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_121{font-family:"David Bold",serif;font-size:48.6px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_125{font-family:"David Bold",serif;font-size:73.1px;color:rgb(231,43,125);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_125{font-family:"David Bold",serif;font-size:73.1px;color:rgb(231,43,125);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_126{font-family:"David Bold",serif;font-size:24.1px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_126{font-family:"David Bold",serif;font-size:24.1px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_127{font-family:"Arabic Typesetting",serif;font-size:26.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_127{font-family:"Arabic Typesetting",serif;font-size:26.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_128{font-family:"David",serif;font-size:26.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_128{font-family:"David",serif;font-size:26.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_129{font-family:"David",serif;font-size:25.3px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_129{font-family:"David",serif;font-size:25.3px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_130{font-family:"David",serif;font-size:20.1px;color:rgb(92,91,91);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_130{font-family:"David",serif;font-size:20.1px;color:rgb(92,91,91);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_131{font-family:"David Bold",serif;font-size:19.1px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_131{font-family:"David Bold",serif;font-size:19.1px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_132{font-family:"David Bold",serif;font-size:28.5px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_132{font-family:"David Bold",serif;font-size:28.5px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_133{font-family:"David",serif;font-size:20.1px;color:rgb(69,166,61);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_133{font-family:"David",serif;font-size:20.1px;color:rgb(69,166,61);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_134{font-family:"David Bold",serif;font-size:27.7px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_134{font-family:"David Bold",serif;font-size:27.7px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_135{font-family:"David Bold",serif;font-size:20.1px;color:rgb(0,139,201);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_135{font-family:"David Bold",serif;font-size:20.1px;color:rgb(0,139,201);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_136{font-family:"David",serif;font-size:20.1px;color:rgb(0,139,201);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_136{font-family:"David",serif;font-size:20.1px;color:rgb(0,139,201);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_137{font-family:"David Bold",serif;font-size:34.8px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_137{font-family:"David Bold",serif;font-size:34.8px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_138{font-family:"David Bold",serif;font-size:24.1px;color:rgb(56,186,235);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_138{font-family:"David Bold",serif;font-size:24.1px;color:rgb(56,186,235);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_139{font-family:"David Bold",serif;font-size:57.0px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_139{font-family:"David Bold",serif;font-size:57.0px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_141{font-family:"David Bold",serif;font-size:21.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_141{font-family:"David Bold",serif;font-size:21.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_142{font-family:"David Bold",serif;font-size:28.1px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_142{font-family:"David Bold",serif;font-size:28.1px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_143{font-family:"David Bold",serif;font-size:28.1px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_143{font-family:"David Bold",serif;font-size:28.1px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_144{font-family:"David Bold",serif;font-size:20.1px;color:rgb(229,12,128);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_144{font-family:"David Bold",serif;font-size:20.1px;color:rgb(229,12,128);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_145{font-family:"David",serif;font-size:12.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_145{font-family:"David",serif;font-size:12.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_146{font-family:"David",serif;font-size:21.5px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_146{font-family:"David",serif;font-size:21.5px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_147{font-family:"David Bold",serif;font-size:21.5px;color:rgb(197,164,105);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_147{font-family:"David Bold",serif;font-size:21.5px;color:rgb(197,164,105);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_148{font-family:"David Bold",serif;font-size:28.5px;color:rgb(229,12,128);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_148{font-family:"David Bold",serif;font-size:28.5px;color:rgb(229,12,128);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_149{font-family:"David",serif;font-size:20.1px;color:rgb(207,37,39);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_149{font-family:"David",serif;font-size:20.1px;color:rgb(207,37,39);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_150{font-family:"David",serif;font-size:25.1px;color:rgb(75,74,73);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_150{font-family:"David",serif;font-size:25.1px;color:rgb(75,74,73);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_151{font-family:"David",serif;font-size:6.0px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_151{font-family:"David",serif;font-size:6.0px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_152{font-family:"David Bold",serif;font-size:57.3px;color:rgb(229,12,128);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_152{font-family:"David Bold",serif;font-size:57.3px;color:rgb(229,12,128);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_153{font-family:"David Bold",serif;font-size:55.2px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_153{font-family:"David Bold",serif;font-size:55.2px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_154{font-family:"David Bold",serif;font-size:56.1px;color:rgb(193,39,39);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_154{font-family:"David Bold",serif;font-size:56.1px;color:rgb(193,39,39);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_155{font-family:"David Bold",serif;font-size:58.6px;color:rgb(99,114,180);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_155{font-family:"David Bold",serif;font-size:58.6px;color:rgb(99,114,180);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_156{font-family:"David Bold",serif;font-size:36.0px;color:rgb(131,178,51);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_156{font-family:"David Bold",serif;font-size:36.0px;color:rgb(131,178,51);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_157{font-family:"David Bold",serif;font-size:28.1px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_157{font-family:"David Bold",serif;font-size:28.1px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_158{font-family:"David",serif;font-size:20.1px;color:rgb(36,36,34);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_158{font-family:"David",serif;font-size:20.1px;color:rgb(36,36,34);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_159{font-family:"Arabic Typesetting",serif;font-size:28.1px;color:rgb(36,36,34);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_159{font-family:"Arabic Typesetting",serif;font-size:28.1px;color:rgb(36,36,34);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_160{font-family:"David Bold",serif;font-size:24.1px;color:rgb(36,36,34);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_160{font-family:"David Bold",serif;font-size:24.1px;color:rgb(36,36,34);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_161{font-family:Times,serif;font-size:10.7px;color:rgb(254,255,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_161{font-family:Times,serif;font-size:10.7px;color:rgb(254,255,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_162{font-family:"David",serif;font-size:9.1px;color:rgb(36,36,34);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_162{font-family:"David",serif;font-size:9.1px;color:rgb(36,36,34);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_163{font-family:"David Bold",serif;font-size:20.4px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_163{font-family:"David Bold",serif;font-size:20.4px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_169{font-family:"David Bold",serif;font-size:33.1px;color:rgb(220,34,38);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_169{font-family:"David Bold",serif;font-size:33.1px;color:rgb(220,34,38);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_168{font-family:"David Bold",serif;font-size:33.1px;color:rgb(53,185,235);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_168{font-family:"David Bold",serif;font-size:33.1px;color:rgb(53,185,235);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_167{font-family:"David Bold",serif;font-size:33.1px;color:rgb(112,180,149);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_167{font-family:"David Bold",serif;font-size:33.1px;color:rgb(112,180,149);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_164{font-family:"David Bold",serif;font-size:38.0px;color:rgb(220,34,38);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_164{font-family:"David Bold",serif;font-size:38.0px;color:rgb(220,34,38);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_165{font-family:"David Bold",serif;font-size:38.0px;color:rgb(53,185,235);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_165{font-family:"David Bold",serif;font-size:38.0px;color:rgb(53,185,235);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_166{font-family:"David Bold",serif;font-size:38.0px;color:rgb(112,180,149);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_166{font-family:"David Bold",serif;font-size:38.0px;color:rgb(112,180,149);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_171{font-family:"David Bold",serif;font-size:10.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_171{font-family:"David Bold",serif;font-size:10.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_170{font-family:"David Bold",serif;font-size:28.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_170{font-family:"David Bold",serif;font-size:28.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_172{font-family:"David",serif;font-size:19.9px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_172{font-family:"David",serif;font-size:19.9px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_173{font-family:"David Bold",serif;font-size:19.9px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_173{font-family:"David Bold",serif;font-size:19.9px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_174{font-family:"David Bold",serif;font-size:19.9px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_174{font-family:"David Bold",serif;font-size:19.9px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_175{font-family:"David",serif;font-size:21.8px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_175{font-family:"David",serif;font-size:21.8px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_176{font-family:"David",serif;font-size:7.0px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_176{font-family:"David",serif;font-size:7.0px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_178{font-family:"David",serif;font-size:23.8px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_178{font-family:"David",serif;font-size:23.8px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_179{font-family:"David",serif;font-size:19.9px;color:rgb(218,32,36);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_179{font-family:"David",serif;font-size:19.9px;color:rgb(218,32,36);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_180{font-family:"David",serif;font-size:19.6px;color:rgb(0,121,170);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_180{font-family:"David",serif;font-size:19.6px;color:rgb(0,121,170);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_181{font-family:Arial,serif;font-size:20.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_181{font-family:Arial,serif;font-size:20.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_182{font-family:"David Bold",serif;font-size:29.0px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_182{font-family:"David Bold",serif;font-size:29.0px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_183{font-family:"David Bold",serif;font-size:24.8px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_183{font-family:"David Bold",serif;font-size:24.8px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_184{font-family:"David Bold",serif;font-size:19.8px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_184{font-family:"David Bold",serif;font-size:19.8px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_185{font-family:"David Bold",serif;font-size:24.8px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_185{font-family:"David Bold",serif;font-size:24.8px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_186{font-family:"David",serif;font-size:19.5px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_186{font-family:"David",serif;font-size:19.5px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_187{font-family:"David Bold",serif;font-size:19.5px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_187{font-family:"David Bold",serif;font-size:19.5px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_188{font-family:"David",serif;font-size:16.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_188{font-family:"David",serif;font-size:16.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_189{font-family:"David Bold",serif;font-size:50.0px;color:rgb(230,86,100);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_189{font-family:"David Bold",serif;font-size:50.0px;color:rgb(230,86,100);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_191{font-family:"David Bold",serif;font-size:47.9px;color:rgb(230,86,100);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_191{font-family:"David Bold",serif;font-size:47.9px;color:rgb(230,86,100);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_190{font-family:"David Bold",serif;font-size:47.9px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_190{font-family:"David Bold",serif;font-size:47.9px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_193{font-family:"David Bold",serif;font-size:32.5px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_193{font-family:"David Bold",serif;font-size:32.5px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_192{font-family:"David",serif;font-size:21.3px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_192{font-family:"David",serif;font-size:21.3px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_194{font-family:"David Bold",serif;font-size:25.5px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_194{font-family:"David Bold",serif;font-size:25.5px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_195{font-family:"David Bold",serif;font-size:20.1px;color:rgb(207,37,39);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_195{font-family:"David Bold",serif;font-size:20.1px;color:rgb(207,37,39);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_196{font-family:"David",serif;font-size:30.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_196{font-family:"David",serif;font-size:30.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_197{font-family:"David Bold",serif;font-size:69.1px;color:rgb(139,108,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_197{font-family:"David Bold",serif;font-size:69.1px;color:rgb(139,108,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_198{font-family:"David Bold",serif;font-size:28.4px;color:rgb(139,108,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_198{font-family:"David Bold",serif;font-size:28.4px;color:rgb(139,108,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_199{font-family:"David",serif;font-size:20.2px;color:rgb(218,32,36);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_199{font-family:"David",serif;font-size:20.2px;color:rgb(218,32,36);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_200{font-family:"David",serif;font-size:19.8px;color:rgb(218,32,36);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_200{font-family:"David",serif;font-size:19.8px;color:rgb(218,32,36);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_201{font-family:"David Bold",serif;font-size:25.3px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_201{font-family:"David Bold",serif;font-size:25.3px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_202{font-family:"David Bold",serif;font-size:25.4px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_202{font-family:"David Bold",serif;font-size:25.4px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_203{font-family:"David Bold",serif;font-size:19.5px;color:rgb(31,104,123);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_203{font-family:"David Bold",serif;font-size:19.5px;color:rgb(31,104,123);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_204{font-family:"David",serif;font-size:19.5px;color:rgb(68,121,147);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_204{font-family:"David",serif;font-size:19.5px;color:rgb(68,121,147);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_205{font-family:"David Bold",serif;font-size:24.3px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_205{font-family:"David Bold",serif;font-size:24.3px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_206{font-family:"David Bold",serif;font-size:35.0px;color:rgb(237,111,31);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_206{font-family:"David Bold",serif;font-size:35.0px;color:rgb(237,111,31);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_207{font-family:"David",serif;font-size:25.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_207{font-family:"David",serif;font-size:25.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_208{font-family:"David Bold",serif;font-size:14.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_208{font-family:"David Bold",serif;font-size:14.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_213{font-family:Times,serif;font-size:31.1px;color:rgb(0,52,69);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_213{font-family:Times,serif;font-size:31.1px;color:rgb(0,52,69);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_209{font-family:"David Bold",serif;font-size:22.1px;color:rgb(231,61,70);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_209{font-family:"David Bold",serif;font-size:22.1px;color:rgb(231,61,70);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_210{font-family:"David Bold",serif;font-size:22.1px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_210{font-family:"David Bold",serif;font-size:22.1px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_211{font-family:"David Bold",serif;font-size:22.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_211{font-family:"David Bold",serif;font-size:22.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_212{font-family:"David Bold",serif;font-size:17.8px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_212{font-family:"David Bold",serif;font-size:17.8px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_215{font-family:"David Bold",serif;font-size:18.1px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_215{font-family:"David Bold",serif;font-size:18.1px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_214{font-family:"David Bold",serif;font-size:20.1px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_214{font-family:"David Bold",serif;font-size:20.1px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_216{font-family:"David Bold",serif;font-size:20.1px;color:rgb(68,121,147);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_216{font-family:"David Bold",serif;font-size:20.1px;color:rgb(68,121,147);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_218{font-family:"David Bold",serif;font-size:17.0px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_218{font-family:"David Bold",serif;font-size:17.0px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_219{font-family:"David",serif;font-size:9.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_219{font-family:"David",serif;font-size:9.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_220{font-family:"David",serif;font-size:20.7px;color:rgb(231,61,70);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_220{font-family:"David",serif;font-size:20.7px;color:rgb(231,61,70);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_221{font-family:"David Bold",serif;font-size:27.2px;color:rgb(153,100,25);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_221{font-family:"David Bold",serif;font-size:27.2px;color:rgb(153,100,25);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_222{font-family:"David Bold",serif;font-size:26.1px;color:rgb(153,100,25);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_222{font-family:"David Bold",serif;font-size:26.1px;color:rgb(153,100,25);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_223{font-family:"David Bold",serif;font-size:27.2px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_223{font-family:"David Bold",serif;font-size:27.2px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_224{font-family:"David Bold",serif;font-size:27.2px;color:rgb(229,12,128);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_224{font-family:"David Bold",serif;font-size:27.2px;color:rgb(229,12,128);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_226{font-family:"David Bold",serif;font-size:27.2px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_226{font-family:"David Bold",serif;font-size:27.2px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_225{font-family:"David Bold",serif;font-size:27.2px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_225{font-family:"David Bold",serif;font-size:27.2px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_217{font-family:"David Bold",serif;font-size:20.1px;color:rgb(231,61,70);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_217{font-family:"David Bold",serif;font-size:20.1px;color:rgb(231,61,70);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_227{font-family:"David Bold",serif;font-size:20.7px;color:rgb(231,61,70);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_227{font-family:"David Bold",serif;font-size:20.7px;color:rgb(231,61,70);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_228{font-family:"David Bold",serif;font-size:45.0px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_228{font-family:"David Bold",serif;font-size:45.0px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_229{font-family:"David Bold",serif;font-size:19.6px;color:rgb(233,74,77);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_229{font-family:"David Bold",serif;font-size:19.6px;color:rgb(233,74,77);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_230{font-family:"David Bold",serif;font-size:23.3px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_230{font-family:"David Bold",serif;font-size:23.3px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_232{font-family:"David Bold",serif;font-size:41.0px;color:rgb(243,150,26);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_232{font-family:"David Bold",serif;font-size:41.0px;color:rgb(243,150,26);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_231{font-family:"David Bold",serif;font-size:41.0px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_231{font-family:"David Bold",serif;font-size:41.0px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_140{font-family:"David Bold",serif;font-size:29.3px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_140{font-family:"David Bold",serif;font-size:29.3px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_233{font-family:"David Bold",serif;font-size:6.0px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_233{font-family:"David Bold",serif;font-size:6.0px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_234{font-family:"David Bold",serif;font-size:29.3px;color:rgb(229,12,128);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_234{font-family:"David Bold",serif;font-size:29.3px;color:rgb(229,12,128);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_235{font-family:"David Bold",serif;font-size:29.3px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_235{font-family:"David Bold",serif;font-size:29.3px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_236{font-family:"David Bold",serif;font-size:29.3px;color:rgb(153,100,25);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_236{font-family:"David Bold",serif;font-size:29.3px;color:rgb(153,100,25);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_237{font-family:"David Bold",serif;font-size:24.2px;color:rgb(153,100,25);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_237{font-family:"David Bold",serif;font-size:24.2px;color:rgb(153,100,25);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_240{font-family:"David Bold",serif;font-size:29.1px;color:rgb(229,12,128);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_240{font-family:"David Bold",serif;font-size:29.1px;color:rgb(229,12,128);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_238{font-family:"David Bold",serif;font-size:29.1px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_238{font-family:"David Bold",serif;font-size:29.1px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_239{font-family:"David Bold",serif;font-size:29.1px;color:rgb(248,175,2);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_239{font-family:"David Bold",serif;font-size:29.1px;color:rgb(248,175,2);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_241{font-family:"David",serif;font-size:19.3px;color:rgb(0,160,227);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_241{font-family:"David",serif;font-size:19.3px;color:rgb(0,160,227);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_242{font-family:"Arabic Typesetting",serif;font-size:19.3px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_242{font-family:"Arabic Typesetting",serif;font-size:19.3px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_243{font-family:"David",serif;font-size:19.3px;color:rgb(218,32,36);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_243{font-family:"David",serif;font-size:19.3px;color:rgb(218,32,36);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_244{font-family:"David Bold",serif;font-size:25.0px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_244{font-family:"David Bold",serif;font-size:25.0px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_245{font-family:"David Bold",serif;font-size:19.9px;color:rgb(25,81,148);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_245{font-family:"David Bold",serif;font-size:19.9px;color:rgb(25,81,148);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_246{font-family:"David",serif;font-size:60.0px;color:rgb(207,37,39);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_246{font-family:"David",serif;font-size:60.0px;color:rgb(207,37,39);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_247{font-family:"David",serif;font-size:56.3px;color:rgb(99,114,180);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_247{font-family:"David",serif;font-size:56.3px;color:rgb(99,114,180);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_248{font-family:"David Bold",serif;font-size:52.4px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_248{font-family:"David Bold",serif;font-size:52.4px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_249{font-family:"David Bold",serif;font-size:53.0px;color:rgb(229,12,128);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_249{font-family:"David Bold",serif;font-size:53.0px;color:rgb(229,12,128);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_250{font-family:"Brush Script MT Italic",serif;font-size:50.0px;color:rgb(43,42,41);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_250{font-family:"Brush Script MT Italic",serif;font-size:50.0px;color:rgb(43,42,41);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_251{font-family:"David Bold",serif;font-size:49.1px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_251{font-family:"David Bold",serif;font-size:49.1px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_252{font-family:"David Bold",serif;font-size:57.9px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_252{font-family:"David Bold",serif;font-size:57.9px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_253{font-family:"David Bold",serif;font-size:58.5px;color:rgb(135,179,50);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_253{font-family:"David Bold",serif;font-size:58.5px;color:rgb(135,179,50);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_254{font-family:"David Bold",serif;font-size:20.4px;color:rgb(231,61,70);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_254{font-family:"David Bold",serif;font-size:20.4px;color:rgb(231,61,70);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_255{font-family:"David",serif;font-size:20.4px;color:rgb(231,61,70);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_255{font-family:"David",serif;font-size:20.4px;color:rgb(231,61,70);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_256{font-family:"David",serif;font-size:22.4px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_256{font-family:"David",serif;font-size:22.4px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_257{font-family:"David",serif;font-size:6.0px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_257{font-family:"David",serif;font-size:6.0px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_259{font-family:"David Bold",serif;font-size:29.6px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_259{font-family:"David Bold",serif;font-size:29.6px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_260{font-family:"David Bold",serif;font-size:40.0px;color:rgb(25,81,148);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_260{font-family:"David Bold",serif;font-size:40.0px;color:rgb(25,81,148);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_261{font-family:"David Bold",serif;font-size:22.1px;color:rgb(91,177,58);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_261{font-family:"David Bold",serif;font-size:22.1px;color:rgb(91,177,58);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_262{font-family:"David",serif;font-size:20.7px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_262{font-family:"David",serif;font-size:20.7px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_263{font-family:"David",serif;font-size:19.6px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_263{font-family:"David",serif;font-size:19.6px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_264{font-family:"Calibri",serif;font-size:6.0px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_264{font-family:"Calibri",serif;font-size:6.0px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_265{font-family:"David",serif;font-size:18.6px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_265{font-family:"David",serif;font-size:18.6px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_266{font-family:"David Bold",serif;font-size:20.1px;color:rgb(40,80,132);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_266{font-family:"David Bold",serif;font-size:20.1px;color:rgb(40,80,132);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_267{font-family:"Arabic Typesetting",serif;font-size:20.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_267{font-family:"Arabic Typesetting",serif;font-size:20.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_268{font-family:"David Bold",serif;font-size:28.8px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_268{font-family:"David Bold",serif;font-size:28.8px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_269{font-family:"David",serif;font-size:20.1px;color:rgb(231,61,70);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_269{font-family:"David",serif;font-size:20.1px;color:rgb(231,61,70);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_271{font-family:"David Bold",serif;font-size:19.3px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_271{font-family:"David Bold",serif;font-size:19.3px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_270{font-family:"David",serif;font-size:19.3px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_270{font-family:"David",serif;font-size:19.3px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_272{font-family:"David",serif;font-size:31.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_272{font-family:"David",serif;font-size:31.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_275{font-family:"David Bold",serif;font-size:37.1px;color:rgb(154,47,45);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_275{font-family:"David Bold",serif;font-size:37.1px;color:rgb(154,47,45);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_276{font-family:"David Bold",serif;font-size:37.1px;color:rgb(74,180,137);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_276{font-family:"David Bold",serif;font-size:37.1px;color:rgb(74,180,137);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_274{font-family:"David Bold",serif;font-size:37.1px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_274{font-family:"David Bold",serif;font-size:37.1px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_273{font-family:"David Bold",serif;font-size:50.0px;color:rgb(74,180,137);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_273{font-family:"David Bold",serif;font-size:50.0px;color:rgb(74,180,137);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_277{font-family:"David Bold",serif;font-size:19.5px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_277{font-family:"David Bold",serif;font-size:19.5px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_278{font-family:"David Bold",serif;font-size:19.5px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_278{font-family:"David Bold",serif;font-size:19.5px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_279{font-family:"David Bold",serif;font-size:19.5px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_279{font-family:"David Bold",serif;font-size:19.5px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_280{font-family:"David Bold",serif;font-size:19.7px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_280{font-family:"David Bold",serif;font-size:19.7px;color:rgb(183,35,34);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_281{font-family:"David Bold",serif;font-size:37.9px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_281{font-family:"David Bold",serif;font-size:37.9px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_282{font-family:"David Bold",serif;font-size:38.5px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_282{font-family:"David Bold",serif;font-size:38.5px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_283{font-family:"David Bold",serif;font-size:28.6px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_283{font-family:"David Bold",serif;font-size:28.6px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_284{font-family:"David Bold",serif;font-size:28.6px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_284{font-family:"David Bold",serif;font-size:28.6px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_285{font-family:"David Bold",serif;font-size:28.6px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_285{font-family:"David Bold",serif;font-size:28.6px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_287{font-family:"David Bold",serif;font-size:28.6px;color:rgb(153,100,25);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_287{font-family:"David Bold",serif;font-size:28.6px;color:rgb(153,100,25);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_286{font-family:"David Bold",serif;font-size:28.6px;color:rgb(248,175,2);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_286{font-family:"David Bold",serif;font-size:28.6px;color:rgb(248,175,2);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_289{font-family:"David Bold",serif;font-size:41.0px;color:rgb(232,80,35);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_289{font-family:"David Bold",serif;font-size:41.0px;color:rgb(232,80,35);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_288{font-family:"David Bold",serif;font-size:49.0px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_288{font-family:"David Bold",serif;font-size:49.0px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_290{font-family:"David Bold",serif;font-size:28.7px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_290{font-family:"David Bold",serif;font-size:28.7px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_291{font-family:"David Bold",serif;font-size:20.1px;color:rgb(230,86,100);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_291{font-family:"David Bold",serif;font-size:20.1px;color:rgb(230,86,100);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_292{font-family:"David",serif;font-size:20.1px;color:rgb(230,86,100);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_292{font-family:"David",serif;font-size:20.1px;color:rgb(230,86,100);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_293{font-family:"David Bold",serif;font-size:19.4px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_293{font-family:"David Bold",serif;font-size:19.4px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_294{font-family:"David",serif;font-size:19.4px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_294{font-family:"David",serif;font-size:19.4px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_295{font-family:"David Bold",serif;font-size:19.4px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_295{font-family:"David Bold",serif;font-size:19.4px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_296{font-family:"David Bold",serif;font-size:42.5px;color:rgb(236,98,90);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_296{font-family:"David Bold",serif;font-size:42.5px;color:rgb(236,98,90);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_297{font-family:"David Bold",serif;font-size:49.0px;color:rgb(74,180,137);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_297{font-family:"David Bold",serif;font-size:49.0px;color:rgb(74,180,137);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_298{font-family:"David Bold",serif;font-size:47.4px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_298{font-family:"David Bold",serif;font-size:47.4px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_299{font-family:"David Bold",serif;font-size:51.0px;color:rgb(99,114,180);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_299{font-family:"David Bold",serif;font-size:51.0px;color:rgb(99,114,180);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_300{font-family:"David Bold",serif;font-size:48.8px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_300{font-family:"David Bold",serif;font-size:48.8px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_301{font-family:"David Bold",serif;font-size:19.6px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_301{font-family:"David Bold",serif;font-size:19.6px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_302{font-family:"David Bold",serif;font-size:29.3px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_302{font-family:"David Bold",serif;font-size:29.3px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_303{font-family:"David Bold",serif;font-size:29.3px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_303{font-family:"David Bold",serif;font-size:29.3px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_304{font-family:"David Bold",serif;font-size:29.3px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_304{font-family:"David Bold",serif;font-size:29.3px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_305{font-family:"David",serif;font-size:28.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_305{font-family:"David",serif;font-size:28.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_306{font-family:Times,serif;font-size:16.5px;color:rgb(254,255,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_306{font-family:Times,serif;font-size:16.5px;color:rgb(254,255,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_307{font-family:"David Bold",serif;font-size:38.4px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_307{font-family:"David Bold",serif;font-size:38.4px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_309{font-family:"David Bold",serif;font-size:48.5px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_309{font-family:"David Bold",serif;font-size:48.5px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_308{font-family:"David Bold",serif;font-size:54.0px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_308{font-family:"David Bold",serif;font-size:54.0px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_310{font-family:"David Bold",serif;font-size:29.4px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_310{font-family:"David Bold",serif;font-size:29.4px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_311{font-family:"David Bold",serif;font-size:28.7px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_311{font-family:"David Bold",serif;font-size:28.7px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_312{font-family:"David Bold",serif;font-size:28.7px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_312{font-family:"David Bold",serif;font-size:28.7px;color:rgb(218,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_313{font-family:"David Bold",serif;font-size:28.7px;color:rgb(153,100,25);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_313{font-family:"David Bold",serif;font-size:28.7px;color:rgb(153,100,25);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_314{font-family:"David Bold",serif;font-size:28.7px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_314{font-family:"David Bold",serif;font-size:28.7px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_315{font-family:"David Bold",serif;font-size:28.7px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_315{font-family:"David Bold",serif;font-size:28.7px;color:rgb(69,166,61);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_316{font-family:"David Bold",serif;font-size:28.7px;color:rgb(229,12,128);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_316{font-family:"David Bold",serif;font-size:28.7px;color:rgb(229,12,128);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_317{font-family:"David Bold",serif;font-size:33.0px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_317{font-family:"David Bold",serif;font-size:33.0px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_318{font-family:"David Bold",serif;font-size:30.6px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_318{font-family:"David Bold",serif;font-size:30.6px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_319{font-family:"David",serif;font-size:8.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_319{font-family:"David",serif;font-size:8.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_320{font-family:"David Bold",serif;font-size:48.0px;color:rgb(232,80,35);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_320{font-family:"David Bold",serif;font-size:48.0px;color:rgb(232,80,35);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_321{font-family:"David Bold",serif;font-size:48.0px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_321{font-family:"David Bold",serif;font-size:48.0px;color:rgb(244,151,19);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_323{font-family:"David Bold",serif;font-size:32.0px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_323{font-family:"David Bold",serif;font-size:32.0px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_324{font-family:"David Bold",serif;font-size:34.0px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_324{font-family:"David Bold",serif;font-size:34.0px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_325{font-family:"David Bold",serif;font-size:25.2px;color:rgb(153,100,25);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_325{font-family:"David Bold",serif;font-size:25.2px;color:rgb(153,100,25);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_328{font-family:"David",serif;font-size:20.1px;color:rgb(197,108,49);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_328{font-family:"David",serif;font-size:20.1px;color:rgb(197,108,49);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_329{font-family:"David Bold",serif;font-size:26.2px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_329{font-family:"David Bold",serif;font-size:26.2px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_330{font-family:"David",serif;font-size:22.1px;color:rgb(254,255,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_330{font-family:"David",serif;font-size:22.1px;color:rgb(254,255,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_331{font-family:"David Bold",serif;font-size:19.7px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_331{font-family:"David Bold",serif;font-size:19.7px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_332{font-family:"David",serif;font-size:19.7px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_332{font-family:"David",serif;font-size:19.7px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_333{font-family:"David Bold",serif;font-size:57.0px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_333{font-family:"David Bold",serif;font-size:57.0px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_334{font-family:"David Bold",serif;font-size:51.4px;color:rgb(248,175,2);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_334{font-family:"David Bold",serif;font-size:51.4px;color:rgb(248,175,2);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_335{font-family:"David Bold",serif;font-size:54.3px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_335{font-family:"David Bold",serif;font-size:54.3px;color:rgb(0,160,227);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_336{font-family:"David Bold",serif;font-size:56.8px;color:rgb(237,110,34);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_336{font-family:"David Bold",serif;font-size:56.8px;color:rgb(237,110,34);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_337{font-family:"David Bold",serif;font-size:41.9px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_337{font-family:"David Bold",serif;font-size:41.9px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_338{font-family:Arial,serif;font-size:17.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_338{font-family:Arial,serif;font-size:17.1px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_339{font-family:"David Bold",serif;font-size:26.1px;color:rgb(231,61,70);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_339{font-family:"David Bold",serif;font-size:26.1px;color:rgb(231,61,70);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_340{font-family:"David",serif;font-size:18.1px;color:rgb(231,61,70);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_340{font-family:"David",serif;font-size:18.1px;color:rgb(231,61,70);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_341{font-family:"David Bold",serif;font-size:24.1px;color:rgb(231,61,70);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_341{font-family:"David Bold",serif;font-size:24.1px;color:rgb(231,61,70);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_342{font-family:"David Bold",serif;font-size:50.0px;color:rgb(229,12,128);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_342{font-family:"David Bold",serif;font-size:50.0px;color:rgb(229,12,128);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_343{font-family:Arial,serif;font-size:20.1px;color:rgb(229,12,128);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_343{font-family:Arial,serif;font-size:20.1px;color:rgb(229,12,128);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_347{font-family:"David Bold",serif;font-size:20.1px;color:rgb(91,177,58);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_347{font-family:"David Bold",serif;font-size:20.1px;color:rgb(91,177,58);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_348{font-family:"David Bold",serif;font-size:16.4px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_348{font-family:"David Bold",serif;font-size:16.4px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_349{font-family:"David",serif;font-size:20.6px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_349{font-family:"David",serif;font-size:20.6px;color:rgb(43,42,41);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_350{font-family:"David Bold",serif;font-size:20.6px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_350{font-family:"David Bold",serif;font-size:20.6px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_351{font-family:"David",serif;font-size:20.6px;color:rgb(218,32,36);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_351{font-family:"David",serif;font-size:20.6px;color:rgb(218,32,36);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_353{font-family:"David Bold",serif;font-size:30.1px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_353{font-family:"David Bold",serif;font-size:30.1px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_354{font-family:"David Bold",serif;font-size:30.1px;color:rgb(200,89,156);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_354{font-family:"David Bold",serif;font-size:30.1px;color:rgb(200,89,156);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_355{font-family:Times,serif;font-size:57.0px;color:rgb(231,61,70);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_355{font-family:Times,serif;font-size:57.0px;color:rgb(231,61,70);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_356{font-family:"David Bold",serif;font-size:50.0px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_356{font-family:"David Bold",serif;font-size:50.0px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_357{font-family:"David Bold",serif;font-size:50.0px;color:rgb(148,35,30);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_357{font-family:"David Bold",serif;font-size:50.0px;color:rgb(148,35,30);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_358{font-family:"David",serif;font-size:21.3px;color:rgb(86,137,114);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_358{font-family:"David",serif;font-size:21.3px;color:rgb(86,137,114);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_359{font-family:Arial,serif;font-size:33.1px;color:rgb(86,166,220);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_359{font-family:Arial,serif;font-size:33.1px;color:rgb(86,166,220);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_360{font-family:"David Bold",serif;font-size:36.1px;color:rgb(207,37,39);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_360{font-family:"David Bold",serif;font-size:36.1px;color:rgb(207,37,39);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_361{font-family:"David Bold",serif;font-size:30.1px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_361{font-family:"David Bold",serif;font-size:30.1px;color:rgb(254,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_362{font-family:"David Bold",serif;font-size:26.1px;color:rgb(36,36,34);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_362{font-family:"David Bold",serif;font-size:26.1px;color:rgb(36,36,34);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_363{font-family:Times,serif;font-size:23.1px;color:rgb(254,255,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_363{font-family:Times,serif;font-size:23.1px;color:rgb(254,255,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_364{font-family:"David Bold",serif;font-size:15.1px;color:rgb(227,31,37);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_364{font-family:"David Bold",serif;font-size:15.1px;color:rgb(227,31,37);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_365{font-family:"David",serif;font-size:15.1px;color:rgb(227,31,37);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_365{font-family:"David",serif;font-size:15.1px;color:rgb(227,31,37);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_366{font-family:"David Bold",serif;font-size:15.1px;color:rgb(217,32,36);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_366{font-family:"David Bold",serif;font-size:15.1px;color:rgb(217,32,36);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_367{font-family:"David Bold",serif;font-size:15.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_367{font-family:"David Bold",serif;font-size:15.1px;color:rgb(43,42,41);font-weight:bold;font-style:normal;text-decoration: none}

        ';

        $Arr = array();

        return $this->BreakCSS($str);
    }

    function BreakCSS($css)
    {

        $results = array();

        preg_match_all('/(.+?)\s?\{\s?(.+?)\s?\}/', $css, $matches);
        foreach ($matches[0] AS $i => $original)
            //  $e = explode(';', $matches[2][$i]);
            $results[$matches[1][$i]] = trim($matches[2][$i]);
        //  dd($e);

        return $results;
    }

}



