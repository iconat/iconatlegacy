<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use View;
use Validator;
use App;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use DB;

class Users extends Controller
{
    protected $Browse = FALSE;
    protected $Add = FALSE;
    protected $Edit = FALSE;
    protected $Delete = FALSE;

    protected $Only_Invs = FALSE; //هل هو فقط مستثمر

    protected $thumb_width = 300;
    protected $thumb_height = 300;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->Browse = Auth::user()->ability(array('admin'), array('B_Contracts'));
            $this->Add = Auth::user()->ability(array('admin'), array('A_Contracts'));
            $this->Edit = Auth::user()->ability(array('admin'), array('E_Contracts'));
            $this->Delete = Auth::user()->ability(array('admin'), array('D_Contracts'));

            $this->Only_Invs = (Auth::user()->hasRole('investor') && !(Auth::user()->ability(array('admin'), array('B_Contracts', 'A_Contracts', 'E_Contracts', 'D_Contracts'))));


            View::share('Browse', $this->Browse);
            View::share('Add', $this->Add);
            View::share('Edit', $this->Edit);
            View::share('Delete', $this->Delete);

            View::share('Only_Invs', $this->Only_Invs);

            View::share('thumb_width', $this->thumb_width);
            View::share('thumb_height', $this->thumb_height);

            return $next($request);
        });


    }


    public function index()
    {
        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            return view('admin.users.view_all');
        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('general.You do not have permission to access this content'));
        }
    }


    public function getdata(Request $request)
    {
        App::setLocale($request->lang);

        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            $Objs = \App\User::orderBy('id', 'desc');


            //Check if request is serach
            if (isset($request["action"]) && $request['action'] == "filter") {

                if (!empty($request["username"])) {
                    $Objs = $Objs->where("username", 'LIKE', '%' . $request["username"] . '%');
                }
                if (!empty($request["name"])) {
                    $Objs = $Objs->where("name", 'LIKE', '%' . $request["name"] . '%');
                }
                if (!empty($request["email"])) {
                    $Objs = $Objs->where("email", 'LIKE', '%' . $request["email"] . '%');
                }

                  if (!empty($request["active"])) {
                    $ac = NULL;
                    if ($request["active"] == 'active') {
                        $ac = 1;
                    }
                    $Objs = $Objs->where("active", $ac);
                }

                if (!empty($request["role"])) {
                    $Objs = $Objs->withRole($request["role"]);
                }


                if (!empty($request["date_from"]) || !empty($request["date_to"])) {
                    $date_from = isset($request["date_from"]) ? $request["date_from"] : NULL;
                    $date_to = isset($request["date_to"]) ? $request["date_to"] : NULL;
                    $newDateTo = date('Y-m-d', strtotime($date_to.' +1 day'));
                    if($date_from && $date_to){
                        $Objs = $Objs->whereBetween('created_at', [$date_from, $newDateTo]);
                    }
                    elseif ($date_from && (!$date_to)){
                        $Objs = $Objs->where('created_at',">=", $date_from);
                    }
                    elseif ((!$date_from) && $date_to){
                        $Objs = $Objs->where('created_at',"<=", $newDateTo);
                    }
                }
            }


            $iTotalRecords = $Objs->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $Objs = $Objs->offset($iDisplayStart);
            $Objs = $Objs->limit($iDisplayLength);


            $records = array();
            $records["data"] = array();

            $Roles = App\Models\Role::get();
            foreach ($Objs->get() as $value) {
                $media = $value->lastMedia('contract');
                $created_at = new Carbon($value->created_at);
                $buttons = '';

                if ($this->Edit) {
                    $buttons .= '<button par1="' . $value->id . '" class="AEObject btn btn-sm btn-outline green"><i class="fa fa-edit"></i> ' . trans("general.edit") . '</button>';
                }
                if ($this->Delete) {
                     $buttons .= '<button par1="' . $value->id . '" par2="' . trans("general.user_:name", ["name" => $value->name]) . '" Dtitle="' . trans("general.are_you_sure") . '" Dcontent="' . trans("general.are_you_sure_you_want_to_delete_name", array('name' => $value->name)) . '"  class="Delete btn btn-sm btn-outline red"><i class="fa fa-remove"></i>  ' . trans("general.delete") . '</button>';
                }

                $array_data = array();


                array_push($array_data, $value->username);
                array_push($array_data, $value->name);
                array_push($array_data, $value->email);

                  if ($value->active == 1) {
                        array_push($array_data, trans("general.active") );
                    }

                     if ($value->active == NULL) {
                        array_push($array_data, trans("general.inactive") );
                    }


                array_push($array_data, $created_at->toDayDateTimeString());

                $Ruless = "";
                foreach ($Roles as  $Role){
                    if($value->hasRole($Role->name)){
                        $Ruless.='<div class="label label-success" style="margin: 2px;"> '.trans("general.".$Role->name).' </div>';
                    }
                }
                array_push($array_data, $Ruless);


                array_push($array_data, $buttons);

                $records["data"][] = $array_data;
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);


        }
    }

    public function AE(Request $request)
    {
        $ReqData = $request->all();
        $par1 = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $CurrentObj = NULL;
        if ($par1 == 'add') {
            $title = trans("general.add_name_element", array("name" => trans("general.user")));
        } else {
            $CurrentObj = \App\User::find($par1);
            $title = trans('general.edit_element');
        }

        if (($this->Add && $par1 == 'add') || (!empty($CurrentObj) && ($this->Edit))) {
            $image = !empty($CurrentObj) ? $CurrentObj->get_personal_image() : NULL;
            return json_encode(array(
                'title' => $title,
                'content' => view('admin.users.AE')->with('par1', $par1)->with('image', $image)->with('CurrentObj', $CurrentObj)->render()
            ));
        } else {
            return json_encode(array(
                'title' => trans("general.error"),
                'content' => view('system.errors.show_error_modal')->with('error_description', trans("general.You do not have permission to access this content"))->render()
            ));
        }
    }

    public function store(Request $request)
    {
        App::setLocale($request->lang);

        //$ReqData = \Purifier::clean($request->all());
        $ReqData = $request->all();
        $id = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $CurrentObj = \App\User::where('id', $id)->first();

        $results = array();
        if (($this->Add && empty($CurrentObj)) || (!empty($CurrentObj) && ($this->Edit))) {

            $Validation_Array = array(
                'name' => 'required',

                'shopCompanyName'=> 'required',
                'city'=> 'required',
                'shopAddress'=> 'required',
                'shopTown'=> 'required',
                'shopState'=> 'required',
                'shopPostCode'=> 'required',
                'shopPhone'=> 'required',

                'username' => [
                    'required',
                    Rule::unique('users')->where(function ($query) use ($request, $CurrentObj) {
                        return $query->where('id', '!=', !empty($CurrentObj) ? $CurrentObj->id : NULL)->where('username', $request->username);
                    }),
                ],
                'email' => [
                    'required',
                    'email',
                    Rule::unique('users')->where(function ($query) use ($request, $CurrentObj) {
                        return $query->where('id', '!=', !empty($CurrentObj->id) ? $CurrentObj->id : NULL)->where('email', $request->email);
                    }),
                ],
            );
            if (empty($CurrentObj)) {
                $Validation_Array['password'] = 'required';


            }

            if (!empty($ReqData['password'])) {
                $ReqData['password'] = bcrypt($ReqData['password']);
            } else {
                unset($ReqData['password']);
            }


            Validator::make($ReqData, $Validation_Array)->validate();


            try {
                if ($request->hasFile('img')) {
                    $file = $request->file('img');

                    $uploader = \MediaUploader::fromSource($file);
                    $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                    $uploader->toDirectory('files/users/' . date("Y") . '/' . date("m") . '/' . date("d"));
                    $uploader->useFilename(str_random(5));
                    $orginal_media = $uploader->upload();

                    $folder_ = storage_path("app/public") . '/' . $orginal_media->directory;
                    if (!file_exists($folder_)) {
                        mkdir($folder_, 666, true);
                    }

                    $image = \Image::make($orginal_media->getAbsolutePath())
                        ->resize($this->thumb_width, $this->thumb_height)
                        ->save($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);

                    $thumb = new \Plank\Mediable\Media;
                    $thumb->disk = $orginal_media->disk;
                    $thumb->directory = $orginal_media->directory;
                    $thumb->filename = $orginal_media->filename . '_thumb';
                    $thumb->extension = $orginal_media->extension;
                    $thumb->mime_type = $orginal_media->mime_type;
                    $thumb->aggregate_type = $orginal_media->aggregate_type;
                    $thumb->size = filesize($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);
                    $thumb->save();


                }
            } catch (\Exception $e) {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => $e->getMessage(),
                );
            }

            if (empty($results['Errors'])) {

                $Data = array();
                if (!empty($ReqData['name'])) {
                    $Data['name'] = $ReqData['name'];
                }
                if (!empty($ReqData['email'])) {
                    $Data['email'] = $ReqData['email'];
                }
                if (!empty($ReqData['password'])) {
                    $Data['password'] = $ReqData['password'];
                }
                if (!empty($ReqData['username'])) {
                    $Data['username'] = $ReqData['username'];
                }
                if (!empty($ReqData['active'])) {
                    $Data['active'] = 1;
                }



                $Obj = \App\User::updateOrCreate(
                    ['id' => $id],
                    $Data

                );

                 if (!empty($ReqData['shopCompanyName'])) {
                    $DataInfo['shopCompanyName'] = $ReqData['shopCompanyName'];
                }
                if (!empty($ReqData['city'])) {
                    $DataInfo['city'] = $ReqData['city'];
                }
                if (!empty($ReqData['shopAddress'])) {
                    $DataInfo['shopAddress'] = $ReqData['shopAddress'];
                }
                if (!empty($ReqData['shopTown'])) {
                    $DataInfo['shopTown'] = $ReqData['shopTown'];
                }


                  if (!empty($ReqData['shopState'])) {
                    $DataInfo['shopState'] = $ReqData['shopState'];
                }
                if (!empty($ReqData['shopPostCode'])) {
                    $DataInfo['shopPostCode'] = $ReqData['shopPostCode'];
                }
                if (!empty($ReqData['shopPhone'])) {
                    $DataInfo['shopPhone'] = $ReqData['shopPhone'];
                }
                    $DataInfo['user_id'] = $Obj->id;

                $Info = \App\Models\InfoUser::updateOrCreate(

                    $DataInfo

                );


                if (isset($orginal_media)) {
                    $Obj->attachMedia($orginal_media, ['main_img']);
                }
                if (isset($thumb)) {
                    $Obj->attachMedia($thumb, ['main_img_thumb']);
                }

                if (isset($ReqData['roles'])) {
                    $Obj->roles()->sync($ReqData['roles']);
                } else {
                    $Obj->roles()->sync([]);
                }













                if ($Obj->wasRecentlyCreated) {
                    $content = trans("general.successfully_added_new_:name", ['name' => trans("general.user")]);
                    $Obj->created_by = Auth::user()->id;
                    $Obj->update();
                } else {
                    $content = trans("general.modified_successfully");
                    $Obj->updated_by = Auth::user()->id;
                    $Obj->update();
                }

                if (!empty($Obj->id)) {
                    $results['Success'] = array(
                        'title' => trans('general.done'),
                        'content' => $content,
                    );
                }
            }


        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }

        echo json_encode($results);


    }


    public function Delete(Request $request)
    {
        $results = array();
        if ($this->Delete) {
            $ReqData = $request->all();
            $id = isset($ReqData['id']) ? mb_strtolower($ReqData['id']) : NULL;
            $par2 = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;

            $q = \App\User::where('id', '=', $id)
                ->where('deleted_at', NULL)
                ->update([
                    'deleted_at' => date("Y-m-d H:i:s"),
                    'deleted_by' => Auth::user()->id,
                ]);

            if (!empty($q)) {
                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => trans('general.name_successfully_deleted', array('name' => $par2)),
                );
            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans('general.the_operation_was_not_done_correctly'),
                );
            }
        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);


    }

}



