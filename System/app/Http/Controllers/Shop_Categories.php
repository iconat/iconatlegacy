<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use View;
use Validator;
use App;
use Carbon\Carbon;
use ImageOptimizer;
use File;

class Shop_Categories extends Controller
{
    protected $Browse = FALSE;
    protected $Add = FALSE;
    protected $Edit = FALSE;
    protected $Delete = FALSE;

    protected $main_thumb_width_lg = 1000;
    protected $main_thumb_height_lg = 1000;

    protected $main_thumb_width_md = 700;
    protected $main_thumb_height_md = 700;

    protected $main_thumb_width_sm = 500;
    protected $main_thumb_height_sm = 500;

    protected $main_thumb_width_xs = 300;
    protected $main_thumb_height_xs = 300;


    protected $importance_array = [1 => "general.very_important", 2 => 'general.average_importance', 3 => 'general.not_so_important'];

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->Browse = Auth::user()->ability(array('admin'), array('B_shop_Categories'));
            $this->Add = Auth::user()->ability(array('admin'), array('A_shop_Categories'));
            $this->Edit = Auth::user()->ability(array('admin'), array('E_shop_Categories'));
            $this->Delete = Auth::user()->ability(array('admin'), array('D_shop_Categories'));

            View::share('Browse', $this->Browse);
            View::share('Add', $this->Add);
            View::share('Edit', $this->Edit);
            View::share('Delete', $this->Delete);

            View::share('main_thumb_width_lg', $this->main_thumb_width_lg);
            View::share('main_thumb_height_lg', $this->main_thumb_height_lg);
            View::share('main_thumb_width_md', $this->main_thumb_width_md);
            View::share('main_thumb_height_md', $this->main_thumb_height_md);
            View::share('main_thumb_width_sm', $this->main_thumb_width_sm);
            View::share('main_thumb_height_sm', $this->main_thumb_height_sm);
            View::share('main_thumb_width_xs', $this->main_thumb_width_xs);
            View::share('main_thumb_height_xs', $this->main_thumb_height_xs);


            View::share('importance_array', $this->importance_array);

            return $next($request);
        });
    }


    public function index(Request $request)
    {
        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            $shop_category = NULL;
            $title = trans("shop_categories.main_shop_categories");
              if ($request->has('M')) {
                $Parent_id = $request->input('M');
                $shop_category = \App\Models\Shop_Categories::find($Parent_id);
                if ($shop_category) {
                    $title = trans("categories.subcategories_for:_name", ["name" => $shop_category->Get_Trans(\App::getLocale(), "name")]);
                }
            }
            return view('admin.shop_categories.view_all')
                ->with('shop_category', $shop_category)
                ->with('title', $title);
        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('general.You do not have permission to access this content'));
        }
    }

    public function AE(Request $request)
    {
        $ReqData = $request->all();
        $par1 = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;
        $par2 = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;


        $CurrentObj = NULL;
        if ($par1 == 'add') {
            $title = trans("general.add_name_element", array("name" => trans("shop_categories.categorie")));
        } else {
            $CurrentObj = \App\Models\Shop_Categories::find($par1);
            $title = trans('general.edit_element');
        }

        if (($this->Add && $par1 == 'add') || ($this->Edit && !empty($CurrentObj))) {

            $main_img_thumb = !empty($CurrentObj) ? ((($CurrentObj->lastMedia("main_shop_categories_img")) ? $CurrentObj->lastMedia("main_shop_categories_img")->getUrl() : NULL)) : NULL;

             $Main_img_shop_categories = !empty($CurrentObj) ? ((( $CurrentObj->MainImage()) ?  $CurrentObj->MainImage() : NULL)) : NULL;

            $Parent = NULL;
            if (!empty($par2)) {
                $Parent = \App\Models\Shop_Categories::find($par2);
            } elseif ($CurrentObj) {
                $Parent = $CurrentObj->parent;
            }
            return json_encode(array(
                'title' => $title,
                'content' => view('admin.shop_categories.AE')
                    ->with('par1', $par1)
                    ->with('CurrentObj', $CurrentObj)
                    ->with('Parent', $Parent)
                    ->with('main_img_thumb', $main_img_thumb)
                    ->with('Main_img_shop_categories', $Main_img_shop_categories)
                    ->render()
            ));
        } else {
            return json_encode(array(
                'title' => trans("general.error"),
                'content' => view('system.errors.show_error_modal')->with('error_description', trans("general.You do not have permission to access this content"))->render()
            ));
        }
    }

    public function store(Request $request)
    {

        $ReqData = \Purifier::clean($request->all());
        //$ReqData = $request->all();

        $id = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;
         $Obj = \App\Models\Shop_Categories::where('id', '=', $id)->first();

        $results = array();

        if (($this->Edit && !empty(\App\Models\Shop_Categories::where('id', $id)->count())) || ($this->Add && empty($id))) {


            if(!empty($id) && $request->hasFile('main_shop_categories')){

                 $imagen1 = getcwd()."/storage/". $Obj->GetURL('main_shop_categories_img');
                  $imagen2 = getcwd()."/storage/". $Obj->GetImageUrlLG('main_shop_categories_img');
                  $imagen3 = getcwd()."/storage/". $Obj->GetImageUrlMD('main_shop_categories_img');
                  $imagen4 = getcwd()."/storage/". $Obj->GetImageUrlSM('main_shop_categories_img');
                  $imagen5 = getcwd()."/storage/". $Obj->GetImageUrlXS('main_shop_categories_img');

                  $files = array($imagen1, $imagen2,$imagen3,$imagen4,$imagen5);

                  File::delete($files);

            }

            if (!$request->has('active')) {
                $ReqData['active'] = NULL;
            } else {
                $ReqData['active'] = 1;
            }

            if(empty($ReqData['parent_id'])){
                $ReqData['parent_id'] = NULL;
            }


            $idd = array(
                'ar.name' => 'required',
                'en.name' => 'required',
                'main_shop_categories' => 'required',

            );

            if ($this->Edit && !empty(\App\Models\Shop_Categories::where('id', $id)->count())) {
                unset($idd['main_shop_categories']);
            }
            Validator::make($ReqData, $idd)->validate();






             try {
                if ($request->hasFile('main_shop_categories')) {
                    $file = $request->file('main_shop_categories');

                    $uploader = \MediaUploader::fromSource($file);
                    $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                    $uploader->toDirectory('Shop_Categoreis/' . date("Y") . '/' . date("m") . '/' . date("d"));
                    $uploader->useFilename(str_random(5));
                    $orginal_media = $uploader->upload();
                    ImageOptimizer::optimize($orginal_media->getAbsolutePath());

                    $folder_ = storage_path("app/public") . '/' . $orginal_media->directory;
                    if (!file_exists($folder_)) {
                        mkdir($folder_, 666, true);
                    }

                    $make_thumbs = [
                        'lg' => [800, 800],
                        'md' => [600, 600],
                        'sm' => [300, 300],
                        'xs' => [100, 100],
                    ];

                    foreach ($make_thumbs as $key => $value) {
                        $image = \Image::make($orginal_media->getAbsolutePath())
                            ->resize($value[0], $value[1])
                            ->save($folder_ . '/' . $orginal_media->filename . '_' . $key . '.' . $orginal_media->extension);

                        $thumb = new \Plank\Mediable\Media;
                        $thumb->disk = $orginal_media->disk;
                        $thumb->directory = $orginal_media->directory;
                        $thumb->filename = $orginal_media->filename . '_' . $key;
                        $thumb->extension = $orginal_media->extension;
                        $thumb->mime_type = $orginal_media->mime_type;
                        $thumb->aggregate_type = $orginal_media->aggregate_type;
                        $thumb->size = filesize($folder_ . '/' . $orginal_media->filename . '_' . $key . '.' . $orginal_media->extension);
                        $thumb->save();
                        ImageOptimizer::optimize($thumb->getAbsolutePath());

                        $orginal_media->attachMedia($thumb, [$key]);


                    }

                }

            } catch (\Exception $e) {
                $error = \Illuminate\Validation\ValidationException::withMessages([
                    'image' => [$e->getMessage()],
                ]);
                throw $error;
            }


            // $ReqData['text_ar'] = str_replace('src="' . url(""), 'src="App_Path_Re', $ReqData['text_ar']);

            $Obj = \App\Models\Shop_Categories::updateOrCreate(
                ['id' => $id],
                $ReqData
            );

            if (isset($orginal_media)) {
                $Obj->attachMedia($orginal_media, ['main_shop_categories_img']);
            }

            if (isset($ReqData['MediaFiles'])) {
                $MediaFiles_array = json_decode($ReqData['MediaFiles']);
                foreach ($MediaFiles_array as $HashedID) {
                    $m_id = \Crypt::decrypt($HashedID);
                    $F = $Obj->attachMedia([$m_id], 'shop_categories_images');
                }
            }


            if ($Obj->wasRecentlyCreated) {
                $content = trans("general.successfully_added_new_:name", ['name' => trans("general.element")]);
                $Obj->created_by = Auth::user()->id;
                $Obj->update();

                activity("shop_categories.shop_categories")
                    ->performedOn($Obj)
                    ->causedBy(Auth::user()->id)
                    ->log('shop_categories.added_a_new_categorie');

            } else {
                $content = trans("general.modified_successfully");
                $Obj->updated_by = Auth::user()->id;
                $Obj->update();

                activity("shop_categories.shop_categories")
                    ->performedOn($Obj)
                    ->causedBy(Auth::user()->id)
                    ->log('shop_categories.edited_categorie');

            }

            if (!empty($Obj->id)) {


                $All_Langs = \Waavi\Translation\Models\Language::orderBY('id')->get();
                foreach ($All_Langs as $Lang) {
                    if (isset($ReqData[$Lang->locale])) {
                        foreach ($ReqData[$Lang->locale] as $k => $v) {
                            $ObjTrans = \App\Models\Translations\Shop_CategoriesTranslations::updateOrCreate(
                                ['ref_id' => $Obj->id, 'locale' => $Lang->locale],
                                [$k => $v]
                            );
                            if ($ObjTrans->wasRecentlyCreated) {
                                $ObjTrans->created_by = Auth::user()->id;
                                $ObjTrans->update();

                            } else {
                                $ObjTrans->updated_by = Auth::user()->id;
                                $ObjTrans->update();

                            }
                        }
                    }
                }

                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => $content,
                );
            }

        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }

        echo json_encode($results);


    }


    public function getdata(Request $request)
    {
        App::setLocale($request->lang);
        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            $Objs = \App\Models\Shop_Categories::orderBy('id', 'desc');

            if (!empty($request['par1'])) {
                $Objs = $Objs->where('parent_id', $request['par1']);
            }else{

                $Objs = $Objs->where('parent_id', NULL);

            }


            //Check if request is serach
            if (isset($request["action"]) && $request['action'] == "filter") {


                if (!empty($request["name"])) {
                    $Objs = $Objs->whereHas('Translations', function ($query) use ($request) {
                        $query->where("name", 'LIKE', '%' . $request["name"] . '%');
                    });
                }

                if (!empty($request["active"])) {
                    $ac = NULL;
                    if ($request["active"] == 'active') {
                        $ac = 1;
                    }
                    $Objs = $Objs->where("active", $ac);
                }

                if (!empty($request["date_from"]) || !empty($request["date_to"])) {
                    $date_from = $request["date_from"];
                    $date_to = $request["date_to"];
                    $Objs = $Objs->whereBetween('created_at', [$date_from, $date_to]);
                }
            }


            $iTotalRecords = $Objs->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $Objs = $Objs->offset($iDisplayStart);
            $Objs = $Objs->limit($iDisplayLength);


            $records = array();
            $records["data"] = array();


            $Objs = $Objs->get();


            foreach ($Objs as $value) {

                $AcState = trans("general.not_active");
                $shop = \App\Models\Shops::where('shop_categories' , $value->id)->first();
                if ($value->active == 1) {
                    $AcState = trans("general.active");
                }

                $buttons = '';
                if ($this->Edit) {
                    $buttons .= '<button par1="' . $value->id . '" class="AEObject btn btn-sm btn-outline green"><i class="fa fa-edit"></i> ' . trans("general.edit") . '</button>';
                }
                if (($this->Browse) && (empty($shop))) {
                    $buttons .= '<a href="' . url("/Manage/Shop_Categories?M=" . $value->id) . '" class="btn btn-sm btn-outline purple-sharp"><i class="fa fa-list"></i> ' . trans("categories.subcategories") . '</a>';
                }
                if ($this->Delete) {
                    $buttons .= '<button par1="' . $value->id . '" par2="' . $value->name . '" Dtitle="' . trans("general.are_you_sure") . '" Dcontent="' . trans("general.are_you_sure_you_want_to_delete_name", array('name' => $value->{'name'})) . '"  class="Delete btn btn-sm btn-outline red"><i class="fa fa-remove"></i>  ' . trans("general.delete") . '</button>';
                }
                $created_at = new Carbon($value->created_at);
                $records["data"][] = array(
                    $value->Get_Trans(App::getLocale(), 'name'),
                    $AcState,
                    $created_at->toDayDateTimeString(),
                    $buttons,
                );
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);


        }
    }



    public function Delete(Request $request)
    {
        $results = array();
        if ($this->Delete) {
            $ReqData = $request->all();
            $id = isset($ReqData['id']) ? mb_strtolower($ReqData['id']) : NULL;
            $par2 = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;

            $Obj = $q = \App\Models\Shop_Categories::where('id', '=', $id)->first();
            $CanDeleted = $Obj->CanDeleted();
            if ($CanDeleted === true) {
                $q = \App\Models\Shop_Categories::where('id', '=', $id)
                    ->where('deleted_at', NULL)
                    ->update([
                        'deleted_at' => date("Y-m-d H:i:s"),
                        'deleted_by' => Auth::user()->id,
                    ]);


                    $imagen1 = getcwd()."/storage/". $Obj->GetURL('main_shop_categories_img');
                  $imagen2 = getcwd()."/storage/". $Obj->GetImageUrlLG('main_shop_categories_img_lg');
                  $imagen3 = getcwd()."/storage/". $Obj->GetImageUrlMD('main_shop_categories_img_md');
                  $imagen4 = getcwd()."/storage/". $Obj->GetImageUrlSM('main_shop_categories_img_sm');
                  $imagen5 = getcwd()."/storage/". $Obj->GetImageUrlXS('main_shop_categories_img_xs');

                  $files = array($imagen1, $imagen2,$imagen3,$imagen4,$imagen5);

                  File::delete($files);

                if (!empty($q)) {

                    activity("shop_categories.shop_categories")
                        ->performedOn($Obj)
                        ->causedBy(Auth::user()->id)
                        ->log('shop_categories.deleted_categorie');

                    $results['Success'] = array(
                        'title' => trans('general.done'),
                        'content' => trans('general.name_successfully_deleted', array('name' => $par2)),
                    );
                } else {
                    $results['Errors'] = array(
                        'title' => trans('general.error'),
                        'content' => trans('general.the_operation_was_not_done_correctly'),
                    );
                }
            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => $CanDeleted,
                );
            }
        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);


    }

}



