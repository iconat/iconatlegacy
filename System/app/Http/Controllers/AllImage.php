<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use View;
use Validator;
use App;
use Carbon\Carbon;
use ImageOptimizer;
use File;

class AllImage extends Controller
{
    protected $Browse = FALSE;
    protected $Add = FALSE;
    protected $Edit = FALSE;
    protected $Delete = FALSE;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->Browse = Auth::user()->ability(array('admin'), array('B_AllImage'));
            $this->Add = Auth::user()->ability(array('admin'), array('A_AllImage'));
            $this->Edit = Auth::user()->ability(array('admin'), array('E_AllImage'));
            $this->Delete = Auth::user()->ability(array('admin'), array('D_AllImage'));

            View::share('Browse', $this->Browse);
            View::share('Add', $this->Add);
            View::share('Edit', $this->Edit);
            View::share('Delete', $this->Delete);


            return $next($request);
        });
    }


    public function index()
    {

        $shops =  \App\Models\Shops::where('deleted_at', '=', NULL)->orderBY("id", 'desc')->paginate(2);


        $existingImages = array();
         $Images = \App\Models\Media::where('deleted_at','=', NULL)->where('aggregate_type', '=', 'image')->orderBY("id",'desc')->get();
         foreach($Images as $image){


             if(File::exists(public_path().'/storage/'.$image->directory.'/'.$image->filename.'.'.$image->extension)){
                 $existingImages[] = $image;
             }
         }

        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            return view('admin.allimage.view_all')->with("Images", $Images)->with("shops", $shops)->with('existingImages', $existingImages);
        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('general.You do not have permission to access this content'));
        }
    }

















}



