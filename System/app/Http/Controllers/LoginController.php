<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Invs;
use App\Models\Identity;
use Auth;
use App\Http\Requests\LoginValidation;

class LoginController extends Controller
{
    public function authenticate(Request $request)
    {


        $user = User::where('email', '=', $request->username)->orWhere('username', '=', $request->username)->first();


        if ($user) {

            $unam = "email";
            $username=strtolower ($request->username);
            if ($username  == $user->username) {
             $unam = "username";
            }



         if ($user->active == 1 ) {


            if (Auth::attempt([$unam => $request->username, 'password' => $request->password ],($request->remember == 1) ? true : false)) {
                // Authentication passed...

                $r_to = url("/My");

                if (Auth::user()->hasRole(['dealer']) ) {

                      $r_to = url("/Account") ;

                }
                else {
                $r_to = url("/My");
              }




                return response()->json(['Success' => ['redirectto' => $r_to] ]);

            } else {
                return response()->json(['Errors' => [
                    'title' => trans("general.error",[],$request->lang),
                    'content' => trans("general.username_or_password_is_incorrect",[],$request->lang)
                ]]);
            }


} else {
            return response()->json(['Errors' => [
                'title' => trans("general.error",[],$request->lang),
                'content' => trans("general.account_not_activated_yet_please_wait_while_your_account_is_activated",[],$request->lang)
            ]]);
        }




        } else {
            return response()->json(['Errors' => [
                'title' => trans("general.error",[],$request->lang),
                'content' => trans("general.username_or_password_is_incorrect",[],$request->lang)
            ]]);
        }





        //  return json_encode(array("Result"=>$dd));

    }


}



