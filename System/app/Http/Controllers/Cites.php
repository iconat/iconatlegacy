<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use View;
use Validator;
use App;
use Carbon\Carbon;
use ImageOptimizer;
use File;


class Cites extends Controller
{
    protected $Browse = FALSE;
    protected $Add = FALSE;
    protected $Edit = FALSE;
    protected $Delete = FALSE;


    protected $main_thumb_width_lg = 1000;
    protected $main_thumb_height_lg = 1000;

    protected $main_thumb_width_md = 700;
    protected $main_thumb_height_md = 700;

    protected $main_thumb_width_sm = 500;
    protected $main_thumb_height_sm = 500;

    protected $main_thumb_width_xs = 300;
    protected $main_thumb_height_xs = 300;


    protected $importance_array = [1 => "general.very_important", 2 => 'general.average_importance', 3 => 'general.not_so_important'];

    protected $order_array = [1 => "1", 2 => '2', 3 => '3', 4 => '4', 5 => '5', 6 => '6', 7 => '7', 8 => '8', 9 => '9', 10 => '10', 11 => '11', 12 => '12', 13 => '13', 14 => '14', 15 => '15', 16 => '16', 17 => '17', 18 => '18', 19 => '19', 20 => '20', 21 => '21', 22 => '22', 23 => '23', 24 => '24', 25 => '25', 26 => '26', 27 => '27', 28 => '28', 29 => '29', 30 => '30', 31 => '31', 32 => '32', 33 => '33', 34 => '34'];

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->Browse = Auth::user()->ability(array('admin'), array('B_Cites'));
            $this->Add = Auth::user()->ability(array('admin'), array('A_Cites'));
            $this->Edit = Auth::user()->ability(array('admin'), array('E_Cites'));
            $this->Delete = Auth::user()->ability(array('admin'), array('D_Cites'));

            View::share('Browse', $this->Browse);
            View::share('Add', $this->Add);
            View::share('Edit', $this->Edit);
            View::share('Delete', $this->Delete);


            View::share('main_thumb_width_lg', $this->main_thumb_width_lg);
            View::share('main_thumb_height_lg', $this->main_thumb_height_lg);
            View::share('main_thumb_width_md', $this->main_thumb_width_md);
            View::share('main_thumb_height_md', $this->main_thumb_height_md);
            View::share('main_thumb_width_sm', $this->main_thumb_width_sm);
            View::share('main_thumb_height_sm', $this->main_thumb_height_sm);
            View::share('main_thumb_width_xs', $this->main_thumb_width_xs);
            View::share('main_thumb_height_xs', $this->main_thumb_height_xs);


            View::share('importance_array', $this->importance_array);
            View::share('order_array', $this->order_array);

            return $next($request);
        });
    }


    public function index(Request $request)
    {
        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            $City = NULL;
            $title = trans("Cites.main_Cites");
            if ($request->has('M')) {
                $Parent_id = $request->input('M');
                $City = \App\Models\Cites::find($Parent_id);
                if ($City) {
                    $title = trans("Cites.subCites_for:_name", ["name" => $City->Get_Trans(\App::getLocale(), "name")]);
                }
            }
            return view('admin.cites.view_all')
                ->with('City', $City)
                ->with('title', $title);
        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('general.You do not have permission to access this content'));
        }
    }

    public function AE(Request $request)
    {
        $ReqData = $request->all();
        $par1 = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;
        $par2 = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;


        $CurrentObj = NULL;
        if ($par1 == 'add') {
            $title = trans("general.add_name_element", array("name" => trans("Cites.city")));
        } else {
            $CurrentObj = \App\Models\Cites::find($par1);
            $title = trans('general.edit_element');
        }

        if (($this->Add && $par1 == 'add') || ($this->Edit && !empty($CurrentObj))) {

            $main_img_thumb = !empty($CurrentObj) ? ((($CurrentObj->lastMedia("main_city_img")) ? $CurrentObj->lastMedia("main_city_img")->getUrl() : NULL)) : NULL;

             $Main_img_city = !empty($CurrentObj) ? ((( $CurrentObj->MainImage()) ?  $CurrentObj->MainImage() : NULL)) : NULL;







            $Parent = NULL;
            if (!empty($par2)) {
                $Parent = \App\Models\Cites::find($par2);
            } elseif ($CurrentObj) {
                $Parent = $CurrentObj->parent;
            }
            return json_encode(array(
                'title' => $title,
                'content' => view('admin.cites.AE')
                    ->with('par1', $par1)
                    ->with('CurrentObj', $CurrentObj)
                    ->with('Parent', $Parent)
                    ->with('main_img_thumb', $main_img_thumb)
                    ->with('Main_img_city', $Main_img_city)
                    ->render()
            ));
        } else {
            return json_encode(array(
                'title' => trans("general.error"),
                'content' => view('system.errors.show_error_modal')->with('error_description', trans("general.You do not have permission to access this content"))->render()
            ));
        }
    }

    public function store(Request $request)
    {

        $ReqData = \Purifier::clean($request->all());
        //$ReqData = $request->all();

        $id = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;
        $Obj  = \App\Models\Cites::where('id', '=', $id)->first();
        $results = array();

        if (($this->Edit && !empty(\App\Models\Cites::where('id', $id)->count())) || ($this->Add && empty($id))) {



            if(!empty($id) && $request->hasFile('main_city')){

                 $imagen1 = getcwd()."/storage/". $Obj->GetURL('main_city_img');
                  $imagen2 = getcwd()."/storage/". $Obj->GetImageUrlLG('main_city_img_lg');
                  $imagen3 = getcwd()."/storage/". $Obj->GetImageUrlMD('main_city_img_md');
                  $imagen4 = getcwd()."/storage/". $Obj->GetImageUrlSM('main_city_img_sm');
                  $imagen5 = getcwd()."/storage/". $Obj->GetImageUrlXS('main_city_img_xs');

                  $files = array($imagen1, $imagen2,$imagen3,$imagen4,$imagen5);

                  File::delete($files);

            }

            if (!$request->has('active')) {
                $ReqData['active'] = NULL;
            } else {
                $ReqData['active'] = 1;
            }


            if(empty($ReqData['parent_id'])){
                $ReqData['parent_id'] = NULL;
            }




            $idd = array(
                'ar.name' => 'required',
                'en.name'=> 'required',
                'main_city' => 'required',

            );

            if($this->Edit && !empty(\App\Models\Cites::where('id', $id)->count())){
                unset($idd['main_city']);
            }
            Validator::make($ReqData, $idd)->validate();





             try {
                if ($request->hasFile('main_city')) {
                    $file = $request->file('main_city');

                    $uploader = \MediaUploader::fromSource($file);
                    $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                    $uploader->toDirectory('City/' . date("Y") . '/' . date("m") . '/' . date("d"));
                    $uploader->useFilename(str_random(5));
                    $orginal_media = $uploader->upload();
                    ImageOptimizer::optimize($orginal_media->getAbsolutePath());

                    $folder_ = storage_path("app/public") . '/' . $orginal_media->directory;
                    if (!file_exists($folder_)) {
                        mkdir($folder_, 666, true);
                    }

                    $make_thumbs = [
                        'lg' => [800, 800],
                        'md' => [600, 600],
                        'sm' => [300, 300],
                        'xs' => [100, 100],
                    ];

                    foreach ($make_thumbs as $key => $value) {
                        $image = \Image::make($orginal_media->getAbsolutePath())
                            ->resize($value[0], $value[1])
                            ->save($folder_ . '/' . $orginal_media->filename . '_' . $key . '.' . $orginal_media->extension);

                        $thumb = new \Plank\Mediable\Media;
                        $thumb->disk = $orginal_media->disk;
                        $thumb->directory = $orginal_media->directory;
                        $thumb->filename = $orginal_media->filename . '_' . $key;
                        $thumb->extension = $orginal_media->extension;
                        $thumb->mime_type = $orginal_media->mime_type;
                        $thumb->aggregate_type = $orginal_media->aggregate_type;
                        $thumb->size = filesize($folder_ . '/' . $orginal_media->filename . '_' . $key . '.' . $orginal_media->extension);
                        $thumb->save();
                        ImageOptimizer::optimize($thumb->getAbsolutePath());

                        $orginal_media->attachMedia($thumb, [$key]);


                    }

                }

            } catch (\Exception $e) {
                $error = \Illuminate\Validation\ValidationException::withMessages([
                    'image' => [$e->getMessage()],
                ]);
                throw $error;
            }



            // $ReqData['text_ar'] = str_replace('src="' . url(""), 'src="App_Path_Re', $ReqData['text_ar']);

            $Obj = \App\Models\Cites::updateOrCreate(
                ['id' => $id],
                $ReqData
            );

            if (isset($orginal_media)) {
                $Obj->attachMedia($orginal_media, ['main_city_img']);
            }

             if (isset($ReqData['MediaFiles'])) {
                $MediaFiles_array = json_decode($ReqData['MediaFiles']);
                foreach ($MediaFiles_array as $HashedID) {
                    $m_id = \Crypt::decrypt($HashedID);
                    $F = $Obj->attachMedia([$m_id], 'city_images');
                }
            }


            if ($Obj->wasRecentlyCreated) {
                $content = trans("general.successfully_added_new_:name", ['name' => trans("general.element")]);
                $Obj->created_by = Auth::user()->id;
                $Obj->update();

                activity("Cites.Cites")
                    ->performedOn($Obj)
                    ->causedBy(Auth::user()->id)
                    ->log('Cites.added_a_new_city');

            } else {
                $content = trans("general.modified_successfully");
                $Obj->updated_by = Auth::user()->id;
                $Obj->update();

                activity("Cites.Cites")
                    ->performedOn($Obj)
                    ->causedBy(Auth::user()->id)
                    ->log('Cites.edited_city');

            }

            if (!empty($Obj->id)) {


                $All_Langs = \Waavi\Translation\Models\Language::orderBY('id')->get();
                foreach ($All_Langs as $Lang) {
                    if (isset($ReqData[$Lang->locale])) {
                        foreach ($ReqData[$Lang->locale] as $k => $v) {
                            $ObjTrans = \App\Models\Translations\CitesTranslations::updateOrCreate(
                                ['ref_id' => $Obj->id, 'locale' => $Lang->locale],
                                [$k => $v]
                            );
                            if ($ObjTrans->wasRecentlyCreated) {
                                $ObjTrans->created_by = Auth::user()->id;
                                $ObjTrans->update();

                            } else {
                                $ObjTrans->updated_by = Auth::user()->id;
                                $ObjTrans->update();

                            }
                        }
                    }
                }

                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => $content,
                );
            }

        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }

        echo json_encode($results);


    }


    public function getdata(Request $request)
    {
        App::setLocale($request->lang);
        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            $Objs = \App\Models\Cites::orderBy('id', 'desc');

            // if (!empty($request['par1'])) {
            //     $Objs = $Objs->where('parent_id', $request['par1']);
            // }else{
            //     $Objs = $Objs->where('parent_id', NULL);

            // }


            //Check if request is serach
            if (isset($request["action"]) && $request['action'] == "filter") {


                if (!empty($request["name"])) {
                    $Objs = $Objs->whereHas('Translations', function ($query) use ($request) {
                        $query->where("name", 'LIKE', '%' . $request["name"] . '%');
                    });
                }

                if (!empty($request["active"])) {
                    $ac = NULL;
                    if ($request["active"] == 'active') {
                        $ac = 1;
                    }
                    $Objs = $Objs->where("active", $ac);
                }

                if (!empty($request["date_from"]) || !empty($request["date_to"])) {
                    $date_from = $request["date_from"];
                    $date_to = $request["date_to"];
                    $Objs = $Objs->whereBetween('created_at', [$date_from, $date_to]);
                }
            }


            $iTotalRecords = $Objs->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $Objs = $Objs->offset($iDisplayStart);
            $Objs = $Objs->limit($iDisplayLength);


            $records = array();
            $records["data"] = array();


            $Objs = $Objs->get();

            foreach ($Objs as $value) {

                $AcState = trans("general.not_active");

                if ($value->active == 1) {
                    $AcState = trans("general.active");
                }

                $buttons = '';
                if ($this->Edit) {
                    $buttons .= '<button par1="' . $value->id . '" class="AEObject btn btn-sm btn-outline green"><i class="fa fa-edit"></i> ' . trans("general.edit") . '</button>';
                }
                // if ($this->Browse) {
                //     $buttons .= '<a href="' . url("/Manage/Cites?M=" . $value->id) . '" class="btn btn-sm btn-outline purple-sharp"><i class="fa fa-list"></i> ' . trans("Cites.subCites") . '</a>';
                // }
                if ($this->Delete) {
                    $buttons .= '<button par1="' . $value->id . '" par2="' . $value->name . '" Dtitle="' . trans("general.are_you_sure") . '" Dcontent="' . trans("general.are_you_sure_you_want_to_delete_name", array('name' => $value->{'name'})) . '"  class="Delete btn btn-sm btn-outline red"><i class="fa fa-remove"></i>  ' . trans("general.delete") . '</button>';
                }
                $created_at = new Carbon($value->created_at);
                $records["data"][] = array(
                    $value->Get_Trans(App::getLocale(), 'name'),
                    // $value->name,
                    $AcState,
                    $created_at->toDayDateTimeString(),
                    $buttons,
                );
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);


        }
    }


    public function Delete(Request $request)
    {
        $results = array();
        if ($this->Delete) {
            $ReqData = $request->all();
            $id = isset($ReqData['id']) ? mb_strtolower($ReqData['id']) : NULL;
            $par2 = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;

            $Obj = $q = \App\Models\Cites::where('id', '=', $id)->first();
            $CanDeleted = $Obj->CanDeleted();
            if ($CanDeleted === true) {
                $q = \App\Models\Cites::where('id', '=', $id)
                    ->where('deleted_at', NULL)
                    ->update([
                        'deleted_at' => date("Y-m-d H:i:s"),
                        'deleted_by' => Auth::user()->id,
                    ]);


                    $imagen1 = getcwd()."/storage/". $Obj->GetURL('main_city_img');
                  $imagen2 = getcwd()."/storage/". $Obj->GetImageUrlLG('main_city_img_lg');
                  $imagen3 = getcwd()."/storage/". $Obj->GetImageUrlMD('main_city_img_md');
                  $imagen4 = getcwd()."/storage/". $Obj->GetImageUrlSM('main_city_img_sm');
                  $imagen5 = getcwd()."/storage/". $Obj->GetImageUrlXS('main_city_img_xs');

                  $files = array($imagen1, $imagen2,$imagen3,$imagen4,$imagen5);

                  File::delete($files);

                if (!empty($q)) {

                    activity("Cites.Cites")
                        ->performedOn($Obj)
                        ->causedBy(Auth::user()->id)
                        ->log('Cites.deleted_city');

                    $results['Success'] = array(
                        'title' => trans('general.done'),
                        'content' => trans('general.name_successfully_deleted', array('name' => $par2)),
                    );
                } else {
                    $results['Errors'] = array(
                        'title' => trans('general.error'),
                        'content' => trans('general.the_operation_was_not_done_correctly'),
                    );
                }
            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => $CanDeleted,
                );
            }
        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);


    }

}



