<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ImageOptimizer;

class GeneralController extends Controller
{


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function UploadForCkeditor5(Request $request)
    {
        try {
            $file = $request->file('upload');

            $uploader = \MediaUploader::fromSource($file);
            $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
            $uploader->toDirectory('files/' . date("Y") . '/' . date("m") . '/' . date("d"));
            $uploader->useFilename(str_random(5));
            $media = $uploader->upload();


            return response()->json([
                'uploaded' => true,
                'url' => $media->getUrl()
            ]);
        } catch (\Exception $e) {
            return response()->json(
                [
                    'uploaded' => false,
                    'error' => [
                        'message' => $e->getMessage()
                    ]
                ]
            );
        }
    }

    public function DropzoneUpload(Request $request)
    {
        try {
            $Folder = 'files';
            $make_thumbs = [];
            $scales = ['lg', 'md', 'sm', 'xs'];
            if (!empty($request->Section)) {
                $Folder = $request->Section;
                switch ($request->Section) {
                    case 'Product':
                        $make_thumbs = [
                            'lg' => [800, 800],
                            'md' => [600, 600],
                            'sm' => [300, 300],
                            'xs' => [100, 100],
                        ];
                        break;
                    default:
                        break;
                }
            }

            $file = $request->file('file');

            $uploader = \MediaUploader::fromSource($file);
            $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
            $uploader->toDirectory($Folder . '/' . date("Y") . '/' . date("m") . '/' . date("d"));
            $uploader->useFilename(str_random(5));
            $orginal_media = $uploader->upload();
            ImageOptimizer::optimize($orginal_media->getAbsolutePath());
            //Make Thumbs
            if (!empty($make_thumbs)) {
                $folder_ = storage_path("app/public") . '/' . $orginal_media->directory;
                if (!file_exists($folder_)) {
                    mkdir($folder_, 666, true);
                }
                foreach ($make_thumbs as $key => $value) {

                    $image = \Image::make($orginal_media->getAbsolutePath())
                        ->resize($value[0], $value[1])
                        ->save($folder_ . '/' . $orginal_media->filename . '_'.$key.'.' . $orginal_media->extension);

                    $thumb = new \Plank\Mediable\Media;
                    $thumb->disk = $orginal_media->disk;
                    $thumb->directory = $orginal_media->directory;
                    $thumb->filename = $orginal_media->filename . '_'.$key;
                    $thumb->extension = $orginal_media->extension;
                    $thumb->mime_type = $orginal_media->mime_type;
                    $thumb->aggregate_type = $orginal_media->aggregate_type;
                    $thumb->size = filesize($folder_ . '/' . $orginal_media->filename . '_'.$key.'.' . $orginal_media->extension);
                    $thumb->save();
                    ImageOptimizer::optimize($thumb->getAbsolutePath());

                    $orginal_media->attachMedia($thumb, [$key]);



                }
            }


            return response()->json([
                'uploaded' => true,
                'url' => $orginal_media->getUrl(),
                'media' => \Crypt::encrypt($orginal_media->id)
            ]);
        } catch (\Exception $e) {
//            return response()->json(
//                [
//                    'uploaded' => false,
//                    'error' => [
//                        'message' => $e->getMessage()
//                    ]
//                ]
//            );
            $error = \Illuminate\Validation\ValidationException::withMessages([
                'image' => [$e->getMessage()],
            ]);
            throw $error;
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function UploadForCkeditor(Request $request)
    {
        try {
            $file = $request->file('upload');

            $uploader = \MediaUploader::fromSource($file);
            $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
            $uploader->toDirectory('files/' . date("Y") . '/' . date("m") . '/' . date("d"));
            $uploader->useFilename(str_random(5));
            $media = $uploader->upload();


            return response()->json([
                'uploaded' => true,
                'url' => $media->getUrl()
            ]);
        } catch (\Exception $e) {
            return response()->json(
                [
                    'uploaded' => false,
                    'error' => [
                        'message' => $e->getMessage()
                    ]
                ]
            );
        }
    }

}
