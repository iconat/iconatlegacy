<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use View;
use Validator;
use App;
use Carbon\Carbon;

class Poll extends Controller
{
    protected $Browse = FALSE;
    protected $Add = FALSE;
    protected $Edit = FALSE;
    protected $Delete = FALSE;

    protected $thumb_width = 300;
    protected $thumb_height = 300;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->Browse = Auth::user()->ability(array('admin'), array('B_Shares'));
            $this->Add = Auth::user()->ability(array('admin'), array('A_Shares'));
            $this->Edit = Auth::user()->ability(array('admin'), array('E_Shares'));
            $this->Delete = Auth::user()->ability(array('admin'), array('D_Shares'));


            View::share('Browse', $this->Browse);
            View::share('Add', $this->Add);
            View::share('Edit', $this->Edit);
            View::share('Delete', $this->Delete);

            View::share('thumb_width', $this->thumb_width);
            View::share('thumb_height', $this->thumb_height);

            return $next($request);
        });


    }


    public function index()
    {
        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            return view('admin.poll.view_all');
        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('general.You do not have permission to access this content'));
        }
    }

    public function AE(Request $request)
    {
        $ReqData = $request->all();
        $par1 = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $CurrentObj = NULL;
        if ($par1 == 'add') {
            $title = trans("general.add_name_element", array("name" => trans("shares.share")));
        } else {
            $CurrentObj = \App\Models\Poll::find($par1);
            $title = trans('general.edit_element');
        }

        if (($this->Add && $par1 == 'add') || (!empty($CurrentObj) && ($this->Edit))) {
            $Items_Of_Order = array();
            if(!empty($CurrentObj)){
                $Items_Of_Order = $CurrentObj->Options()->get();
            }
            return json_encode(array(
                'title' => $title,
                'content' => view('admin.poll.AE')->with('par1', $par1)->with('Items_Of_Order', $Items_Of_Order)->with('CurrentObj', $CurrentObj)->render()
            ));
        } else {
            return json_encode(array(
                'title' => trans("general.error"),
                'content' => view('system.errors.show_error_modal')->with('error_description', trans("general.You do not have permission to access this content"))->render()
            ));
        }
    }

    public function store(Request $request)
    {

        $ReqData = \Purifier::clean($request->all());
        //$ReqData = $request->all();
        $id = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $CurrentObj = \App\Models\Poll::where('id', $id)->first();

        $results = array();
        if (($this->Add && empty($id)) || (!empty($CurrentObj) && ($this->Edit))) {

            if (!empty($ReqData["Options"])) {

                $Poll = array(
                    'question' => 'required',
                    'start_date' => 'required|date',
                    'closing_date' => 'required|date|after_or_equal:'.$ReqData["start_date"],

                );
                Validator::make($ReqData, $Poll)->validate();

                if (isset($ReqData["Options"])) {
                    foreach ($ReqData["Options"] as $option) {
                        if((!empty($option['option'])) || (!empty($option['additional']))){
                            $Poll_Option = array(
                                'option' => 'required',
                                'additional' => 'between:1,99999',
                            );

                            Validator::make($option, $Poll_Option)->validate();
                        }

                    }
                }


                $Obj = \App\Models\Poll::updateOrCreate(
                    ['id' => $id],
                    $ReqData
                );

                foreach ($ReqData["Options"] as $option) {
                    if((!empty($option['option'])) || (!empty($option['additional']))) {
                        $option['poll_id'] = $Obj->id;

                        $ObjO = \App\Models\Poll_Options::updateOrCreate(
                            ['id' => isset($option['id']) ? $option['id'] : NULL],
                            $option
                        );

                        if ($ObjO->wasRecentlyCreated) {
                            $ObjO->created_by = Auth::user()->id;
                            $ObjO->update();

                        } else {
                            $ObjO->updated_by = Auth::user()->id;
                            $ObjO->update();

                        }
                    }
                }


                if ($Obj->wasRecentlyCreated) {
                    $content = trans("general.successfully_added_new_:name", ['name' => trans("poll.indvpoll")]);
                    $Obj->created_by = Auth::user()->id;
                    $Obj->update();

                } else {
                    $content = trans("general.modified_successfully");
                    $Obj->updated_by = Auth::user()->id;
                    $Obj->update();

                }

                if (!empty($Obj->id)) {
                    $results['Success'] = array(
                        'title' => trans('general.done'),
                        'content' => $content,
                    );
                }
            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans('general.fill_out_at_least_one_option'),
                );
            }


        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }

        echo json_encode($results);


    }


    public function getdata(Request $request)
    {

        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            $Objs = \App\Models\Poll::orderBy('id', 'desc');


            //Check if request is serach
            if (isset($request["action"]) && $request['action'] == "filter") {



                if (!empty($request["question"])) {
                    $Objs = $Objs->where("question", 'LIKE', '%' . $request["question"] . '%');
                }


                if (!empty($request["start_date_from"])) {
                    $Objs = $Objs->where('start_date', ">=", $request["start_date_from"]);
                }
                if (!empty($request["start_date_to"])) {
                    $Objs = $Objs->where('start_date', "<=", $request["start_date_to"]);
                }

                if (!empty($request["closing_date_from"])) {
                    $Objs = $Objs->where('closing_date', ">=", $request["closing_date_from"]);
                }
                if (!empty($request["closing_date_to"])) {
                    $Objs = $Objs->where('closing_date', "<=", $request["closing_date_to"]);
                }
            }


            $iTotalRecords = $Objs->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $Objs = $Objs->offset($iDisplayStart);
            $Objs = $Objs->limit($iDisplayLength);


            $records = array();
            $records["data"] = array();


            foreach ($Objs->get() as $value) {

                $buttons = '';
                if ($this->Edit) {
                    $buttons .= '<button par1="' . $value->id . '" class="AEObject btn btn-sm btn-outline green"><i class="fa fa-edit"></i> ' . trans("general.edit") . '</button>';
                }
                if ($this->Delete) {
                    $buttons .= '<button par1="' . $value->id . '" par2="' . trans("poll.poll_:name", ['name' => $value->question]) . '" Dtitle="' . trans("general.are_you_sure") . '" Dcontent="' . trans("general.are_you_sure_you_want_to_delete_name", array('name' => trans("poll.poll_:name", ["name" => $value->question]))) . '"  class="Delete btn btn-sm btn-outline red"><i class="fa fa-remove"></i>  ' . trans("general.delete") . '</button>';
                }
                $records["data"][] = array(
                    $value->question,
                    $value->start_date,
                    $value->closing_date,

                    $buttons,
                );
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);


        }
    }


    public function Delete(Request $request)
    {
        $results = array();
        if ($this->Delete) {
            $ReqData = $request->all();
            $id = isset($ReqData['id']) ? mb_strtolower($ReqData['id']) : NULL;
            $par2 = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;

            $q = \App\Models\Poll::where('id', '=', $id)
                ->where('deleted_at', NULL)
                ->update([
                    'deleted_at' => date("Y-m-d H:i:s"),
                    'deleted_by' => Auth::user()->id,
                ]);

            if (!empty($q)) {
                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => trans('general.name_successfully_deleted', array('name' => $par2)),
                );
            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans('general.the_operation_was_not_done_correctly'),
                );
            }
        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);


    }
    public function DeleteOption(Request $request)
    {
        $results = array();
        if ($this->Delete) {
            $ReqData = $request->all();
            $id = isset($ReqData['id']) ? mb_strtolower($ReqData['id']) : NULL;
            $par2 = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;

            $q = \App\Models\Poll_Options::where('id', '=', $id)
                ->where('deleted_at', NULL)
                ->update([
                    'deleted_at' => date("Y-m-d H:i:s"),
                    'deleted_by' => Auth::user()->id,
                ]);

            if (!empty($q)) {
                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => trans('general.name_successfully_deleted', array('name' => $par2)),
                );
            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans('general.the_operation_was_not_done_correctly'),
                );
            }
        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);


    }

}



