<?php

namespace App\Http\Controllers;

use App\Models\Invs;
use Image;
use App\Models\Identity;
use App\Models\Share_Amount;
use App\Models\Contract;
use App\Models\Payment;
use App\Http\Requests\InvsValidation;
use Validator;
use Illuminate\Validation\Rule;


class InvsController extends Controller
{


    //show form register for invs
    public function invs_reg()
    {
        return view('invs.invs_reg');
    }

    //Save data from investor
    public function store(InvsValidation $request)
    {

        $results = array();

        $user_id = Invs::find($request->input('id'));


        $arr = $request->all();

        try {
            if ($request->hasFile('profileimg')) {
                $file = $request->file('profileimg');

                $uploader = \MediaUploader::fromSource($file);
                $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                $uploader->toDirectory('invs/' . date("Y") . '/' . date("m") . '/' . date("d"));
                $uploader->useFilename(str_random(5));
                $orginal_media = $uploader->upload();

                $folder_ = storage_path("app/public") . '/' . $orginal_media->directory;
                if (!file_exists($folder_)) {
                    mkdir($folder_, 666, true);
                }

                $image = \Image::make($orginal_media->getAbsolutePath())
                    ->resize(300, 300)
                    ->save($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);

                $thumb = new \Plank\Mediable\Media;
                $thumb->disk = $orginal_media->disk;
                $thumb->directory = $orginal_media->directory;
                $thumb->filename = $orginal_media->filename . '_thumb';
                $thumb->extension = $orginal_media->extension;
                $thumb->mime_type = $orginal_media->mime_type;
                $thumb->aggregate_type = $orginal_media->aggregate_type;
                $thumb->size = filesize($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);
                $thumb->save();


            }
        } catch (\Exception $e) {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => $e->getMessage(),
            );
        }


        if (isset($request->password_confirmation)) {
            $arr['show_password'] = $arr['password_confirmation'];
        }


        if (isset($request->no_payment_image) && $request->no_payment_image == 1) {
            $arr['no_payment_image'] = 1;
        }
        if (isset($request->no_contract_image) && $request->no_contract_image == 1) {
            $arr['no_contract_image'] = 1;
        }

        $arr['id_country'] = $request->country;
        $arr['id_country_live'] = $request->country_live;

        if (isset($request->identity_details)) {
            foreach ($request->identity_details as $key => $identity_details) {
                Validator::make($identity_details, [
                    'id_type' => 'required',
                    'id_number' => [
                        'required',
                        Rule::unique('invs_identity')->where(function ($query) use ($identity_details) {
                            return $query->where('id_number', $identity_details['id_number']);
                        }),
                    ],
                ])->validate();
            }
        }

        $cuser = Invs::updateOrCreate(
            ['id' => $request->input('id')],
            $arr
        );


        if ($cuser) {

            if (isset($orginal_media)) {
                $cuser->attachMedia($orginal_media, ['main_img']);
            }

            if (isset($thumb)) {

                $cuser->attachMedia($thumb, ['main_img_thumb']);
            }


            if (isset($request->identity_details)) {
                foreach ($request->identity_details as $key => $identity_details) {

                    if (!empty($identity_details['id_type']) && !empty($identity_details['id_number']) && !empty($identity_details['id_image_path'])) {
                        $id = !empty($identity_details['id']) ? $identity_details['id'] : NULL;
                        $identity_details['invs_id'] = $cuser->id;
                        $identity_details['id_number'] = $this->convert_numbers($identity_details['id_number']);

                        try {

                                $file = $identity_details['id_image_path'];

                                $uploader = \MediaUploader::fromSource($file);
                                $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                                $uploader->toDirectory('identities/' . date("Y") . '/' . date("m") . '/' . date("d"));
                                $uploader->useFilename(str_random(5));
                                $orginal_media = $uploader->upload();

                                $folder_ = storage_path("app/public") . '/' . $orginal_media->directory;
                                if (!file_exists($folder_)) {
                                    mkdir($folder_, 666, true);
                                }

                                $image = \Image::make($orginal_media->getAbsolutePath())
                                    ->resize(300, 300)
                                    ->save($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);

                                $thumb = new \Plank\Mediable\Media;
                                $thumb->disk = $orginal_media->disk;
                                $thumb->directory = $orginal_media->directory;
                                $thumb->filename = $orginal_media->filename . '_thumb';
                                $thumb->extension = $orginal_media->extension;
                                $thumb->mime_type = $orginal_media->mime_type;
                                $thumb->aggregate_type = $orginal_media->aggregate_type;
                                $thumb->size = filesize($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);
                                $thumb->save();



                        } catch (\Exception $e) {
                            $results['Errors'] = array(
                                'title' => trans('general.error'),
                                'content' => $e->getMessage(),
                            );
                        }

                        $identity = Identity::updateOrCreate(
                            ['id' => $id, 'invs_id' => $cuser->id],
                            $identity_details
                        );
                        if($identity){
                            if (isset($orginal_media)) {
                                $identity->attachMedia($orginal_media, ['main_img']);
                            }
                            if (isset($thumb)) {
                                $identity->attachMedia($thumb, ['main_img_thumb']);
                            }
                        }
                    }


                }
            }

            if (isset($request->share_amount_details)) {
                foreach ($request->share_amount_details as $key => $share_amount_details) {

                    if (!empty($share_amount_details['share_amount']) && !empty($share_amount_details['share_number']) && !empty($share_amount_details['share_date'])) {
                        $id = !empty($share_amount_details['id']) ? $share_amount_details['id'] : NULL;
                        $share_amount_details['invs_id'] = $cuser->id;

                        $share_amount = Share_Amount::updateOrCreate(
                            ['id' => $id, 'invs_id' => $cuser->id],
                            $share_amount_details

                        );
                    }


                }
            }


            if (isset($request->contract_details)) {
                foreach ($request->contract_details as $key => $contract_details) {

                    $id = !empty($contract_details['id']) ? $contract_details['id'] : NULL;
                    $contract_details['invs_id'] = $cuser->id;


                    try {

                        $file = $contract_details['contract_image'];

                        $uploader = \MediaUploader::fromSource($file);
                        $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                        $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                        $uploader->toDirectory('contracts/' . date("Y") . '/' . date("m") . '/' . date("d"));
                        $uploader->useFilename(str_random(5));
                        $orginal_media = $uploader->upload();

                        $folder_ = storage_path("app/public") . '/' . $orginal_media->directory;
                        if (!file_exists($folder_)) {
                            mkdir($folder_, 666, true);
                        }

                        $image = \Image::make($orginal_media->getAbsolutePath())
                            ->resize(300, 400)
                            ->save($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);

                        $thumb = new \Plank\Mediable\Media;
                        $thumb->disk = $orginal_media->disk;
                        $thumb->directory = $orginal_media->directory;
                        $thumb->filename = $orginal_media->filename . '_thumb';
                        $thumb->extension = $orginal_media->extension;
                        $thumb->mime_type = $orginal_media->mime_type;
                        $thumb->aggregate_type = $orginal_media->aggregate_type;
                        $thumb->size = filesize($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);
                        $thumb->save();



                    } catch (\Exception $e) {
                        $results['Errors'] = array(
                            'title' => trans('general.error'),
                            'content' => $e->getMessage(),
                        );
                    }


                    $contract = Contract::updateOrCreate(
                        ['id' => $id, 'invs_id' => $cuser->id],
                        $contract_details
                    );

                    if($contract){
                        if (isset($orginal_media)) {
                            $contract->attachMedia($orginal_media, ['main_img']);
                        }
                        if (isset($thumb)) {
                            $contract->attachMedia($thumb, ['main_img_thumb']);
                        }
                    }

                }
            }


            if (isset($request->payment_details)) {
                foreach ($request->payment_details as $key => $payment_details) {

                    $id = !empty($payment_details['id']) ? $payment_details['id'] : NULL;
                    $payment_details['invs_id'] = $cuser->id;

                    try {

                        $file = $payment_details['payment_image'];

                        $uploader = \MediaUploader::fromSource($file);
                        $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                        $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                        $uploader->toDirectory('payments/' . date("Y") . '/' . date("m") . '/' . date("d"));
                        $uploader->useFilename(str_random(5));
                        $orginal_media = $uploader->upload();

                        $folder_ = storage_path("app/public") . '/' . $orginal_media->directory;
                        if (!file_exists($folder_)) {
                            mkdir($folder_, 666, true);
                        }

                        $image = \Image::make($orginal_media->getAbsolutePath())
                            ->resize(300, 400)
                            ->save($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);

                        $thumb = new \Plank\Mediable\Media;
                        $thumb->disk = $orginal_media->disk;
                        $thumb->directory = $orginal_media->directory;
                        $thumb->filename = $orginal_media->filename . '_thumb';
                        $thumb->extension = $orginal_media->extension;
                        $thumb->mime_type = $orginal_media->mime_type;
                        $thumb->aggregate_type = $orginal_media->aggregate_type;
                        $thumb->size = filesize($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);
                        $thumb->save();



                    } catch (\Exception $e) {
                        $results['Errors'] = array(
                            'title' => trans('general.error'),
                            'content' => $e->getMessage(),
                        );
                    }




                    $payment = Payment::updateOrCreate(
                        ['id' => $id, 'invs_id' => $cuser->id],
                        $payment_details
                    );

                    if($payment){
                        if (isset($orginal_media)) {
                            $payment->attachMedia($orginal_media, ['main_img']);
                        }
                        if (isset($thumb)) {
                            $payment->attachMedia($thumb, ['main_img_thumb']);
                        }
                    }

                }
            }




        }


        if ($cuser) {

            $success_msg = 'تم التعديل';

        } else {

            $success_msg = 'تمت الاضافه بنجتح';
        }

        $results['Success'] = array(
            'title' => 'تم بنجاح',
            'content' => $success_msg,
            'redirectto' => '/success'
        );

        echo json_encode($results); // php array to json


    }


    // save image
    public function _save_image($Img, $folder, $width = NULL, $height = NULL)
    {


        $filename = time() . '.' . $Img->getClientOriginalExtension();

        $d_name = $folder;
        $imagepath = $d_name . $filename;
        if (!empty($d_name)) {
            $directory_D = str_replace("\\", "/", $d_name);
            if (!file_exists($directory_D)) {
                mkdir($directory_D, 0777, true);
            }
        }

        $path = $imagepath;

        $dth = $d_name . "thumbs/";
        $imagepath1 = $dth . $filename;
        if (!empty($dth)) {
            $directory_D = str_replace("\\", "/", $dth);
            if (!file_exists($directory_D)) {
                mkdir($directory_D, 0777, true);
            }
        }


        try {

            Image::make($Img->getRealPath())->save($imagepath1);

            if (!empty($width) && !empty($height)) {
                Image::make($Img->getRealPath())->resize($width, $height)->save($path);

            }


        } catch (Exception $e) {

            // do something in case of failure

        }

        return $filename;
    }






    public function check()
    {
        return view('invs.check-request');
    }

    public function success()
    {
        return view('invs.registration_success');
    }

    public function forget()
    {
        return view('invs.forgetpass');
    }

    function convert_numbers($string) {
        $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
        $arabic = ['٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١','٠'];

        $num = range(0, 9);
        $convertedPersianNums = str_replace($persian, $num, $string);
        $englishNumbersOnly = str_replace($arabic, $num, $convertedPersianNums);

        return $englishNumbersOnly;
    }


}



