<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Waavi\Translation\Models\Translation;
use Waavi\Translation\Models\Language;
use Auth;
use Image;
use View;
use Validator;
use Illuminate\Validation\Rule;
use DB;
use App;
use Carbon\Carbon;

class Contact extends Controller
{
    protected $Browse = FALSE;
    protected $Add = FALSE;
    protected $Edit = FALSE;
    protected $Delete = FALSE;
     

   // protected $ShowingNews = FALSE;

    protected $thumb_width = 300;
    protected $thumb_height = 300;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->Browse = Auth::user()->ability(array('admin'), array('B_Contact'));
            $this->Add = Auth::user()->ability(array('admin'), array('A_Contact'));
            $this->Edit = Auth::user()->ability(array('admin'), array('E_Contact'));
            $this->Delete = Auth::user()->ability(array('admin'), array('D_Contact'));

            

            View::share('Browse', $this->Browse);
            View::share('Add', $this->Add);
            View::share('Edit', $this->Edit);
            View::share('Delete', $this->Delete);
            

            View::share('thumb_width', $this->thumb_width);
            View::share('thumb_height', $this->thumb_height);

            return $next($request);
        });


    }


    public function index()
    {
        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            return view('admin.contact.view_all');
        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('general.You do not have permission to access this content'));
        }
    }


      public function Subscribe()
    {
        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            return view('admin.subscribe.view_all');
        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('general.You do not have permission to access this content'));
        }
    }


      public function GetDataSub(Request $request)
    {

        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            $Objs = \App\Models\Subscribe::orderBy('id', 'desc');

            //Check if request is serach
            if (isset($request["action"]) && $request['action'] == "filter") {

                if (!empty($request["email"])) {
                    $Objs = $Objs->where("email", 'LIKE', '%' . $request["email"] . '%');
                }
                
               
            }


            $iTotalRecords = $Objs->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $Objs = $Objs->offset($iDisplayStart);
            $Objs = $Objs->limit($iDisplayLength);


            $records = array();
            $records["data"] = array();


            $Objs = $Objs->get();

            foreach ($Objs as $value) {

                $buttons = '';
                 $array_data = array();

                    array_push($array_data, $value->email);
                    array_push($array_data, $buttons);
               

                  


                $records["data"][] = $array_data;
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);


        }
    }






   


       


    public function getdata(Request $request)
    {

        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            $Objs = \App\Models\Contact::orderBy('id', 'desc');

           

            //Check if request is serach
            if (isset($request["action"]) && $request['action'] == "filter") {





                if (!empty($request["name"])) {
                    $Objs = $Objs->where("name", 'LIKE', '%' . $request["name"] . '%');
                }

                if (!empty($request["email"])) {
                    $Objs = $Objs->where("email", 'LIKE', '%' . $request["email"] . '%');
                }
                 if (!empty($request["subject"])) {
                    $Objs = $Objs->where("subject", 'LIKE', '%' . $request["subject"] . '%');
                }

                

               
            }


            $iTotalRecords = $Objs->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $Objs = $Objs->offset($iDisplayStart);
            $Objs = $Objs->limit($iDisplayLength);


            $records = array();
            $records["data"] = array();


            $Objs = $Objs->get();

            foreach ($Objs as $value) {


                $buttons = '';


                $buttons .= '<button par1="' . $value->id . '" class="SeeMessageObject btn btn-sm btn-outline green"><i class="fa fa-edit"></i> ' . trans("general.view_message") . '</button>';

                  

                // $buttons .= '<button par1="' . $value->id . '" Dtitle="' . trans("general.are_you_sure") . '" Dcontent="' . trans("general.are_you_sure_you_want_to_delete_name", array('name' => $value->{'title_' . App::getLocale()})) . '"  class="Delete btn btn-sm btn-outline red"><i class="fa fa-remove"></i>  ' . trans("general.delete") . '</button>';



               


                 $created_at = new Carbon($value->created_at);

                 $array_data = array();

                  
                    array_push($array_data, $value->name);
                    array_push($array_data, $value->email);
                    array_push($array_data, $value->subject);
                    array_push($array_data, $buttons);
               

                  


                $records["data"][] = $array_data;
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);


        }
    }





       public function Show(Request $request)
    {
        $ReqData = $request->all();
        $par1 = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $CurrentObj = \App\Models\Contact::find($par1);
        $hide_submit = true;  
    
        if (($this->Edit && !empty($CurrentObj))) {


            // $img = !empty($CurrentObj) ? ((($CurrentObj->GetAttachURL('withdraws')) ? $CurrentObj->GetAttachURL('withdraws') : NULL)) : NULL;

            return json_encode(array(
                'title' => 'مشاهدة الرسالة',
                'content' => view('admin.contact.ShowMessage')->with('par1', $par1)->with('CurrentObj', $CurrentObj)->render(),
                  'hide_submit' => $hide_submit  
                
                

            ));
        } else {
            return json_encode(array(
                'title' => trans("general.error"),
                'content' => view('system.errors.show_error_modal')->with('error_description', trans("general.You do not have permission to access this content"))->render()
            ));
        }
    }





}



