<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use View;
use Validator;
use App;
use Carbon\Carbon;

class Shares extends Controller
{
    protected $Browse = FALSE;
    protected $Add = FALSE;
    protected $Edit = FALSE;
    protected $Delete = FALSE;

    protected $thumb_width = 300;
    protected $thumb_height = 300;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->Browse = Auth::user()->ability(array('admin'), array('B_Shares'));
            $this->Add = Auth::user()->ability(array('admin'), array('A_Shares'));
            $this->Edit = Auth::user()->ability(array('admin'), array('E_Shares'));
            $this->Delete = Auth::user()->ability(array('admin'), array('D_Shares'));


            View::share('Browse', $this->Browse);
            View::share('Add', $this->Add);
            View::share('Edit', $this->Edit);
            View::share('Delete', $this->Delete);

            View::share('thumb_width', $this->thumb_width);
            View::share('thumb_height', $this->thumb_height);

            return $next($request);
        });


    }


    public function index()
    {
        if (Auth::user()->hasRole('investor') || $this->Browse || $this->Add || $this->Edit || $this->Delete) {
            return view('admin.shares.view_all');
        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('general.You do not have permission to access this content'));
        }
    }

    public function AE(Request $request)
    {
        $ReqData = $request->all();
        $par1 = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $CurrentObj = NULL;
        if ($par1 == 'add') {
            $title = trans("general.add_name_element", array("name" => trans("shares.share")));
        } else {
            $CurrentObj = \App\Models\Share_Amount::find($par1);
            $title = trans('general.edit_element');
        }

        if (($this->Add && $par1 == 'add') || (!empty($CurrentObj) && ($this->Edit || (isset($CurrentObj->Investor->UserOwner->id) && ($CurrentObj->Investor->UserOwner->id == Auth::user()->id))))) {
            return json_encode(array(
                'title' => $title,
                'content' => view('admin.shares.AE')->with('par1', $par1)->with('CurrentObj', $CurrentObj)->render()
            ));
        } else {
            return json_encode(array(
                'title' => trans("general.error"),
                'content' => view('system.errors.show_error_modal')->with('error_description', trans("general.You do not have permission to access this content"))->render()
            ));
        }
    }

    public function store(Request $request)
    {

        //$ReqData = \Purifier::clean($request->all());
        $ReqData = $request->all();
        $id = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $CurrentObj = \App\Models\Share_Amount::where('id', $id)->first();

        $is_user_owner_share = (isset($CurrentObj->Investor->UserOwner->id) && ($CurrentObj->Investor->UserOwner->id == Auth::user()->id));

        $results = array();
        if (($this->Add && empty($id)) || (!empty($CurrentObj) && ($this->Edit || ($is_user_owner_share)))) {
            $ss = isset($CurrentObj->id) ? $CurrentObj->id : NULL;

            $st = (!empty($ss)) ? 'required,id,' . $ss : 'required';

            $idd = array(
                'invs_id' => $st,
                'share_amount' => 'required',
                'share_number' => 'required',
                'share_date' => 'required',

            );
            Validator::make($ReqData, $idd)->validate();

            if (isset($ReqData['state'])) {
                if ($ReqData['state'] == 'effective') {
                    $ReqData['state'] = 1;
                } else {
                    $ReqData['state'] = NULL;
                }
            }
            //ترحيل التعديلات للمراجعة من قبل المدير
            $will_reviewed = FALSE;
            if ($is_user_owner_share && (!$this->Edit)) {
                if (isset($ReqData['state'])) {
                    unset($ReqData['state']);
                }
                if (isset($ReqData['invs_id'])) {
                    unset($ReqData['invs_id']);
                }
                if (isset($ReqData['share_amount']) && ($CurrentObj->share_amount != $ReqData['share_amount'])) {
                    $ReqData['share_amount_need_review'] = $ReqData['share_amount'];
                    unset($ReqData['share_amount']);
                    $will_reviewed = TRUE;
                }
                if (isset($ReqData['share_number']) && ($CurrentObj->share_number != $ReqData['share_number'])) {
                    $ReqData['share_number_need_review'] = $ReqData['share_number'];
                    unset($ReqData['share_number']);
                    $will_reviewed = TRUE;
                }
                if (isset($ReqData['share_date']) && ($CurrentObj->share_date != $ReqData['share_date'])) {
                    $ReqData['share_date_need_review'] = $ReqData['share_date'];
                    unset($ReqData['share_date']);
                    $will_reviewed = TRUE;
                }
                if (isset($ReqData['field_id']) && ($CurrentObj->field_id != $ReqData['field_id'])) {
                    $ReqData['field_id_need_review'] = $ReqData['field_id'];
                    unset($ReqData['field_id']);
                    $will_reviewed = TRUE;
                }
            }

            $Obj = \App\Models\Share_Amount::updateOrCreate(
                ['id' => $id],
                $ReqData
            );


            if ($Obj->wasRecentlyCreated) {
                $content = trans("general.successfully_added_new_:name", ['name' => trans("general.element")]);
                $Obj->created_by = Auth::user()->id;
                $Obj->update();

            } else {

                $content = ($will_reviewed === TRUE) ? trans("general.saved_success_will_reviewd") : trans("general.modified_successfully");
                $Obj->updated_by = Auth::user()->id;
                $Obj->update();

            }

            if (!empty($Obj->id)) {
                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => $content,
                );
            }

        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }

        echo json_encode($results);


    }


    public function getdata(Request $request)
    {

        if (Auth::user()->hasRole('investor') || $this->Browse || $this->Add || $this->Edit || $this->Delete) {
            $Objs = \App\Models\Share_Amount::orderBy('id', 'desc');


            if (Auth::user()->hasRole('investor') && !($this->Browse || $this->Add || $this->Edit || $this->Delete)) {
                $Objs = $Objs->whereHas('Investor', function ($q) {
                    $q->where("user_id", '=', Auth::user()->id);
                });
            }

            //Check if request is serach
            if (isset($request["action"]) && $request['action'] == "filter") {


                if (!empty($request["investor_name"])) {
                    $Objs = $Objs->whereHas('Investor', function ($q) use ($request) {
                        $q->where(\DB::raw('concat(`firstname`," ",`secondname`," ",`thirdname`," ",`fourthname`)'), 'LIKE', '%' . $request["investor_name"] . '%');
                    });
                }

                if (isset($request["share_amount_from"]) && is_numeric($request["share_amount_from"])) {
                    $Num_From = $request["share_amount_from"];
                    $Objs = $Objs->where('share_amount', ">=", $Num_From);
                }
                if (isset($request["share_amount_to"]) && is_numeric($request["share_amount_to"])) {
                    $Num_To = $request["share_amount_to"];
                    $Objs = $Objs->where('share_amount', "<=", $Num_To);
                }

                if (!empty($request["share_number"])) {
                    $Objs = $Objs->where("share_number", 'LIKE', '%' . $request["share_number"] . '%');
                }
                if (!empty($request["field_id"])) {
                    $Objs = $Objs->where("field_id", $request["field_id"]);
                }
                if (!empty($request["state"])) {
                    if ($request["state"] == 'effective') {
                        $Objs = $Objs->where("state", '=', 1);
                    } elseif ($request["state"] == 'ineffective') {
                        $Objs = $Objs->where("state", '=', NULL);
                    }
                }

                if (!empty($request["date_from"])) {
                    $Objs = $Objs->where('share_date', ">=", $request["date_from"]);
                }
                if (!empty($request["date_to"])) {
                    $Objs = $Objs->where('share_date', "<=", $request["date_to"]);
                }
            }


            $iTotalRecords = $Objs->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $Objs = $Objs->offset($iDisplayStart);
            $Objs = $Objs->limit($iDisplayLength);


            $records = array();
            $records["data"] = array();


            foreach ($Objs->get() as $value) {

                $buttons = '';
                if ($this->Edit) {
                    $buttons .= '<button par1="' . $value->id . '" class="AEObject btn btn-sm btn-outline green"><i class="fa fa-edit"></i> ' . trans("general.edit") . '</button>';
                }
                if ($this->Delete) {
                    $buttons .= '<button par1="' . $value->id . '" par2="'.trans("shares.share_of_investor_:name_note_that_it_:note",['name'=> $value->Investor->get_Short_name(),'note' => str_replace('"',"'",$value->get_State(1))]).'" Dtitle="' . trans("general.are_you_sure") . '" Dcontent="' . trans("general.are_you_sure_you_want_to_delete_name", array('name' => trans("shares.share_of_investor_:name_note_that_it_:note",['name'=> $value->Investor->get_Short_name(),'note' => str_replace('"',"'",$value->get_State(1))]))) . '"  class="Delete btn btn-sm btn-outline red"><i class="fa fa-remove"></i>  ' . trans("general.delete") . '</button>';
                }
                $records["data"][] = array(
                    $value->get_State(1),
                    $value->Investor->get_Full_name(),
                    $value->share_amount . ' $',
                    $value->share_number,
                    $value->share_date,

                    $buttons,
                );
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);


        }
    }


    public function Delete(Request $request)
    {
        $results = array();
        if ($this->Delete) {
            $ReqData = $request->all();
            $id = isset($ReqData['id']) ? mb_strtolower($ReqData['id']) : NULL;
            $par2 = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;

            $q = \App\Models\Share_Amount::where('id', '=', $id)
                ->where('deleted_at', NULL)
                ->update([
                    'deleted_at' => date("Y-m-d H:i:s"),
                    'deleted_by' => Auth::user()->id,
                ]);

            if (!empty($q)) {
                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => trans('general.name_successfully_deleted', array('name' => $par2)),
                );
            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans('general.the_operation_was_not_done_correctly'),
                );
            }
        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);


    }

}



