<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use View;
use Validator;
use App;

class Financial_Reports extends Controller
{

    protected $thumb_width = 300;
    protected $thumb_height = 300;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {

            View::share('thumb_width', $this->thumb_width);
            View::share('thumb_height', $this->thumb_height);

            return $next($request);
        });


    }


    public function index()
    {
        if (Auth::user()->hasRole(['investor'])) {

            $invs_id = isset(Auth::user()->investor_account->id) ? Auth::user()->investor_account->id : NULL;
            if (!empty($invs_id)) {

                $Withdraw = \App\Models\Withdraws::where('invs_id', '=', $invs_id)->first();
                return view('admin.financial.withdraw')->with('Withdraw',$Withdraw);
            } else {
                return view('system.errors.show_error')
                    ->with('error_no', '403')
                    ->with('error_title', trans('general.error'))
                    ->with('error_description', trans('business_fields.edit_profile_is_only_available_to_investors'));
            }


        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('investors.:name_is_only_available_to_investors',["name" => trans("financial.financial_report")]));
        }
    }

    public function Withdraw(Request $request)
    {
        $results = array();
        if (Auth::user()->hasRole(['investor'])) {
            $invs_id = isset(Auth::user()->investor_account->id) ? Auth::user()->investor_account->id : NULL;
            if (!empty($invs_id)) {

                $q = \App\Models\Withdraws::where('invs_id', '=', $invs_id)->first();

                if (!empty($q->id)) {
                    $results['Errors'] = array(
                        'title' => trans('general.error'),
                        'content' => trans('financial.you_have_already_requested_earnings_and_your_credit_has_been_processed'),
                    );
                } else {
                    $q = new \App\Models\Withdraws;

                    $q->invs_id = $invs_id;

                    $q->save();

                    if (!empty($q)) {
                        $results['Success'] = array(
                            'title' => trans('general.done'),
                            'content' => trans('general.your_request_is_being_processed'),
                        );
                    } else {
                        $results['Errors'] = array(
                            'title' => trans('general.error'),
                            'content' => trans('general.the_operation_was_not_done_correctly'),
                        );
                    }
                }


            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans('financial.the_withdrawal_request_is_available_to_investors_only'),
                );
            }

        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);


    }


}



