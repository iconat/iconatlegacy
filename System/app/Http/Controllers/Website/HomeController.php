<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App;
use Cookie;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

   




    public function AboutUs()
    {

        // $Main_4_Whyus = \App\Models\Whyus::orderBy('id', 'desc')->limit(4)->get();
        // ->with("Main_4_Whyus",$Main_4_Whyus)
        return view('website.AboutUs');
    }

 
    
}
