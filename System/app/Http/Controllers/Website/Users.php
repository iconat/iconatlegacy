<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App;
use Auth;
use Cookie;
use Illuminate\Validation\Rule;
use Validator;
use Image;
use ImageOptimizer;
use Carbon\Carbon;
use File;

class Users extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }


    public function RegisterModal(Request $request)
    {
        $ReqData = $request->all();
        $Ref = isset($ReqData['Ref']) ? mb_strtolower($ReqData['Ref']) : NULL;

        if (empty(\Auth::user())) {

            return json_encode(array(
                'hide_submit' => false,
                'title' => trans("general.create_a_new_account"),
                'content' => view('website.RegisterModal')
                    ->with("Ref",$Ref)
                    ->render()
            ));
        } else {
            return json_encode(array(
                'hide_submit' => true,
                'title' => trans("general.error"),
                'content' => view('system.errors.show_error_modal')->with('error_description', trans("general.you_can_not_register_new_members__you_are_currently_signed_in_as_a_member_:name",['name' => \Auth::user()->get_Short_name()]))->render()
            ));
        }
    }


    public function LoginModal(Request $request)
    {
        $ReqData = $request->all();
        $Ref = isset($ReqData['Ref']) ? mb_strtolower($ReqData['Ref']) : NULL;

        if (empty(\Auth::user())) {

            return json_encode(array(
                'hide_submit' => false,
                'title' => trans("general.login"),
                'content' => view('website.LoginModal')
                    ->with("Ref",$Ref)
                    ->render()
            ));
        } else {
            return json_encode(array(
                'hide_submit' => true,
                'title' => trans("general.error"),
                'content' => view('system.errors.show_error_modal')->with('error_description', trans("general.you_are_currently_signed_in_as_a_member_:name",['name' => \Auth::user()->get_Short_name()]))->render()
            ));
        }
    }

  public function Edit (Request $request)
    {
        App::setLocale($request->lang);

        $ReqData = \Purifier::clean($request->all());
        //$ReqData = $request->all();
        $par1 = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;
        $Ref = isset($ReqData['Ref']) ? mb_strtolower($ReqData['Ref']) : NULL;

        $CurrentObj = \App\User::find($par1);

        $InfoUser = \App\Models\InfoUser::where('user_id', $par1)->first();

        //echo $CurrentObj;
        $results = array();
        if (\Auth::user()) {

            $Validation_Array = array(
                'name' => 'required',
                //'password' => 'required',
                // 'lastName'=> 'required',
                // 'shopCompanyName'=> 'required',
                // 'city'=> 'required',
                // 'shopAddress'=> 'required',
                // 'shopTown'=> 'required',
                // 'shopState'=> 'required',
                // 'shopPostCode'=> 'required',
                // 'shopPhone'=> 'required',
                // 'username' => [
                //     'required',
                //     Rule::unique('users')->where(function ($query) use ($request) {
                //         return $query->where('username', $request->email);
                //     }),
                // ],
                // 'email' => [
                //     'required',
                //     'email',
                //     Rule::unique('users')->where(function ($query) use ($request) {
                //         return $query->where('email', $request->email);
                //     }),
                // ],
            );

            if (!empty($ReqData['password'])) {
                $ReqData['password'] = bcrypt($ReqData['password']);
            } else {
                unset($ReqData['password']);
            }

            // $ReqData['username'] = $ReqData['email'];


            if (empty($CurrentObj)) {

                $Validation_Array['shopCompanyName'] = 'required';
                $Validation_Array['shopAddress'] = 'required';
                $Validation_Array['city'] = 'required';
                $Validation_Array['shopState'] = 'required';
                $Validation_Array['shopPostCode'] = 'required';
                $Validation_Array['shopPhone'] = 'required';

            }

            Validator::make($ReqData, $Validation_Array)->validate();


            if (empty($results['Errors'])) {

                $Data = array();
                $Data['ref'] = $Ref;

                if (!empty($ReqData['username'])) {
                    $Data['username'] = $ReqData['username'];
                }
                if (!empty($ReqData['name'])) {
                    $Data['name'] = $ReqData['name'];
                }
                if (!empty($ReqData['email'])) {
                    $Data['email'] = $ReqData['email'];
                }
                // $un_pass = $ReqData['password'];
                if (!empty($ReqData['password'])) {
                    $Data['password'] = $ReqData['password'];
                }
                if (!empty($ReqData['username'])) {
                    $Data['username'] = $ReqData['username'];
                }

                $Obj = \App\User::updateOrCreate(
                    ['id' => $par1],
                    $Data
                );





                 if (!empty($ReqData['shopCompanyName'])) {
                    $DataInfo['shopCompanyName'] = $ReqData['shopCompanyName'];
                }
                if (!empty($ReqData['city'])) {
                    $DataInfo['city'] = $ReqData['city'];
                }
                if (!empty($ReqData['shopAddress'])) {
                    $DataInfo['shopAddress'] = $ReqData['shopAddress'];
                }
                if (!empty($ReqData['shopTown'])) {
                    $DataInfo['shopTown'] = $ReqData['shopTown'];
                }


                  if (!empty($ReqData['shopState'])) {
                    $DataInfo['shopState'] = $ReqData['shopState'];
                }
                if (!empty($ReqData['shopPostCode'])) {
                    $DataInfo['shopPostCode'] = $ReqData['shopPostCode'];
                }
                if (!empty($ReqData['shopPhone'])) {
                    $DataInfo['shopPhone'] = $ReqData['shopPhone'];
                }
                     $DataInfo['user_id'] = $Obj->id;

                $Info = \App\Models\InfoUser::updateOrCreate(
                     ['user_id' =>  $Obj->id ],
                    $DataInfo

                );

                //  $C_Rules = [4];
                // $Obj->roles()->sync($C_Rules);






                    $results['Success'] = array(
                        'title' => trans('general.done'),
                        'content' => trans("general.:name,_your_account_data_has_been_modified",['name' => $Obj->name]),
                        //'redirect' => $redirect,
                    );


            }


        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans("general.you_can_not_register_new_members__you_are_currently_signed_in_as_a_member_:name",['name' => \Auth::user()->get_Short_name()]),
            );
        }

        echo json_encode($results);


    }


    public function CreateUser(Request $request)
    {
        App::setLocale($request->lang);

        $ReqData = \Purifier::clean($request->all());
        //$ReqData = $request->all();
        $Ref = isset($ReqData['Ref']) ? mb_strtolower($ReqData['Ref']) : NULL;

        //$CurrentObj = \App\User::where('id', $id)->first();

        $results = array();
        if (empty(\Auth::user())) {

            $Validation_Array = array(
                'name' => 'required',
                'password' => 'required',
                // 'lastName'=> 'required',
                // 'shopCompanyName'=> 'required',
                // 'city'=> 'required',
                // 'shopAddress'=> 'required',
                // 'shopTown'=> 'required',
                // 'shopState'=> 'required',
                // 'shopPostCode'=> 'required',
                // 'shopPhone'=> 'required',
                'username' => [
                    'required',
                    Rule::unique('users')->where(function ($query) use ($request) {
                        return $query->where('username', $request->email);
                    }),
                ],
                'email' => [
                    'required',
                    'email',
                    Rule::unique('users')->where(function ($query) use ($request) {
                        return $query->where('email', $request->email);
                    }),
                ],
            );

            if(isset($ReqData['username'])){
            $ReqData['username']=strtolower ($ReqData['username']);

            }

            if (!empty($ReqData['password'])) {
                $ReqData['password'] = bcrypt($ReqData['password']);
            } else {
                unset($ReqData['password']);
            }

            // $ReqData['username'] = $ReqData['email'];


            if (empty($CurrentObj)) {

                $Validation_Array['shopCompanyName'] = 'required';
                $Validation_Array['shopAddress'] = 'required';
                $Validation_Array['city'] = 'required';
                $Validation_Array['shopState'] = 'required';
                $Validation_Array['shopPostCode'] = 'required';
                $Validation_Array['shopPhone'] = 'required';

            }

            Validator::make($ReqData, $Validation_Array)->validate();


            if (empty($results['Errors'])) {

                $Data = array();
                $Data['ref'] = $Ref;

                 if (!empty($ReqData['username'])) {
                    $Data['username'] = $ReqData['username'];
                }
                if (!empty($ReqData['name'])) {
                    $Data['name'] = $ReqData['name'];
                }
                if (!empty($ReqData['email'])) {
                    $Data['email'] = $ReqData['email'];
                }
                $un_pass = $ReqData['password'];
                if (!empty($ReqData['password'])) {
                    $Data['password'] = $ReqData['password'];
                }
                if (!empty($ReqData['username'])) {
                    $Data['username'] = $ReqData['username'];
                }

                $Obj = \App\User::create(
                    $Data
                );





                 if (!empty($ReqData['shopCompanyName'])) {
                    $DataInfo['shopCompanyName'] = $ReqData['shopCompanyName'];
                }
                if (!empty($ReqData['city'])) {
                    $DataInfo['city'] = $ReqData['city'];
                }
                if (!empty($ReqData['shopAddress'])) {
                    $DataInfo['shopAddress'] = $ReqData['shopAddress'];
                }
                if (!empty($ReqData['shopTown'])) {
                    $DataInfo['shopTown'] = $ReqData['shopTown'];
                }


                  if (!empty($ReqData['shopState'])) {
                    $DataInfo['shopState'] = $ReqData['shopState'];
                }
                if (!empty($ReqData['shopPostCode'])) {
                    $DataInfo['shopPostCode'] = $ReqData['shopPostCode'];
                }
                if (!empty($ReqData['shopPhone'])) {
                    $DataInfo['shopPhone'] = $ReqData['shopPhone'];
                }
                    $DataInfo['user_id'] = $Obj->id;

                $Info = \App\Models\InfoUser::updateOrCreate(

                    $DataInfo

                );

                 $C_Rules = [4];
                $Obj->roles()->sync($C_Rules);


                if ($Obj->wasRecentlyCreated) {
                    $redirect = true;
                    $content = trans("general.:name,_thank_you_for_registering_with_us,_registration_successful",['name' => $Obj->name]);

                    if($Obj->ref == 'checkout'){
                        $Arr = ['email' => $Obj->email, 'password' => $un_pass];

                        $Login = \Auth::attempt($Arr,true);
                        if ($Login) {
                            $content = trans("products.:name,_thank_you_for_registering_with_us,_you_will_now_sign_in_automatically_to_continue_your_purchase_",['name' => $Obj->name]);
                        }

                    }

                    $results['Success'] = array(
                        'title' => trans('general.done'),
                        'content' => trans($content),
                        'redirect' => $redirect,
                    );
                }

            }


        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans("general.you_can_not_register_new_members__you_are_currently_signed_in_as_a_member_:name",['name' => \Auth::user()->get_Short_name()]),
            );
        }

        echo json_encode($results);


    }


     public function generate_code()
    {
        $randomString = str_random(7);
        while (true){
            if(\App\Models\Shops::where('shop_code', $randomString)->count() > 0){
                return $this->generate_code();
            }
            else{
                return $randomString;
            }
        }

    }

    public function authenticate(Request $request)
    {

        $user = \App\User::where('email', '=', $request->username)->orWhere('username', '=', $request->username)->first();


        if ($user) {
            $unam = "email";
            if ($request->username == $user->username) {
                $unam = "username";
            }

            if (\Auth::attempt([$unam => $request->username, 'password' => $request->password],($request->remember == 1) ? true : false)) {
                // Authentication passed...

                $r_to = $request->url();

                return response()->json(['Success' => ['redirectto' => $r_to] ]);
            } else {
                return response()->json(['Errors' => [
                    'title' => trans("general.error"),
                    'content' => trans("general.username_or_password_is_incorrect")
                ]]);
            }

        } else {
            return response()->json(['Errors' => [
                'title' => trans("general.error"),
                'content' => trans("general.username_or_password_is_incorrect")
            ]]);
        }


        //  return json_encode(array("Result"=>$dd));

    }

    public function LoginFromModal(Request $request)
    {

        $user = \App\User::where('email', '=', $request->email)->orWhere('username', '=', $request->email)->first();


        if ($user) {
            $unam = "email";
            if ($request->username == $user->username) {
                $unam = "username";
            }

            if (\Auth::attempt([$unam => $request->email, 'password' => $request->password],($request->remember == 1) ? true : false)) {
                // Authentication passed...

                $r_to = $request->url();

                return response()->json(['Success' => [
                    'redirect' => $r_to,
                    'title' => trans("general.welcome"),
                    'content' => trans("general.logged_in_successfully,_the_page_will_now_be_updated"),
                ] ]);
            } else {
                return response()->json(['Errors' => [
                    'title' => trans("general.error"),
                    'content' => trans("general.username_or_password_is_incorrect")
                ]]);
            }

        } else {
            return response()->json(['Errors' => [
                'title' => trans("general.error"),
                'content' => trans("general.username_or_password_is_incorrect")
            ]]);
        }


        //  return json_encode(array("Result"=>$dd));

    }




 public function ShopEdit(Request $request)
    {

        $ReqData = \Purifier::clean($request->all());
        //$ReqData = $request->all();

        $id = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;
        $Obj = \App\Models\Shops::where('id', '=', $id)->first();

        $results = array();

        if ((!empty(\App\Models\Shops::where('id', $id)->count())) || (empty($id))) {





            if (!empty($id) && $request->hasFile('main_logo_shop')) {

                $imagen1 = getcwd() . "/storage/" . $Obj->GetURL('main_logo_shop_img');
                $imagen2 = getcwd() . "/storage/" . $Obj->GetImageUrlLG('main_logo_shop_img_lg');
                $imagen3 = getcwd() . "/storage/" . $Obj->GetImageUrlMD('main_logo_shop_img_md');
                $imagen4 = getcwd() . "/storage/" . $Obj->GetImageUrlSM('main_logo_shop_img_sm');
                $imagen5 = getcwd() . "/storage/" . $Obj->GetImageUrlXS('main_logo_shop_img_xs');

                $files = array($imagen1, $imagen2, $imagen3, $imagen4, $imagen5);

                File::delete($files);
            }

            if (!empty($id) && $request->hasFile('main_shop')) {

                $imagen1 = getcwd() . "/storage/" . $Obj->GetURL('main_shop_img');
                $imagen2 = getcwd() . "/storage/" . $Obj->GetImageUrlLG2('main_shop_img_lg');
                $imagen3 = getcwd() . "/storage/" . $Obj->GetImageUrlMD2('main_shop_img_md');
                $imagen4 = getcwd() . "/storage/" . $Obj->GetImageUrlSM2('main_shop_img_sm');
                $imagen5 = getcwd() . "/storage/" . $Obj->GetImageUrlXS2('main_shop_img_xs');

                $files = array($imagen1, $imagen2, $imagen3, $imagen4, $imagen5);

                File::delete($files);
            }

            Validator::make($ReqData, [
               'mobile' =>'required',
               'user_id' => 'unique:shops',
               'shop_categories'=> 'required',

            ])->validate();

            try {
                if ($request->hasFile('main_shop')) {
                    $file = $request->file('main_shop');

                    $uploader = \MediaUploader::fromSource($file);
                    $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                    $uploader->toDirectory('Shop/' . date("Y") . '/' . date("m") . '/' . date("d"));
                    $uploader->useFilename(str_random(5));
                    $orginal_media = $uploader->upload();
                    ImageOptimizer::optimize($orginal_media->getAbsolutePath());

                    $folder_ = storage_path("app/public") . '/' . $orginal_media->directory;
                    if (!file_exists($folder_)) {
                        mkdir($folder_, 666, true);
                    }

                    $make_thumbs = [
                        'lg' => [800, 800],
                        'md' => [600, 600],
                        'sm' => [300, 300],
                        'xs' => [100, 100],
                    ];

                    foreach ($make_thumbs as $key => $value) {
                        $image = \Image::make($orginal_media->getAbsolutePath())
                            ->resize($value[0], $value[1])
                            ->save($folder_ . '/' . $orginal_media->filename . '_' . $key . '.' . $orginal_media->extension);

                        $thumb = new \Plank\Mediable\Media;
                        $thumb->disk = $orginal_media->disk;
                        $thumb->directory = $orginal_media->directory;
                        $thumb->filename = $orginal_media->filename . '_' . $key;
                        $thumb->extension = $orginal_media->extension;
                        $thumb->mime_type = $orginal_media->mime_type;
                        $thumb->aggregate_type = $orginal_media->aggregate_type;
                        $thumb->size = filesize($folder_ . '/' . $orginal_media->filename . '_' . $key . '.' . $orginal_media->extension);
                        $thumb->save();
                        ImageOptimizer::optimize($thumb->getAbsolutePath());

                        $orginal_media->attachMedia($thumb, [$key]);


                    }

                }





                if ($request->hasFile('main_logo_shop')) {
                    $file2 = $request->file('main_logo_shop');

                    $uploader2 = \MediaUploader::fromSource($file2);
                    $uploader2->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                    $uploader2->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                    $uploader2->toDirectory('Shop_logo/' . date("Y") . '/' . date("m") . '/' . date("d"));
                    $uploader2->useFilename(str_random(5));
                    $orginal_media2 = $uploader2->upload();
                    ImageOptimizer::optimize($orginal_media2->getAbsolutePath());

                    $folder_2 = storage_path("app/public") . '/' . $orginal_media2->directory;
                    if (!file_exists($folder_2)) {
                        mkdir($folder_2, 666, true);
                    }

                    $make_thumbs2 = [
                        'lg' => [500, 500],
                        'md' => [400, 400],
                        'sm' => [300, 300],
                        'xs' => [200, 200],
                    ];

                    foreach ($make_thumbs2 as $key => $value) {
                        $image2 = \Image::make($orginal_media2->getAbsolutePath())
                            ->resize($value[0], $value[1])
                            ->save($folder_2 . '/' . $orginal_media2->filename . '_' . $key . '.' . $orginal_media2->extension);

                        $thumb2 = new \Plank\Mediable\Media;
                        $thumb2->disk = $orginal_media2->disk;
                        $thumb2->directory = $orginal_media2->directory;
                        $thumb2->filename = $orginal_media2->filename . '_' . $key;
                        $thumb2->extension = $orginal_media2->extension;
                        $thumb2->mime_type = $orginal_media2->mime_type;
                        $thumb2->aggregate_type = $orginal_media2->aggregate_type;
                        $thumb2->size = filesize($folder_2 . '/' . $orginal_media2->filename . '_' . $key . '.' . $orginal_media2->extension);
                        $thumb2->save();
                        ImageOptimizer::optimize($thumb2->getAbsolutePath());

                        $orginal_media2->attachMedia($thumb2, [$key]);


                    }

                }




            } catch (\Exception $e) {
                $error = \Illuminate\Validation\ValidationException::withMessages([
                    'image' => [$e->getMessage()],
                ]);
                throw $error;
            }

            // $ReqData['text_ar'] = str_replace('src="' . url(""), 'src="App_Path_Re', $ReqData['text_ar']);



               $ReqData['user_id'] = Auth::user()->id;


            $Obj = \App\Models\Shops::updateOrCreate(
                ['id' => $id],
                $ReqData
            );

            if (isset($orginal_media)) {
                $Obj->attachMedia($orginal_media, ['main_shop_img']);
            }
               if (isset($orginal_media2)) {
                $Obj->attachMedia($orginal_media2, ['main_logo_shop_img']);
            }

            if (isset($ReqData['MediaFiles'])) {
                $MediaFiles_array = json_decode($ReqData['MediaFiles']);
                foreach ($MediaFiles_array as $HashedID) {
                    $m_id = \Crypt::decrypt($HashedID);
                    $F = $Obj->attachMedia([$m_id], 'shop_images');
                }
            }










            if ($Obj->wasRecentlyCreated) {
                $content = trans("general.successfully_added_new_:name", ['name' => trans("general.element")]);
                $Obj->created_by = Auth::user()->id;
                $Obj->shop_code = $this->generate_code();
                $Obj->update();



                activity("shops.shops")
                    ->performedOn($Obj)
                    ->causedBy(Auth::user()->id)
                    ->log('shops.added_a_new_shop');

            } else {
                $content = trans("general.modified_successfully");
                $Obj->updated_by = Auth::user()->id;
                $Obj->update();

                activity("shops.shops")
                    ->performedOn($Obj)
                    ->causedBy(Auth::user()->id)
                    ->log('slider.edited_the_slider');

            }

            if (!empty($Obj->id)) {


                $All_Langs = \Waavi\Translation\Models\Language::orderBY('id')->get();
                foreach ($All_Langs as $Lang) {
                    if (isset($ReqData[$Lang->locale])) {
                        foreach ($ReqData[$Lang->locale] as $k => $v) {
                            $ObjTrans = \App\Models\Translations\ShopsTranslations::updateOrCreate(
                                ['ref_id' => $Obj->id, 'locale' => $Lang->locale],
                                [$k => $v]
                            );
                            if ($ObjTrans->wasRecentlyCreated) {
                                $ObjTrans->created_by = Auth::user()->id;
                                $ObjTrans->update();

                            } else {
                                $ObjTrans->updated_by = Auth::user()->id;
                                $ObjTrans->update();

                            }
                        }
                    }
                }

                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => $content,
                );
            }

        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }

        echo json_encode($results);

    }





















}
