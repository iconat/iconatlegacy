<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App;
use Cookie;
use Auth;

class Products extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function ShowList()
    {
        $Products = \App\Models\Products::orderBy('id','desc')->paginate(12);
        return view('website.products.ShowAll')
            ->with('Products',$Products);
    }


    public function ViewProductModal(Request $request)
    {
        $ReqData = $request->all();
        $p = isset($ReqData['p']) ? mb_strtolower($ReqData['p']) : NULL;

        $CurrentObj = \App\Models\Products::find($p);

        if ((!empty($CurrentObj))) {

            $All_Images = $CurrentObj->AllImage(['product_images']);
            $main_img_thumb = !empty($CurrentObj) ? ((($CurrentObj->lastMedia("main_product_img")) ? $CurrentObj->lastMedia("main_product_img") : NULL)) : NULL;


            return json_encode(array(
                'title' => $CurrentObj->Get_Trans(App::getLocale(), "name"),
                'content' => view('website.products_modal')
                    ->with('CurrentObj', $CurrentObj)
                    ->with('All_Images', $All_Images)
                    ->with('main_img_thumb', $main_img_thumb)
                    ->render()
            ));
        } else {
            return json_encode(array(
                'title' => trans("general.error"),
                'content' => view('system.errors.show_error_modal')->with('error_description', trans("general.You do not have permission to access this content"))->render()
            ));
        }
    }


    public function GetPaymentForm(Request $request)
    {
        $ReqData = \Purifier::clean($request->all());
        $method_id = isset($ReqData['method_id']) ? mb_strtolower($ReqData['method_id']) : NULL;

        $CurrentObj = \App\Models\PayMethods::find($method_id);

        $Total_Price = '0 ' . config('Default_currency_Name');

        if ((!empty($CurrentObj))) {

            $cartCollection = \Cart::session(config("Cart_ID"));

            $Total_Price = $cartCollection->getTotal() . ' ' . config('Default_currency_Name');

            return json_encode(["Success" => [
                'form' => view("website.payment_form")->with("Total_Price", $Total_Price)->with('PayMethod', $CurrentObj)->render(),
            ]
            ]);
        } else {
            return json_encode(["Errors" => [
                'Redirect' => url("/Cart/Checkout"),
                'title' => trans("general.error"),
                'content' => trans("paymethods.payment_method_is_not_available"),
            ]
            ]);
        }
    }

    public function AddToCart(Request $request)
    {
        $results = [];
        $ReqData = \Purifier::clean($request->all());

        if (!empty($ReqData['qty']) && !empty($ReqData['product_id'])) {

            $Product = \App\Models\Products::find($ReqData['product_id']);

            if ($Product) {

                $Added = \Cart::session(config("Cart_ID"))->add(array(
                    'id' => $Product->id,
                    'name' => $Product->Get_Trans(\App::getLocale(), "name"),
                    'price' => $Product->Get_Price_Depend_Currency($Product->Get_Price()),
                    'quantity' => $ReqData['qty'],
                    'attributes' => array()
                ));


                $results['Success'] = array(
                    'cart_mega' => view("website.cart_menu")->render(),
                    'title' => trans('general.done'),
                    'content' => trans("products.the_item_has_been_successfully_added_to_the_basket"),
                );

            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans("products.the_product_was_not_found"),
                );
            }


        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans("products.product_or_quantity_is_not_specified"),
            );
        }
        return $results;

    }

    public function DeleteItem(Request $request)
    {
        $results = [];
        $ReqData = \Purifier::clean($request->all());

        if (!empty($ReqData['product_id'])) {

            $CurrentObj = \App\Models\Products::find($ReqData['product_id']);

            $Product_Name = '';

            if ((!empty($CurrentObj))) {

                $Product_Name = $CurrentObj->Get_Trans(\App::getLocale(), "name");

            }


            $cartCollection = \Cart::session(config("Cart_ID"));

            $Deleted = $cartCollection->remove($ReqData['product_id']);


            if ($Deleted) {

                $results['Success'] = array(
                    'cart_mega' => view("website.cart_menu")->render(),
                    'cart_details' => view("website.cart_details")->render(),
                    'title' => trans('general.done'),
                    'content' => trans("products.:name_successfuly_deleted_from_cart", ["name" => $Product_Name]),
                );

            } else {
                $results['Errors'] = array(
                    'cart_mega' => view("website.cart_menu")->render(),
                    'title' => trans('general.error'),
                    'content' => trans("products.product_not_founded_in_your_cart"),
                );

            }


        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans("products.product_not_founded"),
            );
        }
        return $results;

    }

    public function ChangeQTY(Request $request)
    {
        $results = [];
        $ReqData = \Purifier::clean($request->all());

        if (!empty($ReqData['product_id']) && (!empty($ReqData['process'] && in_array($ReqData['process'], ['m', 'p'])))) {

            $CurrentObj = \App\Models\Products::find($ReqData['product_id']);

            $Product_Name = '';

            if ((!empty($CurrentObj))) {

                $Product_Name = $CurrentObj->Get_Trans(\App::getLocale(), "name");

            }


            $cartCollection = \Cart::session(config("Cart_ID"));

            if ($ReqData['process'] == 'm') {
                $p = -1;
            } elseif ($ReqData['process'] == 'p') {
                $p = +1;
            }

            $Update = $cartCollection->update($ReqData['product_id'], array(
                'quantity' => $p, // so if the current product has a quantity of 4, it will subtract 1 and will result to 3
            ));


            if ($Update) {

                $results['Success'] = array(
                    'cart_mega' => view("website.cart_menu")->render(),
                    'cart_details' => view("website.cart_details")->render(),
                );

            } else {
                $results['Errors'] = array(
                    'cart_mega' => view("website.cart_menu")->render(),
                    'title' => trans('general.error'),
                    'content' => trans("products.product_not_founded_in_your_cart"),
                );

            }


        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans("products.product_not_founded"),
            );
        }
        return $results;

    }

    public function SetPaymentMethod(Request $request)
    {
        $results = [];
        $ReqData = \Purifier::clean($request->all());

        $paymethod_id = !empty($ReqData['paymethod']) ? $ReqData['paymethod'] : NULL;

        $Paymethod = \App\Models\PayMethods::find($paymethod_id);

        if ($Paymethod && ($Paymethod->active == 1)) {
            \Validator::make($ReqData, [
                'paymethod' => 'required',
            ])->validate();


            $Cart = [];

            if (!empty(session('Cart'))) {
                $Cart = session('Cart');
            }

            $Cart['Payemnt_Method'] = $Paymethod->id;

            $request->session()->put('Cart', $Cart);

            $results['Success'] = array(
                'title' => trans('general.done'),
                'content' => trans('products.your_payment_method_has_been_successfully_saved'),
                'go_to_next' => trans('orders.process_order'),
            );
        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans("products.payment_method_is_not_available"),
            );

        }


        return $results;

    }

    public function SetDeliveryMethod(Request $request)
    {
        $results = [];
        $ReqData = \Purifier::clean($request->all());

        $delivery_method_id = !empty($ReqData['delivery_method']) ? $ReqData['delivery_method'] : NULL;

        $delivery_method = \App\Models\DeliveryMethods::find($delivery_method_id);

        if ($delivery_method && ($delivery_method->active == 1)) {
            \Validator::make($ReqData, [
                'delivery_method' => 'required',
            ])->validate();


            $Cart = [];

            if (!empty(session('Cart'))) {
                $Cart = session('Cart');
            }

            $Cart['Delivery_Method'] = $delivery_method->id;

            $request->session()->put('Cart', $Cart);

            $results['Success'] = array(
                'title' => trans('general.done'),
                'content' => trans('deliverymethods.delivery_method_saved_successfully'),
                'go_to_cart_details' => trans('products.go_to_review_the_order'),
            );
        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans("deliverymethods.delivery_method_unavailable"),
            );

        }


        return $results;

    }

    public function Set_Shipping_Information(Request $request)
    {
        $results = [];
        $ReqData = \Purifier::clean($request->all());

        $AllFieldsName = ['contact_name', 'country', 'street_address', 'second_address', 'city', 'region', 'postal_code', 'mobile'];

        \Validator::make($ReqData, [
            'contact_name' => 'required',
            'country' => 'required',
            'street_address' => 'required',
            'city' => 'required',
            'region' => 'required',
            'mobile' => 'required',
        ])->validate();

        $Array_Data = [];

        foreach ($AllFieldsName as $FName) {
            $Array_Data[$FName] = isset($ReqData[$FName]) ? $ReqData[$FName] : NULL;
        }


        $Cart = [];

        if (!empty(session('Cart'))) {
            $Cart = session('Cart');
        }

        $Cart['ShippingInformation'] = $Array_Data;

        $request->session()->put('Cart', $Cart);

        $results['Success'] = array(
            'title' => trans('general.done'),
            'content' => trans('products.shipping_information_saved_successfully'),
            'go_to_shipping_method' => trans('products.go_to_the_shipping_method'),
        );


        return $results;

    }

    public function AboutUs()
    {

        // $Main_4_Whyus = \App\Models\Whyus::orderBy('id', 'desc')->limit(4)->get();
        // ->with("Main_4_Whyus",$Main_4_Whyus)
        return view('website.AboutUs');
    }

    public function Checkout()
    {
        $Cart = session('Cart');

        $ShippingInformation = !empty($Cart['ShippingInformation']) ? $Cart['ShippingInformation'] : [];


        return view('website.Checkout')->with("Cart", $Cart)->with('ShippingInformation', $ShippingInformation);
    }

    public function CheckOutCart(Request $request)
    {

        $results = [];

        $cartCollection = \Cart::session(config("Cart_ID"));
        $CartArray = $cartCollection->getContent();

        if ($CartArray->count() > 0) {

            $Cart = [];
            if (!empty(session('Cart'))) {
                $Cart = session('Cart');
            }

            //Check Shiping information
            if (empty($Cart['ShippingInformation'])) {
                $results['Notes']['ShippingInformation'] = [
                    "title" => trans("general.alert"),
                    "content" => trans("deliverymethods.please_enter_shipping_informations"),
                ];
            }

            //Check Shiping Method
            if (empty($Cart['Delivery_Method'])) {
                $results['Notes']['Delivery_Method'] = [
                    "title" => trans("general.alert"),
                    "content" => trans("deliverymethods.please_choose_a_delivery_method"),
                ];
            }

            //Check Payemnt Method
            if (empty($Cart['Payemnt_Method'])) {
                $results['Notes']['Payemnt_Method'] = [
                    "title" => trans("general.alert"),
                    "content" => trans("paymethods.please_select_your_payment_method"),
                ];
            }

            if (empty($results['Notes']) && empty($results['Errors'])) {

                do
                {
                    $code = mt_rand(100000000, 999999999);
                    $Orders_code = \App\Models\Orders::where('order_no', $code)->get();
                }
                while(!$Orders_code->isEmpty());


                $OrderObj = \App\Models\Orders::create(
                    [
                        'delivery_method' => $Cart['Delivery_Method'],
                        'order_no' => $code,
                        'state' => setting('default_order_state' , NULL),
                        'total_cost' => $cartCollection->getTotal(),
                        'sub_total_cost' => $cartCollection->getSubTotal(),
                    ]
                );

                if ($OrderObj->wasRecentlyCreated) {
                    $OrderObj->created_by = (!empty(Auth::user()->id)) ? Auth::user()->id : NULL;
                    $OrderObj->update();
                }

                if ($OrderObj) {

                    //Store Shipping Informations

                    $ShippingInformation = \App\Models\Shipments_Informations::create(
                        $Cart['ShippingInformation']
                    );

                    if ($ShippingInformation->wasRecentlyCreated) {
                        $ShippingInformation->created_by = (!empty(Auth::user()->id)) ? Auth::user()->id : NULL;
                        $ShippingInformation->update();

                        $Shipments_Informations_Relations = \App\Models\Orders_Shipments_Informations_Relations::create(
                            [
                                'order_id' => $OrderObj->id,
                                'shipping_info_id' => $ShippingInformation->id,
                            ]
                        );
                    }

                    //Store Products of cart

                    foreach ($CartArray as $P) {
                        $Item = \App\Models\Products::find($P->id);

                        if ($Item) {
                            $Cart_Products = \App\Models\Order_Products::create(
                                [
                                    'order_id' => $OrderObj->id,
                                    'product_id' => $Item->id,
                                    'unit_price' => $Item->Get_Price_Depend_Currency($Item->Get_Price()),
                                    'price_currency' => config("Default_currency_id"),
                                    'quantity' => $P->quantity,
                                ]
                            );
                        }
                    }


                    //Store Payments Info

                    $Order_Payment_Information = \App\Models\Payment_Informations::create(
                        ['method_id' => $Cart['Payemnt_Method']]
                    );

                    if ($Order_Payment_Information->wasRecentlyCreated) {
                        $Order_Payment_Information->created_by = (!empty(Auth::user()->id)) ? Auth::user()->id : NULL;
                        $Order_Payment_Information->update();

                        $Order_Payment_Information_Relations = \App\Models\Order_Payment_Method_Relations::create(
                            [
                                'order_id' => $OrderObj->id,
                                'payment_info_id' => $Order_Payment_Information->id,
                            ]
                        );
                    }

                    $cartCollection->clear();

                    $results['Success'] = [
                        "Redirect" => url("/"),
                        "title" => trans("general.done"),
                        "content" => trans("products.we_have_received_the_order_successfully,_we_will_contact_you_soon"),
                    ];


                }


            }


        } else {
            $results['Errors'] = [
                "title" => trans("general.error"),
                "content" => trans("products.your_cart_is_empty"),
            ];
        }

        return $results;

    }
}
