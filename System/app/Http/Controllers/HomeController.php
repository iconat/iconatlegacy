<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App;
use App\Http\Controllers\Controller;
use App\Mail\DemoEmail;
use Illuminate\Support\Facades\Mail;
use Auth;
use App\Models\Invs;
use App\Models\Identity;
use Lang;

class HomeController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        // $this->middleware('auth');

    }
    //show form register for invs
    public function index() {

        $locale = Lang::locale();

        if($locale == 'ar'){
                $Slides = \App\Models\Sliders::where('active', 1)->orderBY("order", 'asc')->get();
            }else{
                $Slides = \App\Models\Sliders::where('active', 1)->orderBY("order", 'asc')->get();
            }






        $Main_8_Cites = \App\Models\Cites::where('active', 1)->orderBY("order", 'desc')->limit(9)->get();
        $Main_8_Ads = \App\Models\Ads::where('active', 1)->orderBY("order", 'asc')->limit(8)->get();
        $Main_8_Gallery = \App\Models\Gallery::where('active', 1)->orderBY("order", 'asc')->limit(6)->get();
        $Main_4_Cats = \App\Models\Categories::where('active', 1)->orderBY("importance")->get();
        $Main_4_Whyus = \App\Models\Whyus::orderBy('id', 'desc')->get();
        $Main_2_News = \App\Models\News::orderBy('id', 'desc')->get();
        // $Main_8_Cats = \App\Models\Categories::where('active',1)->orderBY("id",'desc')->limit(8)->get();
        return view('website.home')->with("Slides", $Slides)->with("Main_8_Cites", $Main_8_Cites)->with("Main_8_Ads", $Main_8_Ads)->with("Main_8_Gallery", $Main_8_Gallery)->with("Main_4_Cats", $Main_4_Cats)->with("Main_4_Whyus", $Main_4_Whyus)->with("Main_2_News", $Main_2_News);
    }
    public function LoginForm() {
        return view('website.AboutUs');
    }
    public function AboutUs() {
        return view('website.AboutUs');
    }
    public function Cites() {
        return view('website.Cites');
    }





















    public function Categoreis(App\Models\Cites $city, Request $request) {

        if ($city) {
            $CatsToShow = [];
            $CatFromURL = NULL;
            if (!empty($request->cat)) {
                $CatFromURL = App\Models\Shop_Categories::find($request->cat);
            }
            if (!isset($CatFromURL)) {
                $Cats = $city->Shops()->groupBy('shop_categories')->where('active', 1)->get();
                foreach ($Cats as $Cat) {
                    if ($Cat->Shop_Categories->parent) {
                        while (!is_null($Cat->Shop_Categories->parent)) {
                            if (is_null($Cat->Shop_Categories->parent->parent)) {
                                array_push($CatsToShow, $Cat->Shop_Categories->parent);
                                break;
                            }
                        }
                    } else {
                        array_push($CatsToShow, $Cat->Shop_Categories);
                    }
                }
            } elseif (isset($CatFromURL) && $CatFromURL) {
                ///////////////////////////////////
                $Parents_cats = [];
                $Cats = $city->Shops()->groupBy('shop_categories')->where('active', 1)->get();
                foreach ($Cats as $Cat) {
                    array_push($Parents_cats, $Cat->Shop_Categories->id);
                    if ($Cat->Shop_Categories->parent) {
                        $parent = $Cat->Shop_Categories->parent;
                        while (!is_null($parent)) {
                            array_push($Parents_cats, $parent->id);
                            $parent = $parent->parent;
                        }
                    } else {
                        array_push($Parents_cats, $Cat->Shop_Categories->id);
                    }
                }
                //////////////////////////
                // $city



                foreach ($CatFromURL->children as $value) {
                    if (in_array($value->id, $Parents_cats)) {
                        array_push($CatsToShow, $value);
                    }
                }


                //$CatsToShow = $CatFromURL->children();

            }
            //dd($CatsToShow);
            $output = [];
            $ids = [];
            foreach($CatsToShow as $cat){
                if(!in_array($cat->id, $ids)){
                    $output[] = $cat;
                    $ids[] = $cat->id;
                }

            }
            $CatsToShow = $output;
            return view('website.Categoreis')->with("CatsToShow", $CatsToShow)->with("city", $city)->with('CatFromURL', $CatFromURL);
        }
    }









    public function AllCategoreis(Request $request) {
        $parent_id = NULL;
        if (!empty($request->parent)) {
            $parent_id = $request->parent;
        }
        $Parent = \App\Models\Shop_Categories::find($parent_id);
        if ($Parent) {
            $Categories = \App\Models\Shop_Categories::where('active', 1)->where('parent_id', $Parent->id)->orderBY("importance")->get();
        } else {
            $Categories = \App\Models\Shop_Categories::where('active', 1)->where('parent_id', NULL)->orderBY("importance")->get();
        }
        $Shops = [];
        if ($Parent) {
            $Shops = $Parent->Shops;
        }
        return view('website.ALLCategoreis')->with("Parent", $Parent)->with('Categories', $Categories)->with("Shops", $Shops);
    }















    public function Contact() {
        return view('website.Contact');
    }
    public function Shops(App\Models\Shop_Categories $id) {
        return view('website.shops')->with("CatId", $id);
    }
    public function ShopsDetails(App\Models\Shops $id) {
        return view('website.shop_details')->with("Shops_Details", $id);
    }
    public function Account() {
        $UserInfo = \App\User::where('deleted_at', '=', NULL)->where('id', '=', Auth::user()->id)->first();
        return view('website.account')->with("UserInfo", $UserInfo);
    }
    public function ShopEdit() {
        $par1 = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;
        $CurrentObj = \App\Models\Shops::where('deleted_at', '=', NULL)->where('user_id', '=', Auth::user()->id)->first();
        $main_img_thumb = !empty($CurrentObj) ? ((($CurrentObj->lastMedia("main_shop_img")) ? $CurrentObj->lastMedia("main_shop_img")->getUrl() : NULL)) : NULL;
        $main_logo_img_thumb = !empty($CurrentObj) ? ((($CurrentObj->lastMedia("main_logo_shop_img")) ? $CurrentObj->lastMedia("main_logo_shop_img")->getUrl() : NULL)) : NULL;
        return view('website.shop_edit')->with("par1", $par1)->with("CurrentObj", $CurrentObj)->with('main_img_thumb', $main_img_thumb)->with('main_logo_img_thumb', $main_logo_img_thumb);
    }
    public function RSites() {
        return view('website.RSites');
    }
    public function BookPage(\App\Models\Books $Book) {
        if ($Book) {
            return view('website.book_page')->with("Book", $Book);
        }
    }
    public function LessonPage(\App\Models\Paragraphs $Paragraph) {
        if ($Paragraph) {
            $next_lessons = \App\Models\Paragraphs::where("book_id", $Paragraph->book_id)->where("order", ">", $Paragraph->order)->limit(2)->OrderBY("order")->get();
            $pre_lessons = \App\Models\Paragraphs::where("book_id", $Paragraph->book_id)->where("order", "<", $Paragraph->order)->limit(2)->OrderBY("order", 'desc')->get()->sortBy("order");
            return view('website.lesson_page')->with("Paragraph", $Paragraph)->with("next_lessons", $next_lessons)->with("pre_lessons", $pre_lessons);
        }
    }
    public function WorkSheetPDF(\App\Models\Worksheets $WorkSheet) {
        $media = $WorkSheet->lastMedia('Worksheet');
        $url = $media->getUrl();
        $url = str_replace(url("/storage/"), storage_path("app/public"), $url);
        return response()->file($url);
    }
    public function Users_Guide(\App\Models\Classes $Class) {
        $media = $Class->lastMedia('users_guide');
        $url = $media->getUrl();
        $url = str_replace(url("/storage/"), storage_path("app/public"), $url);
        return response()->file($url);
    }
}
