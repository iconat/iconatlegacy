<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use View;
use Validator;
use App;
use Illuminate\Validation\Rule;

class Investors extends Controller
{

    protected $AnYofPerm = FALSE;
    protected $Browse = FALSE;
    protected $Add = FALSE;
    protected $Edit = FALSE;
    protected $Delete = FALSE;
    protected $Approve_Investors = FALSE;


    protected $thumb_width = 300;
    protected $thumb_height = 300;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->Browse = Auth::user()->ability(array('admin'), array('B_Contracts'));
            // $this->Add = Auth::user()->ability(array('admin'), array('A_Contracts'));
            $this->Edit = Auth::user()->ability(array('admin'), array('E_Contracts'));
            $this->Delete = Auth::user()->ability(array('admin'), array('D_Contracts'));
            $this->Approve_Investors = Auth::user()->ability(array('admin'), array('Approve_Investors'));

            $this->AnYofPerm = $this->Browse || $this->Add || $this->Edit || $this->Delete || $this->Approve_Investors;


            View::share('Approve_Investors', $this->Approve_Investors);
            View::share('AnYofPerm', $this->AnYofPerm);
            View::share('Browse', $this->Browse);
            View::share('Add', $this->Add);
            View::share('Edit', $this->Edit);
            View::share('Delete', $this->Delete);


            View::share('thumb_width', $this->thumb_width);
            View::share('thumb_height', $this->thumb_height);

            return $next($request);
        });


    }


    public function index()
    {
        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            return view('admin.investors.view_all');
        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('general.You do not have permission to access this content'));
        }
    }


    public function getdata(Request $request)
    {

        if ($this->AnYofPerm) {
            $Objs = \App\Models\Invs::orderBy('id', 'desc');


            //Check if request is serach
            if (isset($request["action"]) && $request['action'] == "filter") {

                if (!empty($request["state"])) {
                    if ($request["state"] == 'effective') {
                        $Objs = $Objs->where("user_id", '!=', NULL);
                    } elseif ($request["state"] == 'ineffective') {
                        $Objs = $Objs->where("user_id", '=', NULL);
                    }
                }

                if (!empty($request["name"])) {
                    $item_search = $request["name"];
                    $Objs = $Objs->where(\DB::raw('lower(concat(`firstname`," ",`secondname`," ",`thirdname`," ",`fourthname`))'), 'LIKE', '%' . mb_strtolower($item_search) . '%');

                }
                if (!empty($request["phone"])) {
                    $Objs = $Objs->where("phone", 'LIKE', '%' . $request["phone"] . '%');
                }
                if (!empty($request["email"])) {
                    $Objs = $Objs->where("email", 'LIKE', '%' . $request["email"] . '%');
                }

            }


            $iTotalRecords = $Objs->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $Objs = $Objs->offset($iDisplayStart);
            $Objs = $Objs->limit($iDisplayLength);


            $records = array();
            $records["data"] = array();

            foreach ($Objs->get() as $value) {

                $buttons = '';

                if ($this->Edit) {
                    $buttons .= '<button par1="' . $value->id . '" class="AEObject btn btn-sm btn-outline green"><i class="fa fa-edit"></i> ' . trans("general.edit") . '</button>';
                }
                if ($this->Delete) {
                    $buttons .= '<button par1="' . $value->id . '" par2="' . trans("investors.investors_:name", ["name" => $value->get_Short_name()]) . '" Dtitle="' . trans("general.are_you_sure") . '" Dcontent="' . trans("general.are_you_sure_you_want_to_delete_name", array('name' => $value->get_Short_name())) . '"  class="Delete btn btn-sm btn-outline red"><i class="fa fa-remove"></i>  ' . trans("general.delete") . '</button>';
                }

                $buttons .= '<button par1="' . $value->id . '" class="ShowInvsProfile btn btn-sm btn-outline blue-chambray"><i class="fa fa-eye"></i> ' . trans("investors.profile") . '</button>';


                $array_data = array();


                if (empty($value->user_id)) {
                    $state = '<button par1="' . $value->id . '"  Dtitle="' . trans("general.are_you_sure") . '" Dcontent="' . trans("investors.do_you_want_to_activate_the_membership_of_the_investor_:name", array('name' => $value->get_Short_name())) . '"  class="ActiveM btn btn-sm btn-outline green"><i class="fa fa-check"></i>  ' . trans("investors.activate_membership") . '</button>';

                } else {
                    $state = trans('investors.effective_investor');

                }

                array_push($array_data, $state);
                array_push($array_data, $value->get_Full_name());
                array_push($array_data, $value->email);


                array_push($array_data, $buttons);

                $records["data"][] = $array_data;
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);


        }
    }
    function convert_numbers($string) {
        $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
        $arabic = ['٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١','٠'];

        $num = range(0, 9);
        $convertedPersianNums = str_replace($persian, $num, $string);
        $englishNumbersOnly = str_replace($arabic, $num, $convertedPersianNums);

        return $englishNumbersOnly;
    }
    public function AE(Request $request)
    {
        $ReqData = $request->all();
        $par1 = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $CurrentObj = NULL;
        if ($par1 == 'add') {
            $title = trans("general.add_name_element", array("name" => trans("general.investor")));
        } else {
            $CurrentObj = \App\Models\Invs::find($par1);
            $title = trans('general.edit_element');
        }

        if (($this->Add && $par1 == 'add') || (!empty($CurrentObj) && ($this->Edit))) {


            $image = !empty($CurrentObj) ? ((($CurrentObj->lastMedia("main_img_thumb")) ? $CurrentObj->lastMedia("main_img_thumb")->getUrl() : NULL)) : NULL;

            $Items_Of_Payment = array();
            if (!empty($CurrentObj)) {
                $Items_Of_Payment = $CurrentObj->Payments()->get();
            }
            $Items_Of_Contracts = array();
            if (!empty($CurrentObj)) {
                $Items_Of_Contracts = $CurrentObj->Contracts()->get();
            }
            $Items_Of_Identities = array();
            if (!empty($CurrentObj)) {
                $Items_Of_Identities = $CurrentObj->Identities()->get();
            }
            $Items_Of_Shares = array();
            if (!empty($CurrentObj)) {
                $Items_Of_Shares = $CurrentObj->Share_Amounts()->get();
            }

            return json_encode(array(
                'title' => $title,
                'content' => view('admin.investors.AE')
                    ->with('par1', $par1)
                    ->with('image', $image)
                    ->with('CurrentObj', $CurrentObj)
                    ->with('Items_Of_Payment', $Items_Of_Payment)
                    ->with('Items_Of_Contracts', $Items_Of_Contracts)
                    ->with('Items_Of_Identities', $Items_Of_Identities)
                    ->with('Items_Of_Shares', $Items_Of_Shares)
                    ->render()
            ));
        } else {
            return json_encode(array(
                'title' => trans("general.error"),
                'content' => view('system.errors.show_error_modal')->with('error_description', trans("general.You do not have permission to access this content"))->render()
            ));
        }
    }

    public function Profile(Request $request)
    {
        $ReqData = $request->all();
        $par1 = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $CurrentObj = \App\Models\Invs::find($par1);

        if (!empty($CurrentObj) && ($this->AnYofPerm)) {


            $personal_img = !empty($CurrentObj) ? ((($CurrentObj->lastMedia("main_img_thumb")) ? $CurrentObj->lastMedia("main_img_thumb")->getUrl() : NULL)) : NULL;

            $Identities = $CurrentObj->Identities;
            $Payments = $CurrentObj->Payments;
            $Contracts = $CurrentObj->Contracts;
            $Share_Amounts = $CurrentObj->Share_Amounts;

//            foreach ($Identities as $value){
//                $res = $this->ConvertImageToMediable($value->img,'identities/2018/07/22',300,300);
//                if (isset($res['main_img'])) {
//                    $value->attachMedia($res['main_img'], ['main_img']);
//                }
//                if (isset($res['main_img_thumb'])) {
//                    $value->attachMedia($res['main_img_thumb'], ['main_img_thumb']);
//                }
//            }
//            foreach ($Payments as $value){
//                $res = $this->ConvertImageToMediable($value->img,'payments/2018/07/22',300,400);
//                if (isset($res['main_img'])) {
//                    $value->attachMedia($res['main_img'], ['main_img']);
//                }
//                if (isset($res['main_img_thumb'])) {
//                    $value->attachMedia($res['main_img_thumb'], ['main_img_thumb']);
//                }
//            }
//            foreach ($Contracts as $value){
//                $res = $this->ConvertImageToMediable($value->img,'contracts/2018/07/22',300,400);
//                if (isset($res['main_img'])) {
//                    $value->attachMedia($res['main_img'], ['main_img']);
//                }
//                if (isset($res['main_img_thumb'])) {
//                    $value->attachMedia($res['main_img_thumb'], ['main_img_thumb']);
//                }
//            }

            return json_encode(array(
                'title' => trans("investors.profile_of_investor_:name", ['name' => $CurrentObj->get_Short_name()]),
                'content' => view('admin.investors.Profile')
                    ->with('par1', $par1)
                    ->with('personal_img', $personal_img)
                    ->with('CurrentObj', $CurrentObj)
                    ->with('Identities', $Identities)
                    ->with('Payments', $Payments)
                    ->with('Contracts', $Contracts)
                    ->with('Shares', $Share_Amounts)
                    ->render()
            ));
        } else {
            return json_encode(array(
                'title' => trans("general.error"),
                'content' => view('system.errors.show_error_modal')->with('error_description', trans("general.You do not have permission to access this content"))->render()
            ));
        }
    }

    public function store(Request $request)
    {

        //$ReqData = \Purifier::clean($request->all());
        $ReqData = $request->all();
        $id = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $CurrentObj = \App\Models\Invs::where('id', $id)->first();

        $results = array();
        if (($this->Add && empty($CurrentObj)) || (!empty($CurrentObj) && ($this->Edit))) {

            $Validation_Array = array(
                'firstname' => 'required',
                'secondname' => 'required',
                'thirdname' => 'required',
                'fourthname' => 'required',
                'phone' => 'required',
                'address' => 'required',
                'birthday' => 'required',
                'country' => 'required',
                'id_country_live' => 'required',
                'city' => 'required',
                'state' => 'required',
                'email' => [
                    'required',
                    'email',
                    Rule::unique('users')->where(function ($query) use ($request, $CurrentObj) {
                        return $query->where('id', '!=', !empty($CurrentObj->id) ? $CurrentObj->id : NULL)->where('email', $request->email);
                    }),
                ],
            );

            Validator::make($ReqData, $Validation_Array)->validate();

            try {
                if ($request->hasFile('img')) {
                    $file = $request->file('img');

                    $uploader = \MediaUploader::fromSource($file);
                    $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                    $uploader->toDirectory('invs/' . date("Y") . '/' . date("m") . '/' . date("d"));
                    $uploader->useFilename(str_random(5));
                    $orginal_media = $uploader->upload();

                    $folder_ = storage_path("app/public") . '/' . $orginal_media->directory;
                    if (!file_exists($folder_)) {
                        mkdir($folder_, 666, true);
                    }

                    $image = \Image::make($orginal_media->getAbsolutePath())
                        ->resize($this->thumb_width, $this->thumb_height)
                        ->save($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);

                    $thumb = new \Plank\Mediable\Media;
                    $thumb->disk = $orginal_media->disk;
                    $thumb->directory = $orginal_media->directory;
                    $thumb->filename = $orginal_media->filename . '_thumb';
                    $thumb->extension = $orginal_media->extension;
                    $thumb->mime_type = $orginal_media->mime_type;
                    $thumb->aggregate_type = $orginal_media->aggregate_type;
                    $thumb->size = filesize($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);
                    $thumb->save();


                }
            } catch (\Exception $e) {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => $e->getMessage(),
                );
            }

            if (empty($results['Errors'])) {

                $Data = array();

                $Obj = \App\Models\Invs::updateOrCreate(
                    ['id' => $id],
                    $ReqData

                );

                if (isset($orginal_media)) {
                    $Obj->attachMedia($orginal_media, ['main_img']);
                }
                if (isset($thumb)) {
                    $Obj->attachMedia($thumb, ['main_img_thumb']);
                }

                //بدء التعامل مع وصولات الدفع
                if (isset($request->Payments)) {
                    foreach ($request->Payments as $key => $payment_details) {

                        $id_p = !empty($payment_details['id']) ? $payment_details['id'] : NULL;
                        $payment_details['invs_id'] = $Obj->id;

                        $file = isset($payment_details['payment_image']) ? $payment_details['payment_image'] : NULL;

                        if (!empty($file)) {
                            try {


                                $uploader = \MediaUploader::fromSource($file);
                                $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                                $uploader->toDirectory('payments/' . date("Y") . '/' . date("m") . '/' . date("d"));
                                $uploader->useFilename(str_random(5));
                                $orginal_media = $uploader->upload();

                                $folder_ = storage_path("app/public") . '/' . $orginal_media->directory;
                                if (!file_exists($folder_)) {
                                    mkdir($folder_, 666, true);
                                }

                                $image = \Image::make($orginal_media->getAbsolutePath())
                                    ->resize(300, 400)
                                    ->save($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);

                                $thumb = new \Plank\Mediable\Media;
                                $thumb->disk = $orginal_media->disk;
                                $thumb->directory = $orginal_media->directory;
                                $thumb->filename = $orginal_media->filename . '_thumb';
                                $thumb->extension = $orginal_media->extension;
                                $thumb->mime_type = $orginal_media->mime_type;
                                $thumb->aggregate_type = $orginal_media->aggregate_type;
                                $thumb->size = filesize($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);
                                $thumb->save();


                            } catch (\Exception $e) {
                                $results['Errors'] = array(
                                    'title' => trans('general.error'),
                                    'content' => $e->getMessage(),
                                );
                            }


                            $payment = \App\Models\Payment::updateOrCreate(
                                ['id' => $id_p, 'invs_id' => $Obj->id],
                                $payment_details
                            );

                            if ($payment) {
                                if (isset($orginal_media)) {
                                    $payment->attachMedia($orginal_media, ['main_img']);
                                }
                                if (isset($thumb)) {
                                    $payment->attachMedia($thumb, ['main_img_thumb']);
                                }
                            }
                        }


                    }
                }

                //بدء التعامل مع عقود الاستثمار
                if (isset($request->contract_details)) {
                    foreach ($request->contract_details as $key => $contract_details) {

                        $id_c = !empty($contract_details['id']) ? $contract_details['id'] : NULL;
                        $contract_details['invs_id'] = $Obj->id;

                        $file = isset($contract_details['contract_image']) ? $contract_details['contract_image'] : NULL;
                        if (!empty($file)) {
                            try {
                                $uploader = \MediaUploader::fromSource($file);
                                $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                                $uploader->toDirectory('contracts/' . date("Y") . '/' . date("m") . '/' . date("d"));
                                $uploader->useFilename(str_random(5));
                                $orginal_media = $uploader->upload();

                                $folder_ = storage_path("app/public") . '/' . $orginal_media->directory;
                                if (!file_exists($folder_)) {
                                    mkdir($folder_, 666, true);
                                }

                                $image = \Image::make($orginal_media->getAbsolutePath())
                                    ->resize(300, 400)
                                    ->save($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);

                                $thumb = new \Plank\Mediable\Media;
                                $thumb->disk = $orginal_media->disk;
                                $thumb->directory = $orginal_media->directory;
                                $thumb->filename = $orginal_media->filename . '_thumb';
                                $thumb->extension = $orginal_media->extension;
                                $thumb->mime_type = $orginal_media->mime_type;
                                $thumb->aggregate_type = $orginal_media->aggregate_type;
                                $thumb->size = filesize($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);
                                $thumb->save();


                            } catch (\Exception $e) {
                                $results['Errors'] = array(
                                    'title' => trans('general.error'),
                                    'content' => $e->getMessage(),
                                );
                            }


                            $contract = \App\Models\Contract::updateOrCreate(
                                ['id' => $id_c, 'invs_id' => $Obj->id],
                                $contract_details
                            );

                            if ($contract) {
                                if (isset($orginal_media)) {
                                    $contract->attachMedia($orginal_media, ['main_img']);
                                }
                                if (isset($thumb)) {
                                    $contract->attachMedia($thumb, ['main_img_thumb']);
                                }
                            }
                        }


                    }
                }

                //بداء التعامل مع الوثائق القانونية
                if (isset($request->identity_details)) {
                    foreach ($request->identity_details as $key => $identity_details) {

                        if (!empty($identity_details['id_type']) && !empty($identity_details['id_number'])) {


                            $id = !empty($identity_details['id']) ? $identity_details['id'] : NULL;
                            $identity_details['invs_id'] = $Obj->id;
                            $identity_details['id_number'] = $this->convert_numbers($identity_details['id_number']);

                            $file = isset($identity_details['id_image_path']) ? $identity_details['id_image_path'] : NULL;

                            if (!empty($file)) {
                                try {


                                    $uploader = \MediaUploader::fromSource($file);
                                    $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                                    $uploader->toDirectory('identities/' . date("Y") . '/' . date("m") . '/' . date("d"));
                                    $uploader->useFilename(str_random(5));
                                    $orginal_media = $uploader->upload();

                                    $folder_ = storage_path("app/public") . '/' . $orginal_media->directory;
                                    if (!file_exists($folder_)) {
                                        mkdir($folder_, 666, true);
                                    }

                                    $image = \Image::make($orginal_media->getAbsolutePath())
                                        ->resize(300, 300)
                                        ->save($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);

                                    $thumb = new \Plank\Mediable\Media;
                                    $thumb->disk = $orginal_media->disk;
                                    $thumb->directory = $orginal_media->directory;
                                    $thumb->filename = $orginal_media->filename . '_thumb';
                                    $thumb->extension = $orginal_media->extension;
                                    $thumb->mime_type = $orginal_media->mime_type;
                                    $thumb->aggregate_type = $orginal_media->aggregate_type;
                                    $thumb->size = filesize($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);
                                    $thumb->save();


                                } catch (\Exception $e) {
                                    $results['Errors'] = array(
                                        'title' => trans('general.error'),
                                        'content' => $e->getMessage(),
                                    );
                                }


                            }

                            $identity = \App\Models\Identity::updateOrCreate(
                                ['id' => $id, 'invs_id' => $Obj->id],
                                $identity_details
                            );
                            if ($identity) {
                                if (isset($orginal_media)) {
                                    $identity->attachMedia($orginal_media, ['main_img']);
                                }
                                if (isset($thumb)) {
                                    $identity->attachMedia($thumb, ['main_img_thumb']);
                                }
                            }


                        }


                    }
                }

                //بداية التعامل مع الحصص الاستثمارية
                if (isset($request->share_amount_details)) {
                    foreach ($request->share_amount_details as $key => $share_amount_details) {

                        if (!empty($share_amount_details['share_amount']) && !empty($share_amount_details['share_number']) && !empty($share_amount_details['share_date'])) {
                            $id = !empty($share_amount_details['id']) ? $share_amount_details['id'] : NULL;
                            $share_amount_details['invs_id'] = $Obj->id;

                            $share_amount = \App\Models\Share_Amount::updateOrCreate(
                                ['id' => $id, 'invs_id' => $Obj->id],
                                $share_amount_details

                            );
                        }


                    }
                }


                if ($Obj->wasRecentlyCreated) {
                    $content = trans("general.successfully_added_new_:name", ['name' => trans("general.investor")]);
                    $Obj->created_by = Auth::user()->id;
                    $Obj->update();
                } else {
                    $content = trans("general.modified_successfully");
                    $Obj->updated_by = Auth::user()->id;
                    $Obj->update();
                }

                if (!empty($Obj->id)) {
                    $results['Success'] = array(
                        'title' => trans('general.done'),
                        'content' => $content,
                    );
                }
            }


        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }

        echo json_encode($results);


    }


    public function Delete(Request $request)
    {
        $results = array();
        if ($this->Delete) {
            $ReqData = $request->all();
            $id = isset($ReqData['id']) ? mb_strtolower($ReqData['id']) : NULL;
            $par2 = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;

            $q = \App\Models\Invs::where('id', '=', $id)
                ->where('deleted_at', NULL)
                ->update([
                    'deleted_at' => date("Y-m-d H:i:s"),
                    'deleted_by' => Auth::user()->id,
                ]);

            if (!empty($q)) {
                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => trans('general.name_successfully_deleted', array('name' => $par2)),
                );
            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans('general.the_operation_was_not_done_correctly'),
                );
            }
        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);


    }

    public function DeleteChild(Request $request)
    {
        $results = array();
        if ($this->Delete) {
            $ReqData = $request->all();
            $id = isset($ReqData['id']) ? mb_strtolower($ReqData['id']) : NULL;
            $par2 = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;
            $par3 = isset($ReqData['par3']) ? mb_strtolower($ReqData['par3']) : NULL;
            if (in_array($par3, array("payments", "contract", "identitie","share"))) {
                if ($par3 == "payments") {
                    $q = \App\Models\Payment::where('id', '=', $id)
                        ->where('deleted_at', NULL)
                        ->update([
                            'deleted_at' => date("Y-m-d H:i:s"),
                            'deleted_by' => Auth::user()->id,
                        ]);
                } elseif ($par3 == "contract") {
                    $q = \App\Models\Contract::where('id', '=', $id)
                        ->where('deleted_at', NULL)
                        ->update([
                            'deleted_at' => date("Y-m-d H:i:s"),
                            'deleted_by' => Auth::user()->id,
                        ]);

                } elseif ($par3 == "identitie") {
                    $q = \App\Models\Identity::where('id', '=', $id)
                        ->where('deleted_at', NULL)
                        ->update([
                            'deleted_at' => date("Y-m-d H:i:s"),
                            'deleted_by' => Auth::user()->id,
                        ]);

                }elseif ($par3 == "share") {
                    $q = \App\Models\Share_Amount::where('id', '=', $id)
                        ->where('deleted_at', NULL)
                        ->update([
                            'deleted_at' => date("Y-m-d H:i:s"),
                            'deleted_by' => Auth::user()->id,
                        ]);

                }


                if (!empty($q)) {
                    $results['Success'] = array(
                        'title' => trans('general.done'),
                        'content' => trans('general.name_successfully_deleted', array('name' => $par2)),
                    );
                } else {
                    $results['Errors'] = array(
                        'title' => trans('general.error'),
                        'content' => trans('general.the_operation_was_not_done_correctly'),
                    );
                }
            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans('general.the_operation_was_not_done_correctly'),
                );
            }


        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);


    }

    public function Active(Request $request)
    {
        $results = array();
        if ($this->Approve_Investors) {
            $ReqData = $request->all();
            $id = isset($ReqData['id']) ? mb_strtolower($ReqData['id']) : NULL;

            $q = \App\Models\Invs::find($id);

            if (!empty($q)) {

                $Identities = $q->Identities;

                if (count($Identities) > 0) {
                    $Data = array();

                    $id_no = '';
                    foreach ($Identities as $Idn) {
                        $id_no = $Idn->id_number;
                        break;
                    }

                    $Data['name'] = $q->get_Short_name();
                    $Data['id_no'] = $id_no;
                    $Data['email'] = $q->email;
                    $Data['password'] = bcrypt($q->show_password);

                    $Validation_Array = array(
                        'name' => 'required',
                        'id_no' => [
                            'required',
                            Rule::unique('users')->where(function ($query) use ($Data, $q) {
                                return $query->where('id', '!=', !empty($q) ? $q->id : NULL)->where('id_no', $Data['id_no']);
                            }),
                        ],
                        'email' => [
                            'required',
                            'email',
                            Rule::unique('users')->where(function ($query) use ($Data, $q) {
                                return $query->where('id', '!=', !empty($q->id) ? $q->id : NULL)->where('email', $Data->email);
                            }),
                        ],
                    );


                    $Obj = \App\User::updateOrCreate(
                        ['id' => $id],
                        $Data

                    );

                    if ($Obj) {
                        $q->user_id = $Obj->id;
                        $q->update();
                    }


                    $results['Success'] = array(
                        'title' => trans('general.done'),
                        'content' => trans('investors.the_investor_membership_has_been_successfully_activated_for_:name', array('name' => $q->get_Short_name())),
                    );
                } else {
                    $results['Errors'] = array(
                        'title' => trans('general.error'),
                        'content' => trans('investors.no_id_number_for_this_investor_was_found'),
                    );
                }


            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans('general.the_operation_was_not_done_correctly'),
                );
            }
        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);


    }

    public function ConvertImageToMediable($Img_Link, $PathToSave, $thumb_width, $thumb_height)
    {


        $folder_ = storage_path("app/public/") . $PathToSave;
        if (!file_exists($folder_)) {
            mkdir($folder_, 666, true);
        }
        $exp = explode("/", $Img_Link);
        $file_name = '';
        foreach ($exp as $x) {
            $file_name = $x;
        }

        $image = \Image::make(public_path("/" . $Img_Link))
            // ->resize($this->thumb_width, $this->thumb_height)
            ->save($folder_ . '/' . strtolower($file_name));
        $Filee = explode('.', $file_name);
        $Img = new \Plank\Mediable\Media;
        $Img->disk = "public";
        $Img->directory = $PathToSave;
        $Img->filename = $Filee[0];
        $Img->extension = $Filee[1];
        $Img->mime_type = mime_content_type($folder_ . '/' . $file_name);
        $Img->aggregate_type = "image";
        $Img->size = filesize($folder_ . '/' . $file_name);
        $Img->save();


        $image = \Image::make(public_path("/" . $Img_Link))
            ->resize($thumb_width, $thumb_height)
            ->save($folder_ . '/' . strtolower($Filee[0] . '_thumb.' . $Filee[1]));
        $Filee = explode('.', $file_name);
        $thumb = new \Plank\Mediable\Media;
        $thumb->disk = "public";
        $thumb->directory = $PathToSave;
        $thumb->filename = $Filee[0] . '_thumb';
        $thumb->extension = $Filee[1];
        $thumb->mime_type = mime_content_type($folder_ . '/' . $file_name);
        $thumb->aggregate_type = "image";
        $thumb->size = filesize($folder_ . '/' . $file_name);
        $thumb->save();


        return array(
            'main_img' => $Img,
            'main_img_thumb' => $thumb,
        );


    }

}



