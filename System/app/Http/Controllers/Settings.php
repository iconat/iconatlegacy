<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use View;
use Validator;
use App;

class Settings extends Controller
{

    protected $Edit_Settings = FALSE;
    protected $header_web_logo_width = 94;
    protected $header_web_logo_height = 20;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->Edit_Settings = Auth::user()->ability(array('admin'), array('Edit_Settings'));

            View::share('header_web_logo_width', $this->header_web_logo_width);
            View::share('header_web_logo_height', $this->header_web_logo_height);

            return $next($request);
        });


    }


    public function index()
    {
        if ($this->Edit_Settings) {
            return view('admin.settings.general')->with('Obj', Auth::user());
        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('general.You do not have permission to access this content'));
        }
    }

    public function store(Request $request)
    {
        App::setLocale($request->lang);

        $results = array();

        if ($this->Edit_Settings) {
            //$ReqData = \Purifier::clean($request->all());
            $ReqData = $request->all();
            $par1 = isset($ReqData['par1']) ? $ReqData['par1'] : NULL;
            if (isset($ReqData['par1'])) {
                unset($ReqData['par1']);
            }
            if (in_array($par1, array('General_Settings','HomePage_Settings','AboutUs_Settings'))) {
                $MeSetings = [];
                switch ($par1) {
                    case 'General_Settings':
                        //Start Upload Logos
                        try {
                            $All_Langs = \Waavi\Translation\Models\Language::orderBY('id')->get();
                            foreach ($All_Langs as $Lang) {
                                //For Upload logos of website
                                $File_Name = "website_logo_" . $Lang->locale;
                                if ($request->hasFile($File_Name)) {
                                    $file = $request->file($File_Name);

                                    $uploader = \MediaUploader::fromSource($file);
                                    $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                                    $uploader->toDirectory('general/' . date("Y") . '/' . date("m") . '/' . date("d"));
                                    $uploader->useFilename(str_random(5));
                                    $orginal_media = $uploader->upload();

                                    $folder_ = storage_path("app/public") . '/' . $orginal_media->directory;
                                    if (!file_exists($folder_)) {
                                        mkdir($folder_, 666, true);
                                    }

                                    $image = \Image::make($orginal_media->getAbsolutePath())
                                        ->resize($this->header_web_logo_width, $this->header_web_logo_height)
                                        ->save($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);

                                    $thumb = new \Plank\Mediable\Media;
                                    $thumb->disk = $orginal_media->disk;
                                    $thumb->directory = $orginal_media->directory;
                                    $thumb->filename = $orginal_media->filename . '_thumb';
                                    $thumb->extension = $orginal_media->extension;
                                    $thumb->mime_type = $orginal_media->mime_type;
                                    $thumb->aggregate_type = $orginal_media->aggregate_type;
                                    $thumb->size = filesize($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);
                                    $thumb->save();

                                    $MeSetings[$File_Name] = [$File_Name => $orginal_media, $File_Name . '_thumb' => $thumb];

                                }
                            }

                        } catch (\Exception $e) {
                            $results['Errors'] = array(
                                'title' => trans('general.error'),
                                'content' => $e->getMessage(),
                            );
                        }
                        //End Upload Logos


                        Validator::make($ReqData, [
                            // 'shares_available' => 'required|integer',
                        ])->validate();
                        break;

                         case 'AboutUs_Settings':
                        //Start Upload Logos
                        try {

                                //For Upload logos of website
                                $File_Name = "about_img";
                                if ($request->hasFile($File_Name)) {
                                    $file = $request->file($File_Name);

                                    $uploader = \MediaUploader::fromSource($file);
                                    $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                                    $uploader->toDirectory('general/' . date("Y") . '/' . date("m") . '/' . date("d"));
                                    $uploader->useFilename(str_random(5));
                                    $orginal_media = $uploader->upload();

                                    $folder_ = storage_path("app/public") . '/' . $orginal_media->directory;
                                    if (!file_exists($folder_)) {
                                        mkdir($folder_, 666, true);
                                    }

                                    $image = \Image::make($orginal_media->getAbsolutePath())
                                        ->resize(2484, 1600)
                                        ->save($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);

                                    $thumb = new \Plank\Mediable\Media;
                                    $thumb->disk = $orginal_media->disk;
                                    $thumb->directory = $orginal_media->directory;
                                    $thumb->filename = $orginal_media->filename . '_thumb';
                                    $thumb->extension = $orginal_media->extension;
                                    $thumb->mime_type = $orginal_media->mime_type;
                                    $thumb->aggregate_type = $orginal_media->aggregate_type;
                                    $thumb->size = filesize($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);
                                    $thumb->save();

                                    $MeSetings[$File_Name] = [$File_Name => $orginal_media, $File_Name . '_thumb' => $thumb];

                                }


                        } catch (\Exception $e) {
                            $results['Errors'] = array(
                                'title' => trans('general.error'),
                                'content' => $e->getMessage(),
                            );
                        }
                        //End Upload Logos


                        Validator::make($ReqData, [
                            // 'shares_available' => 'required|integer',
                        ])->validate();
                        break;






                    case 'HomePage_Settings':
                        //Start Upload Logos
                        try {
                            $All_Langs = \Waavi\Translation\Models\Language::orderBY('id')->get();
                            foreach ($All_Langs as $Lang) {

                                //For Upload website b1
                                $File_Name = "image_1_of_the_main_categories_" . $Lang->locale;
                                if ($request->hasFile($File_Name)) {
                                    $file = $request->file($File_Name);

                                    $uploader = \MediaUploader::fromSource($file);
                                    $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                                    $uploader->toDirectory('general/' . date("Y") . '/' . date("m") . '/' . date("d"));
                                    $uploader->useFilename(str_random(5));
                                    $orginal_media = $uploader->upload();

                                    $folder_ = storage_path("app/public") . '/' . $orginal_media->directory;
                                    if (!file_exists($folder_)) {
                                        mkdir($folder_, 666, true);
                                    }

                                    $image = \Image::make($orginal_media->getAbsolutePath())
                                        ->resize(94, 20)
                                        ->save($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);

                                    $thumb = new \Plank\Mediable\Media;
                                    $thumb->disk = $orginal_media->disk;
                                    $thumb->directory = $orginal_media->directory;
                                    $thumb->filename = $orginal_media->filename . '_thumb';
                                    $thumb->extension = $orginal_media->extension;
                                    $thumb->mime_type = $orginal_media->mime_type;
                                    $thumb->aggregate_type = $orginal_media->aggregate_type;
                                    $thumb->size = filesize($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);
                                    $thumb->save();

                                    $MeSetings[$File_Name] = [$File_Name => $orginal_media, $File_Name . '_thumb' => $thumb];

                                }

                                //For Upload website b2
                                $File_Name = "image_2_of_the_main_categories_" . $Lang->locale;
                                if ($request->hasFile($File_Name)) {
                                    $file = $request->file($File_Name);

                                    $uploader = \MediaUploader::fromSource($file);
                                    $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                                    $uploader->toDirectory('general/' . date("Y") . '/' . date("m") . '/' . date("d"));
                                    $uploader->useFilename(str_random(5));
                                    $orginal_media = $uploader->upload();

                                    $folder_ = storage_path("app/public") . '/' . $orginal_media->directory;
                                    if (!file_exists($folder_)) {
                                        mkdir($folder_, 666, true);
                                    }

                                    $image = \Image::make($orginal_media->getAbsolutePath())
                                        ->resize(280, 100)
                                        ->save($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);

                                    $thumb = new \Plank\Mediable\Media;
                                    $thumb->disk = $orginal_media->disk;
                                    $thumb->directory = $orginal_media->directory;
                                    $thumb->filename = $orginal_media->filename . '_thumb';
                                    $thumb->extension = $orginal_media->extension;
                                    $thumb->mime_type = $orginal_media->mime_type;
                                    $thumb->aggregate_type = $orginal_media->aggregate_type;
                                    $thumb->size = filesize($folder_ . '/' . $orginal_media->filename . '_thumb.' . $orginal_media->extension);
                                    $thumb->save();

                                    $MeSetings[$File_Name] = [$File_Name => $orginal_media, $File_Name . '_thumb' => $thumb];

                                }
                            }

                        } catch (\Exception $e) {
                            $results['Errors'] = array(
                                'title' => trans('general.error'),
                                'content' => $e->getMessage(),
                            );
                        }
                        //End Upload Logos


                        Validator::make($ReqData, [
                            // 'shares_available' => 'required|integer',
                        ])->validate();
                        break;
                    default:
                        break;
                }
                //To delete files
                foreach ($ReqData as $ke=>$va){
                    if ($request->hasFile($ke)) {
                        unset($ReqData[$ke]);
                    }
                }
                setting($ReqData)->save();

                //Media
                foreach ($MeSetings as $key => $value) {

                    $Obj = \App\Models\SettingsMediable::updateOrCreate(
                        ['key' => $key],
                        ['key' => $key]
                    );

                    if ($Obj->wasRecentlyCreated) {
                        $Obj->created_by = Auth::user()->id;
                        $Obj->update();

                    } else {
                        $Obj->updated_by = Auth::user()->id;
                        $Obj->update();

                    }


                    if ($Obj) {
                        foreach ($value as $k=>$v){
                            $Obj->attachMedia($v, [$k]);
                        }

                    }

                }


                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => trans('general.:name_saved_successfully', ['name' => trans("general.general_settings")]),
                );

            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans('general.the_package_settings_are_wrong'),
                );
            }


        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }

        echo json_encode($results);


    }


}



