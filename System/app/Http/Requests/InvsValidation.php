<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
 use App\Models\Invs;



class InvsValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ae=array(

            
            'firstname' => 'required',
            'secondname' => 'required',
            'thirdname' => 'required',
            'fourthname' => 'required',

            // 'profileimg'=> 'required',

            'country' => 'required',
            'phone' => 'required',
            'birthday' => 'required',
            'email' => 'required|email|unique:invs',

            // 'password' => 'required',
            // 'password_confirmation' => 'required',


            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'address' => 'required',


      
          

        );

      

        return $ae;
    }



        public function messages()
    {
        return [

         
         'firstname.required' => ' الاسم اجباري',
         'secondname.required' => ' الاسم اجباري',
          'thirdname.required' => ' الاسم اجباري',
         'email.required' => ' الايميل اجباري',
         'email.email' => ' يجب ان يكون صيغه ايميل',
         'password.required' => ' الرقم السري اجباري',
         'password_confirmation.required' => 'التاكد من الرقم السري اجباري',


            'fourthname.required' => 'الاسم اجباري',

            'profileimg.required'=> 'الصوره الشخصيه اجباري',
                'country.required' => 'الدوله اجباري',
            'phone.required' => 'رقم الهاتف اجباري',
            'birthday.required' => 'تاريخ الميلاد اجباري',




            'address.required' => 'العنوان اجابري',
            'city.required' => 'المدينه اجباري',
            'state.required' => 'المنطقه اجباري',

         
     ];
 }





}
