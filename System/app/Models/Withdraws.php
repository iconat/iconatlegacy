<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Withdraws extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'withdraws';

    protected $fillable = ['invs_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];




}







