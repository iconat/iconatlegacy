<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Classes extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'classes';

    protected $fillable = ['name'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function Shares()
    {
        return $this->hasMany('App\Models\Share_Amount', 'field_id');
    }

    public function TotalShares()
    {
        return $this->Shares->sum('share_amount');
    }


}







