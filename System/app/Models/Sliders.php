<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Sliders extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'slider';

    protected $fillable = ['link_video','order','active','shop_link'];

    public function Translations()
    {
        return $this->hasMany('App\Models\Translations\SlidersTranslations', 'ref_id');
    }

    public function GetSliderText($lang)
    {
        $Translation = $this->Translations->where("locale",$lang)->last();
        if($Translation){
            return $Translation->text;
        }
        return NULL;
    }

    public function viewImage($imgTag)
    {
        return ($this->lastMedia($imgTag)) ? $this->lastMedia($imgTag)->getUrl() : NULL;
    }

    public function MainImage()
    {
        $MediaTag = 'main_slider_img_thumb_'.config('col');
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->getUrl():NULL;
    }
    public function SecondImage()
    {
        $MediaTag = 'second_slider_img_thumb';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->getUrl():NULL;
    }
    public function SecondImage2()
    {
        $MediaTag = 'second_slider_img_thumb2';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->getUrl():NULL;
    }


    public function GetvideoURL($VideoTag)
    {
        $MediaTag = 'video';
    return ($this->lastMedia($VideoTag)) ? $this->lastMedia($VideoTag)->getUrl() : NULL;

    }

      public function ShopsName()
    {
        return $this->belongsTo('App\Models\Shops', 'shop_link');
    }


     public function GetURL($imgTag)
    {
        return ($this->lastMedia($imgTag)) ? $this->lastMedia($imgTag)->getDiskPath() : NULL;
    }




}







