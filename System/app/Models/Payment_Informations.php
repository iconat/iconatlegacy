<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Payment_Informations extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'payment_info';

    protected $fillable = ['method_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];






}







