<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Order_Products extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'order_products';

    protected $fillable = ['order_id','product_id','unit_price','quantity','price_currency'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function Product()
    {
        return $this->belongsTo('App\Models\Products', 'product_id');
    }


    public function Currency()
    {
        return $this->belongsTo('App\Models\Currencies', 'price_currency');
    }

}







