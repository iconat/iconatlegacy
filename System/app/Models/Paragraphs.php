<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Paragraphs extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'paragraphs';

    protected $fillable = ['title', 'text', 'book_id','order'];
    public function GetImageURL($imgTag)
    {
        return ($this->lastMedia($imgTag)) ? $this->lastMedia($imgTag)->getUrl() : NULL;
    }
    public function BookOf()
    {
        return $this->belongsTo('App\Models\Books', 'book_id');
    }
    public function Worksheets()
    {
        return $this->hasMany('App\Models\Worksheets', 'paragraph_id');
    }
}







