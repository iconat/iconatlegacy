<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Shipments_Informations extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'shipments_informations';

    protected $fillable = ['contact_name', 'country', 'street_address', 'second_address', 'city', 'region', 'postal_code', 'mobile'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];








}







