<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Media extends Model
{
    use SoftDeletes;

    public $table = 'media';

    protected $fillable = ['id', 'disk','directory','filename','extension','deleted_at'];




    public function GetURL($imgTag)
    {
        return ($this->lastMedia($imgTag)) ? $this->lastMedia($imgTag)->getDiskPath() : NULL;
    }
    public function GetImageUrlXS()
    {
        $MediaTag = 'shop_images';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('xs')->getDiskPath() : NULL;
    }
    public function GetImageUrlSM()
    {
        $MediaTag = 'shop_images';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('sm')->getDiskPath() : NULL;
    }

    public function GetImageUrlMD()
    {
        $MediaTag = 'shop_images';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('md')->getDiskPath() : NULL;
    }

    public function GetImageUrlLG()
    {
        $MediaTag = 'shop_images';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('lg')->getDiskPath() : NULL;
    }



}







