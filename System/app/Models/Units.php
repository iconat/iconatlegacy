<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Units extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'units';

    protected $fillable = ['active','fractions_in_original'];

    public function Translations()
    {
        return $this->hasMany('App\Models\Translations\UnitsTranslations', 'ref_id');
    }

    public function Get_Trans($lang,$attr)
    {
        $Translation = $this->Translations->where("locale",$lang)->last();
        if($Translation){
            return $Translation->{$attr};
        }
        return NULL;
    }





}







