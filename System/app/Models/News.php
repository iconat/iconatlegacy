<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;
use Auth;

class News extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'news';

    protected $fillable = ['id'];

    public function Translations()
    {
        return $this->hasMany('App\Models\Translations\NewsTranslations', 'ref_id');
    }



    public function Get_Trans($lang,$attr)
    {
        $Translation = $this->Translations->where("locale",$lang)->last();
        if($Translation){
            return $Translation->{$attr};
        }
        return NULL;
    }

    public function Get_URL($lang = NULL)
    {

        if(empty($lang)){
            $lang  = \App::getLocale();
        }

        return url("/News/V/".$this->id."/". GetTextDashed($this->Get_Trans($lang,'name')));
    }



    public function MainImage()
    {
        $MediaTag = 'main_news_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia(config('col'))->getUrl():NULL;
    }





}







