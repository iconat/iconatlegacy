<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Worksheets extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'worksheets';

    protected $fillable = ['att_id', 'paragraph_id', 'title'];

    public function ParagraphOf()
    {
        return $this->belongsTo('App\Models\Paragraphs', 'paragraph_id');
    }

}







