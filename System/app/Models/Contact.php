<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Contact extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'contact';

    protected $fillable = ['name','email','subject','message'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];




}







