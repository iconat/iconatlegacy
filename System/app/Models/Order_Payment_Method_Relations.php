<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order_Payment_Method_Relations extends Model
{

    public $table = 'order_payments_method_relations';

    protected $fillable = ['order_id', 'payment_info_id'];



    public function Payment_Information()
    {
        return $this->belongsTo('App\Models\Payment_Informations', 'payment_info_id');
    }


}







