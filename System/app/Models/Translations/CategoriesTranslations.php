<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class CategoriesTranslations extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'translations_categories';

    protected $fillable = ['ref_id','locale','name'];

    public function Categorie()
    {
        return $this->belongsTo('App\Models\Categories', 'ref_id');
    }



}







