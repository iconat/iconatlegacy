<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class ProductsTranslations extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'translations_products';

    protected $fillable = ['ref_id','locale','name','text'];

    public function Product(){
        return $this->belongsTo('App\Models\Products', 'ref_id');
    }
}







