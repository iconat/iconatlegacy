<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Shop_CategoriesTranslations extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'translations_shop_categories';

    protected $fillable = ['ref_id','locale','name'];

    public function Shop_Categorie()
    {
        return $this->belongsTo('App\Models\Shop_Categories', 'ref_id');
    }



}







