<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Slider_CategoryTranslations extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'translations_slider_category';

    protected $fillable = ['ref_id','locale','title','text'];

    public function Slide_cat()
    {
        return $this->belongsTo('App\Models\Slider_Category', 'ref_id');
    }



}







