<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class UnitsTranslations extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'translations_units';

    protected $fillable = ['ref_id','locale','name','fraction'];

    public function Unit()
    {
        return $this->belongsTo('App\Models\Units', 'ref_id');
    }



}







