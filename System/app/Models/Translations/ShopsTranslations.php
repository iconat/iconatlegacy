<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class ShopsTranslations extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'translations_shops';

    protected $fillable = ['ref_id','locale','name','text','address'];

    public function Shop(){
        return $this->belongsTo('App\Models\Shops', 'ref_id');
    }
}







