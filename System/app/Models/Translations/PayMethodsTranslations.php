<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class PayMethodsTranslations extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'translations_pay_methods';

    protected $fillable = ['ref_id','locale','name'];

    public function DeliveryMethod()
    {
        return $this->belongsTo('App\Models\DeliveryMethods', 'ref_id');
    }



}







