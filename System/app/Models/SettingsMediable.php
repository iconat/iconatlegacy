<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class SettingsMediable extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'settings_mediable';

    protected $fillable = ['key'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];




}







