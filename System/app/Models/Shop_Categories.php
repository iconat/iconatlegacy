<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Shop_Categories extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'shop_categories';

    protected $fillable = ['importance','active','parent_id'];

    /*
     * return true if user can delete this object
     */

    public function CanDeleted()
    {

        if (!empty($this->Shops->count())) {
            return trans("general.this_item_can_not_be_deleted_because_it_linked_with_:name", ['name' => trans("shops.shops")]);
        }
        if (!empty($this->children->count())) {
            return trans("general.this_item_can_not_be_deleted_because_it_linked_with_:name", ['name' => trans("shop_categories.subshop_categories_one")]);
        }


        return true;
    }

    /*
     * hasMany Relations
     */
    public function Shops()
    {
        return $this->hasMany('App\Models\Shops', 'shop_categories');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Shop_Categories', 'parent_id');
    }
    public function parent()
    {
        return $this->belongsTo('App\Models\Shop_Categories', 'parent_id');
    }


    public function Translations()
    {
        return $this->hasMany('App\Models\Translations\Shop_CategoriesTranslations', 'ref_id');
    }

    public function Get_Trans($lang, $attr)
    {
        $Translation = $this->Translations->where("locale", $lang)->last();
        if ($Translation) {
            return $Translation->{$attr};
        }
        return NULL;
    }

    public function viewImage($imgTag)
    {
        return ($this->lastMedia($imgTag)) ? $this->lastMedia($imgTag)->getUrl() : NULL;
    }

        public function MainImage()
    {
        $MediaTag = 'main_shop_categories_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia(config('col'))->getUrl():NULL;
    }

    public function GetThumb()
    {
        $MediaTag = 'main_shop_categories_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('md')->getUrl():NULL;
    }
    public function AllImage($Arry_of_Tags = [])
    {
        return $this->getMedia($Arry_of_Tags);
        //return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia(config('col'))->getUrl():NULL;
    }

    public function getParentsAttribute()
    {
        $parents = collect([]);

        $parent = $this->parent;



        while (!is_null($parent)) {
            $parents->push($parent);
            $parent = $parent->parent;
        }

        return $parents->reverse();
    }











     public function GetURL($imgTag)
    {
        return ($this->lastMedia($imgTag)) ? $this->lastMedia($imgTag)->getDiskPath() : NULL;
    }
     public function GetImageUrlXS()
    {
        $MediaTag = 'main_shop_categories_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('xs')->getDiskPath():NULL;
    }
      public function GetImageUrlSM()
    {
        $MediaTag = 'main_shop_categories_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('sm')->getDiskPath():NULL;
    }

  public function GetImageUrlMD()
    {
        $MediaTag = 'main_shop_categories_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('md')->getDiskPath():NULL;
    }

  public function GetImageUrlLG()
    {
        $MediaTag = 'main_shop_categories_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('lg')->getDiskPath():NULL;
    }


}







