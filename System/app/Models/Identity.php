<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Identity extends Model
{
    use SoftDeletes;
    use Mediable;
    public $table = 'invs_identity';
    protected $fillable = ['id_type', 'id_number',  'invs_id'];


    public function Identity()
    {
        return $this->belongsTo('App\Models\Invs', 'invs_id');
    }

    public function GetImageURL($imgTag)
    {
        return ($this->lastMedia($imgTag)) ? $this->lastMedia($imgTag)->getUrl() : NULL;
    }

}




