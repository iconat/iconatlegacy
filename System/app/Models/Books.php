<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Books extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'books';

    protected $fillable = ['name', 'text', 'class_id','brief_description'];

    public function Paragraphs()
    {
        return $this->hasMany('App\Models\Paragraphs', 'book_id');
    }

    public function ClassOf()
    {
        return $this->belongsTo('App\Models\Classes', 'class_id');
    }

    public function Paragraphs_No()
    {
        return $this->Paragraphs->count();
    }

    public function GetImageURL($imgTag)
    {
        return ($this->lastMedia($imgTag)) ? $this->lastMedia($imgTag)->getUrl() : NULL;
    }
}







