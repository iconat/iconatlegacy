<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;
class Poll extends Model
{
    use SoftDeletes;
    use Mediable;
    public $table = 'poll';

    protected $fillable = ['question','start_date','closing_date'];

    protected $dates = ['deleted_at'];

    public function Options()
    {
        return $this->hasMany('App\Models\Poll_Options', 'poll_id');
    }

}




