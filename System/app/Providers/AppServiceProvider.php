<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Jenssegers\Agent\Agent;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Set Agent Globally
        $agent = new Agent();
        config(['agent' => $agent]);

        $col = 'lg';

        if ($agent->isMobile() && $agent->isPhone()) {
            $col = 'xs';
        } elseif ($agent->isTablet()) {
            $col = 'md';
        }
        $screen_width = \Cookie::get('screen_width');
        if (!empty($screen_width) && is_numeric($screen_width)) {

            if ($screen_width <= 500) {
                $col = 'xs';
            } elseif ($screen_width <= 1000) {
                $col = 'sm';
            } elseif ($screen_width <= 1500) {
                $col = 'md';
            } else {
                $col = 'lg';
            }

        }

        config(['col' => $col]);

        //Default_currency
        $default_currency = setting('default_currency' , 1);
        if(!empty(\Cookie::get('Cart_ID')) && (\App\Models\Currencies::where("id",\Cookie::get('Currency'))->where('active',1)->first())){
            $default_currency = \Cookie::get('Currency');
        }

        $Default_currency = \App\Models\Currencies::find($default_currency);

        config(['Default_currency_id' => $Default_currency->id]);
        config(['Default_currency_Icon' => $Default_currency->icon]);
        config(['Default_currency_symbols' => $Default_currency->symbols]);
        config(['Default_currency_Code' => $Default_currency->iso_code]);
        config(['Default_currency_Name' => $Default_currency->Get_Trans(\App::getLocale(),'name')]);


        //Cart_ID
        $Cart_ID = NULL;
        if(\auth()->user()){
            $Cart_ID = \auth()->user()->id;
        }elseif(!empty(\Cookie::get('Cart_ID'))){
            $Cart_ID = \Cookie::get('Cart_ID');
        }else{
            $Cart_ID = substr(base64_encode(sha1(mt_rand())), 0, 16);
            \Cookie::queue('Cart_ID', $Cart_ID, 800);
        }
        config(['Cart_ID' => $Cart_ID]);



    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
